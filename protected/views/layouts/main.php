<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= CHtml::encode($this->pageTitle); ?></title>

        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/animate.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/custom.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/sweetalert.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/jquery.datetimepicker.css" />

        <link rel="shortcut icon" href="<?= Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <?php
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/ready.js", CClientScript::POS_READY);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/main.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/head.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/sweetalert.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/WebNotifications.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/moment/moment.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.nicescroll.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.datetimepicker.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.noty.packaged.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/datepicker/daterangepicker.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jstree.min.js', CClientScript::POS_END);

        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/highcharts.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/exporting.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/highcharts-3d.js', CClientScript::POS_HEAD);
        ?>

    </head>
    <body>
        <?php
        $busqueda = array(
            "label" => '<i class="fa fa-search"></i> Busqueda',
            'url' => Yii::app()->createUrl('buscar'),
            "htmlOptions" => array("class" => "glyphicon glyphicon-play", "style" => "display:none"),
        );

        $oportunidades = array(
            "label" => '<i class="fa fa-check"></i>  Oportunidades',
            'url' => Yii::app()->createUrl('admopo'),
            "htmlOptions" => array("class" => "glyphicon glyphicon-play", "style" => "display:none"),
        );

        $citas = array('label' => '<i class="fa fa-user"></i> Contactos',
            'url' => '#',
            'items' => array(
                array('label' => '<i class="fa fa-users"></i> Todos', 'url' => Yii::app()->createUrl('admcon')),
                array('label' => '<i class="fa fa-user"></i>  Crear', 'url' => Yii::app()->createUrl('admcon/crear')),
            )
        );

        $descartadas = array('label' => '<i class="fa fa-trash"></i> Descartadas',
            'url' => '#', 'items' => array(
                array('label' => '<i class="fa fa-dollar"></i> Presupuesto', 'url' => Yii::app()->createUrl('descartado/presupuesto')),
                array('label' => '<i class="fa fa-random"></i> Competencia', 'url' => Yii::app()->createUrl('descartado/competencia')),
                array('label' => '<i class="fa fa-trash"></i>  Solo Informacion', 'url' => Yii::app()->createUrl('descartado/error'))
            )
        );

        $asesores = array('label' => '<i class="fa fa-users"></i> Asesores', 'url' => Yii::app()->createUrl('asesores/citas'));

        $agendados = array('label' => '<i class="fa fa-edit"></i> Agendados', 'url' => Yii::app()->createUrl('admage'));

        $informes = array('label' => '<i class="fa fa-signal"></i> Informes',
            'url' => '#', 'items' => array(
                array('label' => '<i class="fa fa-user"></i>   Oportunidades', 'url' => Yii::app()->createUrl('informes/oportunidades')),
                array('label' => '<i class="fa fa-users"></i>  Contactos', 'url' => Yii::app()->createUrl('informes/contactos')),
                array('label' => '<i class="fa fa-dollar"></i> ventas', 'url' => Yii::app()->createUrl('informes/ventas')),
                array('label' => '<i class="fa fa-signal"></i> Dashboard', 'url' => Yii::app()->createUrl('informes/dashboard'))));

        $config = array('label' => '<i class="fa fa-wrench"></i> Configuracion', 'url' => Yii::app()->createUrl('config'));

        $menus = array();
        if ($this->entrar("tele.busqueda", true)) :
            $menus[] = $busqueda;
        endif;
        if ($this->entrar("tele.oportunidades", true)) :
            $menus[] = $oportunidades;
        endif;
        if ($this->entrar("tele.citas", true)) :
            $menus[] = $citas;
        endif;
        if ($this->entrar("tele.asesores", true)) :
            $menus[] = $asesores;
        endif;
        if ($this->entrar("tele.agendados", true)) :
            $menus[] = $agendados;
        endif;
        if ($this->entrar("tele.informes", true)) :
            $menus[] = $informes;
        endif;
        if ($this->entrar("tele.configuracion", true)) :
            $menus[] = $config;
        endif;
        $usuario = ucwords(strtolower(Yii::app()->user->getState("nombre_usuario")));
        $this->widget(
                'booster.widgets.TbNavbar', array(
            'type' => 'null',
            'brand' => '<i class="fa fa-angellist"></i> Telemark',
            'brandUrl' => '#',
            'collapse' => true,
            'fixed' => false,
            'fluid' => true,
            'items' => array(
                array(
                    'class' => 'booster.widgets.TbMenu',
                    'type' => 'navbar',
                    'encodeLabel' => false,
                    'items' => $menus
                ),
                array(
                    'class' => 'booster.widgets.TbMenu',
                    'encodeLabel' => false,
                    'htmlOptions' => array('class' => 'pull-right'),
                    'type' => 'navbar',
                    'items' => array(
                        array('label' => '<i class="fa fa-cog"></i> ' . $usuario, 'url' => '#', 'items' => array(array('label' => 'Perfil', 'url' => '#'), array('label' => 'Salir', 'url' => Yii::app()->createUrl('site/logout'))))
                    ),
                ),
            )
                )
        );
        ?>
        <div class="loadings"></div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sx-12 col-xs-12">
                <?php
                if ($this->entrar("tele.alertas", true)) :
                    ?>
                    <div class="tile_stats_count left col-lg-12 col-md-12 col-sx-12 col-xs-12" id="contnotificaciones">
                        <?php
                        $this->notificaciones();
                        ?>
                    </div>
                    <?php
                endif;
                if ($this->entrar("tele.estadisticas", true)) :
                    ?>
                    <div class="tile_stats_count left col-lg-12 col-md-12 col-sx-12 col-xs-12" id="contestadisticas">
                        <?php
                        $this->estadisticas();
                        ?>
                    </div>
                    <?php
                endif;
                ?>
            </div>
            <div class="col-lg-10 col-md-10 col-sx-12 col-xs-12 " style="font-size: 9pt">
                <?= $content ?>
            </div>
        </div>
    </div>
    <footer>
        <div class="copyright-info">
            <p class="pull-right">Telesentinel Ltda - Dto Comercial</p>
        </div>
    </footer>
    <?php
/////////////////////////ventana modal general/////////////////////
    $this->beginWidget(
            'booster.widgets.TbModal', array(
        'id' => 'modalAlerta',
        "htmlOptions" => array(
            'size' => "modal-lg"
        )
    ));
    ?>
    <div id="modalcuerpo"></div>
    <div id="modalwait" style="display:none;">
        <div class="col-lg-12 modal-body" style="text-align:center;background-color:#fff;  border-radius: 10px;">
            <?php echo CHtml::image(Yii::app()->baseUrl . "/images/loading.gif") ?>
            <br />
            Un Momento ... Espere...
        </div>
    </div>
    <?php $this->endWidget(); ?>
</body>
</html>