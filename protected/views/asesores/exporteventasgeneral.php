<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <table class="table table-bordered">
            <tr>
                <td><b>REGIONAL</b></td>
                <td><b>ASESOR</b></td>
                <td><b>ID</b></td>
                <td><b>CONTRATO</b></td>
                <td><b>RAZON SOCIAL</b></td>
                <td><b>COMERCIAL</b></td>
                <td><b>COMENTARIOS</b></td>
                <td><b>FECHA</b></td>
                <td><b>ESTADO</b></td>
            </tr>
            <?php
            $z = 1;
            foreach ($personal as $p) :
                $contadoropct = 0;
                $contadorct = 0;
                $contadorop = 0;
                $regional = RegionalAgencia::model()->findByAttributes(array('ID_Agencia' => $p['idagencia']));
                foreach ($p['citas'] as $t => $f) :
                    ?>
                    <tr>
                        <td><?= $regional->iDRegional->Descripcion ?> </td>
                        <td><?= $p['nombre'] ?> </td>
                        <td><?= $f['id'] ?> </td>
                        <td><?= $f['contrato'] ?> </td>
                        <td><?= $f['razon_social'] ?> </td>
                        <td><?= $f['comercial'] ?> </td>
                        <td><?= $f['comentarios'] ?> </td>
                        <td><?= $f['dia'] ?> </td>
                        <td><?= $f['estado'] ?> </td>
                    </tr>    
                    <?php
                endforeach;
            endforeach;
            ?>
        </table>
    </body>
</html>   