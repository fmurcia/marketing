<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <table class="table table-bordered">

            <tr>
                <td style="text-align:center" width="100"><b>REGIONAL</b></td>
                <td style="text-align:center" width="250"><b>ASESOR</b></td>
                <td style="text-align:center" width="75"><b>T OPOR</b></td>
                <td style="text-align:center" width="75"><b>T OP C</b></td>
                <td style="text-align:center" width="75"><b>T CITA</b></td>
            </tr>
            <?php
            $z = 1;
            foreach ($personal as $p) :
                $contadoropct = 0;
                $contadorct = 0;
                $contadorop = 0;
                $regional = RegionalAgencia::model()->findByAttributes(array('ID_Agencia' => $p['idagencia']));
                ?>
                <tr>
                    <td style="font-size:8pt" width="100"><?= $regional->iDRegional->Descripcion ?> </td>
                    <td style="font-size:8pt" width="250"><?= $p['nombre'] ?> </td>
                    <td style="text-align:center;">
                        <?php
                        if (COUNT($p['toport']) > 0) :
                            $contadorop = $p['toport'];
                        endif;
                        echo $contadorop;
                        ?>
                    </td>
                    <td style="text-align:center;">
                        <?php
                        if (COUNT($p['tco']) > 0) :
                            $contadoropct = $p['tco'];
                        endif;
                        echo $contadoropct;
                        ?>
                    </td>
                    <td style="text-align:center;">
                        <?php
                        if (COUNT($p['tcitas']) > 0) :
                            $contadorct = $p['tcitas'];
                        endif;
                        echo $contadorct;
                        ?>
                    </td>
                </tr>
                <?php
            endforeach;
            ?>
        </table>
    </body>
</html>    