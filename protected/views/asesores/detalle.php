<table class="table table-striped table-condensed table-bordered">
    <tr><th>ID</th><th>COMERCIAL</th><th>RAZON SOCIAL</th><th>COMENTARIOS</th><th>FECHA</th><th>HORA</th></tr>
    <?PHP
    if ($tabla == 'Oportunidad') :
        foreach ($registros as $r) :
            ?>
            <tr>
                <td style="text-align:center"><?= $r->ID ?></td>
                <td><?= $r->telemercaderista->Nombre ?></td>
                <td><?= $r->Razon_social ?></td>
                <td><?= $r->Observaciones ?></td>
                <td><?= $r->Fecha_creacion ?></td>
                <td><?= $r->Hora_creacion ?></td>
            </tr>
            <?PHP
        endforeach;
    else :
        foreach ($registros as $r) :
            if ($r->ID_Contacto != 0) :
                $con = Contacto::model()->findByPk($r->ID_Contacto);
            else :
                $con = Oportunidad::model()->findByPk($r->ID_Oportunidad);
            endif;
            $time = explode(' ', $con->Fecha_Ultgestion);
            if ($con != NULL) :
                $razon = $con->Razon_social;
                $observacion = $con->Observaciones;
                ?>
                <tr>
                    <td style="text-align:center"><?= $con->ID ?></td>
                    <td><?= $con->telemercaderista->Nombre ?></td>
                    <td><?= $razon ?></td>
                    <td><?= $observacion ?></td>
                    <td><?= $time[0] ?></td>
                    <td><?= $time[1] ?></td>
                </tr>
                <?PHP
            else :
                ?>
                <tr>
                    <td style="text-align:center" colspan="6">Sin Registros</td>
                </tr>
                <?PHP
            endif;
        endforeach;
    endif;
    ?>
</table>
<?php
