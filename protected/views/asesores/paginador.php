<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rcorners">
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="tile-stats">
            <div class="icon" style="color: #337ab7"><i class="fa fa-users"></i></div>
            <div class="count"><?= $totalo ?></div>
            <h3>TOTAL OPORTUNIDADES</h3>
            <p>Registros Oportunidades Triangulo</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="tile-stats">
            <div class="icon" style="color: red"><i class="fa fa-arrow-down"></i></div>
            <div class="count"><?= $totalco ?></div>
            <h3>TOTAL OPORTUNIDAD EN CITA</h3>
            <p>Registros Oportunidades en Cita</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="tile-stats">
            <div class="icon" style="color: #FF7729"><i class="fa fa-balance-scale"></i></div>
            <div class="count"><?= $totalc ?></div>
            <h3>TOTAL CITAS</h3>
            <p>Registros Citas Telemercaderistas</p>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="col-lg-2">
        <center>
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'url' => '#',
                'context' => 'danger',
                'encodeLabel' => false,
                'label' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                'htmlOptions' => array('onclick' => 'changeSemana("atras", "' . $iniciosemana . '")')
                    )
            );
            ?>
        </center>
    </div>
    <div class="col-lg-8">
        <div class="alert alert-dark" style="text-align: center">
            <b><?= strtoupper($mes) ?></b>
        </div>
    </div>
    <div class="col-lg-2">
        <center>
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'url' => '#',
                'context' => 'danger',
                'encodeLabel' => false,
                'label' => '<span class="glyphicon glyphicon-chevron-right"></span>',
                'htmlOptions' => array('onclick' => 'changeSemana("siguiente", "' . $finsemana . '")')
                    )
            );
            ?>
        </center>
    </div>
</div>

<div class="col-lg-12 citas">
    <table class="citastb">
        <tr>
            <td width="600">
                <div class="fixedTable">
                    <header class="fixedTable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td style="text-align:center" width="100"><b>REGIONAL</b></td>
                                    <td style="text-align:center" width="250"><b>ASESOR</b></td>
                                    <td style="text-align:center" width="75"><b>T OPOR</b></td>
                                    <td style="text-align:center" width="75"><b>T OP C</b></td>
                                    <td style="text-align:center" width="75"><b>T CITA</b></td>
                                </tr>
                            </thead>
                        </table>    
                    </header>  
                </div>
            </td>
            <td>
                <div class="fixedTable">
                    <header class="fixedTable-header">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <?php
                                    for ($i = 0; $i < sizeof($dias); $i++) :
                                        $color = "";
                                        $signo = "";
                                        if ($i > 0) :
                                            $signo = "+ ";
                                        endif;
                                        $contdia = $signo . $i . ' day ';
                                        $fechaIntervalo = date('Y-m-d', strtotime($contdia, strtotime($iniciosemana)));
                                        $diames = date_format(date_create($fechaIntervalo), 'd');

                                        if ($diames == date('d')) :
                                            $color = "background: #4ba4df; color:white";
                                        endif;
                                        ?>
                                        <td style="text-align:center; <?= $color ?>" width="100">
                                            <b><?= strtoupper($dias[$i]) . " - " . $diames ?></b>
                                        </td>
                                        <?php
                                    endfor;
                                    ?>
                                </tr>
                            </thead>
                        </table>
                    </header>
                </div>
            </td>
        </tr>
        <tr>
            <td width="600">
                <aside class="fixedTable-sidebar">
                    <table class="table table-bordered">
                        <tbody>
                            <?php
                            $z = 1;
                            foreach ($personal as $p) :
                                $regional = RegionalAgencia::model()->findByAttributes(array('ID_Agencia' => $p['idagencia']));
                                ?>
                                <tr>
                                    <td style="font-size:8pt" width="100"><?= $regional->iDRegional->Descripcion ?> </td>
                                    <td style="font-size:8pt" width="250"><?= $p['nombre'] ?> </td>
                                    <td style="text-align:center;">
                                        <?php
                                        if ($p['toport'] > 0) :
                                            $contexto = 'btn-primary';
                                            $onclick = 'detalle("' . $p['idasesor'] . '", "Oportunidad", "0", "rango")';
                                        else :
                                            $contexto = 'btn-default';
                                            $onclick = '';
                                        endif;
                                        $this->widget(
                                                'booster.widgets.TbButton', array(
                                            'label' => $p['toport'],
                                            'size' => 'extra_small',
                                            'htmlOptions' => array('class' => $contexto, 'onclick' => $onclick)
                                                )
                                        );
                                        ?>
                                    </td>
                                    <td style="text-align:center;">
                                        <?php
                                        if ($p['tco'] > 0) :
                                            $contexto = 'btn-danger';
                                            $onclick = 'detalle("' . $p['idasesor'] . '", "Citao", "0", "rango")';
                                        else :
                                            $contexto = 'btn-default';
                                            $onclick = '';
                                        endif;

                                        $this->widget(
                                                'booster.widgets.TbButton', array(
                                            'label' => $p['tco'],
                                            'size' => 'extra_small',
                                            'htmlOptions' => array('class' => $contexto, 'onclick' => $onclick)
                                                )
                                        );
                                        ?>
                                    </td>
                                    <td style="text-align:center;">
                                        <?php
                                        if ($p['tcitas'] > 0) :
                                            $contexto = 'btn-warning';
                                            $onclick = 'detalle("' . $p['idasesor'] . '", "Citac", "0", "rango")';
                                        else :
                                            $contexto = 'btn-default';
                                            $onclick = '';
                                        endif;

                                        $this->widget(
                                                'booster.widgets.TbButton', array(
                                            'label' => $p['tcitas'],
                                            'size' => 'extra_small',
                                            'htmlOptions' => array('class' => $contexto, 'onclick' => $onclick)
                                                )
                                        );
                                        ?>
                                    </td>
                                </tr>    
                                <?php
                                $z++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </aside>
            </td>
            <td>
                <div class="fixedTable-body">
                    <table class="table table-bordered">
                        <tbody>
                            <?php
                            foreach ($personal as $p) :
                                ?>
                                <tr>
                                    <?php
                                    for ($i = 0; $i < sizeof($dias); $i++) :
                                        $contadorop = 0;
                                        $contextop = 'btn-default';
                                        $contadorct = 0;
                                        $contextoc = 'btn-default';
                                        $contadoropct = 0;
                                        $contextopoc = 'btn-default';
                                        ?>
                                        <td style="text-align:center;">
                                            <?php
                                            $signo = "";
                                            if ($i > 0) :
                                                $signo = "+ ";
                                            endif;

                                            $contdia = $signo . $i . ' day ';
                                            $fechaIntervalo = date('Y-m-d', strtotime($contdia, strtotime($iniciosemana)));
                                            $diames = date_format(date_create($fechaIntervalo), 'd');

                                            if (COUNT($p['oport']) > 0 && isset($p['oport'])) :
                                                foreach ($p['oport'] as $t => $f) :
                                                    if ($f['dia'] == $diames) :
                                                        if ($f['cantidad'] > 0) :
                                                            $contadorop = $f['cantidad'];
                                                            $contextop = 'btn-primary';
                                                            $onclick = 'detalle("' . $p['idasesor'] . '", "Oportunidad", "' . $diames . '", "diario")';
                                                        else :
                                                            $onclick = '';
                                                        endif;
                                                    endif;
                                                endforeach;
                                            endif;

                                            $this->widget(
                                                    'booster.widgets.TbButton', array(
                                                'label' => $contadorop,
                                                'size' => 'extra_small',
                                                'htmlOptions' => array('class' => $contextop, 'onclick' => $onclick)
                                                    )
                                            );

                                            if (COUNT($p['oprtcit']) > 0 && isset($p['oprtcit'])) :
                                                foreach ($p['oprtcit'] as $t => $f) :
                                                    if ($f['dia'] == $diames) :
                                                        $contadoropct = $f['cantidad'];
                                                        $contextopoc = 'btn-danger';
                                                        $onclick = 'detalle("' . $p['idasesor'] . '", "Citao", "' . $diames . '", "diario")';
                                                    else :
                                                        $onclick = '';
                                                    endif;
                                                endforeach;
                                            endif;

                                            $this->widget(
                                                    'booster.widgets.TbButton', array(
                                                'label' => $contadoropct,
                                                'size' => 'extra_small',
                                                'htmlOptions' => array('class' => $contextopoc, 'onclick' => $onclick)
                                                    )
                                            );

                                            if (COUNT($p['citas']) > 0 && isset($p['citas'])) :
                                                foreach ($p['citas'] as $t => $f) :
                                                    if ($f['dia'] == $diames) :
                                                        $contadorct = $f['cantidad'];
                                                        $contextoc = 'btn-warning';
                                                        $onclick = 'detalle("' . $p['idasesor'] . '", "Citac", "' . $diames . '", "diario")';
                                                    else :
                                                        $onclick = '';
                                                    endif;
                                                endforeach;
                                            endif;

                                            $this->widget(
                                                    'booster.widgets.TbButton', array(
                                                'label' => $contadorct,
                                                'size' => 'extra_small',
                                                'htmlOptions' => array('class' => $contextoc, 'onclick' => $onclick)
                                                    )
                                            );
                                            ?>
                                        </td>
                                        <?php
                                    endfor;
                                    ?>       
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>  
    </table>
</div>
<div class="clearfix"></div>
<script>
    (function () {
        var demo, fixedTable;
        fixedTable = function (el) {
            var $body, $header, $sidebar;
            $body = $(el).find('.fixedTable-body');
            $sidebar = $(el).find('.fixedTable-sidebar table');
            $header = $(el).find('.fixedTable-header table');
            return $($body).scroll(function () {
                $($sidebar).css('margin-top', -$($body).scrollTop());
                return $($header).css('margin-left', -$($body).scrollLeft());
            });
        };
        demo = new fixedTable($('.citas'));
    }.call(this));
</script>