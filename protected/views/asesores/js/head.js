/*
 * @param {type} accion
 * @param {type} fecha
 * @returns {Boolean}         
 */
function changeRange()
{
    jQuery.ajax({
        'type': 'POST',
        'async': true, 
        'url': '<?php echo Yii::app()->createUrl("asesores/citasrango") ?>',
        'data': 'accion=rango&fecha1=' + $('#rango_fecha1').val() + '&fecha2=' + $('#rango_fecha2').val() + '&funcionario=' + $('#filasesor').val(),
        'success':
                function (respuesta) {
                    $('#paginadorMes').fadeOut('fast');
                    $('#paginadorMes').html(respuesta);
                    $('#paginadorMes').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}
    
function exportRange(){
    var fecha1 = $('#rango_fecha1').val();
    var fecha2 = $('#rango_fecha2').val();
    console.log(fecha1 + ' - - - ' + fecha2);
    jQuery.ajax({
        'type': 'POST',
        'async': true, 
        'url': '<?php echo Yii::app()->createUrl("asesores/exporte") ?>',
        'data': 'accion=rango&fecha1=' + $('#rango_fecha1').val() + '&fecha2=' + $('#rango_fecha2').val() + '&funcionario=' + $('#filasesor').val(),
        'success':
                function (respuesta) {
                    $('#paginadorMes').fadeOut('fast');
                    $('#paginadorMes').html(respuesta);
                    $('#paginadorMes').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function changeSemana(accion, fecha)
{
    jQuery.ajax({
        'type': 'POST',
        'async': true, 
        'url': '<?php echo Yii::app()->createUrl("asesores/citassemana") ?>',
        'data': 'accion=' + accion + '&fecha=' + fecha + '&funcionario=' + $('#filasesor').val() + '&fecha1=' + $('#rango_fecha1').val() + '&fecha2=' + $('#rango_fecha2').val(),
        'success':
                function (respuesta) {
                    $('#paginadorMes').fadeOut('fast');
                    $('#paginadorMes').html(respuesta);
                    $('#paginadorMes').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function detalle(idasesor, tabla, dia, accion){
    $("#modalAlerta").modal('show');
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("asesores/detalle") ?>',
        'data': 'idasesor=' + idasesor + '&fechainicial=' + $('#rango_fecha1').val() 
                + '&fechafinal=' + $('#rango_fecha2').val() + '&dia=' + dia + '&tabla=' + tabla + '&accion=' + accion,
        'success':
                function (respuesta) {
                    $("#modalcuerpo").html('\
                        <div class="modal-header">\n\
                            <a class="close" data-dismiss="modal">&times;</a>\n\
                            <h4>Detalle</h4>\n\
                        </div>\n\
                        <div class="modal-body"><p>' + respuesta + '</p></div>');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}