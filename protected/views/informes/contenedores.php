<div class="contenedorventas col-lg-12 col-md-12 col-sx-12 col-sx-12"  style="overflow: auto; height: 770px">
    <div class="col-lg-12">
        <?php $this->renderPartial('totales_ventas') ?>
    </div>
    <div class="col-lg-12">
        <?php $this->renderPartial('registros_ventas') ?>
    </div>
</div>
<div class="contenedorasesor col-lg-12 col-md-12 col-sx-12 col-sx-12"  style="display:none; overflow: auto; height: 770px">
    <div class="col-lg-12">
        <?php $this->renderPartial('totales_asesor') ?>
    </div>
    <div class="col-lg-12">
        <?php $this->renderPartial('registros_asesor') ?>
    </div>
</div>
<div class="contenedorcomercial col-lg-12 col-md-12 col-sx-12 col-sx-12" style="display:none; overflow: auto; height: 770px">
    <div class="col-lg-12">
        <?php $this->renderPartial('totales_comercial') ?>
    </div>
    <div class="col-lg-12   ">
        <?php $this->renderPartial('registros_comercial') ?>
    </div>
</div>
<div class="contenedorpreventas col-lg-12 col-md-12 col-sx-12 col-sx-12" style="display:none;  overflow: auto; height: 770px">
    <div class="col-lg-12">
        <?php $this->renderPartial('totales_preventas') ?>
    </div>
    <div class="col-lg-12">
        <?php $this->renderPartial('registros_preventas') ?>
    </div>
</div>