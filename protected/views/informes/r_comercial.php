<?php

$ttlunidad = $this->registrosUnidades();
$ttlagrund = $this->registroAgrupadoUnidad();
$ttlagrciu = $this->registroAgrupadoCiudad();

$monitoreo = 0; $televideo = 0; $rastreo = 0; 

$m = 0; $o = 0; $totalregactual = 0; $mapfre = 0; $sms = 0; $extra = 0;
$mantenimiento = 0; $mantenimiento_televideo = 0; $mantenimiento_rastreo = 0; 

$ven_m = 0; $ven_t = 0; $ven_r = 0;
$ins_m = 0; $ins_t = 0; $ins_r = 0; 
$fac_m = 0; $fac_t = 0; $fac_r = 0; 
$des_m = 0; $des_t = 0; $des_r = 0; 

$arr_tl = array(); $barLine = array(); 
$pieLine = array(); $arr_nomservicios = array();

$arr_nomservicios[0] = 'Monitoreo';
$arr_nomservicios[1] = 'Televideo';
$arr_nomservicios[2] = 'Rastreo';

foreach ($ttlunidad as $t) :    
    if ($t->Codigo4DDetalleServicio == 1) :
        $monitoreo += $t->Total;
    elseif ($t->Codigo4DDetalleServicio == 10) :
        $sms += $t->Total;
    elseif ($t->Codigo4DDetalleServicio == 16) :
        $televideo += $t->Total;
    elseif ($t->Codigo4DDetalleServicio == 31) :
        $mantenimiento += $t->Total;
    elseif ($t->Codigo4DDetalleServicio == 32) :
        $mantenimiento_televideo += $t->Total;
    elseif ($t->Codigo4DDetalleServicio == 36) :
        $rastreo += $t->Total;
    elseif ($t->Codigo4DDetalleServicio == 39) :
        $mantenimiento_rastreo += $t->Total;
    elseif ($t->Codigo4DDetalleServicio ==  41) :
        $mapfre += $t->Total;
    elseif ($t->Codigo4DDetalleServicio == 44) :
        $extra += $t->Total;
    endif;
endforeach;

/* Variables para las Barras 3D */
foreach ($ttlagrund as $t) :
    if ($t->CodigoServicioDetalle == 1) :
        if ($t->EstadoDetalle == 2) :
            $ven_m += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 10) :
            $ins_m += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 14) :
            $fac_m += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 19) :
            $des_m += $t->ValorServicioDetalle;
        endif;
    endif;       
    if ($t->CodigoServicioDetalle == 2) :
        if ($t->EstadoDetalle == 2) :
            $ven_t += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 10) :
            $ins_t += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 14) :
            $fac_t += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 19) :
            $des_t += $t->ValorServicioDetalle;
        endif;
    endif;        
    if ($t->CodigoServicioDetalle == 3) :
        if ($t->EstadoDetalle == 2) :
            $ven_r += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 10) :
            $ins_r += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 14) :
            $fac_r += $t->ValorServicioDetalle;
        endif;
        if ($t->EstadoDetalle == 19) :
            $des_r += $t->ValorServicioDetalle;    
        endif;
    endif;    
endforeach;

$barLine[0] = array('name' => 'Ventas', 'data' => array($ven_m, $ven_t, $ven_r), 'stack' => 'male');
$barLine[1] = array('name' => 'Instalaciones', 'data' => array($ins_m, $ins_t, $ins_r), 'stack' => 'female');
$barLine[2] = array('name' => 'Facturacion', 'data' => array($fac_m, $fac_t, $fac_r), 'stack' => 'male');
$barLine[3] = array('name' => 'Descartados', 'data' => array($des_m, $des_t, $des_r), 'stack' => 'female');

/* Variables para el PIE */
foreach ($ttlagrciu as $c) :
    $totalregactual += $c->ValorServicioDetalle;
endforeach;

if($totalregactual == 0) :
    $totalregactual = 1;
endif;

foreach ($ttlagrciu as $t) :
    $regional = Regional::model()->findByPk($t->IDRegionalCliente);
    if ($regional != NULL) :
        $pieLine[$o] = array('name' => $regional->Descripcion, 'y' => round((($t->Total / $totalregactual) * 100), 1));
    endif;
    $o++;
endforeach;
?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content detalcolor">
                <div class="x_title">
                    <h2>Total en Unidad de Negocio</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-4 title">
                        <span>Monitoreo</span>
                        <h2><span>$<?= number_format($monitoreo) ?></span></h2>
                    </div>
                    <div class="col-md-4 title">
                        <span>Televideo</span>
                        <h2><span>$<?= number_format($televideo) ?></span></h2>
                    </div>
                    <div class="col-md-4 title">
                        <span>Rastreo</span>
                        <h2><span>$<?= number_format($rastreo) ?></span></h2>
                    </div>
                </div>
                <div class="x_title">
                    <h2>Total en Adicionales</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-4 title">
                        <span>Mapfre</span>
                        <h2><span>$<?= number_format($mapfre) ?></span></h2>
                    </div>
                    <div class="col-md-4 title">
                        <span>SMS</span>
                        <h2><span>$<?= number_format($sms) ?></span></h2>
                    </div>
                    <div class="col-md-4 title">
                        <span>Tarifa Extra</span>
                        <h2><span>$<?= number_format($extra) ?></span></h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-4 title">
                        <span>Mantenimiento Monitoreo</span>
                        <h2><span>$<?= number_format($mantenimiento) ?></span></h2>
                    </div>
                    <div class="col-md-4 title">
                        <span>Mantenimiento Televideo</span>
                        <h2><span>$<?= number_format($mantenimiento_televideo) ?></span></h2>
                    </div>
                    <div class="col-md-4 title">
                        <span>Mantenimiento Rastreo</span>
                        <h2><span>$<?= number_format($mantenimiento_rastreo) ?></span></h2>
                    </div>
                </div>
                <div class="x_title">
                    <h2>Reporte Vigente <small>Progreso del Mes</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6"><center><div id="container_bar3d" style="height: 400px"></div></center></div>
                <div class="col-md-6 col-sm-6 col-xs-6"><center><div id="container_pie3d" style="height: 400px"></div></center></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#container_bar3d').highcharts({
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    viewDistance: 25,
                    depth: 40
                }
            },
            title: {
                text: 'Total Unidad de Negocio'
            },
            xAxis: {
                categories: <?= json_encode($arr_nomservicios) ?>
            },
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: 'Unidades de Negocio'
                }
            },
            tooltip: {
                headerFormat: '<b>{point.key}</b><br>',
                pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackValorServicioDetalle}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    depth: 40
                }
            },
            series: <?= json_encode($barLine) ?>
        });

        $('#container_pie3d').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Proyeccion de Venta - Ciudades'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        },
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                    name: '%',
                    data: <?= json_encode($pieLine) ?>
                }]
        });
    });
</script>    