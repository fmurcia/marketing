<?php

//retorna el total de registros Proyectados para el Cierre

$ttlfac = $this->totalCierreVentas('Facturacion');
$ttlins = $this->totalCierreVentas('Instalacion');
$ttlven = $this->totalCierreVentas('Venta');
$ttldes = $this->totalCierreVentas('Descartado');

$ttlase = $this->totalProyeccion('Pasesor');
$ttlcom = $this->totalProyeccion('Pcomercial');
$ttlpre = $this->totalProyeccion('Preventa');

$totalregistrado = $ttlase + $ttlcom + $ttlpre;
$totalcierre = $ttlfac + $ttlins + $ttlven + $ttldes;

?>
<div class="row top_tiles">
    <div class="x_title">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <h2>Proyeccion en Venta Cierre de <b><?= Yii::app()->user->getState("MesCierre")  ?></b></h2>
        </div>
        <div class="col-lg-1 col-md-1 col-lg-offset-5 col-md-offset-5 col-sm-1 col-xs-1 filtrodinamico" style=" cursor:pointer">
            <strong class="right"><span class="fa fa-search-plus fa-2x" style="color:black;"></span> Filtros </strong>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rcorners">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #46B29D"><i class="fa fa-users"></i></div>
                    <div class="count">$<?= number_format($ttlase); ?></div>
                    <h3>PENDIENTES ASESOR</h3>
                    <p>Registros de Asesores Comerciales</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #FF7729"><i class="fa fa-asterisk"></i></div>
                    <div class="count">$<?= number_format($ttlcom); ?></div>
                    <h3>PENDIENTES COMERCIAL</h3>
                    <p>Registros Cierre Vigente Area Comercial</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #FFEAC9"><i class="fa fa-server"></i></div>
                    <div class="count">$<?= number_format($ttlpre); ?></div>
                    <h3>PREVENTA</h3>
                    <p>Registros del Area de Revision y Procesos</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #F23C55"><i class="fa fa-dollar"></i></div>
                    <div class="count">$<?= number_format($totalregistrado); ?></div>
                    <h3>TOTAL PROYECTADO</h3>
                    <p>Total Registrado</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rcorners">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #326BAB"><i class="fa fa-recycle"></i></div>
                    <div class="count">$<?= number_format($ttlven); ?></div>
                    <h3>VENTA</h3>
                    <p>Registros en Venta Cierre Vigente</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #326BAB"><i class="fa fa-cogs"></i></div>
                    <div class="count">$<?= number_format($ttlins); ?></div>
                    <h3>INSTALACION</h3>
                    <p>Registros en Instalacion Cierre Vigente</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #326BAB"><i class="fa fa-sort-amount-desc"></i></div>
                    <div class="count">$<?= number_format($ttlfac); ?></div>
                    <h3>FACTURACION</h3>
                    <p>Registros en Facturacion Cierre Vigente</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon" style="color: #326BAB"><i class="fa fa-dollar"></i></div>
                    <div class="count">$<?= number_format($totalcierre); ?></div>
                    <h3>TOTAL CIERRE</h3>
                    <p>Registros en Totales Cierre Vigente</p>    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 rcorners">
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="tile-stats" style="background: #326BAB">
                    <div class="row">    
                        <div class="col-lg-6"> 
                            <h3 style="color:white; padding-top: 10px "><b>TOTAL GENERAL</b></h3>
                            <p style="color:white">Registros Generales</p>    
                        </div> 
                        <div class="col-lg-6">    
                            <div class="count"  style="color:white">$<?= number_format($totalcierre + $totalregistrado); ?></div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 rcorners">
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="tile-stats" style="background: #eaeaea">
                    <div class="row">    
                        <div class="col-lg-6"> 
                            <h3 style="color:#73879c; padding-top: 10px "><b>DESCARTADO</b></h3>
                            <p style="color:#73879c">Registros Descartados Cierre</p>    
                        </div> 
                        <div class="col-lg-6">    
                            <div class="count"  style="color:#73879c">$<?= number_format($ttldes); ?></div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>