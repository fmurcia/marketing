<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <table border="1"  style="font-size:7px;font-family:tahoma,arial;" cellspacing="0" bordercolor="#666666">
            <tr>
                <td style="text-align:center"><strong>ID</strong></td>
                <td style="text-align:center"><strong>COMERCIAL</strong></td>
                <td style="text-align:center"><strong>ASESOR</strong></td>
                <td style="text-align:center"><strong>RAZON SOCIAL</strong></td>
                <td style="text-align:center"><strong>EMAIL</strong></td>
                <td style="text-align:center"><strong>MEDIO CONTACTO</strong></td>
                <td style="text-align:center"><strong>CIUDAD</strong></td>
                <td style="text-align:center"><strong>ID CIUDAD</strong></td>
                <td style="text-align:center"><strong>SERVICIO</strong></td>
                <td style="text-align:center"><strong>TIPO CLIENTE</strong></td>
                <td style="text-align:center"><strong>FECHA CREACION</strong></td>
                <td style="text-align:center"><strong>TIPO CONTACTO</strong></td>
                <td style="text-align:center"><strong>MOTIVO DESCARTADO</strong></td>
                <td style="text-align:center"><strong>FECHA GESTION</strong></td>
                <td style="text-align:center"><strong>COMENTARIOS</strong></td>
                <td style="text-align:center"><strong>MES CIERRE</strong></td>
                <td style="text-align:center"><strong>TIPO DESCARTADO ANT</strong></td>
                <td style="text-align:center"><strong>TEL FIJO</strong></td>
                <td style="text-align:center"><strong>CELULAR</strong></td>
                <td style="text-align:center"><strong>DIRECCION</strong></td>
            </tr>
            <?php
            if (isset($excel_data)):
                foreach ($excel_data as $e) :
                    $ci = 'Ninguno';
                    $idcliente = Cliente::model()->find('ID_Contacto =:idcontacto', array('idcontacto' => $e['ID']));
                    if ($idcliente != NULL) :
                        $cliente_venta = ClienteVenta::model()->find('ID_cliente =:idcliente', array('idcliente' => $idcliente['ID']));
                        if ($cliente_venta != NULL) :
                            $cierre = Cierres::model()->findByPk($cliente_venta->Cierre);
                            if ($cierre != NULL) :
                                $ci = $cierre->Mes;
                            endif;
                        endif;
                    endif;
                    
                    $dtd = AccionGestion::model()->findByAttributes(array('ID' => $e['TipoDescartado']));            
                    
                    $tpCl = 'Ninguno';
                    $tpCliente = TipoCliente::model()->findByAttributes(array('ID' => $e['Tipo']));
                    if($tpCliente != NULL) :
                        $tpCl = $tpCliente->Nombre;
                    endif;
                    ?>
                    <tr>
                        <td style="text-align:center"><?= $e['ID'] ?></td>
                        <td style="text-align:center"><?= $e['Comercial'] ?></td>
                        <td style="text-align:center"><?= $e['Asesor'] ?></td>
                        <td style="text-align:center"><?= $e['Razon_social'] ?></td>
                        <td style="text-align:center"><?= $e['Email'] ?></td>
                        <td style="text-align:center"><?= $e['Medio_contacto'] ?></td>
                        <td style="text-align:center"><?= $e['Ciudad'] ?></td>
                        <td style="text-align:center"><?= $e['ID_Ciudad'] ?></td>
                        <td style="text-align:center"><?= $e['Servicio'] ?></td>
                        <td style="text-align:center"><?= $tpCl ?></td>
                        <td style="text-align:center"><?= $e['Fecha_creacion'] ?></td>
                        <td style="text-align:center"><?= $e['Tipo_contacto'] ?></td>
                        <td style="text-align:center"><?= $e['EstadoWeb'] ?></td>
                        <td style="text-align:center"><?= $e['Fecha_gestion'] ?></td>
                        <td style="text-align:center"><?= $e['Comentarios'] ?></td>
                        <td style="text-align:center"><?= $ci ?></td>
                        <td style="text-align:center"><?= $dtd->Descripcion ?></td>
                        <td style="text-align:center"><?= isset($e['Telefono']) ? $e['Telefono'] : 0 ?></td>
                        <td style="text-align:center"><?= isset($e['Celular']) ? $e['Celular'] : 0 ?></td>
                        <td style="text-align:center"><?= isset($e['Direccion']) ? $e['Direccion'] : ''?></td>
                    </tr>
                    <?php
                endforeach;
            endif;
            ?>
        </table>
    </body>
</html>