<div class="col-lg-12 col-md-12 col-sx-12 col-xs-12 ajustetree">
    <ul class="menucolores nav navbar-nav">
        <li class="active"><a href="javascript:loadcontenedor('ventas');"  class="btn-primary azul" ><p><b>Proyeccion Ventas</b></p></a></li>
        <li><a href="javascript:loadcontenedor('asesor');"  class="btn-success verde"><p><b>Pendientes Asesor</b></p></a></li>
        <li><a href="javascript:loadcontenedor('comercial');"  class="btn-warning amarillo"><p><b>Pendientes Comercial</b></p></a></li>
        <li><a href="javascript:loadcontenedor('preventas');"  class="btn-danger rojo"><p><b>Pendientes Preventa</b></p></a></li>
    </ul>
</div>
<div class="col-lg-12 col-md-12 col-sx-12 col-xs-12">
    <div class="contadores">
        <?php $this->renderPartial('contenedores'); ?>
    </div>
</div>
<div id="panelfiltro">
    <div class="ajustetree">
        <div class="filtventas">
            <?php $this->renderPartial('filtros') ?>
        </div>
    </div>
</div>