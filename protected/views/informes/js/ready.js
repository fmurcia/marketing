var doc = $(document);
var tree = $('#jstree');
tree.on('changed.jstree', function (e, data)
{
    var i, j, r = [];
    for (i = 0, j = data.selected.length; i < j; i++)
    {
        var code = data.instance.get_node(data.selected[i]).data.code;
        if (code !== undefined) {
            r.push(code);
        }
    }
    actfildash(r.join(','));
}).jstree({
    'core': {
        'themes': {"stripes": true}
    },
    'plugins': ['checkbox']
});

doc.on('click', '.filtrodinamico', function (e) {
    e.preventDefault();
    $("#panelfiltro").animate({
        width: "toggle",
        opacity: "toggle",
        right: parseInt($("#panelfiltro").css("right")) == 11 ? "-=" + $("#panelfiltro").outerWidth() : 0
    });
    return false;
});




var cb = function (start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
};

var optionSet1 = {
    startDate: moment().subtract(29, 'days'),
    endDate: moment(),
    minDate: '01/01/2017',
    maxDate: '12/31/2019',
    dateLimit: {
        days: 60
    },
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: true,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'left',
    buttonClasses: ['btn btn-default'],
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small',
    format: 'MM/DD/YYYY',
    separator: ' a ',
    locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Limpiar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Rango',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
};
$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' Hasta ' + moment().format('MMMM D, YYYY'));
$('#reportrange').daterangepicker(optionSet1, cb);
$('#reportrange').on('show.daterangepicker', function () {
    console.log("show event fired");
});
$('#reportrange').on('hide.daterangepicker', function () {
    console.log("hide event fired");
});
$('#reportrange').on('apply.daterangepicker', function (ev, picker) {
    console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " a " + picker.endDate.format('MMMM D, YYYY'));
    $("#rango_fecha1").val(picker.startDate.format('YYYY-MM-DD'));
    $("#rango_fecha2").val(picker.endDate.format('YYYY-MM-DD'));
});
$('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
    console.log("cancel event fired");
});
$('#options1').click(function () {
    $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
});
$('#destroy').click(function () {
$('#reportrange').data('daterangepicker').remove();
});
