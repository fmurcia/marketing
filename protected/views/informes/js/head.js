function generarExporteContactos()
{
    $.ajax({
        type: 'POST',
        url: '<?= Yii::app()->createUrl("informes/informe") ?>',
        data: 'fecha1=' + $("#rango_fecha1").val() + '&fecha2=' + $("#rango_fecha2").val() + '&tabla=' + $('#tabla').val(),
        beforeSend : function (){
            $(".loading").fadeIn('fast');
        },
        success: function (respuesta)
        {
            $(".loading").fadeOut('fast');
            $("#tableexport").fadeOut("fast");
            $("#tableexport").html(respuesta);
            $("#tableexport").fadeIn("fast");
        },
        error: function (m, e, a) {
            console.log(m.responseText);
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function generarReporteVentas()
{
    $.ajax({
        type: 'POST',
        url: '<?= Yii::app()->createUrl("informes/infoventas") ?>',
        data: 'idcierre=' + $("#cierresid").val(),
        beforeSend : function (){
            $(".loading").fadeIn('fast');
        },
        success: function (respuesta)
        {
            $(".loading").fadeOut('fast');
            $("#tableexport").fadeOut("fast");
            $("#tableexport").html(respuesta);
            $("#tableexport").fadeIn("fast");
        },
        error: function (m, e, a) {
            console.log(m.responseText);
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}



function actfildash(flt) {
    filtros = flt;
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('informes/updfltr') ?>",
        data: {
            fltr: filtros,
            idcierre: $('#qrycierre').val()
        },
        beforeSend: function () {
            $('#modalAlerta').modal('show');
        },
        success: function (data) {
            $('#modalAlerta').modal('hide');
            $(".changeCierre").html(data);
            busquedasyncdash();
        },
        error: function (err) {
            error(err.responseText);
        }
    });
}

function busquedasyncdash() {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('informes/totales') ?>",
        data: {
            fltr: filtros,
            idcierre: $('#qrycierre').val()
        },
        beforeSend: function () {
            $('.contadores').html($('#modalwait').html());
        },
        success: function (data) {
            $('.contadores').html(data);
        },
        error: function (err) {
            error(err.responseText);
        }

    });
}


function filtrocierrecomision() {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('informes/divcomisiones')?>",
        data: {
            idcierre: $('#qrycierrecom').val()
        },
        beforeSend: function () {
            $('.divcomisiones').html($('#modalwait').html());
        },
        success: function (data) {
            $('.divcomisiones').html(data);
        },
        error: function (err) {
            error(err.responseText);
        }

    });
}