<div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <label class="control-label"><h2>Cierres</h2></label>
        <div class="clearfix"></div>
    </div>
    <div>
        <?php
        $this->widget(
                'booster.widgets.TbSelect2', array(
            'asDropDownList' => true,
            'id' => 'qrycierre',
            'name' => 'qrycierre',
            "data" => Cierres::model()->getCierres(),
            "value" => Cierres::model()->getCierre(date('Y-m-d')),
            "htmlOptions" => array(
                "class" => "form-control",
                "onchange" => "filtrocierre()"
            ),
            'options' => array(
                'placeholder' => 'Cierre',
                'width' => '100%',
            ))
        );
        ?>
    </div>
</div>
<br><br><br>
<div class="flspace">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <label class="control-label"><h2>Regionales</h2></label>
        <div id="jstree">
            <ul>
                <?php
                $regionales = $this->getRegionalesXNivel();
                if (!empty($regionales)) :
                    foreach ($regionales as $regional) :
                        $agencias = $regional->getAgencias();
                        ?>
                        <li data-code="reg-<?= $regional->ID_Regional ?>" data-jstree='{"icon":"glyphicon glyphicon-map-marker"}'><?= strtoupper($regional->Descripcion) ?>     
                            <ul>
                                <li data-jstree='{"icon":"glyphicon glyphicon-globe"}'>AGENCIAS
                                    <?php
                                    if (count($agencias) > 0) {
                                        ?>
                                        <ul>
                                            <?php
                                            foreach ($agencias as $agencia) :
                                                if ($agencia->TipoAgencia == 1) :
                                                    ?>
                                                    <li data-code="age-<?= $agencia->ID ?>" data-jstree='{"icon":"glyphicon glyphicon-map-marker"}'><?php echo strtoupper($agencia->Nombre) ?></li>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                                <li data-jstree='{"icon":"glyphicon glyphicon-globe"}'>FUERZA VENTA
                                    <?php
                                    if (count($agencias) > 0) {
                                        ?>
                                        <ul>
                                            <?php
                                            foreach ($agencias as $agencia) :
                                                if ($agencia->TipoAgencia == 3) :
                                                    ?>
                                                    <li data-code="age-<?= $agencia->ID ?>" data-jstree='{"icon":"glyphicon glyphicon-map-marker"}'><?php echo strtoupper($agencia->Nombre) ?></li>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            </ul>
                        </li>
                        <?php
                    endforeach;
                endif;
                ?>
            </ul>
        </div>
    </div>    
</div>