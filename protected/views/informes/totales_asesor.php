<?php

//Funcion que Retorna las Estadisticas Vigentes del cierre y/o ciudades Filtradas
$totalproyeccion = $this->totalProyeccionDetalle('Pasesor');

$monitoreo = 0; $televideo = 0; $rastreo = 0; $sms = 0; $mapfre = 0; 

$mantenimiento_monitoreo = 0;
$mantenimiento_televideo = 0;
$mantenimiento_rastreo = 0;

foreach($totalproyeccion as $t) :
    if($t->Codigo4DDetalleServicio == 1) :
        $monitoreo += $t->ValorServicioDetalle;
    endif;
    if($t->Codigo4DDetalleServicio == 36) :
        $rastreo += $t->ValorServicioDetalle;
    endif;
    if($t->Codigo4DDetalleServicio == 16) :
        $televideo += $t->ValorServicioDetalle;
    endif;
    if($t->Codigo4DDetalleServicio == 10) :
        $sms += $t->ValorServicioDetalle;
    endif;
    if($t->Codigo4DDetalleServicio == 41) :
        $mapfre += $t->ValorServicioDetalle;
    endif;
    if($t->Codigo4DDetalleServicio == 31) :
        $mantenimiento_monitoreo += $t->ValorServicioDetalle;
    endif;
    if($t->Codigo4DDetalleServicio == 32) :
        $mantenimiento_televideo += $t->ValorServicioDetalle;
    endif;
    if($t->Codigo4DDetalleServicio == 39) :
        $mantenimiento_rastreo += $t->ValorServicioDetalle;
    endif;
endforeach;

$total = $monitoreo + $televideo + $rastreo + $sms + $mapfre + $mantenimiento_monitoreo + $mantenimiento_televideo + $mantenimiento_rastreo;

?>

<div class="row top_tiles">
    <div class="x_title">
        <h2>Proyeccion en Pendientes Asesor</h2>
        <div class="clearfix"></div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-signal"></i></div>
            <div class="count">$<?= number_format($monitoreo); ?></div>
            <h3>MONITOREO</h3>
            <p>Registros de Venta Cierre Vigente por Asesores Comerciales</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-camera"></i></div>
            <div class="count">$<?= number_format($televideo); ?></div>
            <h3>TELEVIDEO</h3>
            <p>Registros Cierre Vigente Area Comercial</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-automobile"></i></div>
            <div class="count">$<?= number_format($rastreo); ?></div>
            <h3>RASTREO</h3>
            <p>Registros de Rastreo Vehicular </p>
        </div>
    </div>
    
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-envelope"></i></div>
            <div class="count">$<?= number_format($sms); ?></div>
            <h3>SMS</h3>
            <p>Total Registrado</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-star"></i></div>
            <div class="count">$<?= number_format($mapfre); ?></div>
            <h3>MAPFRE</h3>
            <p>Total Registrado</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-wrench"></i></div>
            <div class="count">$<?= number_format($mantenimiento_monitoreo); ?></div>
            <h3>MANT. MONITOREO</h3>
            <p>Total Registrado</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-wrench"></i></div>
            <div class="count">$<?= number_format($mantenimiento_televideo); ?></div>
            <h3>MANT. TELEVIDEO</h3>
            <p>Total Registrado</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-wrench"></i></div>
            <div class="count">$<?= number_format($mantenimiento_rastreo); ?></div>
            <h3>MANT. RASTREO</h3>
            <p>Total Registrado</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-dollar"></i></div>
            <div class="count">$<?= number_format($total); ?></div>
            <h3>TOTAL</h3>
            <p>Total Registrado</p>
        </div>
    </div>
    <div class="x_title">
        <h2>Cierre de <?= Yii::app()->user->getState("MesCierre") ?></h2>
        <div class="clearfix"></div>
    </div>
</div>