<?php
//retorna el total de registros Detallados en la Proyeccion
$total = $this->totalProyeccionDetalle('Pcomercial');

$criteria = new CDbCriteria();
$criteria->addCondition('Estado = 1');
$criteria->order = 'ID ASC';
$servicios = Servicios::model()->findAll($criteria);

$ultcli = 0;
$arr_view = array();
$totalven = 0;

foreach ($total as $r) :
    $agencia = Agencia::model()->findByPk($r->AgenciaCliente);
    $cliente = Cliente::model()->findByAttributes(array('ID' => $r->IDCliente));
    $contacto = Contacto::model()->findByAttributes(array('ID' => $cliente->ID_Contacto));
    
    if($cliente->ID_Contacto != NULL && $cliente->ID_Contacto != 0) :
        $arr_view[$r->IDCliente]['IDContacto'] = $cliente->ID_Contacto;
        $arr_view[$r->IDCliente]['TipoContacto'] = $contacto->Tipo_contacto;
    else :
        $arr_view[$r->IDCliente]['IDContacto'] = 0;
        $arr_view[$r->IDCliente]['TipoContacto'] = 0;
    endif;
    
    if ($cliente != NULL) :
        $arr_view[$r->IDCliente]['Contrato'] = $cliente->Contrato;
    else :
        $arr_view[$r->IDCliente]['Contrato'] = 'SIN CONTRATO';
    endif;

    if ($agencia != NULL) :
        $arr_view[$r->IDCliente]['Agencia'] = strtoupper($agencia->Nombre);
    else :
        $arr_view[$r->IDCliente]['Agencia'] = 'SIN AGENCIA';
    endif;

    if ($r->IDCliente == $ultcli) :
        foreach ($servicios as $s) :
            if ($s->Codigo_4D == $r->Codigo4DDetalleServicio) :
                $arr_view[$r->IDCliente][$s->Codigo_4D] += $r->ValorServicioDetalle;
                $totalven += $r->ValorServicioDetalle;
            endif;
        endforeach;
    else :
        foreach ($servicios as $s) :
            if ($s->Codigo_4D == $r->Codigo4DDetalleServicio) :
                $arr_view[$r->IDCliente][$s->Codigo_4D] = $r->ValorServicioDetalle;
                $totalven = $r->ValorServicioDetalle;
            else :
                $arr_view[$r->IDCliente][$s->Codigo_4D] = 0;
            endif;
        endforeach;
    endif;

    $arr_view[$r->IDCliente]['Total'] = $totalven;
    $ultcli = $r->IDCliente;
endforeach;

$totalgeneral = 0;
?>
<div class="row">  
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>PENDIENTES COMERCIAL<small>Actividad General</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="editab">
                    <thead>
                        <tr>
                            <th>CONTRATO</th>
                            <th>AGENCIA</th>
                            <?php foreach ($servicios as $s) : ?>
                                <th><?= strtoupper($s->Nombre) ?></th>
                            <?php endforeach; ?>
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($arr_view as $ar => $key) : ?>   
                            <tr>
                                <td onclick="formulariocontacto(<?= $key['IDContacto'] ?>, <?= $key['TipoContacto'] ?>)"><?= $key['Contrato'] ?></td>
                                <td><?= strtoupper($key['Agencia']) ?></td>
                                <?php foreach ($servicios as $s) : ?>
                                    <td>$<?= number_format($key[$s->Codigo_4D]) ?></td>
                                <?php endforeach; ?>
                                <td>$<?= number_format($key['Total']) ?></td>
                            </tr>
                            <?php
                            $totalgeneral += $key['Total'];
                        endforeach;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td style="text-align: right" ><b>TOTALES :</b></td>
                            <td style="text-align: right"><b>$<?= number_format($totalgeneral) ?></b></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>