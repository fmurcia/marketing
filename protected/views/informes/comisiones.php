<div class="row">  
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <div class="x_title">
            <h2>MOVIMIENTO  <small>Actividad General</small></h2>
            <div class="clearfix"></div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <?php
            $this->widget(
                    'booster.widgets.TbSelect2', array(
                'asDropDownList' => true,
                'id' => 'qrycierrecom',
                'name' => 'qrycierrecom',
                "data" => Cierres::model()->getCierres(),
                "value" => Cierres::model()->getCierre(date('Y-m-d')),
                "htmlOptions" => array(
                    "class" => "form-control",
                    "onchange" => "filtrocierrecomision()"
                ),
                'options' => array(
                    'placeholder' => 'Cierre',
                    'width' => '100%',
                ))
            );
            ?>
            <br >
        </div>
        <div class="clearfix"></div>
        <div class="divcomisiones col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 750px; overflow: auto; font-size: 7pt">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center">ID <strong>CONTACTO</strong></th>
                        <th style="text-align:center">RAZON SOCIAL <strong>CONTACTO</strong></th>
                        <th style="text-align:center">ASESOR <strong>CLIENTE</strong></th>
                        <th style="text-align:center">AGENCIA <strong> CLIENTE </strong></th>
                        <th style="text-align:center">RAZON SOCIAL <strong>CLIENTE</strong></th>
                        <th style="text-align:center">CONTRATO <strong> CLIENTE</strong></th>
                        <th style="text-align:center">CODIGO <strong> COTIZACION </strong></th>
                        <th style="text-align:center">SERVICIO <strong> CLIENTE VENTA </strong></th>
                        <th style="text-align:center">FECHA CREACION <strong>CONTACTO</strong></th>
                        <th style="text-align:center">FECHA ULT GESTION <strong>CONTACTO</strong></th>
                        <th style="text-align:center">FECHA CREACION <strong> CLIENTE</strong></th>
                        <th style="text-align:center">ESTADO <strong>ESTADO PROCESO</strong></th>
                        <th style="text-align:center">VALOR SERVICIO <strong> </strong></th>
                        <th style="text-align:center">VALOR TELEMERCADEO <strong> </strong></th>
                        <th style="text-align:center">PAGO <strong> </strong></th>
                    </tr>
                </thead>    
                <tbody>
                    <?php
                    foreach ($telemark as $key) :
                        $count = HistoricoGestion::model()->totalRegistrado($key['IDContacto']);
                        if (sizeof($count) > 2) :
                        ?>   
                        <tr>
                            <td><?= $key['IDContacto'] ?></td>
                            <td><?= $key['RazonSocialContacto'] ?></td>
                            <td><?= $key['AsesorCliente'] ?></td>
                            <td><?= $key['AgenciaCliente'] ?></td>
                            <td><?= $key['RazonSocialCliente'] ?></td>
                            <td style="text-align:center"><?= $key['ContratoCliente'] ?></td>
                            <td style="text-align:center"><?= $key['IDCotizacion'] ?></td>
                            <td style="text-align:center"><?= $key['ServicioClienteVenta'] ?></td>
                            <td style="text-align:center"><?= $key['FechaCreacionContacto'] ?></td>
                            <td style="text-align:center"><?= $key['FechaUltGestionContacto'] ?></td>
                            <td style="text-align:center"><?= $key['FechaCreacionClienteVenta'] ?></td>
                            <td><?= $key['EstadoDetalle'] ?></td>
                            <td style="text-align:right">$<?= number_format($key['ValorServicioDetalle']) ?></td>
                            <td style="text-align:right">$0</td>
                            <td>NULL</td>
                        </tr>    
                        <?php
                        endif;
                    endforeach;
                    ?>        
                </tbody>            
            </table>    
        </div>
    </div>
</div>