<?php
if (count($data) > 0) :
    ?>
    <div class="col-lg-5 col-lg-offset-5">
        <?php
        $this->widget('booster.widgets.TbMenu', array(
            'items' => array(
                array(
                    'label' => 'DESCARGAR <img src="' . Yii::app()->request->baseUrl . '/images/excel.png" style = "width:30px">',
                    'url' => array('exportventas?idcierre=' . $idcierre),
                ),
            ),
            'encodeLabel' => false,
        ));
        ?>
    </div>
    <table border="1" style="font-size:12px;font-family:tahoma,arial;color:black" cellspacing="0" bordercolor="black">
        <tr>
            <td style="text-align:center"><strong>ID</strong></td>
            <td style="text-align:center"><strong>REGIONAL</strong></td>
            <td style="text-align:center"><strong>ASESOR</strong></td>
            <td style="text-align:center"><strong>CONTRATO</strong></td>
            <td style="text-align:center"><strong>RAZON SOCIAL</strong></td>
            <td style="text-align:center"><strong>COMERCIAL</strong></td>
            <td style="text-align:center"><strong>COMENTARIOS</strong></td>
            <td style="text-align:center"><strong>ESTADO PROCESO</strong></td>
            <td style="text-align:center"><strong>FECHA CREACION</strong></td>
            <td style="text-align:center"><strong>PAGO </strong></td>
            <td style="text-align:center"><strong>FECHA CAMBIO</strong></td>
            <td style="text-align:center"><strong>SERVICIO</strong></td>
        </tr>
        <?php
        foreach ($data as $p) :
            foreach ($p['citas'] as $t => $f) :
                ?>
                <tr>
                    <td style="text-align:center"><?= $f['id'] ?> </td>
                    <td style="text-align:center"><?= $f['regional'] ?> </td>
                    <td style="text-align:center"><?= $f['nombre'] ?> </td>
                    <td style="text-align:center"><?= $f['contrato'] ?> </td>
                    <td style="text-align:center"><?= $f['razon_social'] ?> </td>
                    <td style="text-align:center"><?= $f['comercial'] ?> </td>
                    <td style="text-align:center"><?= $f['comentarios'] ?> </td>
                    <td style="text-align:center"><?= $f['estado'] ?> </td>
                    <td style="text-align:center"><?= $f['fecha_creacion'] ?> </td>
                    <td style="text-align:center"><strong>EN VALIDACION</strong></td>
                    <td style="text-align:center"><?= $f['fecha_modificacion'] ?></td>
                    <td style="text-align:center"><?= $f['servicio'] ?></td>
                </tr>    
                <?php
            endforeach;
        endforeach;
        ?>
    </table>
    <?php
else :
    echo "No se encontraron Registros";
endif;
?>