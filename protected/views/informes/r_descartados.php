<?php
$totalr = 0;
foreach ($ttlvengrades as $t) :
    $totalr += $t->Total;
endforeach;

if ($totalr == 0) :
    $totalr = 1;
endif;

$b = 0;
$barLine = array();
foreach ($ttlvengrades as $t) :
    $servicios = Servicios::model()->findByPk($t->CodigoServicioDetalle);
    $barLine[$b] = array('name' => $servicios->Nombre, 'y' => round((($t->Total / $totalr) * 100), 1), 'drilldown' => $t->CodigoServicioDetalle);
    $b++;
endforeach;

$criteria = new CDbCriteria();
$criteria->addCondition('Tipo = 1');
$criteria->order = 'ID ASC';
$servicios = Servicios::model()->findAll($criteria);

$ultcli = 0;
$arr_view = array();
$totalven = 0;

foreach ($ttlvenregdes as $r) :
    $regional = Regional::model()->findByPk($r->IDRegionalCliente);
    $agencia = Agencia::model()->findByPk($r->AgenciaCliente);
    
    if ($regional != NULL) :
        $arr_view[$r->AgenciaCliente]['Regional'] = $regional->Descripcion;
    else :
        $arr_view[$r->AgenciaCliente]['Regional'] = "SIN REGIONAL";
    endif;
    

    if ($agencia != NULL) :
        $arr_view[$r->AgenciaCliente]['Agencia'] = strtoupper($agencia->Nombre);
    else :
        $arr_view[$r->AgenciaCliente]['Agencia'] = 'SIN AGENCIA';
    endif;

    if ($r->AgenciaCliente == $ultcli) :
        foreach ($servicios as $s) :
            if ($s->ID == $r->CodigoServicioDetalle) :
                $arr_view[$r->AgenciaCliente][$s->ID] += $r->Total;
                $totalven += $r->Total;
            endif;
        endforeach;
    else :
        foreach ($servicios as $s) :
            if ($s->ID == $r->CodigoServicioDetalle) :
                $arr_view[$r->AgenciaCliente][$s->ID] = $r->Total;
                $totalven = $r->Total;
            else :
                $arr_view[$r->AgenciaCliente][$s->ID] = 0;
            endif;
        endforeach;
    endif;

    $arr_view[$r->AgenciaCliente]['Total'] = $totalven;
    $ultcli = $r->AgenciaCliente;
endforeach;

$totalgeneral = 0;
$total = array();
?>
<div class="row">  
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>MOVIMIENTO AGENCIAS <small>Actividad General</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="penvesagns">
                <table class="table editabv">
                    <thead>
                        <tr>
                            <th>REGIONAL</th>
                            <th>AGENCIA</th>
                            <?php foreach ($servicios as $s) : ?>
                                <th><?= strtoupper($s->Nombre) ?></th>
                            <?php endforeach; ?>
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($arr_view as $ar => $key) : ?>   
                            <tr>
                                <td><?= strtoupper($key['Regional']) ?></td>
                                <td><?= strtoupper($key['Agencia']) ?></td>
                                <?php 
                                    
                                    foreach ($servicios as $s) : ?>
                                        <td>$<?= number_format($key[$s->ID]) ?></td>
                                        <?php
                                        $total[$s->ID][] = $key[$s->ID];                                    
                                    endforeach; ?>
                                <td>$<?= number_format($key['Total']) ?></td>
                            </tr>
                            <?php
                            $totalgeneral += $key['Total'];
                        endforeach;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td style="text-align: center">TOTALES : </td>
                            <?php 
                                foreach ($servicios as $s) :
                                    if (isset($total[$s->ID])) : ?>
                                        <td>$<?= number_format(array_sum($total[$s->ID])) ?></td>
                                        <?php
                                    endif;
                                endforeach;
                            ?>
                            <td>$<?= number_format($totalgeneral) ?></td>
                        </tr>
                    </tfoot>
                </table> 
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" id="Ventasagnd"></div>
            <div class="clearfix"></div>
            <div class="col-lg-10 col-md-10 col-xs-12 col-sm-12" id="container_d" style="height: 300px;"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#container_d').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Descartados'
            },
            subtitle: {
                text: 'Servicios'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total Registrado'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> del Total<br/>'
            },
            series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: <?= json_encode($barLine) ?>
                }]
        });
    });
</script>