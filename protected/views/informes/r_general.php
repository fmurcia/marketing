<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>REGISTROS GLOBALES <small>Actividad General</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <center><div id="container_piramide" style="height: 400px; margin: 0 auto"></div></center>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#container_piramide').highcharts({
            chart: {
                type: 'pyramid',
                marginRight: 100
            },
            title: {
                text: 'Comportamiento',
                x: -50
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y:,.0f})',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                    name: 'Unique users',
                    data: [
//                        ['Contactos', 15654],
//                        ['Cotizaciones', 4064],
                        ['PreVenta', 1987],
                        ['Pendientes Venta', 976],
                        ['Venta', 846]
                    ]
                }]
        });
    });
</script>