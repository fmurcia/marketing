<?php
$b = 0;
$totalr = 0;
$ultagen = '';
$arr_view = array();

foreach ($ttlpengrac as $t) :
    $totalr += $t->Total;
endforeach;

if ($totalr == 0) :
    $totalr = 1;
endif;

$barLine = array();
foreach ($ttlpengrac as $t) :
    $servicios = Servicios::model()->findByPk($t->CodigoServicioDetalle);
    $barLine[$b] = array('name' => $servicios->Nombre, 'y' => round((($t->Total / $totalr) * 100), 1), 'drilldown' => $t->CodigoServicioDetalle);
    $b++;
endforeach;

$servicios = Servicios::model()->findAll('Tipo = 1');

foreach ($ttlpenregc as $r) :
    $regional = Regional::model()->findByPk($r->IDRegionalCliente);
    $agencia = Agencia::model()->findByPk($r->AgenciaCliente);
    if ($regional != NULL) :
        $arr_view[$r->AgenciaCliente]['Regional'] = $regional->Descripcion;
    else :
        $arr_view[$r->AgenciaCliente]['Regional'] = "SIN REGIONAL";
    endif;
    if ($r->AgenciaCliente == $ultagen) :
        foreach ($servicios as $s) :
            if ($s->Nombre == $r->CodigoServicioDetalle) :
                $arr_view[$r->AgenciaCliente][$s->Nombre] = number_format($r->Total);
                $arr_view[$r->AgenciaCliente]['Total'] += $r->Total;
            endif;
        endforeach;
    else :
        if ($agencia != NULL) :
            $arr_view[$r->AgenciaCliente]['Agencia'] = strtoupper($agencia->Nombre);
        else :
            $arr_view[$r->AgenciaCliente]['Agencia'] = 'SIN AGENCIA';
        endif;
        foreach ($servicios as $s) :
            if ($s->Nombre == $r->CodigoServicioDetalle) :
                $arr_view[$r->AgenciaCliente][$s->Nombre] = number_format($r->Total);
            else :
                $arr_view[$r->AgenciaCliente][$s->Nombre] = 0;
            endif;
            $arr_view[$r->AgenciaCliente]['Total'] = $r->Total;
        endforeach;
    endif;
    $ultagen = $r->AgenciaCliente;
endforeach;
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>PENDIENTES COMERCIAL <small>Actividad General</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>MOVIMIENTO AGENCIAS <small>Actividad General</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="penvesagns">
                            <table class="citastb">
                                <tr>
                                    <td>
                                        <div class="fixedTable">
                                            <header class="fixedTable-header">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr style="text-align:center;">
                                                            <th width="10%" style="text-align:center;">REGIONAL</th>
                                                            <th width="20%" style="text-align:center;">AGENCIA</th>
                                                            <?php foreach ($servicios as $s) : ?>
                                                                <th width="10%" style="text-align:center;"><?= strtoupper($s->Nombre) ?></th>
                                                            <?php endforeach; ?>
                                                            <th width="10%" style="text-align:center;">TOTAL</th>
                                                            <th style="text-align:center;width: 2%"><span class="fa fa-exchange"></span></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </header>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="fixedTable-body">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <?php foreach ($arr_view as $ar) : ?>
                                                    <tr>
                                                        <td width="10%" style="text-align:center;"><?= strtoupper($ar['Regional']) ?></td>
                                                        <td width="20%"><?= strtoupper($ar['Agencia']) ?></td>
                                                        <td width="10%" style="text-align:center;">$<?= $ar['Monitoreo'] ?></td>
                                                        <td width="10%" style="text-align:center;">$<?= $ar['Rastreo'] ?></td>
                                                        <td width="10%" style="text-align:center;">$<?= $ar['Televideo'] ?></td>
                                                        <td width="10%" style="text-align:center;">$<?= number_format($ar['Total']) ?></td>
                                                        <td style="text-align:center; width: 2%"></td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>  
                            </table>    
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" id="PreVentasagnd"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="container_penc" style="height: 400px; auto"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#container_penc').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Pendientes Comercial'
            },
            subtitle: {
                text: 'Servicios'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total Registrado'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: <?= json_encode($barLine) ?>
                }]
        });
    });
</script>