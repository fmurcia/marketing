<div class="x_title">
    <h2>Estadistico Detallado</h2>
    <div class="clearfix"></div>
</div>
<?php
$this->widget(
        'booster.widgets.TbWizard', 
        array(
            'type' => 'tabs',
            'tabs' => 
            array
            (
                array('label' => 'General','content' => $this->renderPartial('r_comercial', array(), true), 'active' => true),
                array('label' => 'Ventas', 'content' => $this->renderPartial('r_ventas', array(), true)),
                array('label' => 'Instalaciones', 'content' => $this->renderPartial('r_instalaciones', array(), true)),
                array('label' => 'Facturacion', 'content' => $this->renderPartial('r_facturacion', array(), true))
            )
        )
);
