<input type="hidden" id="tabla" value="<?= $informe ?>">
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Exporte de Contactos</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="border: 1px solid #e5e5e5; height: 700px; ">
                <p>Selecciona el Rango de Fechas a Generar</p>
                <div class="row">
                    <div class="col-md-6">
                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 20px; border: 1px solid #ccc">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span></span><b class="caret"></b>
                        </div>
                        <input type="hidden" id="rango_fecha1" value="<?= date('Y-m-d') ?>"/>
                        <input type="hidden" id="rango_fecha2" value="<?= date('Y-m-d') ?>"/>
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-app" onclick="cleartime()">
                            <i class="fa fa-close"></i> Cancelar
                        </a>
                        <a class="btn btn-app" onclick="generarExporteContactos(); /*alert('Error FATAL! Ver el Telelego Por Favor')*/">
                            <i class="fa fa-check"></i> Generar
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-lg-12" id="tableexport" style="height: 400px;overflow: auto"></div>
                </div> 
            </div>
        </div>
    </div>
</div>