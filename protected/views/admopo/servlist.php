<div class="col-lg-12">
    <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
        <h5><b>Servicio</b></h5>
    </div>
    <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
        <h5><b>¿Esta interesado?</b></h5>
    </div>
    <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
        <h5><b>¿Tiene equipos?</b></h5>
    </div>
    <div class="clearfix"></div>
    <?php
    $servicios = $model->getServicios();

    if (count($servicios) > 0) :
        $cont = 0;
        foreach ($servicios as $servicio) :
            echo CHtml::hiddenField("Opciones[" . $cont . "][Servicio]", $servicio->ID)
            ?>
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                <?php echo $servicio->Nombre; ?>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                <?php
                $this->widget(
                        'booster.widgets.TbSwitch', array(
                    'name' => "Opciones[" . $cont . "][Estado]",
                    'value' => $model->getOpcion($servicio->ID, "Estado"),
                    'options' => array(
                        "onText" => "Si",
                        "offText" => "No",
                        "wrapperClass" => "wrapper swajuste",
                        "handleWidth" => "auto",
                    ),
                    "events" => array(
                        'switchChange' => "js:function(e,data){
                        if(data==true){
                        $('#OpcEq" . $cont . "').bootstrapSwitch('disabled',false);
                            upchangecampo('ServicioInteresOp', 'ID_Servicio', '$servicio->ID-1', '$model->ID')
                            }
                            else{
                            $('#OpcEq" . $cont . "').bootstrapSwitch('state',false);
                            $('#OpcEq" . $cont . "').bootstrapSwitch('disabled',true);
                                upchangecampo('ServicioInteresOp', 'ID_Servicio', '$servicio->ID-0', '$model->ID')
                            }
                        return true;
                            }",
                    ),
                    "htmlOptions" => array(
                        "style" => "width:100%"
                    ))
                );
                ?>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                <?php
                $this->widget(
                        'booster.widgets.TbSwitch', array(
                    'name' => "Opciones[" . $cont . "][Equipos]",
                    'value' => $model->getOpcion($servicio->ID, "Equipos"),
                    'events' => array(
                        'switchChange' => "js:function(e,data){
                            if(data==true){
                                upchangecampo('ServicioInteres', 'Equipos', '$servicio->ID-1', '$model->ID')
                            }
                            else{
                                upchangecampo('ServicioInteres', 'Equipos', '$servicio->ID-0', '$model->ID')
                            }
                        }",
                    ),
                    'options' => array(
                        "onText" => "Si",
                        "offText" => "No",
                        "wrapperClass" => "wrapper swajuste",
                        "handleWidth" => "auto",
                        "disabled" => ($model->getOpcion($servicio->ID, "Estado") == true ? false : true),
                    ),
                    "htmlOptions" => array(
                        "style" => "width:100%",
                        "id" => "OpcEq" . $cont,
                    ))
                );
                ?>
            </div>
            <div class="clearfix"></div>
            <?php
            $cont++;
        endforeach;
    endif;
    ?>
</div>
<div class="clearfix"></div>