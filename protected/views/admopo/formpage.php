<div class="moduladorpag">
    <?php
    $contactoestado = OportunidadEstado::model()->getEstadoProceso($model->ID);

    if ($contactoestado == NULL) :
        $class_seguimiento = 'off';
        $class_descartado = 'off';
        $class_interesado = 'on';
    else :
        
        $ultest = $contactoestado->ID_Estadoproceso;
        $descartados = array(19, 29, 16, 17, 18);
        $seguimientos = array(12, 16, 20, 21, 22);
        
        if (in_array($ultest, $descartados)) :
            $class_seguimiento = 'off';
            $class_descartado = 'on';
            $class_interesado = 'off';

        elseif (in_array($ultest, $seguimientos)) :
            $class_seguimiento = 'on';
            $class_interesado = 'on';
            $class_descartado = 'off';
        else :
            $class_seguimiento = 'off';
            $class_descartado = 'off';
            $class_interesado = 'on';
        endif;
        
    endif;

    if (isset($model->Tipo_contacto)) :
        if ($model->Tipo_contacto == 1) :
            $titulo = "LLAMADA DE ENTRADA";
            $icono = "fa fa-phone fa-2x";
        elseif ($model->Tipo_contacto == 2) :
            $titulo = "BASE FRIA";
            $icono = "fa fa-folder-open fa-2x";
        elseif ($model->Tipo_contacto == 3) :
            $titulo = "CAMPAÑA WEB";
            $icono = "fa fa-globe fa-2x";
        elseif ($model->Tipo_contacto == 4) :
            $titulo = "CLIENTE ACTIVO";
            $icono = "fa fa-user fa-2x";
        elseif ($model->Tipo_contacto == 5) :
            $titulo = "CLIENTE CHAT";
            $icono = "fa fa-wechat fa-2x";
        else :
            $titulo = "CONTACTO DESCARTADO";
            $icono = "fa fa-warning fa-2x";
        endif;
        $model->Tipo_contacto = TipoContacto::model()->findByPk($model->Tipo_contacto);
        $contexto = $model->Tipo_contacto->Contexto;
        ?>
        <input type="hidden" name="idestweb" id="idestweb" value="" />
        <input name="idoportunidad" id="idoportunidad" type="hidden" value="<?= $model->ID ?>" />
        <div class="col-lg-2 col-md-2 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contexto ?>">
                <div class="panel-heading">
                    <span class="<?= $icono ?>"></span>
                    <?= $titulo ?> 
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contexto ?>">
                <div class="panel-heading">
                    <span class="fa fa-map-pin fa-2x"></span>
                    ID <?= $model->ID ?>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contexto ?>">
                <div class="panel-heading">
                    <span class="fa fa-commenting fa-2x"></span>
                    <?= strtoupper($model->estadoProceso->Nombre) ?>
                </div>
            </div>
        </div>
        
        <div class="col-lg-2 col-md-2 col-sx-12 col-xs-12 panel-danger">
            <div class="col-lg-5 col-md-5 col-sx-12 col-xs-12 panel-heading" style="text-align:right; font-weight: bold">
                <span class="fa fa-book fa-2x"></span>
                PAGINA</div>
            <div class="col-lg-7 col-md-7 col-sx-12 col-xs-12">
                <select id="ctopages" class="form-control" onchange="paginaCta(this.value, <?= $block ?>)">
                    <?php for ($i = 1; $i < $pages; $i++) : ?>
                        <option <?= ($pag == $i) ? 'Selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        
        <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
            <?php
            $this->widget(
                    'booster.widgets.TbMenu', array(
                'type' => 'navbar',
                'items' => array(
                    array('label' => 'Marketing', 'url' => '#', 'icon' => 'fa fa-edit fa-2x', 'itemOptions' => array('onclick' => 'marketing()')),
                    array('label' => 'Transferencia', 'url' => '#', 'icon' => 'fa fa-refresh fa-2x', 'itemOptions' => array('onclick' => 'transferencia()')),
                    array('label' => 'Coincidencias!!!', 'url' => '#', 'icon' => 'fa fa-cubes fa-2x', 'itemOptions' => array('onclick' => 'searchCoincidencias()')),
                ),
                    )
            );
            ?>
        </div>

        <div class="clearfix"></div>

        <div class="marketing col-lg-12 col-md-12 col-sx-12 col-xs-12" style="display:none">
            <div class="row">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal" onclick="$('.marketing').fadeOut('fast');">&times;</a>
                    <h4>MAILCHIMP</h4>
                </div>
                <div class="modal-body">
                    <div class="cotelenav">
                        <span class="arrow-left <?= $class_descartado ?>"></span>
                        <span class="cubed <?= $class_descartado ?>" onclick="mailchimp(1, '<?= $model->ID ?>', 'INVALIDO');"><span class="fa fa-map-pin"></span> INVALIDO</span>
                        <span class="crop-left <?= $class_descartado ?>"></span>

                        <span class="arrow-left <?= $class_descartado ?>"></span>
                        <span class="cubed <?= $class_descartado ?>" onclick="mailchimp(2, '<?= $model->ID ?>', 'DESCARTADO');"><span class="fa fa-bug"></span> DESCARTADO</span>
                        <span class="crop-left <?= $class_descartado ?>"></span>

                        <span class="cubed-space"></span>

                        <span class="crop-right <?= $class_interesado ?>"></span>
                        <span class="cube <?= $class_interesado ?>" onclick="mailchimp(3, '<?= $model->ID ?>', 'INTERESADO');"><span class="fa fa-user"></span> INTERESADO</span>
                        <span class="arrow-right <?= $class_interesado ?>"></span>

                        <span class="crop-right <?= $class_seguimiento ?>"></span>
                        <span class="cube <?= $class_seguimiento ?>" onclick="mailchimp(4, '<?= $model->ID ?>', 'SEGUIMIENTO');"><span class="fa fa-signal"></span> SEGUIMIENTO</span>
                        <span class="arrow-right <?= $class_seguimiento ?>"></span>
                    </div>
                    <br />
                    <div class="divocl col-lg-8 col-lg-offset-2 col-md-7 col-md-offset-2 col-sx-12 col-xs-12"></div>
                </div>
                <div class="gestionador col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sx-12 col-xs-12" style="display:none">
                    <div class="modal-footer">
                        <textarea id="comentarios" class="form-control" placeholder="Escriba sus comentarios"></textarea>
                        <br />
                        <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                            <?php
                            $this->widget(
                                    'booster.widgets.TbButton', array(
                                'label' => 'Enviar',
                                'context' => 'success',
                                'htmlOptions' => array(
                                    "class" => "pull-right",
                                    'onclick' => "guardarGestion()"
                                )
                                    )
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="volverallamar col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12" style="display:none">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" onclick="$('.volverallamar').fadeOut('fast');">&times;</a>
                        <h4>VOLVER A LLAMAR</h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <div class="input-group">
                                <br>
                                <input id="fecha_agendada" style="text-align: center;" name="fecha_agendada" value="" class="form-control" readonly="true"  type="hidden"/>
                                <span class="btn input-group-addon" style="cursor:pointer"  id="calendarag">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </span>
                            </div>
                        </center>
                        <?php
                        $script = '$("#calendarag").datetimepicker({
                                    inline: true,
                                    timepicker: true,
                                    format: "Y-m-d H:i:s",
                                    onChangeDateTime: function (dp, $input) {
                                        $("#fecha_agendada").val($input.val());
                                    }
                                })';
                        Yii::app()->clientScript->registerScript(uniqid(), $script);
                        ?>
                        <textarea name="comentarios_agenda" id="comentarios_agenda" class="form-control" placeholder="Escriba sus comentarios"></textarea>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                            <?php
                            $this->widget(
                                    'booster.widgets.TbButton', array(
                                'label' => 'Agendar',
                                'context' => 'success',
                                'htmlOptions' => array(
                                    "class" => "pull-right",
                                    'onclick' => "guardarGestion()"
                                )
                                    )
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class=" asignacion col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12" style="display:none">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" onclick="$('.asignacion').fadeOut('fast');">&times;</a>
                        <h4>CITA CONTACTO</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $form = $this->beginWidget(
                                'booster.widgets.TbActiveForm', array(
                            'id' => 'enviarcomercial',
                            'type' => 'vertical',
                            'action' => "javascript:void(0)",
                                )
                        );
                        ?>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">FECHA Y HORA </div>
                        <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                            <center>
                                <div class="input-group">
                                    <br>
                                    <input id="fecha_asignada" style="text-align: center;" name="fecha_asignada" value="" class="form-control" readonly="true"  type="hidden"/>
                                    <span class="btn input-group-addon" style="cursor:pointer"  id="calendarco">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </center>
                            <?php
                            $scriptcal = '$("#calendarco").datetimepicker({
                                inline: true,
                                timepicker: true,
                                format: "Y-m-d H:i:s",
                                onChangeDateTime: function (dp, $input) {
                                    $("#fecha_asignada").val($input.val());
                                }
                            })';
                            Yii::app()->clientScript->registerScript(uniqid(), $scriptcal);
                            ?>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">OBSERVACIONES </div>
                        <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                            <textarea id="comentarios_cita" name="comentarios_cita" class="form-control"></textarea>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3 col-md-9 col-sx-12 col-xs-12">
                        <?php
                        $this->widget('booster.widgets.TbButton', array('label' => 'Asignar', 'htmlOptions' => array('class' => 'btn btn-success', 'onclick' => "convertirCita()")));
                        ?>
                    </div>
                </div>
                <div class="reagendamiento col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12" style="display:none">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" onclick="$('.asignacion').fadeOut('fast');">&times;</a>
                        <h4>REAGENDAMIENTO</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $form = $this->beginWidget(
                                'booster.widgets.TbActiveForm', array(
                            'id' => 'enviarcomercial',
                            'type' => 'vertical',
                            'action' => "javascript:void(0)",
                                )
                        );
                        ?>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">FECHA Y HORA </div>
                        <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                            <center>
                                <div class="input-group">
                                    <br>
                                    <input id="fecha_agendada" style="text-align: center;" name="fecha_agendada" value="" class="form-control" readonly="true"  type="hidden"/>
                                    <span class="btn input-group-addon" style="cursor:pointer"  id="calendarcor">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </center>
                            <?php
                            $scriptcal = '$("#calendarcor").datetimepicker({
                                inline: true,
                                timepicker: true,
                                format: "Y-m-d H:i:s",
                                onChangeDateTime: function (dp, $input) {
                                    $("#fecha_agendada").val($input.val());
                                }
                            })';
                            Yii::app()->clientScript->registerScript(uniqid(), $scriptcal);
                            ?>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">OBSERVACIONES </div>
                        <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                            <textarea id="comentarios_agenda" name="comentarios_agenda" class="form-control"></textarea>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                    <div class="modal-footer col-lg-9 col-lg-offset-3 col-sx-12 col-xs-12">
                        <?php
                        $this->widget('booster.widgets.TbButton', array('label' => 'Asignar', 'htmlOptions' => array('class' => 'btn btn-success', 'onclick' => "guardarGestion()")));
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="transferencia"  style="display:none">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12 ">
                <div class="row">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" onclick="$('.transferencia').fadeOut('fast');">&times;</a>
                        <h4>TRANSFERENCIA</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $formu = $this->beginWidget(
                                'booster.widgets.TbActiveForm', array(
                            'id' => 'formTransfer',
                            'enableAjaxValidation' => false,
                            'action' => "javascript:void(0)"
                        ));
                        ?>
                        <h4>Comercial</h4>
                        <?php
                        $this->widget(
                                'booster.widgets.TbSelect2', array(
                            'asDropDownList' => true,
                            'name' => 'comercial',
                            "data" => Asesor::model()->getTelemercaderistas(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "style" => "font-size: 9pt",
                                "placeholder" => '...Seleccione...',
                            ),
                            'options' => array(
                                'width' => '100%',
                                "style" => "width:100%",
                                'required' => true,
                            ))
                        );
                        ?>

                        <h4>Comentarios detallados</h4>
                        <textarea class="form-control" name="comentarios" required ></textarea>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                            <?php
                            $this->widget(
                                    'booster.widgets.TbButton', array(
                                'label' => 'Enviar',
                                'context' => 'success',
                                'htmlOptions' => array(
                                    "class" => "pull-right",
                                    'onclick' => "enviarContacto()"
                                )
                                    )
                            );
                            ?>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <div class = "clearfix"></div>
        <div class = "col-lg-12 col-md-12 col-sx-12 col-xs-12" id = "paginadorcot">
            <?php $this->renderPartial('modulo', array('model' => $model));
            ?>
        </div>
        <?php
    endif;
    ?>
</div>