<?php
$model = HistoricoGestionOp::model()->totalRegistrado($idmodel);
?>
<table class="table table-striped">
    <th>ID</th>
    <th>Seguimiento</th>
    <th>Asesor</th>
    <th>Accion</th>
    <th>Fecha</th>
    <th>Hora</th>
    <th>Observacion</th>
<?php
foreach ($model as $data) :
    $accionweb = EstadosWeb::model()->findByPk($data->ID_Accion);
        if($accionweb->Hijo != 0) :
            $est = EstadosWeb::model()->findByPk($accionweb->Hijo);
            $seg = $est->Descripcion;
        else :
            $seg = $accionweb->Descripcion;
        endif;
    ?>
        <tr>
            <td><?= $data->ID_Contacto ?></td>
            <td><?= $data->iDSeguimiento->Nombre ?></td>
            <td><?= $data->iDAsesor->Nombre ?></td>
            <td><?= strtoupper($accionweb->Descripcion) ?></td>
            <td><?= $data->Fecha ?></td>
            <td><?= $data->Hora ?></td>
            <td><?= $data->Memo ?></td>
        </tr>    
    <?php
endforeach;
?>
</table>