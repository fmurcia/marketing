<div class="form">
    <?php
    /* @var $this ContactoController */
    /* @var $model Contacto */
    /* @var $form CActiveForm */

    $form = $this->beginWidget(
            'booster.widgets.TbActiveForm', array(
        'id' => 'oportunidad-form',
        'type' => 'vertical',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'action' => 'javascript:void(0)'
            )
    );
    ?>
    <fieldset>
        <input type="hidden" id="idoportunidad" value="<?php echo $model->ID ?>">
        <div class="col-lg-6 col-md-6 col-xs-12 col-sx-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <?php
                    if ($model->Asesor == 1 || $model->Asesor == 60) :
                        $asesor = strtoupper("SIN ASESOR");
                    else :
                        $asesor = strtoupper($model->asesor->Nombre);
                    endif;
                    ?>
                    Asesor Comercial : <?= $asesor ?>
                </div>
            </div>
        </div>    

        <div class="col-lg-6 col-md-6 col-xs-12 col-sx-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    Ult. Comentario : <?= strtolower($model->Observaciones) ?>
                </div>
            </div>
        </div>    

        <div class="clearfix"></div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space">
            <div class="well">
                <h4>Informacion basica</h4>

                <div class="col-lg-6 col-md-6 col-xs-12 col-sx-12">
                    <?php echo $form->textFieldGroup($model, 'Razon_social', array('widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Oportunidad', 'Razon_social', this.value, $model->ID)")))); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12 col-sx-12">
                    <?php echo $form->textFieldGroup($model, 'Nombre_completo', array('widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Oportunidad', 'Nombre_completo', this.value, $model->ID)")))); ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <?php
                    echo $form->select2Group($model, 'Ciudad', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getCiudades(),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque la ciudad',
                                'width' => '100%'
                            ),
                            'htmlOptions' => array(
                                "onchange" => "upchangecampo('Oportunidad', 'Ciudad', this.value, $model->ID)",
                                "class" => "form-control"
                            ),
                        )
                    ));
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <?php
                    echo $form->select2Group($model, 'Barrio', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getBarrios(),
                            'htmlOptions' => array(
                                "onchange" => "upchangecampo('Oportunidad', 'Barrio', this.value, $model->ID)",
                                "class" => "form-control"
                            ),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque el barrio',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <?=
                    $form->textFieldGroup($model, 'Direccion', array(
                        "widgetOptions" => array("htmlOptions" => array("class" => "col-lg-2", "readonly" => true, "onchange" => "upchangecampo('Oportunidad', 'Direccion', this.value, $model->ID)")),
                        "wrapperHtmlOptions" => array("class" => "col-lg-2"),
                        "append" => "<span class='input-group-btn'><button class='btn btn-default' onclick=\"direccion('Oportunidad_Direccion')\" /><span class='fa fa-refresh'></span> Dir </button></span>",
                        "appendOptions" => array("class" => "input-group-btn","isRaw" => true)));
                    ?>
                </div>

                <div class="clearfix"></div>
                <div class="col-lg-4 col-md-6 col-sx-12 col-xs-12">
                    <?php echo $form->textFieldGroup($model, 'Telefono', array('widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Oportunidad', 'Telefono', this.value, $model->ID)")))); ?>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12 col-sx-12">
                    <?php echo $form->textFieldGroup($model, 'Celular', array('widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Oportunidad', 'Celular', this.value, $model->ID)")))); ?>
                </div>
                <div class="col-lg-4 col-md-6 col-sx-12 col-xs-12">
                    <?php echo $form->textFieldGroup($model, 'Email', array('widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Oportunidad', 'Email', this.value, $model->ID)")))); ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sx-12 col-xs-12 space">
            <div class="well">
                <h4>Encuesta de interes</h4>
                <?php echo $this->renderPartial('servlist', array("model" => $model)) ?>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sx-12 col-xs-12 space">
            <div class="well">
                <h4>Informacion adicional</h4>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <?php echo $form->textFieldGroup($model, 'Nit', array('widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Oportunidad', 'Nit', this.value, $model->ID)")))); ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <?php
                    echo $form->select2Group($model, 'Actividad', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getActividades(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "onchange" => "upchangecampo('Oportunidad', 'Actividad', this.value, $model->ID)",
                            ),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque la actividad',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <?php
                    echo $form->select2Group($model, 'Tipo_cliente', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getTipoCliente(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "onchange" => "upchangecampo('Oportunidad', 'Tipo_cliente', this.value, $model->ID)",
                            ),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque la actividad',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </fieldset>
    <?php $this->endWidget(); ?>
</div><!-- form -->