<?php
$contactoestado = OportunidadEstado::model()->getEstadoWeb($idoportunidad);

$arr_col = array();
$i = 0;
foreach ($contactoestado as $ce) :
    $arr_col[$i] = $ce->ID_Estadoweb;
    $i++;
endforeach;

$class_int = (in_array(1, $arr_col)) ? 'green' : 'danger'; //Interesado
$class_rep = (in_array(2, $arr_col)) ? 'green' : 'danger'; //Repetido
$class_pqr = (in_array(3, $arr_col)) ? 'green' : 'danger'; //Pqr
$class_tra = (in_array(4, $arr_col)) ? 'green' : 'danger'; //Trabajo
$class_seg = (in_array(5, $arr_col)) ? 'green' : 'danger'; //Seguimiento
$class_noc = (in_array(6, $arr_col)) ? 'green' : 'danger'; //No Contesta
$class_err = (in_array(7, $arr_col)) ? 'green' : 'danger'; //Errado
$class_cit = (in_array(8, $arr_col)) ? 'green' : 'danger'; //Cita
$class_cot = (in_array(9, $arr_col)) ? 'green' : 'danger'; //Cotizacion
$class_co1 = (in_array(10, $arr_col)) ? 'green' : 'danger'; //Cotizacion-1
$class_co2 = (in_array(11, $arr_col)) ? 'green' : 'danger'; //Cotizacion-2
$class_co3 = (in_array(12, $arr_col)) ? 'green' : 'danger'; //Cotizacion-3
$class_ven = (in_array(13, $arr_col)) ? 'green' : 'danger'; //Venta
$class_fac = (in_array(14, $arr_col)) ? 'green' : 'danger'; //Facturacion
$class_des = (in_array(15, $arr_col)) ? 'green' : 'danger'; //Descartado
$class_prs = (in_array(16, $arr_col)) ? 'green' : 'danger'; //Presupuesto
$class_com = (in_array(17, $arr_col)) ? 'green' : 'danger'; //Compentencia
$class_sli = (in_array(18, $arr_col)) ? 'green' : 'danger'; //SoloInfo
$class_rea = (in_array(19, $arr_col)) ? 'green' : 'danger'; //Reagendamiento
$class_ret = (in_array(20, $arr_col)) ? 'green' : 'danger'; //Retirado
$class_ase = (in_array(21, $arr_col)) ? 'green' : 'danger'; //Pendientes Asesor
$class_pnc = (in_array(22, $arr_col)) ? 'green' : 'danger'; //Pendientes Comercial
$class_pre = (in_array(23, $arr_col)) ? 'green' : 'danger'; //Preventa

?>
<br />
<table class="table table-responsive">
    <tr class="info">
        <th colspan="2" style="text-align: center; font-size: 15pt"><?= strtoupper($titulo) ?></th>
    </tr>
    <tr class="btn-primary">
        <th style="text-align: center">MAIL</th>
        <th style="text-align: center">ESTADO</th>
    </tr>
    <?php
    if ($op == 1) :
        ?>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk  <?= $class_pqr ?>"></span> Pqr</td>
            <td style="text-align:center; cursor: pointer" id="opc_3" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_pqr ?>"></span></td>
        </tr>    
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_tra ?>"></span> Trabajo</td>
            <td style="text-align:center; cursor: pointer" id="opc_4" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_tra ?>"></span></td>
        </tr>    
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_err ?>"></span> Repetido</td>
            <td style="text-align:center; cursor: pointer" id="opc_2" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_rep ?>"></span></td>
        </tr>    
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_err ?>"></span> Errado</td>
            <td style="text-align:center; cursor: pointer" id="opc_7" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_err ?>"></span></td>
        </tr>    
        
        <?php
    elseif ($op == 2) :
        ?>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_prs ?>"></span> Presupuesto</td>
            <td style="text-align:center; cursor: pointer" id="opc_16" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_prs ?>"></span></td>
        </tr>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_prs ?>"></span> Competencia</td>
            <td style="text-align:center; cursor: pointer" id="opc_17" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_com ?>"></span></td>
        </tr>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_prs ?>"></span> SoloInfo</td>
            <td style="text-align:center; cursor: pointer" id="opc_18" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_sli ?>"></span></td>
        </tr>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_prs ?>"></span> Retirados</td>
            <td style="text-align:center; cursor: pointer" id="opc_20" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_ret ?>"></span></td>
        </tr>
        <?php
    elseif ($op == 3) :
        ?>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk"></span> Mensaje de Bienvenida</td>
            <td style="text-align:center"><span class="fa fa-check fa-2x green"></span></td>
        </tr>    
        <?php
    elseif ($op == 4) :
        ?>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_rea ?>"></span> Volver a Llamar </td>
            <td style="text-align:center; cursor: pointer" id="opc_19" onclick='volverallamar(this);'><span class="fa fa-edit fa-2x <?= $class_rea ?>"></span></td>
        </tr>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_cit ?>"></span> Cita</td>
            <td style="text-align:center;cursor: pointer" onclick="comercial()"><span class="fa fa-edit fa-2x <?= $class_cit ?>"></span></td>
        </tr>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_cit ?>"></span> Reagendamiento</td>
            <td style="text-align:center;cursor: pointer" id="opc_19" onclick="reagendamiento(this)"><span class="fa fa-edit fa-2x <?= $class_cit ?>"></span></td>
        </tr>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_noc ?>"></span> No Contesta</td>
            <td style="text-align:center; cursor: pointer" id="opc_6" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_noc ?>"></span></td>
        </tr>
        <tr>
            <td style="font-size: 11pt"><span class="fa fa-asterisk <?= $class_noc ?>"></span> Confirmacion de datos</td>
            <td style="text-align:center; cursor: pointer" id="opc_5" onclick='gestionador(this)'><span class="fa fa-edit fa-2x <?= $class_seg ?>"></span></td>
        </tr>
    <?php
    endif;
    ?>
</table>