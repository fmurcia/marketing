<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#top1">Informacion Preliminar</a></li>
    <li><a data-toggle="tab" href="#top3">Historico Comentarios</a></li>
</ul>
<div class="tab-content">
    <div id="top1" class="tab-pane fade in active">
        <p><?php $this->renderPartial('upcontacto', array('model' => $model)) ?></p>
    </div>
    <div id="top3" class="tab-pane fade">
        <p><?php $this->renderPartial('historicocontacto', array('idmodel' => $model->ID)) ?></p>
    </div>
</div>