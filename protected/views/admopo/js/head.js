
var filtros = "";

function paginaCto(pag) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admopo/qrycontacto')?>",
        data: {
            pag: pag
        },
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success: function (data) {
            $('.ctospace').html(data);
        }
    });
}

function actfiltros(flt) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admopo/updfltr')?>",
        data: {
            fltr: flt
        },
        success: function (data) {
            if (data === "OK") {
                busquedaavanzada();
            }
        },
        error: function (err) {
            error(err.responseText);
        }
    });
    filtros = flt;
    return false;
}

function buscartexto() {
    actfiltros(filtros);
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admopo/srchtrm')?>",
        data: {
            term: $("#searchterm").val()
        },
        success: function (status) {
            if (status === "OK") {
                busquedaavanzada();
            }
        },
        error: function (err) {
            error(err.responseText);
        }
    });
    return false;
}

function busquedaavanzada() {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admopo/qrycontacto') ?>",
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success: function (data) {
            $('.ctospace').html(data);
        },
        error: function (err) {
            error(err.responseText);
        }
    });
}

function upcontact(idcontacto) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admopo/upcontact') ?>",
        data: "id=" + idcontacto,
        success: function (data) {
            console.log(data);
        }
    });
}



function gestionOportunidad(id)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("oportunidad/gestion")?>',
        'data': "idoportunidad=" + id,
        'success':
                function (respuesta) {
                    $('#modalGestion').modal('show');
                    $("#divgestion").hide();
                    $("#divgestion").html(respuesta);
                    $("#divgestion").fadeIn("fast");
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}


function asignarcomercial(id)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("oportunidad/comercial")?>',
        'data': "idoportunidad=" + id,
        'success':
                function (respuesta) {
                    $('#modalComercial').modal('show');
                    $("#divComercial").hide();
                    $("#divComercial").html(respuesta);
                    $("#divComercial").fadeIn("fast");
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function asignarAsesorOportunidad()
{
    var form = $("#enviarcomercialop").serialize();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/comercial")?>',
        'data': form + "&idoportunidad=" + $('#idoportunidad').val(),
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    $('.asignacion').fadeOut('fast');
                    if (respuesta == 'OK') {
                        swal("¡Hecho!", "Registro Actualizado", "success");

                    } else {
                        swal("¡Alto!", "El registro no se puede modificar", "error");
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function asignarContacto()
{
    if ($("#motivotransfer").val() != "")
    {
        jQuery.ajax({
            'type': 'POST',
            'url': '<?= Yii::app()->createUrl("oportunidad/enviarcontacto")?>',
            'data': "idcontacto=" + $('#idcontact').val() + '&comentario=' + $("#motivotransfer").val() + '&idasesor=' + $("#teletransfer").val(),
            'beforeSend': function () {
                $('.ctospace').html($('#modalwait').html());
            },
            'success':
                    function (respuesta) {
                        if (respuesta != 'Error') {
                            swal({
                                title: " Hecho! ",
                                text: " Enviado a " + respuesta,
                                timer: 1500,
                                showConfirmButton: false
                            });
                            $('#modalTransferencia').modal('hide');

                            window.location.href = "https://www.telesentinel.com/telemark/index.php/seguimientos/nuevos";
                        } else
                        {
                            swal({
                                title: " Alto! ",
                                text: " Todos los Campos son Obligatorios! ",
                                timer: 1500,
                                showConfirmButton: false
                            });
                        }
                    },
            'error': function (m, e, a) {
                swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
            },
            'cache': false
        });
        return false;
    } else {
        swal("¡Alto!", "Es Obligatorio el Comentario...", "error");
    }
}


function searchCoincidencias()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/coincidencias")?>',
        'data': 'idoportunidad=' + $('#idoportunidad').val(),
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success:
                function (respuesta) {
                    if (respuesta == 'OK') {
                        swal("¡Bien!", "Ninguna Coincidencia", "success");
                    } else {
                        swal({
                            title: "!Coincidencias!",
                            text: "<span style='text-align:justify; font-size: 9pt'>" + respuesta + "<span> ",
                            html: true
                        });
                    }
                },
        error: function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function upOption(cont, op, eq) {
    var idcontacto = $('#idcontact').val();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/upservlist")?>',
        'data': 'idcontacto=' + $('#idcontact').val() + '&idservicio=' + cont + '&valor=' + op + '&equipo=' + eq,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success:
                function (respuesta) {
                    console.log(respuesta);
                },
        error: function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function upchangecampo(modelo, campo, valor, id)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("admopo/upcontacto") ?>',
        'data': 'model=' + modelo + '&campo=' + campo + '&valor=' + valor + '&id=' + id,
        'success':
                function (respuesta) {
                    console.log(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Error de Actualizacion...", "error");
        },
        'cache': false
    });
    return false;
}


function mostrarNomenclatura()
{
    var destino = 'nomenclatura';
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("contacto/nomenclatura")?>',
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    $("#" + destino).hide();
                    $("#" + destino).html(respuesta);
                    $("#" + destino).fadeIn("fast");
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false,
    });
    return false;
}


function paginaCta(start, block) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admopo/pageslist') ?>",
        data: {
            start: start,
            block: block
        },
        beforeSend: function () {
            $('.moduladorpag').html($('#modalwait').html());
        },
        success: function (data) {
            $('.moduladorpag').html(data);
        }
    });
}


function inputdireccion(formulario, input)
{
    $("#modalAlerta").modal('show');
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/direccion")?>',
        'data': 'formulario=' + formulario + '&input=' + input,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    $("#modalcuerpo").html('\
                        <div class="modal-header">\n\
                            <a class="close" data-dismiss="modal">&times;</a>\n\
                            <h4>Direccion</h4>\n\
                        </div>\n\
                        <div class="modal-body"><p>' + respuesta + '</p></div>');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function direccion(input)
{
    inputdireccion('oportunidad-form', input);
}


function concatenaString(valorString2, campoSalida) {
// cancatena dos string y los deja en la variable campoSalida.
// valorString1:  parametro de entrada con l string a concatenar.
// valorString1:  parametro de entrada con 2 string a concatenar.
// campoSalida: parametro de entrada con el nombre de campo que contendrá el string concatenado.
    var campoStr = $("#armardir").val();
    salida = "";
    var j = 0;
    for (i = campoStr.length - 1; i > -1; i--) {
        var ca = campoStr.charAt(i);
        if (ca != ' ')
            j = 1;
        if (j == 1)
            salida = salida + ca;
    }
    campoStr = salida;
    salida = "";
    j = 0;
    for (i = 0; i < campoStr.length; i++) {
        var ca = campoStr.charAt(i);
        if (ca == ' ')
            j = 1;
        if (j != 1)
            salida = salida + ca;
    }
    campoStr = salida;
    salida = "";
    for (i = campoStr.length - 1; i > -1; i--) {
        var ca = campoStr.charAt(i);
        salida = salida + ca;
    }
    var anterior = salida;
    if (anterior.length > 0) {
        if (validaDireccion(valorString2, anterior)) {
            salida = $("#armardir").val() + " " + valorString2.toUpperCase() + " ";
            document.getElementById(campoSalida).value = salida;
        } else {
            error("no puede seleccionar dos nomenclaturas iguales ni más de dos letras seguidas");
        }
    } else {
        salida = $("#armardir").val() + " " + valorString2.toUpperCase() + " ";
        document.getElementById(campoSalida).value = salida;
    }
}

function concatenaNumero(valorString1, campoSalida) {
// cancatena dos string y los deja en la variable campoSalida.
// valorString1:  parametro de entrada con l string a concatenar.
// valorString1:  parametro de entrada con 2 string a concatenar.
// campoSalida: parametro de entrada con el nombre de campo que contendrá el string concatenado.
    var salida = "";
    var valorString2 = $("#armardir").val();
    if (valorString2 == "") {
        error("Se debe seleccionar un componente de direccion ");
    } else {
        var n = valorString1.length - 1;
        var aux = valorString1.substr(n, 1);
        if (aux == "0" || aux == "1" || aux == "2" || aux == "3" || aux == "4" || aux == "5" || aux == "6" ||
                aux == "7" || aux == "8" || aux == "9") {
            salida = valorString1.substring(0, n + 1);
            salida = $("#armardir").val() + salida;
        } else {
            salida = $("#armardir").val() + " " + valorString1 + " ";
        }
        document.getElementById(campoSalida).value = salida;
    }
}

function validaDireccion(valor1, valor2) {
    var letras = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
    var valida = true;
    var letra1 = false;
    var letra2 = false;
    var control = parseInt(document.getElementById('controlaLetras').value);
    if (valor1 != null && valor2 != null) {
        /*   if (valor1 == valor2) {
         valida = false;
         } */
        for (i = 0; i < letras.length; i++) {
            if (valor1 == letras[i])
                letra1 = true;
            if (valor2 == letras[i])
                letra2 = true;
        }
        if (letra1 && letra2) {
            control++;
            document.getElementById('controlaLetras').value = control;
            if (control > 2)
                valida = false;
        } else {
            if (valor1 == valor2) {
                valida = false;
            }
            document.getElementById('controlaLetras').value = "0";
        }
        return valida;
    }
}

function inputChange(tipo, idinput)
{
    if (tipo == 'del')
    {
        $("#armardir").val("");
    }
    if (tipo == 'ret')
    {
        var text = $("#armardir").val();
        $("#armardir").val(text.slice(0, -3));
    }
    if (tipo == 'env')
    {
        if ($("#direccion" + idinput).length > 0)
        {
            $("#direccion" + idinput).val($("#armardir").val());
            $("#comentarios_direccion" + idinput).val($("#comentarios_direccion").val());
        } else if ($("#direccion_encargado" + idinput).length > 0)
        {
            $("#direccion_encargado" + idinput).val($("#armardir").val());
        } else if ($("#direccion_servicio").length > 0)
        {
            $("#direccion_servicio").val($("#armardir").val());
            $("#comentarios_direccion" + idinput).val($("#comentarios_direccion").val());
        } else if ($("#Oportunidad_Direccion").length > 0)
        {
            $("#Oportunidad_Direccion").val($("#armardir").val());
            $("#Oportunidad_Observaciones").val($("#comentarios_direccion").val());
            upchangecampo('Oportunidad', 'Direccion', $("#armardir").val(), $("#idoportunidad").val());
        } else
        {
            var valor;

            if ($('#armardir').val().length > 0)
            {
                valor = $('#armardir').val();
            } else
            {
                valor = $('#armardiraux').val();
            }

            $("#" + idinput).val(valor);
            $("#Oportunidad_Direccion").val(valor);

            upchangecampo('Oportunidad', 'Direccion', valor, $("#idoportunidad").val());
            upchangecampo('Oportunidad', 'Observaciones', $("#observaciones_direccion").val(), $("#idoportunidad").val());
        }

        $("#modalAlerta").modal("hide");
    }
}

function verDetalle(input, model)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/detalle")?>',
        'data': 'input=' + input + '&model=' + model,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    generatedetalle(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}


function generatedetalle(text) {
    var n = noty({text: text,
        type: 'info',
        dismissQueue: true,
        layout: 'topLeft',
        closeWith: ['click'],
        theme: 'relax',
        maxVisible: 10,
        animation: {
            open: 'animated bounceInLeft',
            close: 'animated bounceOutLeft',
            easing: 'swing',
            speed: 500
        }
    });
    console.log('ht ml: ' + n.options.id);
}


function gestion(id)
{
    $('.gestionador').fadeIn('fast');
    $('.asignacion').fadeOut('fast');
    $('.transferencia').fadeOut('fast');
}


function transferencia()
{
    $('.gestionador').fadeOut('fast');
    $('.asignacion').fadeOut('fast');
    $('.transferencia').fadeIn('fast');
}

function asignacion()
{
    $('.gestionador').fadeOut('fast');
    $('.transferencia').fadeOut('fast');
    $('.asignacion').fadeIn('fast');
}

function guardarGestion()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("admopo/guardargestion")?>',
        'data':   'idoportunidad=' + $('#idoportunidad').val()
                + '&accion=' + $('#idestweb').val()
                + '&comentarios=' + $('#comentarios').val()
                + '&comentarios_agenda=' + $('#comentarios_agenda').val()
                + '&comentarios_cita=' + $('#comentarios_cita').val()
                + '&fecha_agendada=' + $('#fecha_agendada').val()
                + '&fecha_asignada=' + $('#fecha_asignada').val(),
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    if (respuesta == 'OK')
                    {
                        swal({
                            title: "Gestion Guardada",
                            text: "Completar Informacion",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        location.reload();
                    } 
                    else
                    {
                        swal("¡Error!", respuesta, 'error');
                    }
                    $("#modalGestion").modal("hide");
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function convertirCita()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/comercial") ?>',
        'data':   'idoportunidad=' + $('#idoportunidad').val()
                + '&accion=' + $('#idestweb').val()
                + '&comentarios=' + $('#comentarios').val()
                + '&comentarios_agenda=' + $('#comentarios_agenda').val()
                + '&comentarios_cita=' + $('#comentarios_cita').val()
                + '&fecha_agendada=' + $('#fecha_agendada').val()
                + '&fecha_asignada=' + $('#fecha_asignada').val(),
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    if (respuesta != 'Error') {
                        swal({
                            title: " Hecho! ",
                            text: "Asignado",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        window.location.href = "<?= Yii::app()->createUrl('admopo') ?>";
                    } 
                    else
                    {
                        swal({
                            title: " Alto! ",
                            text: " Todos los Campos son Obligatorios! ",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function marketing()
{
    $('.marketing').fadeIn('fast');
    $('.transferencia').fadeOut('fast');
}

function mailchimp(op, id, titulo) {

    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/estweb")?>',
        'data': 'id=' + id + '&op=' + op + '&titulo=' + titulo + '&idoportunidad=' + $('#idoportunidad').val(),
        'beforeSend': function () {
            $('.divocl').fadeOut('fast');
            $('.reagendamiento').fadeOut('fast');
            $('.volverallamar').fadeOut('fast');
            $('.asignacion').fadeOut('fast');
            $('.gestionador').fadeOut('fast');
        },
        'success':
                function (respuesta) {
                    $('.divocl').fadeIn('fast');
                    $('.divocl').html(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function gestionador(obj) {
    $('.gestionador').fadeIn('fast');
    var opc = obj.id.split('opc_');
    obj.style.background = (obj.style.background == '') ? '#2a84c0' : '';
    document.getElementById('idestweb').value = (obj.style.background == '') ? '' : opc[1];
    if (obj.style.background == '') {
        $('td span').addClass('danger');
        $('td span').removeClass('whitesel');
        $('.gestionador').fadeOut('fast');
    } else {
        $('td span').addClass('whitesel');
        $('td span').removeClass('danger');
        $('.gestionador').fadeIn('fast');
    }
}

function reagendamiento(obj) {
    var opc = obj.id.split('opc_');
    $('.reagendamiento').fadeIn('fast');
    $('.volverallamar').fadeOut('fast');
    $('.gestionador').fadeOut('fast');
    $('.asignacion').fadeOut('fast');
    document.getElementById('idestweb').value = opc[1];
}

function volverallamar(obj) {
    var opc = obj.id.split('opc_');
    $('.reagendamiento').fadeOut('fast');
    $('.volverallamar').fadeIn('fast');
    $('.gestionador').fadeOut('fast');
    $('.asignacion').fadeOut('fast');
    document.getElementById('idestweb').value = opc[1];
}

function comercial() {
    $('.reagendamiento').fadeOut('fast');
    $('.volverallamar').fadeOut('fast');
    $('.gestionador').fadeOut('fast');
    $('.asignacion').fadeIn('fast');
}


function enviarContacto()
{
    var form = $('#formTransfer').serialize();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admopo/enviarcontacto")?>',
        'data': form + "&idoportunidad=" + $('#idoportunidad').val(),
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {

                    if (respuesta != 'Error') {

                        swal({
                            title: "Deseas Hacer la Transferencia?",
                            text: "Luego de esto el registro sera movido de tu cuenta",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Si",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?= Yii::app()->createUrl('admopo') ?>";
                                    }
                                });

                    } else
                    {
                        swal({
                            title: " Alto! ",
                            text: " Todos los Campos son Obligatorios! ",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}
