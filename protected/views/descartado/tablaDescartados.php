<?php

$this->widget(
        'booster.widgets.TbJsonGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'cacheTTL' => 10, // cache will be stored 10 seconds (see cacheTTLType)
    'cacheTTLType' => 's', // type can be of seconds, minutes or hours
    'responsiveTable' => true,
    'htmlOptions'=>array('style'=>'font-size: 9pt'),
    'template' => "{items}{pager}{summary}",
    'columns' => array(
        array('name' => 'ID', 'value' => '$data->ID', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Fecha_creacion', 'value' => '$data->Fecha_creacion', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Razon_social', 'value' => '$data->Razon_social', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Nit', 'value' => '$data->Nit', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Nombre_completo', 'value' => '$data->Nombre_completo', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'searchasesor', 'value' => '$data->asesor->Nombre', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'searchagencia', 'value' => '$data->agencia->Nombre', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'searchciudad', 'value' => '$data->ciudad->Nombre', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Direccion', 'value' => '$data->Direccion', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Celular', 'value' => '$data->Celular', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Telefono', 'value' => '$data->Telefono', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'Email', 'value' => '$data->Email', 'htmlOptions' => array('style' => 'text-align: center')),
        array('name' => 'searchtipocontacto', 'value' => '$data->tipoContacto->Descripcion', 'htmlOptions' => array('style' => 'text-align: center')),
        array(
            'header' => Yii::t('ses', 'Ir'),
            'class' => 'booster.widgets.TbJsonButtonColumn',
            'template' => '{llamada}{basefria}{web}{activo}{chat}',
            'buttons' => array(
                'llamada' => array(
                    'visible' => '$data->Tipo_contacto == 1',
                    'label' => '',
                    'options' => array('class' => 'fa fa-phone fa-2x'),
                    'url' => 'Yii::app()->createUrl("/admcon/formulario", array("idcontacto" => $data->ID, "tipocontacto" => $data->Tipo_contacto))',
                ),
                'basefria' => array(
                    'visible' => '$data->Tipo_contacto == 2',
                    'label' => '',
                    'options' => array('class' => 'fa fa-folder-open fa-2x'),
                    'url' => 'Yii::app()->createUrl("/admcon/formulario", array("idcontacto" => $data->ID, "tipocontacto" => $data->Tipo_contacto))',
                ),
                'web' => array(
                    'visible' => '$data->Tipo_contacto == 3',
                    'label' => '',
                    'options' => array('class' => 'fa fa-globe fa-2x'),
                    'url' => 'Yii::app()->createUrl("/admcon/formulario", array("idcontacto" => $data->ID, "tipocontacto" => $data->Tipo_contacto))',
                ),
                'activo' => array(
                    'visible' => '$data->Tipo_contacto == 4',
                    'label' => '',
                    'options' => array('class' => 'fa fa-user fa-2x'),
                    'url' => 'Yii::app()->createUrl("/admcon/formulario", array("idcontacto" => $data->ID, "tipocontacto" => $data->Tipo_contacto))',
                ),
                'chat' => array(
                    'visible' => '$data->Tipo_contacto == 5',
                    'label' => '',
                    'options' => array('class' => 'fa fa-wechat fa-2x'),
                    'url' => 'Yii::app()->createUrl("/admcon/formulario", array("idcontacto" => $data->ID, "tipocontacto" => $data->Tipo_contacto))',
                )
            )
        )
    )
        )
);