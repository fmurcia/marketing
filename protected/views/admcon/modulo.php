<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#top1">Actualizacion</a></li>
    <li><a data-toggle="tab" href="#top2">Gestion Comercial</a></li>
    <li><a data-toggle="tab" href="#top3">Historico Contacto</a></li>
</ul>
<div class="tab-content">
    <div id="top1" class="tab-pane fade in active">
        <p><?php $this->renderPartial('upcontacto', array('model' => $model)) ?></p>
    </div>
    <div id="top2" class="tab-pane fade">
        <p><?php $this->renderPartial('gescomercial', array('model' => $model)) ?></p>
    </div>
    <div id="top3" class="tab-pane fade">
        <p><?php $this->renderPartial('historicocontacto', array('idmodel' => $model->ID)) ?></p>
    </div>
</div>