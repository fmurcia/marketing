<div class="headtitle">
    <span class="glyphicon glyphicon-list"></span> Filtros
</div>
<div class="col-md-12 col-sm-12 col-xs-12">    
    <div id="jstreecot">
        <ul>       
            <li data-jstree='{"icon":"fa fa-bell","opened":true}'> Estados
                <ul>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-1"> Interesados</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-5"> Seguimiento</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-8"> Cita</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-10"> Cotizados 1</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-11"> Cotizados 2</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-12"> Cotizados 3</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-14"> Facturacion</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-16"> Presupuesto</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-17"> Competencia</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-18"> Solo Informacion</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-21"> Pendientes Asesor</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-22"> Pendientes Comercial</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-23"> Preventa</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-13"> Venta</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-6"> No Contesta</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-4"> Trabajo</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-wrench","opened":true}'> Tipos
                <ul>
                    <li data-jstree='{"icon":"fa fa-phone"}' data-code="tip-1"> Llamada Entrante</li>
                    <li data-jstree='{"icon":"fa fa-folder"}' data-code="tip-2"> Base Fria</li>
                    <li data-jstree='{"icon":"fa fa-globe"}' data-code="tip-3"> Campaña Web</li>
                    <li data-jstree='{"icon":"fa fa-user"}' data-code="tip-4"> Cliente Activo</li>
                    <li data-jstree='{"icon":"fa fa-commenting"}' data-code="tip-5"> Chat</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-globe","opened":true}'> Regionales
                <ul>
                    <?php
                    $regionales = $this->getRegionalesXNivel();
                    if (!empty($regionales)) :
                        foreach ($regionales as $regional) :
                            $agencias = $regional->getAgencias();
                            ?>
                            <li data-code="reg-<?= $regional->ID_Regional ?>" data-jstree='{"icon":"glyphicon glyphicon-globe"}'><?= strtolower($regional->Descripcion) ?>     
                                <ul>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-globe"}'>Agencias
                                        <?php
                                        if (count($agencias) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($agencias as $agencia) :
                                                    if ($agencia->TipoAgencia == 1) :
                                                        ?>
                                                        <li data-code="age-<?= $agencia->ID ?>" data-jstree='{"icon":"glyphicon glyphicon-map-marker"}'><?php echo strtolower($agencia->Nombre) ?></li>
                                                        <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-globe"}'>Fuerza de Venta
                                        <?php
                                        if (count($agencias) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($agencias as $agencia) :
                                                    if ($agencia->TipoAgencia == 3) :
                                                        ?>
                                                        <li data-code="age-<?= $agencia->ID ?>" data-jstree='{"icon":"glyphicon glyphicon-map-marker"}'><?php echo strtolower($agencia->Nombre) ?></li>
                                                        <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </ul>
            </li>
<!--            <li data-jstree='{"icon":"fa fa-calendar","opened":true}'> Gestionados
                <ul>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-1"> Ayer </li>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-2"> 15 Días </li>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-3"> < 1 Mes</li>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-4"> > 1 Mes</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-edit","opened":true}'> Agendados
                <ul>
                    <li data-jstree='{"icon":"fa fa-edit"}' data-code="agen-1"> Vigentes</li>
                    <li data-jstree='{"icon":"fa fa-edit"}' data-code="agen-2"> Vencidos</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-home","opened":true}'> Propiedad Horizontal
                <ul>
                    <li data-jstree='{"icon":"fa fa-home"}' data-code="pro-1"> Activo</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-calendar","opened":true}'> Temporalidad
                <ul>
                    <li data-jstree='{"icon":"fa fa-map"}' data-code="time-2015-01"> 2015
                        <ul>
                            <li data-code="time-2015-01" data-jstree='{"icon":"fa fa-map"}'>Enero</li>
                            <li data-code="time-2015-02" data-jstree='{"icon":"fa fa-map"}'>Febrero</li>
                            <li data-code="time-2015-03" data-jstree='{"icon":"fa fa-map"}'>Marzo</li>
                            <li data-code="time-2015-04" data-jstree='{"icon":"fa fa-map"}'>Abril</li>
                            <li data-code="time-2015-05" data-jstree='{"icon":"fa fa-map"}'>Mayo</li>
                            <li data-code="time-2015-06" data-jstree='{"icon":"fa fa-map"}'>Junio</li>
                            <li data-code="time-2015-07" data-jstree='{"icon":"fa fa-map"}'>Julio</li>
                            <li data-code="time-2015-08" data-jstree='{"icon":"fa fa-map"}'>Agosto</li>
                            <li data-code="time-2015-09" data-jstree='{"icon":"fa fa-map"}'>Septiembre</li>
                            <li data-code="time-2015-10" data-jstree='{"icon":"fa fa-map"}'>Octubre</li>
                            <li data-code="time-2015-11" data-jstree='{"icon":"fa fa-map"}'>Noviembre</li>
                            <li data-code="time-2015-12" data-jstree='{"icon":"fa fa-map"}'>Diciembre</li>
                        </ul>
                    </li>
                    <li data-jstree='{"icon":"fa fa-map"}'  data-code="time-2016-01"> 2016
                        <ul>
                            <li data-code="time-2016-01" data-jstree='{"icon":"fa fa-map"}'>Enero</li>
                            <li data-code="time-2016-02" data-jstree='{"icon":"fa fa-map"}'>Febrero</li>
                            <li data-code="time-2016-03" data-jstree='{"icon":"fa fa-map"}'>Marzo</li>
                            <li data-code="time-2016-04" data-jstree='{"icon":"fa fa-map"}'>Abril</li>
                            <li data-code="time-2016-05" data-jstree='{"icon":"fa fa-map"}'>Mayo</li>
                            <li data-code="time-2016-06" data-jstree='{"icon":"fa fa-map"}'>Junio</li>
                            <li data-code="time-2016-07" data-jstree='{"icon":"fa fa-map"}'>Julio</li>
                            <li data-code="time-2016-08" data-jstree='{"icon":"fa fa-map"}'>Agosto</li>
                            <li data-code="time-2016-09" data-jstree='{"icon":"fa fa-map"}'>Septiembre</li>
                            <li data-code="time-2016-10" data-jstree='{"icon":"fa fa-map"}'>Octubre</li>
                            <li data-code="time-2016-11" data-jstree='{"icon":"fa fa-map"}'>Noviembre</li>
                            <li data-code="time-2016-12" data-jstree='{"icon":"fa fa-map"}'>Diciembre</li>
                        </ul>
                    </li>
                    <li data-jstree='{"icon":"fa fa-map"}'  data-code="time-2017-01"> 2017
                        <ul>
                            <li data-code="time-2017-01" data-jstree='{"icon":"fa fa-map"}'>Enero</li>
                            <li data-code="time-2017-02" data-jstree='{"icon":"fa fa-map"}'>Febrero</li>
                            <li data-code="time-2017-03" data-jstree='{"icon":"fa fa-map"}'>Marzo</li>
                            <li data-code="time-2017-04" data-jstree='{"icon":"fa fa-map"}'>Abril</li>
                            <li data-code="time-2017-05" data-jstree='{"icon":"fa fa-map"}'>Mayo</li>
                            <li data-code="time-2017-06" data-jstree='{"icon":"fa fa-map"}'>Junio</li>
                            <li data-code="time-2017-07" data-jstree='{"icon":"fa fa-map"}'>Julio</li>
                            <li data-code="time-2017-08" data-jstree='{"icon":"fa fa-map"}'>Agosto</li>
                            <li data-code="time-2017-09" data-jstree='{"icon":"fa fa-map"}'>Septiembre</li>
                            <li data-code="time-2017-10" data-jstree='{"icon":"fa fa-map"}'>Octubre</li>
                            <li data-code="time-2017-11" data-jstree='{"icon":"fa fa-map"}'>Noviembre</li>
                            <li data-code="time-2017-12" data-jstree='{"icon":"fa fa-map"}'>Diciembre</li>
                        </ul>
                    </li>
                    <li data-jstree='{"icon":"fa fa-map"}'  data-code="time-2018-01"> 2018
                        <ul>
                            <li data-code="time-2018-01" data-jstree='{"icon":"fa fa-map"}'>Enero</li>
                            <li data-code="time-2018-02" data-jstree='{"icon":"fa fa-map"}'>Febrero</li>
                            <li data-code="time-2018-03" data-jstree='{"icon":"fa fa-map"}'>Marzo</li>
                            <li data-code="time-2018-04" data-jstree='{"icon":"fa fa-map"}'>Abril</li>
                            <li data-code="time-2018-05" data-jstree='{"icon":"fa fa-map"}'>Mayo</li>
                            <li data-code="time-2018-06" data-jstree='{"icon":"fa fa-map"}'>Junio</li>
                            <li data-code="time-2018-07" data-jstree='{"icon":"fa fa-map"}'>Julio</li>
                            <li data-code="time-2018-08" data-jstree='{"icon":"fa fa-map"}'>Agosto</li>
                            <li data-code="time-2018-09" data-jstree='{"icon":"fa fa-map"}'>Septiembre</li>
                            <li data-code="time-2018-10" data-jstree='{"icon":"fa fa-map"}'>Octubre</li>
                            <li data-code="time-2018-11" data-jstree='{"icon":"fa fa-map"}'>Noviembre</li>
                            <li data-code="time-2018-12" data-jstree='{"icon":"fa fa-map"}'>Diciembre</li>
                        </ul>
                    </li>
                </ul>
            </li>-->
        </ul>
    </div>
</div>