<div class="moduladorpag">
    <?php
    $contactoestado = ContactoEstado::model()->getEstadoProceso($model->ID);

    if ($contactoestado == NULL) :
        $class_seguimiento = 'on';
        $class_cotizacion = 'off';
        $class_descartado = 'off';
        $class_interesado = 'on';
        $class_preventa = 'off';
        $class_venta = 'off';
    else :

        $descartados = array(19, 29, 16, 17, 18);
        $seguimientos = array(12, 16, 20, 21, 22);
        $cotizados = array(23);
        $preventas = array(1, 2, 4, 3, 5, 6, 7, 8, 9, 10, 11, 25, 27);
        $ventas = array(13, 14);

        $ultest = $contactoestado->ID_Estadoproceso;

        if (in_array($ultest, $descartados)) :
            $class_seguimiento = 'off';
            $class_descartado = 'on';
            $class_cotizacion = 'off';
            $class_interesado = 'off';
            $class_preventa = 'off';
            $class_venta = 'off';

        elseif (in_array($ultest, $seguimientos)) :
            $class_seguimiento = 'on';
            $class_descartado = 'off';
            $class_cotizacion = 'off';
            $class_interesado = 'on';
            $class_preventa = 'off';
            $class_venta = 'off';
        elseif (in_array($ultest, $cotizados)) :
            $class_seguimiento = 'on';
            $class_descartado = 'off';
            $class_cotizacion = 'on';
            $class_interesado = 'on';
            $class_preventa = 'off';
            $class_venta = 'off';
        elseif (in_array($ultest, $preventas)) :
            $class_seguimiento = 'on';
            $class_descartado = 'off';
            $class_cotizacion = 'on';
            $class_interesado = 'on';
            $class_preventa = 'on';
            $class_venta = 'off';
        elseif (in_array($ultest, $ventas)) :
            $class_seguimiento = 'on';
            $class_descartado = 'off';
            $class_cotizacion = 'on';
            $class_interesado = 'on';
            $class_preventa = 'on';
            $class_venta = 'on';
        else :
            $class_seguimiento = 'on';
            $class_descartado = 'off';
            $class_cotizacion = 'off';
            $class_interesado = 'on';
            $class_preventa = 'off';
            $class_venta = 'off';
        endif;
    endif;

    if (isset($model->Tipo_contacto)) :
        if ($model->Tipo_contacto == 1) :
            $titulo = "LLAMADA DE ENTRADA";
            $icono = "fa fa-phone fa-2x";
        elseif ($model->Tipo_contacto == 2) :
            $titulo = "BASE FRIA";
            $icono = "fa fa-folder-open fa-2x";
        elseif ($model->Tipo_contacto == 3) :
            $titulo = "CAMPAÑA WEB";
            $icono = "fa fa-globe fa-2x";
        elseif ($model->Tipo_contacto == 4) :
            $titulo = "CLIENTE ACTIVO";
            $icono = "fa fa-user fa-2x";
        elseif ($model->Tipo_contacto == 5) :
            $titulo = "CLIENTE CHAT";
            $icono = "fa fa-wechat fa-2x";
        elseif ($model->Tipo_contacto == 6) :
            $titulo = "CLIENTE EMAIL MARKETING";
            $icono = "fa fa-envelope fa-2x";
        else :
            $titulo = "CONTACTO DESCARTADO";
            $icono = "fa fa-warning fa-2x";
        endif;

        if ($model->Oportunidad != 0) :
            $contextoop = 'primary';
            $iconoop = 'fa fa-users fa-2x';
            $tituloop = 'OPORTUNIDAD TRIANGULO';
        else :
            $contextoop = 'default';
            $iconoop = 'fa fa-signal fa-2x';
            $tituloop = 'DIRECTO';
        endif;

        $contexto = $model->tipoContacto->Contexto;
        ?>
        <input type="hidden" name="idcontact" id="idcontact"  value="<?= $model->ID ?>" />
        <input type="hidden" name="idestweb" id="idestweb" value="" />
        <div class="col-lg-3 col-md-3 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contexto ?>">
                <div class="panel-heading">
                    <span class="<?= $icono ?>"></span>
                    <?= $titulo ?> 
                </div>
            </div>
        </div>    
        <div class="col-lg-3 col-md-3 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contextoop ?>">
                <div class="panel-heading">
                    <span class="<?= $iconoop ?>"></span>
                    <?= $tituloop ?> 
                </div>
            </div>
        </div> 
        <div class="col-lg-1 col-md-1 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contextoop ?>">
                <div class="panel-heading">
                    <span class="fa fa-map-pin fa-2x"></span>
                    ID <?= $model->ID ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contexto ?>">
                <div class="panel-heading">
                    <span class="fa fa-commenting fa-2x"></span>
                    <?= strtoupper($model->idEstadoweb->Descripcion) ?>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sx-12 col-xs-12 panel-danger">
            <div class="col-lg-5 col-md-5 col-sx-12 col-xs-12 panel-heading" style="text-align:right; font-weight: bold">
                <span class="fa fa-book fa-2x"></span>
                PAGINA</div>
            <div class="col-lg-7 col-md-7 col-sx-12 col-xs-12">
                <select id="ctopages" class="form-control" onchange="paginaCta(this.value, <?= $block ?>)">
                    <?php for ($i = 1; $i < $pages; $i++) : ?>
                        <option <?= ($pag == $i) ? 'Selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-7 col-md-7 col-lg-offset-4 col-sx-12 col-xs-12">   
            <?php
            $this->widget(
                    'booster.widgets.TbMenu', array(
                'type' => 'navbar',
                'items' => array(
                    array('label' => 'Marketing', 'url' => '#', 'icon' => 'fa fa-edit fa-2x', 'itemOptions' => array('onclick' => 'marketing()')),
                    array('label' => 'Reenvios', 'url' => '#', 'icon' => 'fa fa-random fa-2x', 'items' =>
                        array(
                            array("label" => "SMS", 'url' => '#', 'itemOptions' => array('onclick' => 'modalReenvio("SMS", "' . $model->ID . '")')),
                            array("label" => "EMAIL", 'url' => '#', 'itemOptions' => array('onclick' => 'modalReenvio("EMAIL", "' . $model->ID . '")')),
                            array("label" => "4D", 'url' => '#', 'itemOptions' => array('onclick' => 'modalReenvio("4D", "' . $model->ID . '")')),
                        )
                    ),
                    array('label' => 'Transferencia', 'url' => '#', 'icon' => 'fa fa-refresh fa-2x', 'itemOptions' => array('onclick' => 'transferencia()')),
                    array('label' => 'Coincidencias!!!', 'url' => '#', 'icon' => 'fa fa-cubes fa-2x', 'itemOptions' => array('onclick' => 'coincidencias()')),
                ),
                    )
            );
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="transferencia"  style="display:none">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12 ">
                <div class="row">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" onclick="$('.transferencia').fadeOut('fast');">&times;</a>
                        <h4>TRANSFERENCIA</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $formu = $this->beginWidget(
                                'booster.widgets.TbActiveForm', array(
                            'id' => 'formTransfer',
                            'enableAjaxValidation' => false,
                            'action' => "javascript:void(0)"
                        ));
                        ?>
                        <h4>Comercial</h4>
                        <?php
                        $this->widget(
                                'booster.widgets.TbSelect2', array(
                            'asDropDownList' => true,
                            'name' => 'comercial',
                            "data" => Asesor::model()->getTelemercaderistas(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "style" => "font-size: 9pt",
                                "placeholder" => '...Seleccione...',
                            ),
                            'options' => array(
                                'width' => '100%',
                                "style" => "width:100%",
                                'required' => true,
                            ))
                        );
                        ?>

                        <h4>Comentarios detallados</h4>
                        <textarea class="form-control" name="comentarios" required ></textarea>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                            <?php
                            $this->widget(
                                    'booster.widgets.TbButton', array(
                                'label' => 'Enviar',
                                'context' => 'success',
                                'htmlOptions' => array(
                                    "class" => "pull-right",
                                    'onclick' => "enviarContacto()"
                                )
                                    )
                            );
                            ?>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <div class="marketing col-lg-12 col-md-12 col-sx-12 col-xs-12" style="display:none">
            <div class="row">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal" onclick="$('.marketing').fadeOut('fast');">&times;</a>
                    <h4>MAILCHIMP</h4>
                </div>
                <div class="modal-body">
                    <div class="cotelenav">
                        <span class="arrow-left <?= $class_descartado ?>"></span>
                        <span class="cubed <?= $class_descartado ?>" onclick="mailchimp(1, '<?= $model->ID ?>', 'INVALIDO');"><span class="fa fa-pin"></span> INVALIDO</span>
                        <span class="crop-left <?= $class_descartado ?>"></span>

                        <span class="arrow-left <?= $class_descartado ?>"></span>
                        <span class="cubed <?= $class_descartado ?>" onclick="mailchimp(2, '<?= $model->ID ?>', 'DESCARTADO');"><span class="fa fa-bug"></span> DESCARTADO</span>
                        <span class="crop-left <?= $class_descartado ?>"></span>

                        <span class="cubed-space"></span>

                        <span class="crop-right <?= $class_interesado ?>"></span>
                        <span class="cube <?= $class_interesado ?>" onclick="mailchimp(3, '<?= $model->ID ?>', 'INTERESADO');"><span class="fa fa-user"></span> INTERESADO</span>
                        <span class="arrow-right <?= $class_interesado ?>"></span>

                        <span class="crop-right <?= $class_seguimiento ?>"></span>
                        <span class="cube <?= $class_seguimiento ?>" onclick="mailchimp(4, '<?= $model->ID ?>', 'SEGUIMIENTO');"><span class="fa fa-signal"></span> SEGUIMIENTO</span>
                        <span class="arrow-right <?= $class_seguimiento ?>"></span>

                        <span class="crop-right <?= $class_cotizacion ?>"></span>
                        <span class="cube <?= $class_cotizacion ?>" onclick="mailchimp(5, '<?= $model->ID ?>', 'COTIZACION');"><span class="fa fa-edit"></span> COTIZACION</span>
                        <span class="arrow-right <?= $class_cotizacion ?>"></span>

                        <span class="crop-right <?= $class_preventa ?>"></span>
                        <span class="cube <?= $class_preventa ?>" onclick="mailchimp(6, '<?= $model->ID ?>', 'PREVENTA');"><span class="fa fa-map-pin"></span> PREVENTA</span>
                        <span class="arrow-right <?= $class_preventa ?>"></span>

                        <span class="crop-right <?= $class_venta ?>"></span>
                        <span class="cube <?= $class_venta ?>" onclick="mailchimp(7, '<?= $model->ID ?>', 'VENTA');"><span class="fa fa-check"></span> VENTA</span>
                        <span class="arrow-right <?= $class_venta ?>"></span>

                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="divocl col-lg-8 col-lg-offset-2 col-md-7 col-md-offset-2 col-sx-12 col-xs-12"></div>
                    <br />
                    <div class=" asignacion col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12" style="display:none">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" onclick="$('.asignacion').fadeOut('fast');">&times;</a>
                            <h4>ASIGNACION COMERCIAL</h4>
                        </div>
                        <div class="modal-body">
                            <?php
                            $form = $this->beginWidget(
                                    'booster.widgets.TbActiveForm', array(
                                'id' => 'enviarcomercial',
                                'type' => 'vertical',
                                'action' => "javascript:void(0)",
                                    )
                            );
                            ?>
                            <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">CIUDAD </div>
                            <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                                <?php
                                $this->widget(
                                        'booster.widgets.TbSelect2', array(
                                    "asDropDownList" => true,
                                    "name" => 'ciudad',
                                    "data" => Contacto::model()->getRegionales(),
                                    "htmlOptions" => array(
                                        "class" => "form-control",
                                        "style" => "font-size: 10.5pt",
                                        "placeholder" => '...Seleccione...',
                                    ),
                                    "options" => array(
                                        'width' => '100%'
                                    ),
                                    "events" => array('change' => 'js:function(e) { cargarAgencias(this.value) }')
                                ));
                                ?>
                                <br />
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">AGENCIA COMERCIAL </div>
                            <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                                <div id="div_agencia_asignada">
                                    <input type="text" class="form-control" disabled="true" placeholder="..Seleccione.."/>
                                </div>
                                <br />
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">FECHA Y HORA </div>
                            <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                                <center>
                                    <div class="input-group">
                                        <br>
                                        <input id="fecha_asignada" style="text-align: center;" name="fecha_asignada" value="" class="form-control" readonly="true"  type="hidden"/>
                                        <span class="btn input-group-addon" style="cursor:pointer"  id="calendarco">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </center>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">ASESOR COMERCIAL </div>
                            <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                                <div id="div_asesor_asignada">
                                    <input type="text" class="form-control" disabled="true" placeholder="..Seleccione..">
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">OBSERVACIONES </div>
                            <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                                <textarea name="observaciones" class="form-control"></textarea>
                            </div>
                            <?php $this->endWidget(); ?>
                        </div>
                        <div class="modal-footer col-lg-5 col-lg-offset-3 col-sx-12 col-xs-12">
                            <?php
                            $this->widget('booster.widgets.TbButton', array('label' => 'Asignar', 'htmlOptions' => array('class' => 'btn btn-success', 'onclick' => "asignarAsesor()")));
                            ?>
                        </div>
                    </div>
                    <div class="reagendamiento col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12" style="display:none">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" onclick="$('.reagendamiento').fadeOut('fast');">&times;</a>
                            <h4>AGENDAMIENTO COMERCIAL</h4>
                        </div>
                        <div class="modal-body">
                            <center>
                                <div class="input-group">
                                    <br>
                                    <input id="fecha_agendada" style="text-align: center;" name="fecha_agendada" value="" class="form-control" readonly="true"  type="hidden"/>
                                    <span class="btn input-group-addon" style="cursor:pointer"  id="calendarag">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </center>
                            <textarea name="comentarios" class="form-control" placeholder="Escriba sus comentarios"></textarea>
                        </div>
                        <div class="modal-footer">
                            <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                                <?php
                                $this->widget(
                                        'booster.widgets.TbButton', array(
                                    'label' => 'Agendar',
                                    'context' => 'success',
                                    'htmlOptions' => array(
                                        "class" => "pull-right",
                                        'onclick' => "guardarGestion()"
                                    )
                                        )
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sx-12 col-xs-12" style="display:none" id="gestionador">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" onclick="$('#gestionador').fadeOut('fast');">&times;</a>
                            <h4>GESTION COMERCIAL</h4>
                        </div>
                        <div class="modal-body">
                            <div class="contrato" style="display:none">
                                <input id="cont" class="form-control" placeholder="Digite el No. de Contrato">
                                <p style="font-size:12pt;  text-align: center; color:red">Digite el # de contrato asociado a este contacto para actualizar el estado del contacto</p>
                            </div>
                            <div>
                                <textarea id="comentarios" class="form-control" placeholder="Escriba sus comentarios"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                                <?php
                                $this->widget(
                                    'booster.widgets.TbButton', array(
                                        'label' => 'Enviar',
                                        'context' => 'success',
                                        'htmlOptions' => array(
                                            "class" => "pull-right",
                                            'onclick' => "guardarGestion()"
                                        )
                                    )
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="reenvios" style="display:none"></div>
        <div class="col-lg-12 col-md-12 col-sx-12 col-xs-12" id = "paginadorcot" style="height: 800px">
            <?php $this->renderPartial('modulo', array('model' => $model)); ?>
        </div>
        <?php
    endif;
    ?>
</div>

<?php
$script = '$("#calendarag").datetimepicker({
                inline: true,
                timepicker: true,
                format: "Y-m-d H:i:s",
                onChangeDateTime: function (dp, $input) {
                    $("#fecha_agendada").val($input.val());
                }
            })';



$scriptno = '$("#calendarco").datetimepicker({
                inline: true,
                timepicker: true,
                format: "Y-m-d H:i:s",
                onChangeDateTime: function (dp, $input) {
                    $("#fecha_asignada").val($input.val());
                    cargarAsesor($("#AgenciaAsignada").val());
                }
            })';

Yii::app()->clientScript->registerScript(uniqid(), $script);
Yii::app()->clientScript->registerScript(uniqid(), $scriptno);
?>