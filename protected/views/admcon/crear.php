<?php
$this->beginwidget(
        'booster.widgets.TbPanel', array(
    'title' => '<font size="2pt">&nbsp;PRE - CONTACTO RAPIDO</font>',
    'headerIcon' => 'folder-open',
    'context' => 'default',
    'padContent' => true,
        )
);
?>
<div class="col-lg-5  col-md-5  col-xs-12 col-sx-12 col-lg-offset-3">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'formContactoRapido',
        'enableAjaxValidation' => false,
        'action' => "javascript:void(0)",
        'htmlOptions' => array('onsubmit' => "contactRapid()"),
    ));
    ?>
    <input type="hidden" name="idusuario" id="idusuario" value="<?= Yii::app()->user->getState('id_usuario') ?>">
    <input type="hidden" name="comentarios_direccion" id="comentarios_direccion" value="">
    <div style="border: thick solid #5bc0de; border-collapse: collapse;" id="formtable">
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">Ciudad del Servicio</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12">
                <?php
                $this->widget('booster.widgets.TbSelect2', array(
                    'name' => 'ciudad',
                    'asDropDownList' => true,
                    "data" => Contacto::model()->getRegionales(),
                    "htmlOptions" => array(
                        "class" => "form-control",
                    ),
                    'options' => array(
                        'placeholder' => '... Seleccione ...',
                        'width' => '100%',
                    )
                        )
                );
                ?>
            </div>
        </div>
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">Correo Electronico</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12"><input type="text" class="form-control"  id="email" name="email"></div>
        </div>    
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">Telefono</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12"><input type="text" class="form-control"  id="telefono" name="telefono" onkeypress="return permite(event, 'num')" maxlength="10"></div>
        </div>    
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">Razon Social</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12"><input type="text" class="form-control"  id="razon" name="razon" onkeyup="this.value = this.value.toUpperCase()" onblur="duplicar('formContactoRapido', this.value)" required></div>
        </div>    
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">Quien Habla</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12"><input type="text" class="form-control"  id="persona" name="persona" onkeyup="this.value = this.value.toUpperCase()" required></div>
        </div>    
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">Direccion</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12">
                <input type="text" class="form-control"  id="direccion" name="direccion" readonly="true" required onclick="inputdireccion('formContactoRapido', 'direccion', 'crear')" onkeypress="inputdireccion('formContactoRapido', 'direccion', 'crear')"/>
            </div>
        </div>
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12" style="">Medio de Contacto</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12" style="">
                <?php
                $this->widget('booster.widgets.TbSelect2', array(
                    'name' => 'mediocontacto',
                    'asDropDownList' => true,
                    "data" => Contacto::model()->getMediocontacto(),
                    "htmlOptions" => array(
                        "class" => "form-control",
                    ),
                    'options' => array(
                        'placeholder' => '... Seleccione ...',
                        'width' => '100%',
                    )
                        )
                );
                ?>
            </div>
        </div>
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12" style="">Tipo de Servicio</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12" style="">
                <?php
                $this->widget('booster.widgets.TbSelect2', 
                    array(
                        'name' => 'tipservicio',
                        'asDropDownList' => true,
                        "data" => Contacto::model()->getServiciosList(),
                        "htmlOptions" => array(
                            "class" => "form-control",
                        ),
                        'options' => array(
                            'placeholder' => '... Seleccione ...',
                            'width' => '100%',
                        )
                    )
                );
                ?>
            </div>
        </div>
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12" style="">Tipo de Persona</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12" style="">
                <?php
                $this->widget('booster.widgets.TbSelect2', 
                    array(
                        'name' => 'tippersona',
                        'asDropDownList' => true,
                        "data" => Contacto::model()->getSegmentacionList(),
                        "htmlOptions" => array(
                            "class" => "form-control",
                        ),
                        'options' => array(
                            'placeholder' => '... Seleccione ...',
                            'width' => '100%',
                        )
                    )
                );
                ?>
            </div>
        </div>
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12" style="">Tipo de Contacto</div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12" style="">
                <?php
                $this->widget('booster.widgets.TbSelect2', array(
                    'name' => 'tipocontacto',
                    'asDropDownList' => true,
                    "data" => Contacto::model()->getTipocontacto(),
                    "htmlOptions" => array(
                        "class" => "form-control",
                    ),
                    'options' => array(
                        'placeholder' => '... Seleccione ...',
                        'width' => '100%',
                    )
                        )
                );
                ?>
            </div>
        </div>    
        <div style="border: 1px solid #5bc0de; border-collapse: collapse;"></div>
        <div class="row" style="padding: 5px">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sx-12">
                <center><?php echo CHtml::submitButton('Guardar ', array('class' => 'btn btn-info', "id" => "savecont")); ?></center>
            </div>
        </div>
    </div>    
    <?php $this->endWidget(); ?>
</div>
<?php $this->endwidget(); ?>