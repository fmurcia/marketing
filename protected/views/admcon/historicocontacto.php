<?php
$model = HistoricoGestion::model()->totalRegistrado($idmodel);
?>
<table class="table table-striped table-responsive">
    <tr>
        <th>ID</th>
        <th>ASESOR</th>
        <th>ACCION</th>
        <th>FECHA</th>
        <th>HORA</th>
        <th>OBSERVACION</th>
    </tr>
    <?php
    foreach ($model as $data) :
        $accionweb = EstadosWeb::model()->findByPk($data->ID_Accion);
        if ($accionweb != NULL) :
            $des = strtoupper($accionweb->Descripcion);
        else :
            $des = 'Item';
        endif;
        ?>
        <tr>
            <td><?= $data->ID ?></td>
            <td><?= $data->iDAsesor->Nombre ?></td>
            <td><?= $des ?></td>
            <td><?= $data->Fecha ?></td>
            <td><?= $data->Hora ?></td>
            <td><?= $data->Memo ?></td>
        </tr>    
        <?php
    endforeach;
    ?>
</table>
<div class="clearfix"></div>