var filtros = "";

function paginaCto(pag) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admcon/qrycontacto')?>",
        data: {
            pag: pag
        },
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success: function (data) {
            $('.ctospace').html(data);
        }
    });
}

function paginaCta(start, block) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admcon/pageslistcon') ?>",
        data: {
            start: start,
            block: block
        },
        beforeSend: function () {
            $('.moduladorpag').html($('#modalwait').html());
        },
        success: function (data) {
            $('.moduladorpag').html(data);
        }
    });
}

function actfiltros(flt) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admcon/updfltr')?>",
        data: {
            fltr: flt
        },
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success: function (data) {
            if (data === "OK") {
                busquedaavanzada();
            }
        },
        error: function (err) {
            error(err.responseText);
        }
    });
    filtros = flt;
    return false;
}

function buscartexto() {
    actfiltros(filtros);
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admcon/srchtrm')?>",
        data: {
            term: $("#searchterm").val()
        },
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success: function (status) {
            if (status === "OK") {
                busquedaavanzada();
            }
        },
        error: function (err) {
            error(err.responseText);
        }
    });
    return false;
}

function busquedaavanzada() {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admcon/qrycontacto') ?>",
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success: function (data) {
            $('.ctospace').html(data);
        },
        error: function (err) {
            error(err.responseText);
        }
    });
}

function upcontact(idcontacto) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admcon/upcontact') ?>",
        data: "id=" + idcontacto,
        beforeSend: function () {
            $('.ctospace').html($('#modalwait').html());
        },
        success: function (data) {
            console.log(data + idcontacto);
        }
    });
}

function duplicar(formulario, valor)
{
    $("#" + formulario).find(':input').each(function () {
        var elemento = this;
        var razon = 'razon';
        if (elemento.id == razon && elemento.value != '')
        {
            $("#persona").val(valor);
        }
    });
}

function permite(elEvento, permitidos) {
    // Variables que definen los caracteres permitidos
    var numeros = "0123456789";
    var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    var numeros_caracteres = numeros + caracteres;
    var teclas_especiales = [8, 9, 37, 39, 46];
    // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha


    // Seleccionar los caracteres a partir del parámetro de la función
    switch (permitidos) {
        case 'num':
            permitidos = numeros;
            break;
        case 'car':
            permitidos = caracteres;
            break;
        case 'num_car':
            permitidos = numeros_caracteres;
            break;
    }

    // Obtener la tecla pulsada 
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    var caracter = String.fromCharCode(codigoCaracter);
    // Comprobar si la tecla pulsada es alguna de las teclas especiales
    // (teclas de borrado y flechas horizontales)
    var tecla_especial = false;
    for (var i in teclas_especiales) {
        if (codigoCaracter == teclas_especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

// Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
// o si es una tecla especial
    return permitidos.indexOf(caracter) != -1 || tecla_especial;
}

function inputdireccion(formulario, input, tipo)
{
    $("#modalAlerta").modal('show');
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/direccion")?>',
        'data': 'formulario=' + formulario + '&input=' + input + '&tipo=' + tipo,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    $("#modalcuerpo").html('\
                        <div class="modal-header">\n\
                            <a class="close" data-dismiss="modal">&times;</a>\n\
                            <h4>Direccion</h4>\n\
                        </div>\n\
                        <div class="modal-body"><p>' + respuesta + '</p></div>');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function concatenaString(valorString2, campoSalida) {
// cancatena dos string y los deja en la variable campoSalida.
// valorString1:  parametro de entrada con l string a concatenar.
// valorString1:  parametro de entrada con 2 string a concatenar.
// campoSalida: parametro de entrada con el nombre de campo que contendrá el string concatenado.
    var campoStr = $("#armardir").val();
    salida = "";
    var j = 0;
    for (i = campoStr.length - 1; i > -1; i--) {
        var ca = campoStr.charAt(i);
        if (ca != ' ')
            j = 1;
        if (j == 1)
            salida = salida + ca;
    }
    campoStr = salida;
    salida = "";
    j = 0;
    for (i = 0; i < campoStr.length; i++) {
        var ca = campoStr.charAt(i);
        if (ca == ' ')
            j = 1;
        if (j != 1)
            salida = salida + ca;
    }
    campoStr = salida;
    salida = "";
    for (i = campoStr.length - 1; i > -1; i--) {
        var ca = campoStr.charAt(i);
        salida = salida + ca;
    }
    var anterior = salida;
    if (anterior.length > 0) {
        if (validaDireccion(valorString2, anterior)) {
            salida = $("#armardir").val() + " " + valorString2.toUpperCase() + " ";
            document.getElementById(campoSalida).value = salida;
        } else {
            error("no puede seleccionar dos nomenclaturas iguales ni más de dos letras seguidas");
        }
    } else {
        salida = $("#armardir").val() + " " + valorString2.toUpperCase() + " ";
        document.getElementById(campoSalida).value = salida;
    }
}

function concatenaNumero(valorString1, campoSalida) {
// cancatena dos string y los deja en la variable campoSalida.
// valorString1:  parametro de entrada con l string a concatenar.
// valorString1:  parametro de entrada con 2 string a concatenar.
// campoSalida: parametro de entrada con el nombre de campo que contendrá el string concatenado.
    var salida = "";
    var valorString2 = $("#armardir").val();
    if (valorString2 == "") {
        error("Se debe seleccionar un componente de direccion ");
    } else {
        var n = valorString1.length - 1;
        var aux = valorString1.substr(n, 1);
        if (aux == "0" || aux == "1" || aux == "2" || aux == "3" || aux == "4" || aux == "5" || aux == "6" ||
                aux == "7" || aux == "8" || aux == "9") {
            salida = valorString1.substring(0, n + 1);
            salida = $("#armardir").val() + salida;
        } else {
            salida = $("#armardir").val() + " " + valorString1 + " ";
        }
        document.getElementById(campoSalida).value = salida;
    }
}

function validaDireccion(valor1, valor2) {
    var letras = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
    var valida = true;
    var letra1 = false;
    var letra2 = false;
    var control = parseInt(document.getElementById('controlaLetras').value);
    if (valor1 != null && valor2 != null) {
        /*   if (valor1 == valor2) {
         valida = false;
         } */
        for (i = 0; i < letras.length; i++) {
            if (valor1 == letras[i])
                letra1 = true;
            if (valor2 == letras[i])
                letra2 = true;
        }
        if (letra1 && letra2) {
            control++;
            document.getElementById('controlaLetras').value = control;
            if (control > 2)
                valida = false;
        } else {
            if (valor1 == valor2) {
                valida = false;
            }
            document.getElementById('controlaLetras').value = "0";
        }
        return valida;
    }
}

function inputChange(tipo, accion)
{

    if (tipo == 'del')
    {
        $("#armardir").val("");
    }
    if (tipo == 'ret')
    {
        var text = $("#armardir").val();
        $("#armardir").val(text.slice(0, -3));
    }
    if (tipo == 'env')
    {
        var valor;

        if ($("#armardir").val().length > 0)
        {
            valor = $('#armardir').val();
            $("#direccion").val(valor);
            $("#Contacto_Direccion").val(valor);
            $("#comentarios_direccion").val($("#observaciones_direccion").val());

        } else if ($("#armardiraux").val().length > 0)
        {
            valor = $('#armardiraux').val();
            $("#direccion").val($("#armardiraux").val());
            $("#comentarios_direccion").val($("#observaciones_direccion").val());
            $("#Contacto_Direccion").val(valor);

        } else if ($("#direccion_encargado").length > 0)
        {
            $("#direccion_encargado").val($("#armardir").val());

        } else if ($("#direccion_servicio").length > 0)
        {
            $("#direccion_servicio").val($("#armardir").val());
            $("#comentarios_direccion").val($("#observaciones_direccion").val());
        } else
        {
            valor = $('#armardiraux').val();
            $("#direccion").val(valor);
            $("#Contacto_Direccion").val(valor);
            $("#comentarios_direccion").val($("#observaciones_direccion").val());
        }

        if (accion == 'actualizar') {
            upchangecampo('Contacto', 'Direccion', valor, $("#idcontact").val());
        }

        $("#modalAlerta").modal("hide");
    }
}

function mostrarNomenclatura()
{
    var destino = 'nomenclatura';
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/nomenclatura")?>',
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    $("#" + destino).hide();
                    $("#" + destino).html(respuesta);
                    $("#" + destino).fadeIn("fast");
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false,
    });
    return false;
}

function contactRapid()
{
    var formulario = $('#formContactoRapido').serialize();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("admcon/guardarcontacto") ?>',
        'data': formulario,
        'beforeSend': function () {
            $('#savecont').attr('disabled', 'disabled');
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    if (respuesta != 'Error')
                    {
                        swal({
                            title: "Contacto PreCargado",
                            text: "Completar Informacion",
                            timer: 1500,
                            showConfirmButton: false
                        });

                        $('#formContactoRapido').each(function () {
                            this.reset();
                        });

                        $('#savecont').removeAttr("disabled");

                        $('#ciudad').select2({placeholder: '...Seleccione...', width: '100%'});
                        $('#mediocontacto').select2({placeholder: '...Seleccione...', width: '100%'});
                        $('#tipocontacto').select2({placeholder: '...Seleccione...', width: '100%'});

                        if (JSON.parse(respuesta).start == 1) {
                            var contacto = JSON.parse(respuesta).contacto;
                            var tipo = JSON.parse(respuesta).tipo;
                            formulariocontacto(contacto, tipo);
                        }
                    } else
                    {
                        swal("¡Alto!", "La Ciudad es Importante!", "error");
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function formulariocontacto(contacto, tipo) {
    if (contacto != 0 && tipo != 0) {
        window.location = '/telemark/index.php/admcon/formulario?idcontacto=' + contacto + '&tipocontacto=' + tipo;
    }
}

function upchangecampo(modelo, campo, valor, id)
{
    var observaciones = "";
    if ($("#comentarios_direccion").val().length > 0) {
        observaciones = $("#comentarios_direccion").val();
    }

    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("admcon/upcontacto") ?>',
        'data': 'model=' + modelo + '&campo=' + campo + '&valor=' + valor + '&id=' + id + '&observaciones=' + observaciones,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    console.log(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Alto!", "Valide los campos por favor, ya que deben estar correctos o en blanco si no tiene", "info");
        },
        'cache': false
    });
    return false;
}

function modalReenvio(tipo, idcontacto)
{
    if (tipo != '4D') {
        $(".reenvios").fadeIn('fast');
    }
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/mensajeria")?>',
        'data': 'tipo=' + tipo + '&idcontacto=' + idcontacto,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
            $('.asignacion').fadeOut('fast');
            $('.gestionador').fadeOut('fast');
            $('.transferencia').fadeOut('fast');
        },
        'success':
                function (respuesta) {
                    if (tipo === '4D' && respuesta == '4D') {
                        swal("¡Hecho!", "Registro Enviado a 4D", "success");
                    } else if (tipo === '4D' && respuesta == 'Error') {
                        swal("¡Alto!", "No se envio a 4D", 'error');
                    } else {
                        $(".reenvios").html(respuesta);
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}


function verDetalle(input, model)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/detalle")?>',
        'data': 'input=' + input + '&model=' + model,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    generatedetalle(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}


function generatedetalle(text) {
    var n = noty({text: text,
        type: 'info',
        dismissQueue: true,
        layout: 'topLeft',
        closeWith: ['click'],
        theme: 'relax',
        maxVisible: 10,
        animation: {
            open: 'animated bounceInLeft',
            close: 'animated bounceOutLeft',
            easing: 'swing',
            speed: 500
        }
    });
    console.log('ht ml: ' + n.options.id);
}


function enviarEmail() {
    var id = $("#idcontactosend").val();
    var persona = $("#namecontra").val();

    if (id) {
        if (persona == '') {
            swal("¡Alto!", "Digite el Nombre o el Contrato del Referido. ", "error");
        } else {

            jQuery.ajax({
                'type': 'POST',
                'url': '<?= Yii::app()->createUrl("admcon/enviarreferido")?>',
                'data': "idcontacto=" + id + '&input=' + persona,
                'beforeSend': function () {
                    $('.ctospace').html($('#modalwait').html());
                },
                'success':
                        function (respuesta) {
                            console.log(respuesta);
                            if (respuesta == 'OK')
                            {
                                swal({
                                    title: " Hecho! ",
                                    text: " Enviado a todosavender@telesentinel.com",
                                    timer: 2200,
                                    showConfirmButton: false
                                });
                            } else
                            {
                                swal({
                                    title: " Alto! ",
                                    text: " No se envio el email Intentalo mas tarde ",
                                    timer: 2200,
                                    showConfirmButton: false
                                });
                            }
                        },
                'error': function (m, e, a) {
                    swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
                },
                'cache': false
            });
            return false;
        }
    } else {
        swal("¡Alto!", "No se Cargo el Contacto actualiza la Pagina...", "error");
    }
}


function enviarContacto()
{
    var form = $('#formTransfer').serialize();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/enviarcontacto")?>',
        'data': form + "&idcontacto=" + $('#idcontactosend').val(),
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {

                    if (respuesta != 'Error') {

                        swal({
                            title: "Deseas Hacer la Transferencia?",
                            text: "Luego de esto el registro sera movido de tu cuenta",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Si",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?= Yii::app()->createUrl('admcon') ?>";
                                    }
                                });

                    } else
                    {
                        swal({
                            title: " Alto! ",
                            text: " Todos los Campos son Obligatorios! ",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}




function cargarAgencias(id)
{
    if (id != "" && id != undefined) {
        jQuery.ajax({
            'type': 'POST',
            'url': '<?= Yii::app()->createUrl("admcon/agencias")?>',
            'data': 'idregional=' + id,
            'beforeSend': function () {
                $('.ctospace').html($('#modalwait').html());
            },
            'success':
                    function (respuesta) {
                        $("#div_agencia_asignada").fadeOut("fast");
                        $("#div_agencia_asignada").html(respuesta);
                        $("#div_agencia_asignada").fadeIn("fast");
                    },
            'error': function (m, e, a) {
                swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
            },
            'cache': false
        });
        return false;
    }
}

function cargarAsesor(id)
{
    if (id != "" && id != undefined) {
        jQuery.ajax({
            'type': 'POST',
            'url': '<?= Yii::app()->createUrl("admcon/asesor")?>',
            'data': 'idagencia=' + id + '&fecha=' + $("#fecha_asignada").val(),
            'beforeSend': function () {
                $('.ctospace').html($('#modalwait').html());
            },
            'success':
                    function (respuesta) {
                        $("#div_asesor_asignada").fadeOut("fast");
                        $("#div_asesor_asignada").html(respuesta);
                        $("#div_asesor_asignada").fadeIn("fast");
                    },
            'error': function (m, e, a) {
                swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
            },
            'cache': false
        });
        return false;
    }
}



function asignarAsesor()
{
    swal({
        title: "¿Deseas enviar SMS?",
        text: "Notificacion al Asesor Comercial",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "¡Claro!",
        cancelButtonText: "No, Solo Registrar",
        closeOnConfirm: false,
        closeOnCancel: false},
            function (isConfirm) {
                if (isConfirm) {
                    ajaxAsesorAsignado(1);
                } else {
                    ajaxAsesorAsignado(0);
                }
            });
}

function ajaxAsesorAsignado(sms)
{
    var form = $("#enviarcomercial").serialize();
    console.log(form);
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/asignarasesor")?>',
        'data': form + '&sms=' + sms + '&idcontacto=' + $('#idcontact').val(),
        'beforeSend': function () {

            $('.reenvios').fadeOut('fast');
            $('.gestionador').fadeOut('fast');
            $('.transferencia').fadeOut('fast');
        },
        'success':
                function (respuesta) {
                    if (respuesta == "OK")
                    {
                        swal("¡Hecho!", "Agendado", 'success');
                        location.reload();
                    } else
                    {
                        swal("¡Alto!", respuesta, 'error');
                    }
                },
        'error': function (m, e, a) {

        },
        'cache': false
    });
    return false;
}


function coincidencias()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/coincidencias")?>',
        'data': 'idcontacto=' + $('#idcontact').val(),
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
            $('.asignacion').fadeOut('fast');
            $('.reenvios').fadeOut('fast');
            $('.gestionador').fadeOut('fast');
            $('.transferencia').fadeOut('fast');
        },
        'success':
                function (respuesta) {
                    if (respuesta == 'OK') {
                        swal("¡Bien!", "Ninguna Coincidencia", "success");
                    } else {
                        swal({
                            title: "!Coincidencias!",
                            text: "<span style='text-align:justify; font-size: 9pt'>" + respuesta + "<span> ",
                            html: true
                        });
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}


function transferencia()
{
    $('.marketing').fadeOut('fast');
    $('.transferencia').fadeIn('fast');
    $('.asignacion').fadeOut('fast');
    $('.reenvios').fadeOut('fast');
    $('.gestionador').fadeOut('fast');
}

function marketing()
{
    $('.marketing').fadeIn('fast');
    $('.transferencia').fadeOut('fast');
    $('.asignacion').fadeOut('fast');
    $('.reenvios').fadeOut('fast');
    $('.gestionador').fadeOut('fast');
}

function gestion()
{
    $('.marketing').fadeOut('fast');
    $('.transferencia').fadeOut('fast');
    $('.asignacion').fadeOut('fast');
    $('.reenvios').fadeOut('fast');

}

function guardarGestion()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/guardargestion")?>',
        'data': 'idcontacto=' + $('#idcontact').val()
                + '&accion=' + $('#idestweb').val()
                + '&comentarios=' + $('#comentarios').val()
                + '&comentarios_agenda=' + $('#comentarios_agenda').val()
                + '&fecha_agendada=' + $('#fecha_agendada').val()
                + '&contrato=' + $('#cont').val()
                + '&fecha_asignada=' + $('#fecha_asignada').val(),
        'beforeSend': function () {
            $('.marketing').fadeOut('fast');
            $('.gestionador').fadeOut('fast');
            swal({
                title: "Accion en Procesos!",
                text: "Por Favor espere mientras se Guarda la Gestion.",
                timer: 2000,
                showConfirmButton: false
            });
        },
        'success':
                function (respuesta) {
                    if (respuesta == 'OK')
                    {
                        swal({
                            title: "Gestion Guardada",
                            text: "Completar Informacion",
                            timer: 2000,
                            showConfirmButton: false
                        });
                        location.reload();
                    } else
                    {
                        swal("¡Error!", respuesta, 'error');
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}


function reenviar(item)
{
    $.ajax({
        type: 'POST',
        url: '<?= Yii::app()->createUrl("admcon/reenviar")?>',
        data: {
            id: item
        },
        beforeSend: function () {
            swal({
                title: "Accion en Procesos!",
                text: "Por Favor espere conectado la Solicitud.",
                timer: 5000,
                showConfirmButton: false
            });
        },
        success:
                function (respuesta) {
                    if (respuesta == 1) {
                        swal("¡Hecho!", "Enviado", 'success');
                    } else {
                        swal("¡Error!", "Intentelo Nuevamente", 'error');
                    }
                    $(".reenvios").fadeOut('fast');
                },
        error: function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        cache: false
    });
    return false;
}

function mailchimp(op, id, titulo) {
    
    if (op == 13) {
        $('.contrato').fadeIn('fast');
    } else {
        $('.contrato').fadeOut('fast');
    }

    jQuery.ajax({
        'type': 'POST',
        'url': '<?= Yii::app()->createUrl("admcon/estweb")?>',
        'data': 'id=' + id + '&op=' + op + '&titulo=' + titulo + '&idcontacto=' + $('#idcontact').val(),
        'beforeSend': function () {
            $('.divocl').fadeOut('fast');
            $('.reagendamiento').fadeOut('fast');   
            $('.asignacion').fadeOut('fast');
            $('.gestionador').fadeOut('fast');
        },
        'success': function (respuesta) {
            $('.divocl').fadeIn('fast');
            $('.divocl').html(respuesta);
        },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function styleCss(obj) {
    var opc = obj.id.split('opc_');
    back = obj.style.background;
    $('#reagendamiento').fadeOut('fast');
    $('#gestionador').fadeOut('fast');
    $('#asignacion').fadeOut('fast');

    if (back === '') {
        obj.style.background = '#2a84c0';
        document.getElementById('idestweb').value = opc[1];
    } else {
        obj.style.background = '';
        document.getElementById('idestweb').value = '';
    }
}

function reagendamiento(obj) {
    styleCss(obj);
    $('#reagendamiento').fadeIn('fast');
}

function comercial(obj) {
    styleCss(obj);
    $('#asignacion').fadeIn('fast');
}

function gestionador(obj) {
    styleCss(obj);
    $('#gestionador').fadeIn('fast');
}