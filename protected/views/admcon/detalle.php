<div style="height: 400px; overflow: auto; font-size: 8pt">
    <table class="table table-bordered">
        <tr class="danger"><th>ID</th><th>RAZON SOCIAL</th><th>CONTRATO</th><th>AGENCIA</th><th>ASESOR</th><th>FECHA CREACION</th><th>TELEMERCADERISTA</th></tr>
        <?php 
            $razon = '';
            $contrato = '';
            $agencia = '';
            $asesor = '';
            $fecha = '';
            $telemercaderista = "";
            
            foreach ($registros as $r) : 
                
                if($model == 'Cliente') :
                    $razon = $r->Razon_social;
                    $contrato = $r->Contrato;
                    $agencia = $r->agencia->Nombre;
                    $asesor = $r->asesor->Nombre;    
                    $fecha = $r->Fecha;
                endif;
                if($model == 'Contacto') :
                    $razon = $r->Razon_social;
                    $contrato = $r->Contrato;
                    $agencia = $r->agencia->Nombre;
                    $asesor = $r->asesor->Nombre;    
                    $fecha = $r->Fecha_creacion;
                    $telemercaderista = $r->telemercaderista->Nombre;
                endif;
                if($model == 'PersonaContacto') :
                    $razon = $r->Nombre_completo;
                    $fecha = $r->Fecha_creacion;
                    $contrato = $r->Contacto;
                    $telemercaderista = $r->contacto->telemercaderista->Nombre;
                    $agencia = $r->contacto->agencia->Nombre;
                    $asesor = $r->contacto->asesor->Nombre;    
                endif;
        ?>
            <tr>
                <td><?= $r->ID ?></td>
                <td><?= $razon ?> </td>
                <td><?= $contrato ?> </td>
                <td><?= $agencia ?> </td>
                <td><?= $asesor ?> </td>
                <td><?= $fecha ?> </td>
                <td><?= $telemercaderista ?> </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>