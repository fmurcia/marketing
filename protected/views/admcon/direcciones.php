<table class="table" style="width:100%">
    <tr>
        <td style="width: 70%">
            <input type="text" class="form-control" placeholder="Direccion..." id="armardir" name="armardir" readonly="readonly" style="font-size: 14pt">
        </td>
        <td><center><?php
        $this->widget(
                'booster.widgets.TbButtonGroup', array(
            'buttons' => array(
                array(
                    'buttonType' => 'button',
                    'url' => '#',
                    'context' => 'danger',
                    'icon' => 'trash',
                    'htmlOptions' => array(
                        'onclick' => "inputChange('del')"
                    )
                ),
                array(
                    'buttonType' => 'button',
                    'url' => '#',
                    'context' => 'info',
                    'icon' => 'backward',
                    'htmlOptions' => array(
                        'onclick' => "inputChange('ret')"
                    )
                ),
            )
                )
        );
        ?></center></td></tr>
        <tr>
            <td style="width: 70%">
                <input type="text" class="form-control" placeholder="Direccion auxiliar..." id="armardiraux" name="armardiraux" maxlength="100" style="font-size: 12pt" onkeyup="this.value = this.value.toUpperCase()">
            </td>
            <td>
                Max. 100 Caracteres.
                Solo...Direccion Compleja!
            </td>    
        </tr>
<tr><td colspan="2" style="text-align:center" class="danger">LOS MAS USADOS</td></tr>
<tr><td colspan="2"><center>
    <?php
    $this->widget(
            'booster.widgets.TbButtonGroup', array(
        'justified' => true,
        'buttons' => array(
            array('label' => '#', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('#', 'armardir')")),
            array('label' => '.', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('.', 'armardir')")),
            array('label' => '-', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('-', 'armardir')")),
            array('label' => 'BIS', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BIS', 'armardir')")),
        )
            )
    );

    $this->widget(
            'booster.widgets.TbButtonGroup', array(
        'justified' => true,
        'buttons' => array(
            array('label' => 'Apartamento', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('APARTAMENTO', 'armardir')")),
            array('label' => 'Avenida', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AVENIDA', 'armardir')")),
            array('label' => 'Autopista', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AUTOPISTA', 'armardir')")),
            array('label' => 'Barrio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BARRIO', 'armardir')")),
        )
            )
    );

    $this->widget(
            'booster.widgets.TbButtonGroup', array(
        'justified' => true,
        'buttons' => array(
            array('label' => 'Calle', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CALLE', 'armardir')")),
            array('label' => 'Carrera', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CARRERA', 'armardir')")),
            array('label' => 'Diagonal', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('DIAGONAL', 'armardir')")),
            array('label' => 'Edificio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('EDIFICIO', 'armardir')")),
        )
            )
    );

    $this->widget(
            'booster.widgets.TbButtonGroup', array(
        'justified' => true,
        'buttons' => array(
            array('label' => 'Norte', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('NORTE', 'armardir')")),
            array('label' => 'Sector', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SECTOR', 'armardir')")),
            array('label' => 'Sur', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SUR', 'armardir')")),
            array('label' => 'Transversal', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('TRANSVERSAL', 'armardir')")),
        )
            )
    );
    ?></center>
</td>
</tr>
<tr>
    <td colspan="2">
        <div class="panel-group" id="accordion" style="font-size:8pt">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="text-align: center" onclick="mostrarNomenclatura()">
                            <center><font size="2pt">NOMENCLATURA</font></center>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div id="nomenclatura"></div>
                    </div>
                </div>
            </div>
        </div>    

    </td>
</tr>
<tr><td colspan="2" style="text-align:center" class="danger">LETRAS</td></tr>
<tr><td colspan="2"><center>
    <?php
    $this->widget(
            'booster.widgets.TbButtonGroup', array(
        'justified' => true,
        'buttons' => array(
            array('label' => 'A', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('A', 'armardir')")),
            array('label' => 'B', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('B', 'armardir')")),
            array('label' => 'C', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('C', 'armardir')")),
            array('label' => 'D', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('D', 'armardir')")),
            array('label' => 'E', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('E', 'armardir')")),
            array('label' => 'F', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('F', 'armardir')")),
            array('label' => 'G', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('G', 'armardir')")),
            array('label' => 'H', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('H', 'armardir')")),
            array('label' => 'I', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('I', 'armardir')")),
            array('label' => 'J', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('J', 'armardir')")),
            array('label' => 'K', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('K', 'armardir')")),
            array('label' => 'L', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('L', 'armardir')")),
            array('label' => 'M', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('M', 'armardir')")),
        )
            )
    );
    $this->widget(
            'booster.widgets.TbButtonGroup', array(
        'justified' => true,
        'buttons' => array(
            array('label' => 'N', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('N', 'armardir')")),
            array('label' => 'O', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('O', 'armardir')")),
            array('label' => 'P', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('P', 'armardir')")),
            array('label' => 'Q', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('Q', 'armardir')")),
            array('label' => 'R', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('R', 'armardir')")),
            array('label' => 'S', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('S', 'armardir')")),
            array('label' => 'T', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('T', 'armardir')")),
            array('label' => 'U', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('U', 'armardir')")),
            array('label' => 'V', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('V', 'armardir')")),
            array('label' => 'W', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('W', 'armardir')")),
            array('label' => 'X', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('X', 'armardir')")),
            array('label' => 'Y', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('Y', 'armardir')")),
            array('label' => 'Z', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('Z', 'armardir')")),
        )
            )
    );
    ?></center>
</td>
</tr>
<tr><td colspan="2" style="text-align:center" class="danger">NUMEROS</td></tr>
<tr><td colspan="2"><center>
    <?php
    $this->widget(
            'booster.widgets.TbButtonGroup', array(
        'buttons' => array(
            array('label' => '<font size="12pt">0</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('0', 'armardir')")),
            array('label' => '<font size="12pt">1</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('1', 'armardir')")),
            array('label' => '<font size="12pt">2</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('2', 'armardir')")),
            array('label' => '<font size="12pt">3</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('3', 'armardir')")),
            array('label' => '<font size="12pt">4</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('4', 'armardir')")),
            array('label' => '<font size="12pt">5</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('5', 'armardir')")),
            array('label' => '<font size="12pt">6</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('6', 'armardir')")),
            array('label' => '<font size="12pt">7</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('7', 'armardir')")),
            array('label' => '<font size="12pt">8</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('8', 'armardir')")),
            array('label' => '<font size="12pt">9</font>', 'url' => '#', 'encodeLabel' => false, 'htmlOptions' => array('onclick' => "concatenaNumero('9', 'armardir')")),
        )
            )
    );
    ?></center>
</td>
</tr>
<tr><td colspan="2" style="text-align:center" class="danger">SITIOS CERCANOS</td></tr>
<tr><td colspan="2"><center><textarea style="width:100%;" id="observaciones_direccion" placeholder="Ej. Cerca a .... Al frente de .... "></textarea></center></td></tr>
</table>

<input name="retorno" id="retorno" value="null" type="hidden" >
<input name="controlaLetras" id="controlaLetras" value="0" type="hidden" >
<center>
    <?php
    $this->widget(
            'booster.widgets.TbButton', array(
        'label' => 'Enviar',
        'context' => 'success',
        'htmlOptions' => array('onclick' => "inputChange('env',  '$tipo')")
            )
    );
    ?>
</center>