<div class="form">
    <?php
    $form = $this->beginWidget(
            'booster.widgets.TbActiveForm', array(
                'id' => 'contact-form',
                'type' => 'vertical',
                 'action' => 'javascript:void(0)'
            )
    );
    ?>
    <input name="idcontactosend" id="idcontactosend" type="hidden" value="<?= $model->ID ?>" />
    <input name="comentarios_direccion" id="comentarios_direccion" type="hidden" value="" />

    <fieldset>
        <?php if ($model->hasErrors()) { ?>
            <div class="col-lg-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        Se encontraron errores en algunos campos, revise lo errores marcados en rojo
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        <?php } ?>

        <?php if (!$model->isNewRecord) { ?>
            <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        ANTIGUEDAD: <?= $model->getAntiguedad() ?>
                    </div>
                </div>
            </div>
        <?php } ?>        

        <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <?php
                    if($model->Asesor == 1 || $model->Asesor == 60) :
                        $asesor = strtoupper("SIN ASESOR");
                    else :
                        $asesor = strtoupper($model->asesor->Nombre);
                    endif;
                    ?>
                    Asesor Comercial : <?=  $asesor ?>
                </div>
            </div>
        </div>    
            
        <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    Ult. Comentario : <?= strtolower($model->Observaciones) ?>
                </div>
            </div>
        </div>    

        <div class="clearfix"></div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="well">
                <h4>Información Basica</h4>               
                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?= $form->textFieldGroup($model, 'Contrato', array('widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Contacto', 'Contrato', this.value, $model->ID)")))); ?>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?= $form->textFieldGroup($model, 'Razon_social', array('size' => 60, 'maxlength' => 500, 'widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Contacto', 'Razon_social', this.value, $model->ID)")))); ?>
                </div>

                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?= $form->textFieldGroup($model, 'Nombre_completo', array('size' => 60, 'maxlength' => 500, 'widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Contacto', 'Nombre_completo', this.value, $model->ID)")))); ?>
                </div>

                <div class="clearfix"></div>

                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?= $form->textFieldGroup($model, 'Nit', array('size' => 60, 'maxlength' => 100, 'widgetOptions' => array('htmlOptions' => array('onblur' => "upchangecampo('Contacto', 'Nit', this.value, $model->ID)")))); ?>
                </div>

                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?=
                    $form->select2Group($model, 'Actividad', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getActividadeconomica(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "onchange" => "upchangecampo('Contacto', 'Actividad', this.value, $model->ID)"
                            ),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque la actividad',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?=
                    $form->select2Group($model, 'Tipo', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getTipoCliente(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "onchange" => "upchangecampo('Contacto', 'Tipo', this.value, $model->ID)"
                            ),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque la actividad',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>

                <div class="clearfix"></div>

                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?=
                    $form->select2Group($model, 'Ciudad', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getCiudades(),
                            'htmlOptions' => array(
                                "onchange" => "upchangecampo('Contacto', 'Ciudad', this.value, $model->ID)"
                            ),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque la ciudad',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?=
                    $form->select2Group($model, 'Barrio', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getBarrios(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "onchange" => "upchangecampo('Contacto', 'Barrio', this.value, $model->ID)"
                            ),
                            'options' => array(
                                'placeholder' => 'Seleccione o busque el barrio',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
                    <?=
                    $form->textFieldGroup($model, 'Direccion', array(
                        "widgetOptions" => array(
                            "htmlOptions" => array(
                                "readonly" => true,
                            ),
                        ),
                        "append" => "<span class='input-group-btn'><button class='btn btn-default' onclick=\"inputdireccion('contact-form', 'Contacto_Direccion', 'actualizar')\" /><span class='fa fa-refresh'></span> Dir </button></span>",
                        "appendOptions" => array(
                            "class" => "input-group-btn",
                            "isRaw" => true
                        )
                    ));
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                    <?=
                    $form->textFieldGroup($model, 'Telefono', array(
                        'maxlength' => 7,
                        "widgetOptions" => array(
                            "htmlOptions" => array('onblur' => "upchangecampo('Contacto', 'Telefono', this.value, $model->ID)")
                        ),
                    ));
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                    <?=
                    $form->textFieldGroup($model, 'Celular', array(
                        'maxlength' => 10,
                        "widgetOptions" => array(
                            "htmlOptions" => array('onblur' => "upchangecampo('Contacto', 'Celular', this.value, $model->ID)")
                        ),
                    ));
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                    <?=
                    $form->textFieldGroup($model, 'Email', array(
                        'size' => 60,
                        'maxlength' => 200,
                        "widgetOptions" => array(
                            "htmlOptions" => array('onblur' => "upchangecampo('Contacto', 'Email', this.value, $model->ID)")
                        )
                    ));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
            <div class="well">
                <h4>Encuesta de Interes</h4>
                <?= $this->renderPartial('servlist', array("model" => $model)) ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
            <div class="well">
                <h4>Estado Actual del Contacto</h4>
                <br />
                <div class="col-lg-6">
                    <?=
                    $form->switchGroup($model, 'Cliente_actual', array(
                        "widgetOptions" => array(
                            'events' => array(
                                'switchChange' => 'js:function(event, state) {
                                       var values = 0;
                                       if(state) { values = 1; }
                                       else{ values = 0; }
                                       upchangecampo("Contacto", "Cliente_actual", values, "' . $model->ID . '")
                                    }'
                            ),
                            'options' => array(
                                "onText" => "Si",
                                "offText" => "No",
                                "wrapperClass" => "wrapper swajuste",
                                "handleWidth" => "auto",
                            ),
                            "htmlOptions" => array(
                                "style" => "width:100%"
                            )
                        )
                    ));
                    ?>
                </div>    
                <div class="col-lg-6">
                    <?=
                    $form->switchGroup($model, 'Bloqueado', array(
                        "widgetOptions" => array(
                            'events' => array(
                                'switchChange' => 'js:function(event, state) {
                                       var values = 0;
                                       if(state) { values = 1; }
                                       else{ values = 0; }
                                       upchangecampo("Contacto", "Bloqueado", values, "' . $model->ID . '")
                                    }'
                            ),
                            'options' => array(
                                "onText" => "Si",
                                "offText" => "No",
                                "wrapperClass" => "wrapper",
                                "handleWidth" => "auto",
                            ),
                            "htmlOptions" => array(
                                "style" => "width:50%"
                            )
                        )
                    ));
                    ?>
                </div>                
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
            <div class="well">
                <h4>Medio de Contacto / Planes </h4>         
                <br />
                <div class="col-lg-6">
                    <?php
                    //Medios de Contactos no permitidos para campañas web
                    $inactivos = array(3,5);
                    if (!in_array($model->Tipo_contacto, $inactivos)) :
                        echo $form->select2Group($model, 'Medio_contacto', array(
                            'widgetOptions' => array(
                                'asDropDownList' => true,
                                "data" => $model->getMedioscontacto($model),
                                "htmlOptions" => array(
                                    "class" => "form-control",
                                    "onchange" => "upchangecampo('Contacto', 'Medio_contacto', this.value, $model->ID)"
                                ),
                                'options' => array(
                                    'placeholder' => 'Medio de contacto',
                                    'width' => '100%'
                                )
                            )
                        ));
                    endif;
                    ?>
                </div>
                <div class="col-lg-6">
                    <?php
                    echo $form->select2Group($model, 'ID_plan', array(
                        'widgetOptions' => array(
                            'asDropDownList' => true,
                            "data" => $model->getPlanes(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "onchange" => "upchangecampo('Contacto', 'ID_plan', this.value, $model->ID)"
                            ),
                            'options' => array(
                                'placeholder' => 'Planes Interesados',
                                'width' => '100%'
                            )
                        )
                    ));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>    
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12 col-md-12 col-sx-12 col-xs-12 space">
            <div class="well">
                <div class="col-lg-9"><h4>Encargados</h4></div><div class="col-lg-3">
                    <?php
                    $this->widget(
                            'booster.widgets.TbButton', array(
                        "id" => "addenc",
                        'icon' => "minus",
                        'context' => 'info',
                        'htmlOptions' => array(
                            "class" => "pull-right",
                            "id" => "delenc",
                            "onclick" => "delEncargado()"
                        )
                            )
                    );
                    $this->widget(
                            'booster.widgets.TbButton', array(
                        'icon' => "plus",
                        'context' => 'info',
                        'htmlOptions' => array(
                            "class" => "pull-right",
                            "id" => "addenc",
                            "onclick" => "addEncargado()"
                        )
                            )
                    );
                    ?>
                    <div  class="pull-right" style="padding:3px;margin-right: 4px;font-size:1.4em;display:none" id="justwait">
                        <span class="glyphicon glyphicon-transfer"></span>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="encargados">
                    <?php $this->renderPartial('encargados', array("form" => $form, "model" => $model)) ?>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </fieldset>
<?php $this->endWidget(); ?>
</div>