<?php
$this->widget(
        'booster.widgets.TbSelect2', array(
    'asDropDownList' => true,
    'name' => 'agencia',
    'data' => Contacto::model()->getAgenciasRegional($idregional),
    'options' => array(
        'placeholder' => 'Selecciona Agencia',
        'width' => '100%',
    ),
    "htmlOptions" => array(
        "class" => "form-control",
        "id" => "AgenciaAsignada",
    ),
        )
);
