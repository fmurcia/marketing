<div class="moduladorpag">
    <?php
    if (isset($model->Tipo_contacto)) :
        if ($model->Tipo_contacto == 1) :
            $titulo = "LLAMADA DE ENTRADA";
            $icono = "fa fa-phone fa-2x";
        elseif ($model->Tipo_contacto == 2) :
            $titulo = "BASE FRIA";
            $icono = "fa fa-folder-open fa-2x";
        elseif ($model->Tipo_contacto == 3) :
            $titulo = "CAMPAÑA WEB";
            $icono = "fa fa-globe fa-2x";
        elseif ($model->Tipo_contacto == 4) :
            $titulo = "CLIENTE ACTIVO";
            $icono = "fa fa-user fa-2x";
        elseif ($model->Tipo_contacto == 5) :
            $titulo = "CLIENTE CHAT";
            $icono = "fa fa-wechat fa-2x";
        else :
            $titulo = "CONTACTO DESCARTADO";
            $icono = "fa fa-warning fa-2x";
        endif;

        if ($model->Oportunidad != 0) :
            $contextoop = 'primary';
            $iconoop = 'fa fa-users fa-2x';
            $tituloop = 'OPORTUNIDAD TRIANGULO';
        else :
            $contextoop = 'default';
            $iconoop = 'fa fa-signal fa-2x';
            $tituloop = 'DIRECTO';
        endif;

        $contexto = $model->tipoContacto->Contexto;
        ?>
        <input name="idcontact" id="idcontact" type="hidden" value="<?= $model->ID ?>" />
        <div class="col-lg-3 col-md-3 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contexto ?>">
                <div class="panel-heading">
                    <span class="<?= $icono ?>"></span>
                    <?= $titulo ?> 
                </div>
            </div>
        </div>    
        <div class="col-lg-3 col-md-3 col-sx-12 col-xs-12">
            <div class="panel panel-<?= $contextoop ?>">
                <div class="panel-heading">
                    <span class="<?= $iconoop ?>"></span>
                    <?= $tituloop ?> 
                </div>
            </div>
        </div>   
        <div class="clearfix"></div>
        <div class="col-lg-8 col-md-8 col-sx-12 col-xs-12">   
            <?php
            $this->widget(
                    'booster.widgets.TbMenu', array(
                'type' => 'navbar',
                'items' => array(
                    array('label' => 'Gestionar Contacto', 'url' => '#', 'icon' => 'fa fa-edit fa-2x', 'itemOptions' => array('onclick' => 'gestion()')),
                    array('label' => 'Reenvios', 'url' => '#', 'icon' => 'fa fa-random fa-2x', 'items' =>
                        array(
                            array("label" => "SMS", 'url' => '#', 'itemOptions' => array('onclick' => 'modalReenvio("SMS", "' . $model->ID . '")')),
                            array("label" => "EMAIL", 'url' => '#', 'itemOptions' => array('onclick' => 'modalReenvio("EMAIL", "' . $model->ID . '")')),
                            array("label" => "4D", 'url' => '#', 'itemOptions' => array('onclick' => 'modalReenvio("4D", "' . $model->ID . '")')),
                        )
                    ),
                    array('label' => 'Transferencia', 'url' => '#', 'icon' => 'fa fa-refresh fa-2x', 'itemOptions' => array('onclick' => 'transferencia()')),
                    array('label' => 'Asignar Asesor', 'url' => '#', 'icon' => 'fa fa-send-o fa-2x', 'itemOptions' => array('onclick' => 'comercial()')),
                    array('label' => 'Coincidencias!!!', 'url' => '#', 'icon' => 'fa fa-cubes fa-2x', 'itemOptions' => array('onclick' => 'coincidencias()')),
                ),
                    )
            );
            ?>
        </div>    
        <div class="col-lg-4 col-md-4 col-sx-12 col-xs-12">
            <div class="col-lg-5 col-md-5 col-sx-12 col-xs-12" style="text-align:right">Navegador de Paginas:</div>
            <div class="col-lg-7 col-md-7 col-sx-12 col-xs-12">
                <select id="ctopages" class="form-control" onchange="paginaCta(this.value, <?= $block ?>)">
                    <?php for ($i = 1; $i < $pages; $i++) : ?>
                        <option <?= ($pag == $i) ? 'Selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="gestionador" style="display:none">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12 ">
                <div class="row">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" onclick="$('.gestionador').fadeOut('fast');">&times;</a>
                        <h4>SEGUIMIENTO</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $form = $this->beginWidget(
                                'booster.widgets.TbActiveForm', array(
                            'id' => 'formGestion',
                            'enableAjaxValidation' => false,
                            'action' => "javascript:void(0)",
                        ));
                        ?>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">ACCION </div>
                        <div class="col-lg-9 col-md-9 col-xs-12 col-sx-12">
                            <?php
                            $this->widget(
                                    'booster.widgets.TbSelect2', array(
                                "asDropDownList" => true,
                                "name" => 'accion',
                                "data" => Contacto::model()->getAcciones(),
                                "htmlOptions" => array(
                                    "class" => "form-control",
                                    "style" => "font-size: 9pt",
                                    "placeholder" => '...Seleccione...',
                                    "onchange" => "changeaccion(this.value)"
                                ),
                                "options" => array(
                                    'width' => '100%'
                                ))
                            );
                            ?>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">MARKETING </div>
                        <div class="col-lg-9 col-md-9 col-xs-12 col-sx-12">
                            <?=
                            $form->switchGroup($model, 'TipoDescartado', array(
                                "label" => '',
                                "widgetOptions" => array(
                                    'events' => array(
                                        'switchChange' => 'js:function(event, state) {
                                       var values = 0;
                                       if(state) { values = 16; }
                                       else{ values = 0; }
                                       upchangecampo("Contacto", "TipoDescartado", values, "' . $model->ID . '")
                                    }'
                                    ),
                                    'options' => array(
                                        "onText" => "Si",
                                        "offText" => "No",
                                        "wrapperClass" => "wrapper swajuste",
                                        "handleWidth" => "auto",
                                    ),
                                    "htmlOptions" => array(
                                        "style" => "width:100%",
                                    )
                                )
                            ));
                            ?>
                        </div>

                        <div id="motivodescartado" style="display:none">
                            <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">MOTIVO </div>
                            <div class="col-lg-9 col-md-9 col-xs-12 col-sx-12">
                                <?php
                                $this->widget(
                                        'booster.widgets.TbSelect2', array(
                                    'asDropDownList' => true,
                                    'name' => 'descartada',
                                    "data" => Contacto::model()->getAccionesDescartadas(),
                                    "htmlOptions" => array(
                                        "class" => "form-control",
                                        "style" => "font-size: 9pt",
                                        "placeholder" => '...Seleccione...',
                                    ),
                                    'options' => array(
                                        'width' => '100%',
                                        "style" => "width:100%",
                                    ))
                                );
                                ?>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">AGENDA </div>
                        <div class="col-lg-9 col-md-9 col-xs-12 col-sx-12">
                            <center>  
                                <div class="input-group">
                                    <br>

                                    <span class="btn input-group-addon" style="cursor:pointer"  id="calendar">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                    <input id="fecha_gestion" style="text-align: center;"  type="hidden" name="fecha_gestion" value="<?= date('Y-m-d H') ?>" class="form-control" readonly="true"/>

                                </div>
                            </center>
                            <?php
                            $script = '$("#calendar").datetimepicker({
                                 inline: true,
                                 timepicker: true,
                                 format: "Y-m-d H",
                                 onChangeDateTime: function (dp, $input) {
                                     $("#fecha_gestion").val($input.val());
                                 }
                             })';
                            Yii::app()->clientScript->registerScript(uniqid(), $script);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php if ($model->Estado_proceso == 22) : ?>

                            <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">ENCUESTA </div>
                            <div class="col-lg-9 col-md-9 col-xs-12 col-sx-12">
                                <h4></h4>
                                <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12">El Asesor asistio Puntualmente a la Cita : </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                                    Si <input type="radio" name="puntualidadop1" value="1"  checked="true">
                                    No <input type="radio" name="puntualidadop1" value="0">
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-8 col-md-8 col-xs-12 col-sx-12">El Asesor fue Claro con la Informacion Comercial : </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 col-sx-12">
                                    Si <input type="radio" name="veracidadop1" value="1"  checked="true">
                                    No <input type="radio" name="veracidadop1" value="0">
                                </div>
                            </div>
                            <br>
                            <?php
                        endif;
                        ?>
                        <div class="clearfix"></div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">COMENTARIOS </div>
                        <div class="col-lg-9 col-md-9 col-xs-12 col-sx-12">
                            <?php
                            echo $form->textAreaGroup(
                                    $model, 'Observaciones', array(
                                "label" => '',
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'htmlOptions' => array('rows' => 5, 'onkeyup' => 'this.value = this.value.toUpperCase()'),
                                )
                                    )
                            );
                            ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                                <?php
                                $this->widget(
                                        'booster.widgets.TbButton', array(
                                    'icon' => "download",
                                    'label' => " Guardar ",
                                    'context' => 'success',
                                    'htmlOptions' => array(
                                        "onclick" => "guardarGestion()",
                                        "class" => "pull-right"
                                    )
                                        )
                                );
                                ?>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                        <script type="text/javascript">
                            function changeaccion(id)
                            {
                                if (id == 11)
                                {
                                    $("#motivodescartado").fadeIn('fast');
                                } else
                                {
                                    $("#motivodescartado").fadeOut('fast');
                                }
                            }
                        </script>                   
                    </div>
                </div>
            </div>
        </div>

        <div class="transferencia"  style="display:none">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12 ">
                <div class="row">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" onclick="$('.transferencia').fadeOut('fast');">&times;</a>
                        <h4>TRANSFERENCIA</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $formu = $this->beginWidget(
                                'booster.widgets.TbActiveForm', array(
                            'id' => 'formTransfer',
                            'enableAjaxValidation' => false,
                            'action' => "javascript:void(0)"
                        ));
                        ?>
                        <h4>Comercial</h4>
                        <?php
                        $this->widget(
                                'booster.widgets.TbSelect2', array(
                            'asDropDownList' => true,
                            'name' => 'comercial',
                            "data" => Asesor::model()->getTelemercaderistas(),
                            "htmlOptions" => array(
                                "class" => "form-control",
                                "style" => "font-size: 9pt",
                                "placeholder" => '...Seleccione...',
                            ),
                            'options' => array(
                                'width' => '100%',
                                "style" => "width:100%",
                                'required' => true,
                            ))
                        );
                        ?>

                        <h4>Comentarios detallados</h4>
                        <textarea class="form-control" name="comentarios" required ></textarea>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-7 col-md-7 col-xs-12 col-sx-12">
                            <?php
                            $this->widget(
                                    'booster.widgets.TbButton', array(
                                'label' => 'Enviar',
                                'context' => 'success',
                                'htmlOptions' => array(
                                    "class" => "pull-right",
                                    'onclick' => "enviarContacto()"
                                )
                                    )
                            );
                            ?>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <div class="asignacion" style="display:none">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12 ">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal" onclick="$('.asignacion').fadeOut('fast');">&times;</a>
                    <h4>ASIGNACION COMERCIAL</h4>
                </div>
                <div class="modal-body">
                    <?php
                    $formas = $this->beginWidget(
                            'booster.widgets.TbActiveForm', array(
                        'id' => 'enviarcomercial',
                        'type' => 'vertical',
                        'action' => "javascript:void(0)",
                            )
                    );
                    ?>
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">CIUDAD </div>
                    <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                        <?=
                        $formas->select2Group(
                                $model, 'Ciudad', array(
                            "label" => '',
                            'widgetOptions' => array(
                                'data' => Contacto::model()->getRegionales(),
                                'asDropDownList' => true,
                                'options' => array(
                                    'placeholder' => 'Selecciona Regional',
                                ),
                                "htmlOptions" => array(
                                    "class" => "form-control",
                                    "onchange" => "cargarAgencias(this.value)",
                                    "id" => "Contacto_Ciudad_1",
                                ),
                            )
                                )
                        );
                        ?>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">AGENCIA COMERCIAL </div>
                    <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                        <div id="div_agencia_asignada">
                            <input type="text" class="form-control" disabled="true" placeholder="..Seleccione.."/>
                        </div>
                        <br>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">FECHA Y HORA </div>
                    <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                        <center>
                            <div class="input-group">
                                <br>
                                <span class="btn input-group-addon" style="cursor:pointer"  id="calendarco">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </span>
                                <input id="fecha_asignada_con" style="text-align: center;"  type="hidden" name="Fecha_asignada" value="<?= date('Y-m-d H') ?>" class="form-control" readonly="true"/>
                            </div>
                            <?php
                            $script = '$("#calendarco").datetimepicker({
                                    inline: true,
                                    timepicker: true,
                                    format: "Y-m-d H",
                                    onChangeDateTime: function (dp, $input) {
                                        $("#fecha_asignada_con").val($input.val());
                                        cargarAsesor($("#AgenciaAsignada").val());
                                    }
                                })';
                            Yii::app()->clientScript->registerScript(uniqid(), $script);
                            ?>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">ASESOR COMERCIAL </div>
                    <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                        <div id="div_asesor_asignada">
                            <input type="text" class="form-control" disabled="true" placeholder="..Seleccione..">
                        </div>
                        <br>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sx-12">OBSERVACIONES </div>
                    <div class="col-lg-9 col-md-9 col-sx-12 col-xs-12">
                        <?=
                        $form->textAreaGroup(
                                $model, 'Observaciones', array(
                            "label" => '',
                            'widgetOptions' => array(
                                'htmlOptions' => array('rows' => 2, 'style' => 'width:100%', 'value' => ''),
                                'placeholder' => 'Observaciones de algun Tipo para el Envio'
                            )
                                )
                        );
                        ?>
                    </div>
                    <?php
                    $this->endWidget();
                    ?>
                </div>
                <div class="modal-footer">      
                    <div class="col-lg-5 col-lg-offset-3 col-sx-12 col-xs-12">
                        <?php
                        $this->widget('booster.widgets.TbButton', array('label' => 'Asignar', 'htmlOptions' => array('class' => 'btn btn-success', 'onclick' => "asignarAsesor()")));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class = "reenvios" style = "display:none"></div>

        <div class = "col-lg-12 col-md-12 col-sx-12 col-xs-12" id = "paginadorcot" style="height: 800px">
            <?php $this->renderPartial('modulo', array('model' => $model)); ?>
        </div>
        <?php
    endif;
    ?>
</div>