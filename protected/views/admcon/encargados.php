<?php
$encargados = PersonaContacto::model()->findAll('Contacto = ' . $model->ID);

if ($encargados != null) :
    $counter = 0;
    foreach ($encargados as $encargado) :
        $this->renderPartial("encargado", array("encargado" => $encargado, "form" => $form, "model" => $model, "counter" => $counter));
        $counter++;
    endforeach;
else :
    ?>
    <div class="col-lg-12">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <span class="fa fa-unlock fa-2x"></span>
                <strong>Warning!</strong> No hay Registros configurados en la plataforma
            </div>
        </div>
    </div>
<?php
endif;
?>
<div class="clearfix"></div>