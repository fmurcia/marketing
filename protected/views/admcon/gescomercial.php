<?php
$cot = GrupoCotizacion::model()->findByAttributes(array('ID_Contacto' => $model->ID));
$info = array();
$cotizas = array();
if ($cot != NULL) :
    $cotiza = Cotizacion::model()->findByAttributes(array('ID_Grupo' => $cot->ID));
    if ($cotiza != null) :
        $detalle = DetalleCotizacion::model()->findAll('ID_Cotizacion = ' . $cotiza->ID);
        if ($detalle != null) :
            $i = 0;
            $j = 0;
            $z = 0;
            foreach ($detalle as $d) :
                $elemento = '';
                if ($d->Codigo != NULL) :
                    $element = Elemento::model()->findByAttributes(array('Codigo' => $d->Codigo));
                    if ($element != null) :
                        $elemento = $element->Nombre;
                    endif;
                endif;

                if ($d->Clasificacion == 'Elemento') :
                    $negmodel = Negociacion::model()->findByPk($d->ID_Negociacion);
                    $negociacion = '';

                    if ($negmodel != null) :
                        $negociacion = $negmodel->Nombre;
                    endif;
                    $info['equipos'][$i]["elementos"] = $elemento;
                    $info['equipos'][$i]["negociacion"] = $negociacion;
                    $info['equipos'][$i]["plazo"] = $d->Cuotas;
                    $info['equipos'][$i]["cantidad"] = $d->Cantidad;
                    $info['equipos'][$i]["valor"] = $d->Valor_venta;
                    $i++;
                endif;
                if ($d->Clasificacion == 'Servicio') :
                    $info['servicio'][$z]["elementos"] = $elemento;
                    $info['servicio'][$z]["valor"] = $d->Valor_venta;
                    $z++;
                endif;
                if ($d->Clasificacion == 'Instalacion') :
                    $info['instalacion'][$j]["elementos"] = $elemento;
                    $info['instalacion'][$j]["cantidad"] = $d->Cantidad;
                    $info['instalacion'][$j]["valor"] = $d->Valor_venta;
                    $j++;
                endif;
            endforeach;
        endif;
        ?>
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="well">
                    <h4>EQUIPOS</h4>
                    <table class="table table-condensed table-striped table-bordered">
                        <tr>
                            <th style="text-align: center">Elemento</th> 
                            <th style="text-align: center">Negociacion</th> 
                            <th style="text-align: center">Plazo</th>
                            <th style="text-align: center">Cantidad</th>
                            <th style="text-align: center">Valor Total</th>
                        </tr>
                        <?php
                        if (isset($info['equipos'])) :
                            foreach ($info['equipos'] as $key) :
                                ?>
                                <tr>
                                    <td><?= $key['elementos'] ?></td>
                                    <td><?= $key['negociacion'] ?></td>
                                    <td style="text-align: center"><?= $key['plazo'] ?></td>
                                    <td style="text-align: center"><?= $key['cantidad'] ?></td>
                                    <td style="text-align: right">$<?= number_format($key['valor']) ?></td>
                                </tr>    
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </table>            
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="well">
                    <h4>INSTALACION</h4>               
                    <table class="table table-condensed table-striped table-bordered">
                        <tr>
                            <th style="text-align: center">Elemento</th>
                            <th style="text-align: center">Cantidad</th>
                            <th style="text-align: center">Valor Total</th>
                        </tr>    
                        <?php
                        if (isset($info['instalacion'])) :
                            foreach ($info['instalacion'] as $key) :
                                ?>
                                <tr>
                                    <td><?= $key['elementos'] ?></td> 
                                    <td style="text-align: center"><?= $key['cantidad'] ?></td>
                                    <td style="text-align: right">$<?= number_format($key['valor']) ?></td>
                                </tr>    
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </table>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="well">
                    <h4>SERVICIOS</h4>               
                    <table class="table table-condensed table-striped table-bordered">
                        <tr>
                            <th style="text-align: center">Elemento</th>
                            <th style="text-align: center">Valor Total</th>
                        </tr>    
                        <?php
                        if (isset($info['servicio'])) :
                            foreach ($info['servicio'] as $key) :
                                ?>
                                <tr>
                                    <td><?= $key['elementos'] ?></td> 
                                    <td style="text-align: right">$<?= number_format($key['valor']) ?></td>
                                </tr>    
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    else :
        ?>
        <div class = "alert alert-warning" style = "text-align: center">
            <b>Sin Cotizaciones</b>
        </div>
    <?php
    endif;
else :
    ?>
    <div class = "alert alert-info" style = "text-align: center">
        <b>SIN COTIZACIONES!!!.....</b>
    </div>
            <?php
endif;