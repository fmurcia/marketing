<?php
$contactoestado = ContactoEstado::model()->getEstadoWeb($idcontacto);
?>
<br />
<table class="table table-responsive">
    <tr class="info">
        <th colspan="2" style="text-align: center; font-size: 15pt"><?= $titulo ?></th>
    </tr>
    <tr class="btn-primary">
        <th style="text-align: center">MAIL</th>
        <th style="text-align: center">ESTADO</th>
    </tr>
    <?php
    $criteria = new CDbCriteria();
    $criteria->addCondition('Hijo = ' . $op);
    $criteria->addCondition('Estado = 1');
    $criteria->order = 'Orden ASC';
    $estadosWeb = EstadosWeb::model()->findAll($criteria);
    $arr_nogestion = array(1, 98, 99);
    $arr_comercial = array(8);
    $arr_agenda = array(24, 29, 31, 32, 37, 38, 19);
    $iconlabel = 'fa-asterisk';
    $click = 'gestionador(this)';
    foreach ($estadosWeb as $e) :
        
        $iconedit = 'fa-edit';
    
        if(in_array($e->ID, $arr_agenda)) :
            $click = 'reagendamiento(this)';
        endif;
        
        if(in_array($e->ID, $arr_comercial)) :
            $click = 'comercial(this)';
        endif;
        
        if ($e->Hijo == 5 && $e->Orden == 7) :
            $iconedit = '';
            $click = '';
        endif;
        
        if (in_array($e->ID, $arr_nogestion)) :
            $iconedit = 'fa-envelope green';
            $click = '';
        endif;
        
        if ($e->Hijo == 5 && $e->Orden == 7) :
            ?>
            <tr class="btn-primary">
                <th style="text-align: center" colspan="2"><?= strtoupper($e->Descripcion) ?></th>
            </tr>
            <?php
        else :
            ?>
            <tr>
                <td style="font-size: 7pt;"><span class="fa <?= $iconlabel ?>"></span> <?= strtoupper($e->Descripcion) ?></td>
                <td style="text-align:center; cursor: pointer" id="opc_<?= $e->ID ?>" onclick='<?= $click ?>'><span class="fa <?= $iconedit ?> fa-2x"></span></td>
            </tr>
        <?php
        endif;        
    endforeach;
    ?>
</table>