<div class="encargado">
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::hiddenField("PersonaContacto[" . $counter . "][ID]", $encargado->ID, array("class" => "pcontid"));
        echo CHtml::label("Nombre", "[" . $counter . "][Nombre_completo]");
        echo CHtml::textField("PersonaContacto[" . $counter . "][Nombre_completo]", $encargado->Nombre_completo, array("placeholder" => "Nombre", "class" => "form-control"))
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Documento", "[" . $counter . "][Documento]");
        echo CHtml::textField("PersonaContacto[" . $counter . "][Documento]", $encargado->Documento, array("placeholder" => "Documento", "class" => "form-control"));
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php echo CHtml::label("Direccion", "[" . $counter . "][Direccion]"); ?>
        <div class="input-group">
            <?php
            echo CHtml::textField("PersonaContacto[" . $counter . "][Direccion]", $encargado->Direccion, array("placeholder" => "Direccion", "class" => "form-control", "readonly" => true,));
            ?>
            <span class="input-group-btn">
                <?php
                echo CHtml::htmlButton("<span class='glyphicon glyphicon-refresh'></span> Dir", array(
                    "class" => "btn btn-default",
                    "onclick" => "inputdireccion('', 'PersonaContacto_" . $counter . "_Direccion')",
                    "id" => "direnc" . $counter
                ))
                ?>
            </span>
        </div>
    </div>
    <div class="col-lg-1 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Telefono", "[" . $counter . "][Telefono]");
        echo CHtml::textField("PersonaContacto[" . $counter . "][Telefono]", ($encargado->Celular != 0) ? $encargado->Celular : $encargado->Telefono, array("placeholder" => "Telefono", "class" => "form-control"));
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Email", "[" . $counter . "][Email]");
        echo CHtml::textField("PersonaContacto[" . $counter . "][Email]", $encargado->Email, array("placeholder" => "Email", "class" => "form-control"));
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Ciudad", "[" . $counter . "][Ciudad]");
        $this->widget(
                'booster.widgets.TbSelect2', array(
            'asDropDownList' => true,
            'name' => 'PersonaContacto[' . $counter . '][Ciudad]',
            "data" => Contacto::model()->getCiudades(),
            "value" => $encargado->Ciudad,
            "htmlOptions" => array(
                "class" => "form-control",
            ),
            'options' => array(
                'placeholder' => 'Ciudad',
                'width' => '100%',
            ))
        );
        ?>
    </div>
    <div class="col-lg-1 col-md-4 col-sm-4 col-xs-12">
        <br>
        <?php
        $this->widget(
                'booster.widgets.TbButton', array(
            'icon' => "check",
            'context' => 'success',
            'htmlOptions' => array(
                "class" => "pull-right",
                "id" => "saveContact",
                "onclick" => "adicionarEncargado('$counter')"
            )
                )
        );
        ?>
    </div>
    <div class="clearfix"></div>

</div>
<div class="hidden-lg visible-md visible-sm visible-xs">
    <hr />
</div>
