<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sx-12 col-xs-12 ">
    <div class="row">
        <div class="modal-header">
            <a class="close" data-dismiss="modal" onclick="$('.reenvios').fadeOut('fast');">&times;</a>
            <h4><?= $titulo ?></h4>
        </div>
        <div class="modal-body">
            <?php
            if (count($mensajeria) > 0) :
                ?>
                <table class="table table-bordered table-striped" style="font-size: 8pt">
                    <tr>
                        <td style="text-align: center">REMITENTE</td>
                        <td style="text-align: center">DESTINATARIO</td>
                        <td style="text-align: center">FECHA</td>
                        <td style="text-align: center">HORA</td>
                        <td></td>
                    </tr>
                    <?php
                    foreach ($mensajeria as $m) :
                        ?>
                        <tr>
                            <td><?= $m->iDRemitente->Nombre ?></td>
                            <td><?= $m->iDDestinatario->Nombre ?></td>
                            <td><?= $m->Fecha ?></td>
                            <td><?= $m->Hora ?></td>
                            <td><?php
                                $this->widget(
                                        'booster.widgets.TbButton', array(
                                    'icon' => 'fa fa-random',
                                    'context' => 'warning',
                                    'htmlOptions' => array('onclick' => 'reenviar("' . $m->ID . '")'),
                                        )
                                );
                                ?></td>
                        </tr>
                        <?php
                    endforeach;
                    ?>        
                </table>
                <?php
            else :

                $user = Yii::app()->getComponent('user');

                $user->setFlash(
                        'info', "<strong>Mensajeria! </strong> No se encontraron registros de envio."
                );

                // Render them all with single `TbAlert`
                $this->widget('booster.widgets.TbAlert', array(
                    'fade' => true,
                    'closeText' => '&times;', // false equals no close link
                    'events' => array(),
                    'htmlOptions' => array(),
                    'userComponentId' => 'user',
                    'alerts' => array(// configurations per alert type
                        // success, info, warning, error or danger
                        'info', // you don't need to specify full config
                    ),
                ));

            endif;
            ?>
        </div>
        <div class="modal-footer">

        </div>
    </div>
</div>