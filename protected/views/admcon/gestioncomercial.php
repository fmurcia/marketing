<input type="hidden" id="contadorges" value="<?= $counter ?>">
<div class="divgestioncomercial col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::hiddenField("GestionComercial[" . $counter . "][ID]", $gestioncomercial->ID, array("class" => "pcontid"));
        echo CHtml::label("Contrato (Si Existe Venta!)", "[" . $counter . "][Contrato]");
        echo CHtml::textField("GestionComercial[" . $counter . "][Contrato]", $gestioncomercial->Contrato, array("placeholder" => "Contrato", "class" => "form-control"));
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">

        <?= CHtml::label("Fecha Visita", "[" . $counter . "][Fecha_visita]"); ?>
        <div class="input-group">
            <?php
            echo CHtml::textField("GestionComercial[" . $counter . "][Fecha_visita]", $gestioncomercial->Fecha_visita, array("placeholder" => "Fecha Visita", "class" => "form-control"));
            ?>
            <span class="input-group-btn">
                <?php
                echo CHtml::htmlButton("<span class='glyphicon glyphicon-calendar'></span>", array(
                    "class" => "btn btn-default",
                    "id" => "cal_$counter",
                    "onclick" => "calendar($counter)",
                ));
                ?>
            </span>
        </div>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?=
        CHtml::label("Servicios", "[" . $counter . "][Servicio]");
        $this->widget(
                'booster.widgets.TbSelect2', array(
            'asDropDownList' => true,
            'name' => 'GestionComercial[' . $counter . '][Servicio]',
            "data" => Contacto::model()->getServiciosList(),
            "value" => $gestioncomercial->Servicio,
            "htmlOptions" => array(
                "class" => "form-control",
            ),
            'options' => array(
                'placeholder' => 'Servicio',
                'width' => '100%',
            ))
        );
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Valor Equipos", "[" . $counter . "][Valor_equipos]");
        echo CHtml::textField("GestionComercial[" . $counter . "][Valor_equipos]", $gestioncomercial->Valor_equipos, array("placeholder" => "Valor Equipos", "class" => "form-control"));
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Valor Instalacion", "[" . $counter . "][Valor_instalacion]");
        echo CHtml::textField("GestionComercial[" . $counter . "][Valor_instalacion]", $gestioncomercial->Valor_instalacion, array("placeholder" => "Valor Instalacion", "class" => "form-control"));
        ?>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Valor Servicio", "[" . $counter . "][Valor_servicio]");
        echo CHtml::textField("GestionComercial[" . $counter . "][Valor_servicio]", $gestioncomercial->Valor_servicio, array("placeholder" => "Valor Servicio", "class" => "form-control"));
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
        <?=
        CHtml::label("Decision de Compra", "[" . $counter . "][Decision_compra]");
        $this->widget(
                'booster.widgets.TbSelect2', array(
            'name' => "GestionComercial[" . $counter . "][Decision_compra]",
            'data' => array('p' => 'Pronta', 'i' => 'Intermedia', 'l' => 'Lejana'),
            'value' => $gestioncomercial->Decision_compra,
            'options' => array(
                'placeholder' => 'Decision de Compra',
            )
                )
        );
        ?>
    </div>
    <div class="col-lg-9 col-md-4 col-sm-4 col-xs-12">
        <?php
        echo CHtml::label("Observaciones ", "[" . $counter . "][Comentarios]");
        echo CHtml::textArea("GestionComercial[" . $counter . "][Comentarios]", $gestioncomercial->Comentarios, array('placeholder' => 'Comentarios de la Gestion Comercial', 'style' => 'width:100%'));
        ?>
    </div>
    <div class="col-lg-1 col-md-4 col-sm-4 col-xs-12">
        <br>
        <?php
        if ($gestioncomercial->Descartar == 0) :
            $this->widget(
                    'booster.widgets.TbButton', array(
                'icon' => "check",
                'label' => "Guardar",
                'context' => 'success',
                'htmlOptions' => array(
                    "class" => "pull-right",
                    "onclick" => "adicionarGestion('$counter')"
                )
                    )
            );
        endif;
        if ($gestioncomercial->Descartar == 0 && $gestioncomercial->ID != 0) :
            $this->widget(
                    'booster.widgets.TbButton', array(
                'icon' => "trash",
                'label' => "Descartar",
                'context' => 'danger',
                'htmlOptions' => array(
                    "class" => "pull-right",
                    "onclick" => "descartarGestion('$gestioncomercial->ID')"
                )
                    )
            );
        endif;
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<br>
<div class="hidden-lg visible-md visible-sm visible-xs"><hr /></div>