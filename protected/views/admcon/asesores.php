<?php
$this->widget(
        'booster.widgets.TbSelect2', array(
    'asDropDownList' => true,
    'name' => 'asesor',
    'data' => Contacto::model()->getAsesor($idagencia, $fecha),
    'options' => array(
        'placeholder' => 'Selecciona el Asesor',
        'width' => '100%',
    ),
    "htmlOptions" => array(
        "class" => "form-control",
    ),
        )
);