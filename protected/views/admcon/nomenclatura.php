<?php

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Adelante', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ADELANTE', 'armardir')")),
        array('label' => 'Administracion', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ADMINISTRACION', 'armardir')")),
        array('label' => 'Aeropuerto', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AEROPUERTO', 'armardir')")),
        array('label' => 'Agencia', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AGENCIA', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Agrupacion', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AGRUPACION', 'armardir')")),
        array('label' => 'Al lado', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AL LADO', 'armardir')")),
        array('label' => 'Almacen', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ALMACEN', 'armardir')")),
        array('label' => 'Altillo', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ALTILLO', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Apartado', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('APARTADO', 'armardir')")),
        array('label' => 'Apartamento', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('APARTAMENTO', 'armardir')")),
        array('label' => 'Atrás', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ATRAS', 'armardir')")),
        array('label' => 'Autopista', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AUTOPISTA', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Avenida', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AVENIDA', 'armardir')")),
        array('label' => 'Avenida calle', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AVENIDA CALLE', 'armardir')")),
        array('label' => 'Avenida carrera', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AVENIDA CARRERA', 'armardir')")),
        array('label' => 'Barrio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BARRIO', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Bloque', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BLOQUE', 'armardir')")),
        array('label' => 'Bodega', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BODEGA', 'armardir')")),
        array('label' => 'Boulevar', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BOULEVAR', 'armardir')")),
        array('label' => 'Calle', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CALLE', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Camino', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CAMINO', 'armardir')")),
        array('label' => 'Carrera', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CARRERA', 'armardir')")),
        array('label' => 'Carretera', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CARRETERA', 'armardir')")),
        array('label' => 'Casa', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CASA', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Caserio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CASERIO', 'armardir')")),
        array('label' => 'Celula', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CELULA', 'armardir')")),
        array('label' => 'Centro', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CENTRO', 'armardir')")),
        array('label' => 'Centro comercial', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CENTRO COMERCIAL', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Circular', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CIRCULAR', 'armardir')")),
        array('label' => 'Circunvalar', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CIRCUNVALAR', 'armardir')")),
        array('label' => 'Ciudadela', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CIUDADELA', 'armardir')")),
        array('label' => 'Conjunto', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CONJUNTO', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Conj. Residencial', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CONJUNTO RESIDENCIAL', 'armardir')")),
        array('label' => 'Consultorio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CONSULTORIO', 'armardir')")),
        array('label' => 'Corregimiento', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CORREGIMIENTO', 'armardir')")),
        array('label' => 'Departamento', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('DEPARTAMENTO', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Deposito', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('DEPOSITO', 'armardir')")),
        array('label' => 'Deposito sotano', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('DEPOSITO SOTANO', 'armardir')")),
        array('label' => 'Diagonal', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('DIAGONAL', 'armardir')")),
        array('label' => 'Edificio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('EDIFICIO', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Entrada', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ENTRADA', 'armardir')")),
        array('label' => 'Escalera', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ESCALERA', 'armardir')")),
        array('label' => 'Esquina', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ESQUINA', 'armardir')")),
        array('label' => 'Este', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ESTE', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Etapa', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ETAPA', 'armardir')")),
        array('label' => 'Exterior', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('EXTERIOR', 'armardir')")),
        array('label' => 'Finca', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('FINCA', 'armardir')")),
        array('label' => 'Garaje', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('GARAJE', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Garaje sotano', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('GARAJE SOTANO', 'armardir')")),
        array('label' => 'Glorieta', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('GLORIETA', 'armardir')")),
        array('label' => 'Hacienda', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('HACIENDA', 'armardir')")),
        array('label' => 'Hangar', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('HANGAR', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Ins. Policial', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('INSPECCION. POLICIAL', 'armardir')")),
        array('label' => 'Ins. Departamental', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('INSPECCION DEPARTAMENTAL', 'armardir')")),
        array('label' => 'Ins. municipal', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('INSPECCION MUNICIPAL', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Interior', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('INTERIOR', 'armardir')")),
        array('label' => 'Kilometro', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('KILOMETRO', 'armardir')")),
        array('label' => 'Local', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('LOCAL', 'armardir')")),
        array('label' => 'Local mezzanine', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('LOCAL MEZZANINE', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Lote', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('LOTE', 'armardir')")),
        array('label' => 'Manzana', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('MANZANA', 'armardir')")),
        array('label' => 'Mezzanine', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('MEZZANINE', 'armardir')")),
        array('label' => 'Modulo', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('MODULO', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Mojon', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('MOJON', 'armardir')")),
        array('label' => 'Muelle', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('MUELLE', 'armardir')")),
        array('label' => 'Municipio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('MUNICIPIO', 'armardir')")),
        array('label' => 'Norte', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('NORTE', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Occidente', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('OCCIDENTE', 'armardir')")),
        array('label' => 'Oeste', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('OESTE', 'armardir')")),
        array('label' => 'Oficina', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('OFICINA', 'armardir')")),
        array('label' => 'Oriente', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ORIENTE', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'OtraNomenclatura', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('(!)', 'armardir')")),
        array('label' => 'Paraje', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PARAJE', 'armardir')")),
        array('label' => 'Parcela', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PARCELA', 'armardir')")),
        array('label' => 'Park way', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PARK WAY', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Parque', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PARQUE', 'armardir')")),
        array('label' => 'Parqueadero', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PARQUEADERO', 'armardir')")),
        array('label' => 'Pasaje', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PASAJE', 'armardir')")),
        array('label' => 'Paseo', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PASEO', 'armardir')")),
    )
        )
);    
        
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(        
        array('label' => 'Penthouse', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PENTHOUSE', 'armardir')")),
        array('label' => 'Piso', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PISO', 'armardir')")),
        array('label' => 'Planta', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PLANTA', 'armardir')")),
        array('label' => 'Porteria', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PORTERIA', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Poste', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('POSTE', 'armardir')")),
        array('label' => 'Plaza de mercado', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PLAZA DE MERCADO', 'armardir')")),
        array('label' => 'Predio', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PREDIO', 'armardir')")),
        array('label' => 'Puente', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PUENTE', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Puesto', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('PUESTO', 'armardir')")),
        array('label' => 'Round point', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ROUND POINT', 'armardir')")),
        array('label' => 'Salon', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SALON', 'armardir')")),
        array('label' => 'Salon comunal', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SALON COMUNAL', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Sector', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SECTOR', 'armardir')")),
        array('label' => 'Semisotano', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SEMISOTANO', 'armardir')")),
        array('label' => 'Solar', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SOLAR', 'armardir')")),
        array('label' => 'Sotano', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SOTANO', 'armardir')")),
  )
        )
);        
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(        
        array('label' => 'Suite', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SUITE', 'armardir')")),
        array('label' => 'Supermanzana', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SUPERMANZANA', 'armardir')")),
        array('label' => 'Sur', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('SUR', 'armardir')")),
        array('label' => 'Terminal', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('TERMINAL', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Terraza', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('TERRAZA', 'armardir')")),
        array('label' => 'Torre', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('TORRE', 'armardir')")),
        array('label' => 'Transversal', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('TRANSVERSAL', 'armardir')")),
        array('label' => 'Unidad', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('UNIDAD', 'armardir')")),
    )
        )
);
$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Urbanizacion', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('URBANIZACION', 'armardir')")),
        array('label' => 'Variante', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('VARIANTE', 'armardir')")),
        array('label' => 'Vereda', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('VEREDA', 'armardir')")),
        array('label' => 'Via', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('VIA', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(        
        array('label' => 'Vias Comunes', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('NOMBRE DE LA VIA', 'armardir')")),
        array('label' => 'Zona', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ZONA', 'armardir')")),
        array('label' => 'Zona franca', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('ZONA FRANCA', 'armardir')")),
        array('label' => 'Americas', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('AMERICAS', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Boyaca', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BOYACA', 'armardir')")),
        array('label' => 'Caracas', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CARACAS', 'armardir')")),
        array('label' => 'Cota', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('COTA', 'armardir')")),
        array('label' => 'NQS', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('NQS', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Medellin', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('MEDELLIN', 'armardir')")),
        array('label' => 'Centenario', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CENTENARIO', 'armardir')")),
        array('label' => 'Bogota', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('BOGOTÁ', 'armardir')")),
        array('label' => 'Industrial', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('INDUSTRIAL', 'armardir')")),
    )
        )
);

$this->widget(
        'booster.widgets.TbButtonGroup', array(
    'justified' => true,
    'buttons' => array(
        array('label' => 'Funza', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('FUNZA', 'armardir')")),
        array('label' => 'Cota', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('COTA', 'armardir')")),
        array('label' => 'Chia', 'url' => '#', 'htmlOptions' => array('onclick' => "concatenaString('CHIA', 'armardir')")),
    )
        )
);
