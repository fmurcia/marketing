<?php

$this->widget(
        'booster.widgets.TbTabs', array(
    'type' => 'tabs', // 'tabs' or 'pills'
    'tabs' => array(
        array('label' => 'Tipo Contacto', 'content' => $this->renderPartial('contactos', array(), true), 'active' => true),
        array('label' => 'Regionales', 'content' => $this->renderPartial('regionales', array(), true)),
        array('label' => 'Agencias', 'content' => $this->renderPartial('agencias', array(), true)),
        array('label' => 'Oportunidades - Pro. Horizontal - Sms', 'content' => $this->renderPartial('oportunidad', array(), true))
    ),
        )
);
