<?php
$comerciales = Asesor::model()->findAll('Nivel = 3 AND Estado = 1 ORDER BY Nombre ASC');
$tipo_contacto = TipoContacto::model()->findAll();
?>
<div class="row">
    <div class="col-lg-12 tpcont">
        <div class="fixedTable">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td style="text-align:center; width:800px"><b>Telemercaderista</b></td>
                        <?php
                        foreach ($tipo_contacto as $t) :
                            ?>
                            <td style="text-align:center;  width:200px"><?= $t->Descripcion ?></td>
                            <?php
                        endforeach;
                        ?>
                    </tr>
                </thead>    
                <tbody>
                    <?php
                    foreach ($comerciales as $c) :
                        ?>
                        <tr>
                            <td style="text-align:center; width: 780px"><?= strtoupper($c->Nombre) ?></td>
                            <?php
                            foreach ($tipo_contacto as $t) :
                                $asesortipo = AsesorTipocontacto::model()->findByAttributes(array('ID_Tipo' => $t->ID, 'ID_Asesor' => $c->ID));
                                if ($asesortipo != NULL) :
                                    if ($asesortipo->Estado == 1) :
                                        $checked = 1;
                                    else :
                                        $checked = 0;
                                    endif;
                                else :
                                    $checked = 0;
                                endif;
                                ?>
                                <td style="text-align:center; width: 195px">
                                    <input type="checkbox" <?= ($checked == 1) ? 'checked' : '' ?> id="check_<?= $c->ID ?>_<?= $t->ID ?>"
                                           onclick="changeState(<?= $c->ID ?>, <?= $t->ID ?>, 'cot')"/>
                                </td>
                                <?php
                            endforeach;
                            ?>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>    
        </div>
    </div>
</div>