<?php
$this->widget(
    'booster.widgets.TbPanel', array(
        'title' => 'Oportunidades - Propiedad Horizontal - SMS',
        'headerIcon' => 'signal',
        'content' => $this->renderPartial('viewopor', array(), true)
    )
);