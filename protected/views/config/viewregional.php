<?php
$comerciales = Asesor::model()->findAll('Nivel = 3 AND Estado = 1 ORDER BY Nombre ASC');
$regionales = Regional::model()->findAll();
?>
<div class="divregionales">
    <div class="row">
        <div class="col-lg-2">
            <table class="table table-bordered table-striped">
                <tr>
                    <th style="text-align:center">REGIONAL</th>
                    <th style="text-align:center">ESTADO</th>
                </tr>
                <?php
                $form = $this->beginWidget(
                        'booster.widgets.TbActiveForm', array(
                    'id' => 'regional-form',
                    'type' => 'vertical',
                    'action' => 'javascript:void(0)'
                        )
                );
                $i = 1;
                foreach ($regionales as $r) :
                    ?>
                    <tr>
                        <td style="vertical-align: middle"><?= strtoupper($r->Descripcion) ?></td>
                        <td style="text-align:center">
                            <?=
                            $form->switchGroup($r, 'Estado', array(
                                'label' => '',
                                'widgetOptions' => array(
                                    'events' => array(
                                        'switchChange' => 'js:function(event, state) {var values = 0; if(state) { values = 1; } else{ values = 0; } upchangecampo("Regional", "Estado", values, "' . $r->ID_Regional . '")}'
                                    ),
                                    'options' => array(
                                        "onText" => "Si",
                                        "offText" => "No",
                                        "wrapperClass" => "wrapper swajuste",
                                        "handleWidth" => "auto",
                                        "size" => "small",
                                    ),
                                    "htmlOptions" => array(
                                        "id" => "est_" . $i,
                                        "style" => "width:100%"
                                    )
                                )
                            ));
                            ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                $this->endWidget();
                ?>
            </table>
            <br>
        </div>
        <div class="col-lg-10 tpcont">
            <div class="fixedTable">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td style="text-align:center; width:410px"><b>COMERCIAL</b></td>
                            <?php
                            foreach ($regionales as $t) :
                                if ($t->Estado == 1) :
                                    ?>
                                    <td style="text-align:center;  width:195px"><?= strtoupper($t->Descripcion) ?></td>
                                    <?php
                                endif;
                            endforeach;
                            ?>
                        </tr>
                    </thead>    
                    <tbody>
                        <?php
                        foreach ($comerciales as $c) :
                            ?>
                            <tr>
                                <td style="text-align:center; width: 390px"><?= strtoupper($c->Nombre) ?></td>
                                <?php
                                foreach ($regionales as $t) :
                                    if ($t->Estado == 1) :
                                        $asesorreg = RegionalAsesor::model()->findByAttributes(array('ID_Regional' => $t->ID_Regional, 'ID_Asesor' => $c->ID));
                                        if ($asesorreg != NULL) :
                                            if ($asesorreg->Estado == 1) :
                                                $checked = 1;
                                            else :
                                                $checked = 0;
                                            endif;
                                        else :
                                            $checked = 0;
                                        endif;
                                        ?>
                                        <td style="text-align:center; width: 190px">
                                            <input type="checkbox" <?= ($checked == 1) ? 'checked' : '' ?> id="check_<?= $c->ID ?>_<?= $t->ID_Regional ?>"
                                                   onclick="changeState(<?= $c->ID ?>, <?= $t->ID_Regional ?>, 'reg')"/>
                                        </td>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>    
            </div>
        </div>
    </div>
</div>