<?php
$as = new Asesor();
$comerciales = $as->getListTelemercaderistas();
?>
<div class="row">
    <div class="col-lg-12 tpcont">
        <div class="fixedTable">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td style="text-align:center; width:410px"><b>Telemercaderista</b></td>
                        <td style="text-align:center; width:410px"><b>Oportunidades</b></td>
                        <td style="text-align:center; width:410px"><b>Pro. Horizontal</b></td>
                        <td style="text-align:center; width:410px"><b>Mensajeria (Sms)</b></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($comerciales as $c) :
                        ?>
                        <tr>
                            <td style="text-align:center; width: 390px"><?= strtoupper($c->Nombre) ?></td>
                            <?php
                            $asesor_oportunidad = AsesorOportunidad::model()->findByAttributes(array('ID_Asesor' => $c->ID));
                            $checked1 = 0;
                            $checked2 = 0;
                            $checked3 = 0;
                            $id_asesor = 0;
                            if ($asesor_oportunidad != NULL) :
                                if ($asesor_oportunidad->ID_Oportunidad == 1) :
                                    $checked1 = 1;
                                endif;
                                if ($asesor_oportunidad->ID_Horizontal == 1) :
                                    $checked2 = 1;
                                endif;
                                if ($asesor_oportunidad->Sms == 1) :
                                    $checked3 = 1;
                                endif;
                                $id_asesor = $asesor_oportunidad->ID_Asesor;
                            endif;
                            ?>
                            <td style="text-align:center; width : 190px">
                                <input type="checkbox" <?= ($checked1 == 1) ? 'checked' : '' ?> id="check_1_<?= $id_asesor ?>" 
                                       onclick="loadOpor(<?= $id_asesor ?>, <?= 1 ?>)"/>
                            </td>
                            <td style="text-align:center; width : 190px">
                                <input type="checkbox" <?= ($checked2 == 1) ? 'checked' : '' ?> id="check_2_<?= $id_asesor ?>" 
                                       onclick="loadOpor(<?= $id_asesor ?>, <?= 2 ?>)"/>
                            </td>
                            <td style="text-align:center; width : 190px">
                                <input type="checkbox" <?= ($checked3 == 1) ? 'checked' : '' ?> id="check_3_<?= $id_asesor ?>" 
                                       onclick="loadOpor(<?= $id_asesor ?>, <?= 3 ?>)"/>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>