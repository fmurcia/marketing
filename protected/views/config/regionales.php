<?php

$this->widget(
        'booster.widgets.TbPanel', array(
    'title' => 'Regionales  (<strong style="font-size:8pt; color:blue">Asignacion y Estado</strong>)',
    'headerIcon' => 'globe',
    'content' => $this->renderPartial('viewregional', array(), true)
        )
);
