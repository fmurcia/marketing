<?php
$form = $this->beginWidget(
        'booster.widgets.TbActiveForm', array(
    'id' => 'agencia-form',
    'type' => 'vertical',
        )
);
?>
<div class="row">
    <div class="col-lg-4 col-lg-offset-4">
        <?=
        $form->switchGroup($model, 'Estado', array(
            'label' => '',
            'widgetOptions' => array(
                'events' => array(
                    'switchChange' => 'js:function(event, state) {var values = 0; if(state) { values = 1; } else{ values = 0; } upchangecampo("Agencia", "Estado", values, "' . $model->ID . '")}'
                ),
                'options' => array(
                    "onText" => "Si",
                    "offText" => "No",
                    "wrapperClass" => "wrapper swajuste",
                    "handleWidth" => "auto",
                    "size" => "small",
                )
            )
        ));
        ?>
    </div>    
</div>    
<?php
$this->endWidget();
