function changeState(id, item, sec) {
    var opcion = 0;
    var urls = '';
    if ($("#check_" + id + "_" + item).is(":checked")) {
        opcion = 1;
    }

    if (sec == 'cot') {
        urls = "<?= Yii::app()->createUrl('config/tipos')?>";
    }
    if (sec == 'reg') {
        urls = "<?= Yii::app()->createUrl('config/regionales')?>";
    }
    if (sec == 'age') {
        urls = "<?= Yii::app()->createUrl('config/agencias')?>";
    }

    $.ajax({
        type: "POST",
        url: urls,
        data: {
            as: id,
            op: opcion,
            it: item
        },
        beforeSend: function (data) {
            $('#modalAlerta').modal('show');
            $('#modalwait').fadeIn('fast');
        },
        success: function (data) {
            $('#modalAlerta').modal('hide');
            $('#modalwait').fadeOut('fast');
            if (sec == 'cot') {
                location.reload();
            }
        },
        error: function (err) {
            swal("¡Error!", "Error de Actualizacion...", "error");
        }
    });
    return false;
}


function upcontact(idcontacto) {
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('admopo/upcontact') ?>",
        data: "id=" + idcontacto,
        success: function (data) {
            console.log(data);
        }
    });
}

function loadOpor(idasesor, tipo) {
    var check = 0;

    if (document.getElementById("check_" + tipo + "_" + idasesor).checked) {
        check = 1;
    }

    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('config/loadopor') ?>",
        data: "idasesor=" + idasesor + '&op=' + check + '&tipo=' + tipo,
        success: function (respuesta) {
            if (respuesta == 'OK') {
                swal("¡Bien!", "Actualizado", "success");
            } else {
                swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
            }
        }
    });
}

function upchangecampo(modelo, campo, valor, id)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("config/upmodelo") ?>',
        'data': 'modelo=' + modelo + '&campo=' + campo + '&valor=' + valor + '&id=' + id,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
            $('.divregionales').fadeOut('fast');
        },
        'success':
                function (respuesta) {
                    if (respuesta == 1) {
                        swal("¡Error!", "No se actualizo el registro", 'error');
                    } else {
                        if (respuesta == 2) {
                            swal("¡Alto!", "No se encontro el ID asociado", 'error');
                        }
                        else {
                            swal("¡Bien!", "Actualizado", "success");
                            $('.divregionales').fadeIn('fast');
                            $('.divregionales').html(respuesta);
                        }
                    }
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Error de Actualizacion...", "error");
        },
        'cache': false
    });
    return false;
}

function upEstadoAgencia(id) {
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("config/upestadoagencia") ?>',
        'data': 'id=' + id,
        'beforeSend': function () {
            $('.ctospace').html($('#modalwait').html());
            $('.divagencias').fadeOut('fast');
        },
        'success':
                function (respuesta) {
                   $('.divagencias').fadeIn('fast');
                   $('.divagencias').html(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Error de Actualizacion...", "error");
        },
        'cache': false
    });
    return false;
}