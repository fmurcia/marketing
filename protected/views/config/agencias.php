<?php

$this->widget(
        'booster.widgets.TbPanel', array(
            'title' => 'Agencias (<strong style="font-size:8pt; color:blue">Asignacion y Estado</strong>)',
            'headerIcon' => 'signal',
            'content' => $this->renderPartial('viewagencias', array(), true)
        )
);