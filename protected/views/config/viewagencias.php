<?php
$tele = array();
$as = new Asesor();
$ra = new RegionalAgencia();
$titulos = array('REGIONAL', 'TIPO', 'AGENCIA');

$regage = $ra->getAgencias();
$comerciales = $as->getListTelemercaderistas();

foreach ($comerciales as $c) :
    $tele[] = strtoupper($c->Nombre);
endforeach;

$encabezados = array_merge($titulos, $tele);
?>
<div class="col-lg-12">
    <table class="table table-bordered table-striped">
        <tr>
            <th style="text-align:center">AGENCIA</th>
            <th style="text-align:center">ESTADO</th>
        </tr>
        <tr>
            <td>
                <?php
                $this->widget(
                        'booster.widgets.TbSelect2', array(
                    'asDropDownList' => true,
                    'name' => 'comercial',
                    "data" => Agencia::model()->getAgencias(),
                    "htmlOptions" => array(
                        "class" => "form-control",
                        "style" => "font-size: 9pt",
                        "placeholder" => '...Seleccione...',
                    ),
                    'options' => array(
                        'width' => '100%',
                        "style" => "width:100%",
                        'required' => true,
                    ),
                    'events' => array('change' => 'js:function(e) { console.log(e.val); upEstadoAgencia(e.val); }')
                        )
                );
                ?>
            </td>
            <td>
                <div class="divagencias"></div>
            </td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-lg-12 tpcont">
    <div class="fixedTable" style="height:630px; overflow: auto">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <?php for ($i = 0; $i < sizeof($encabezados); $i++) : ?>
                        <th style="text-align:center;width: 5%"><?= strtoupper($encabezados[$i]) ?></th>
                    <?php endfor; ?>
                </tr>
            </thead>
            <tbody  style="height:450px;overflow: auto">
                <?php foreach ($regage as $c) : ?>
                    <tr>
                        <td style="text-align:center;width: 5%"><?= $c->iDRegional->Descripcion ?></td>
                        <td style="text-align:center;width: 5%"><?= ($c->iDAgencia->Tipo == 1) ? 'Externa' : 'Directa' ?></td>
                        <td style="text-align:center;width: 10%"><?= strtoupper($c->iDAgencia->Nombre) ?></td>
                        <?php
                        foreach ($comerciales as $a) :
                            $core = ComercialAgencias::model()->findByAttributes(array('ID_Agencia' => $c->ID_Agencia, 'ID_Asesor' => $a->ID));
                            if ($core != NULL) :
                                if ($core->Estado == 1) :
                                    $checked = 1;
                                else :
                                    $checked = 0;
                                endif;
                            else :
                                $checked = 0;
                            endif;
                            ?>
                            <td style="text-align:center;width: 5%">
                                <input type="checkbox" id="check_<?= $a->ID ?>_<?= $c->ID_Agencia ?>"
                                       <?= ($checked == 1) ? 'checked' : '' ?> onclick="changeState(<?= $a->ID ?>, <?= $c->ID_Agencia ?>, 'age')"/>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>    
    </div>
</div>