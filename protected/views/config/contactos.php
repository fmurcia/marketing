<?php

$this->widget(
        'booster.widgets.TbPanel', array(
            'title' => 'Tipo Contacto (<strong style="font-size:8pt; color:blue">Que pueden generar</strong>)',
            'headerIcon' => 'home',
            'content' => $this->renderPartial('viewcontacto', array(), true)
        )
);