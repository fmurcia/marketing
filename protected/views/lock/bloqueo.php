<center>
<?php
/* @var $this LockController */
$this->beginWidget(
    'booster.widgets.TbJumbotron',
    array(
        'heading' => $heading
    )
); ?>
<p><?php echo $message; ?></p>
<?php

$imagen = "";
if($button == 'Mantenimiento') :
    $btn = 'btnlocktools';
else :
    $btn = 'btnlockstop';
endif;

$this->widget('booster.widgets.TbMenu', array(
        'items'=>array(
                 array('label'=>' ', 'url'=>array('create'),'itemOptions'=>array('class' => $btn),
                ),
        ),
        'encodeLabel' => false,
));

?>
<p>
    <?php 
    if(isset($button)) :
        
    $this->widget(
        'booster.widgets.TbButton',
        array(
            'context' => 'danger',
            'size' => 'large',
            'label' => $button,
            'buttonType' => 'link',
            'url' => $href
        )
    ); 
    
    endif;
    ?>
</p>
</center>
<?php $this->endWidget();