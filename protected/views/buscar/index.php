<div style="font-size: 8pt">
<?php
$this->widget(
        'booster.widgets.TbPanel', array(
            'title' => 'Busqueda Avanzada',
            'headerIcon' => 'search',
            'content' => $this->renderPartial('resultado', array(), true)
        )
);
?>
</div>