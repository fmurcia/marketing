
function formBusqueda() {
    var form = $("#formBusqueda").serialize();
    $.ajax({
        type: "POST",
        url: "<?= Yii::app()->createUrl('buscar/resultados')?>",
        data: form,
        beforeSend: function (data) {
            $('#modalAlerta').modal('show');
            $('#modalwait').fadeIn('fast');
        },
        success: function (data) {
            $('#modalAlerta').modal('hide');
            $('#modalwait').fadeOut('fast');
            $(".responsesearch").html(data);
        },
        error: function (err) {
            swal("¡Error!", "Error de Actualizacion...", "error");
        }
    });
    return false;
}

function addFilter(val, pos) {
    if (val == 'AND' || val == 'OR')
    {
        $("#criterio" + pos).removeAttr('disabled');
        $("#modulador" + pos).removeAttr('disabled');
        $("#op" + pos).removeAttr('disabled');
        $("#valor" + pos).removeAttr('readonly');
    } 
    else
    {
        $("#criterio" + pos).attr('disabled', 'true');
        $("#modulador" + pos).attr('disabled', 'true');
        $("#op" + pos).attr('disabled', 'true');
        $("#valor" + pos).attr('readonly', 'true');
    }
}