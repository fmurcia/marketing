<div>
    <form action="javascript:void(0)" class="form-actions" onsubmit="return formBusqueda()" id="formBusqueda">
        <table class="table table-striped table-bordered table-responsive">
            <tr>
                <th style="text-align:center">CRITERIO</th>
                <th style="text-align:center">MODULADOR</th>
                <th style="text-align:center">VALOR</th>
                <th style="text-align:center">OPCION</th>
            </tr>           
            <tr>
                <th style="text-align:center">
                    <select class="form-control" name="criterio1">
                        <option value=""> ...Seleccione... </option>
                        <option value="Razon_social"> Razon Social </option>
                        <option value="Nombre_completo"> Nombre Completo </option>
                        <option value="Direccion"> Direccion </option>
                        <option value="Email"> Email </option>
                        <option value="Celular"> Celular </option>
                        <option value="Telefono"> Telefono </option>
                        <option value="Cedula"> Cedula </option>
                        <option value="Nit"> Nit </option>
                        <option value="Contrato"> Contrato </option>
                        <option value="ID"> ID </option>
                    </select>
                </th>
                <td>
                    <select class="form-control"  name="modulador1">
                        <option value=""> ...Seleccione... </option>
                        <option value="="> = </option>
                        <option value=">"> > </option>
                        <option value=">="> >= </option>
                        <option value="<"> < </option>
                        <option value="<="> <= </option>
                        <option value="LIKE"> CONTIENE </option>
                        <option value="BETWEEN"> ENTRE </option>
                    </select>
                </td>
                <td><input type="text" class="form-control" placeholder="...Escribe..." name="valor1" /></td>
                <td>
                    <select class="form-control"  name="op1" onchange="addFilter(this.value, 2);">
                        <option value=""> ... </option>
                        <option value="AND"> Y </option>
                        <option value="OR"> O </option>
                    </select>
                </td>
            </tr>        
            <tr>
                <th style="text-align:center">
                    <select class="form-control"  id="criterio2" name="criterio2" disabled="">
                        <option value=""> ...Seleccione... </option>
                        <option value="Razon_social"> Razon Social </option>
                        <option value="Nombre_completo"> Nombre Completo </option>
                        <option value="Direccion"> Direccion </option>
                        <option value="Email"> Email </option>
                        <option value="Celular"> Celular </option>
                        <option value="Telefono"> Telefono </option>
                        <option value="Cedula"> Cedula </option>
                        <option value="Nit"> Nit </option>
                        <option value="Contrato"> Contrato </option>
                        <option value="ID"> ID </option>
                    </select>
                </th>
                <td>
                    <select class="form-control" name="modulador2" id="modulador2" disabled="">
                        <option value=""> ...Seleccione... </option>
                        <option value="="> = </option>
                        <option value=">"> > </option>
                        <option value=">="> >= </option>
                        <option value="<"> < </option>
                        <option value="<="> <= </option>
                        <option value="LIKE"> CONTIENE </option>
                        <option value="BETWEEN"> ENTRE </option>
                    </select>
                </td>
                <td><input type="text" class="form-control" placeholder="...Escribe..." name="valor2" id="valor2" readonly="true"/></td>
                <td>
                    <select class="form-control" id="op2" name="op2" disabled=""  onchange="addFilter(this.value, 3)">
                        <option value=""> ... </option>
                        <option value="AND"> Y </option>
                        <option value="OR"> O </option>
                    </select>
                </td>
            </tr>        
            <tr>
                <th style="text-align:center">
                    <select class="form-control"  name="criterio3" id="criterio3" disabled="">
                        <option value=""> ...Seleccione... </option>
                        <option value="Razon_social"> Razon Social </option>
                        <option value="Nombre_completo"> Nombre Completo </option>
                        <option value="Direccion"> Direccion </option>
                        <option value="Email"> Email </option>
                        <option value="Celular"> Celular </option>
                        <option value="Telefono"> Telefono </option>
                        <option value="Cedula"> Cedula </option>
                        <option value="Nit"> Nit </option>
                        <option value="Contrato"> Contrato </option>
                        <option value="ID"> ID </option>
                    </select>
                </th>
                <td>
                    <select class="form-control" name="modulador3" id="modulador3" disabled="">
                        <option value=""> ...Seleccione... </option>
                        <option value="="> = </option>
                        <option value=">"> > </option>
                        <option value=">="> >= </option>
                        <option value="<"> < </option>
                        <option value="<="> <= </option>
                        <option value="LIKE"> CONTIENE </option>
                        <option value="BETWEEN"> ENTRE </option>
                    </select>
                </td>
                <td><input type="text" class="form-control" placeholder="...Escribe..." name="valor3" id="valor3" readonly="true"/></td>
                <td></td>
            </tr>
            <tr>
                <th style="text-align:center">BUSCAR POR TIPO</th>
                <td style="text-align:center"><strong>Cita  <input type="radio" name="tabla" value="Contacto" checked="true"/></strong></td>
                <td style="text-align:center"><strong>Oportunidad  <input type="radio" name="tabla" value="Oportunidad"></strong></td>
                <td></td>
            </tr>
        </table>
        <br />
        <center>
            <button type="submit" class="btn btn-success">Buscar</button>
            <button type="button" class="btn btn-danger">Limpiar</button>
        </center>
    </form>    
</div>
<div class=""></div>
<div class="responsesearch"></div>