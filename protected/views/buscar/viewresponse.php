
<?php
if (sizeof($data) > 0) :
    ?>
    <table class="table table-bordered table-striped">
        <tr>
            <th style="text-align:center">ID</th>
            <th style="text-align:center">IR</th>
            <th style="text-align:center">TIPO</th>
            <th style="text-align:center">ESTADO PROCESO</th>
            <th style="text-align:center">TELEMERCADERISTA</th>
            <th style="text-align:center">RAZON SOCIAL</th>
            <th style="text-align:center">CIUDAD</th>
            <th style="text-align:center">DIRECCION</th>
            <th style="text-align:center">ASESOR</th>
            <th style="text-align:center">FECHA CREACION</th>
            <th style="text-align:center">FECHA GESTION</th>
            <th style="text-align:center">OBSERVACIONES</th>
        </tr>
        <?php
        foreach ($data as $d) :
            $classbtn = '';
            $estado = '';

            if ($d->Tipo_contacto == 1) :
                $classbtn = 'danger';
                $classspan = 'fa fa-phone';
            elseif ($d->Tipo_contacto == 2) :
                $classbtn = 'success';
                $classspan = 'fa fa-folder-open';
            elseif ($d->Tipo_contacto == 3) :
                $classbtn = 'primary';
                $classspan = 'fa fa-globe';
            elseif ($d->Tipo_contacto == 4) :
                $classbtn = 'warning';
                $classspan = 'fa fa-user';
            elseif ($d->Tipo_contacto == 5) :
                $classbtn = 'info';
                $classspan = 'fa fa-wechat';
            endif;

            if ($d->Estado_proceso == 20) :
                $estado = 'Nuevo';
            elseif ($d->Estado_proceso == 12) :
                $estado = 'Pendiente';
            elseif ($d->Estado_proceso == 21) :
                $estado = 'Sin Asesor';
            elseif ($d->Estado_proceso == 22) :
                $estado = 'Asignado';
            elseif ($d->Estado_proceso == 23) :
                $estado = 'Cotizacion';
            elseif ($d->Estado_proceso == 2) :
                $estado = 'Venta';
            elseif ($d->Estado_proceso == 19) :
                $estado = 'Descartado';
            elseif ($d->Estado_proceso == 28) :
                $estado = 'Cita';
            endif;

            if ($d->Telemercaderista == Yii::app()->user->getState('id_usuario')) :
                if ($tipo == 'Contacto') :
                    $link = Yii::app()->createUrl('/admcon/formulario', array('idcontacto' => $d->ID, 'tipocontacto' => $d->Tipo_contacto));
                else :
                    $link = Yii::app()->createUrl('/admopo/formulario', array('idoportunidad' => $d->ID, 'tipocontacto' => $d->Tipo_contacto));
                endif;
            else :
                $link = '#';
            endif;
            ?>
            <tr>
                <td style="text-align:center"><?= $d->ID ?></td>
                <td style="text-align:center" onclick="upcontact('<?= $d->ID ?>')">
                    <a href="<?= $link ?>" class="btn btn-<?= $classbtn ?>"><span class="<?= $classspan ?>"></span></a>
                </td>
                <td style="text-align:center"><?= $tipo ?></td>
                <td style="text-align:center"><?= $d->estadoProceso->Nombre ?></td>
                <td style="text-align:center"><?= $d->telemercaderista->Nombre ?></td>
                <td style="text-align:center"><?= $d->Razon_social ?></td>
                <td style="text-align:center"><?= $d->ciudad->Nombre ?></td>
                <td style="text-align:center"><?= $d->Direccion ?></td>
                <td style="text-align:center"><?= $d->asesor->Nombre ?></td>
                <td style="text-align:center"><?= $d->Fecha_creacion ?></td>
                <td style="text-align:center"><?= $d->Fecha_Ultgestion ?></td>
                <td style="text-align:center"><?= $d->Observaciones ?></td>
            </tr>
            <?php
        endforeach;
        ?>
    </table>
    <?php
else :

    $user = Yii::app()->getComponent('user');

    $user->setFlash(
            'info', "<strong>Sin Registros!</strong> No se tiene Informacion relacionada!."
    );

    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'closeText' => '&times;',
        'events' => array(),
        'htmlOptions' => array(),
        'userComponentId' => 'user',
        'alerts' => array(
            'info',
        )
    ));
endif;
?>