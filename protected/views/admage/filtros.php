<div class="headtitle">
    <span class="glyphicon glyphicon-list"></span> Filtros
</div>
<div class="col-md-12 col-sm-12 col-xs-12">    
    <div id="jstreeage">
        <ul>       
            <li data-jstree='{"icon":"fa fa-home","opened":true}'> Propiedad Horizontal
                <ul>
                    <li data-jstree='{"icon":"fa fa-home"}' data-code="pro-1"> Activo</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-bell","opened":true}'> Estados
                <ul>
                    <li data-jstree='{"icon":"fa fa-briefcase"}' data-code="est-20"> Nuevos</li>
                    <li data-jstree='{"icon":"fa fa-user"}' data-code="est-21"> Sin Asignar</li>
                    <li data-jstree='{"icon":"fa fa-user"}' data-code="est-22"> Asignados</li>
                    <li data-jstree='{"icon":"fa fa-cogs"}' data-code="est-23"> Cotizados</li>
                    <li data-jstree='{"icon":"fa fa-flag"}' data-code="est-2"> Venta</li>
                    <li data-jstree='{"icon":"fa fa-trash"}' data-code="est-19"> Descartados</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-wrench","opened":true}'> Tipos
                <ul>
                    <li data-jstree='{"icon":"fa fa-phone"}' data-code="tip-1"> Llamada Entrante</li>
                    <li data-jstree='{"icon":"fa fa-folder"}' data-code="tip-2"> Base Fria</li>
                    <li data-jstree='{"icon":"fa fa-globe"}' data-code="tip-3"> Campaña Web</li>
                    <li data-jstree='{"icon":"fa fa-user"}' data-code="tip-4"> Cliente Activo</li>
                    <li data-jstree='{"icon":"fa fa-commenting"}' data-code="tip-5"> Chat</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-globe","opened":true}'> Regionales
                <ul>
                    <?php
                    $regionales = $this->getRegionalesXNivel();
                    if (!empty($regionales)) :
                        foreach ($regionales as $regional) :
                            $agencias = $regional->getAgencias();
                            ?>
                            <li data-code="reg-<?= $regional->ID_Regional ?>" data-jstree='{"icon":"glyphicon glyphicon-globe"}'><?= strtolower($regional->Descripcion) ?>     
                                <ul>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-globe"}'>Agencias
                                        <?php
                                        if (count($agencias) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($agencias as $agencia) :
                                                    if ($agencia->TipoAgencia == 1) :
                                                        ?>
                                                        <li data-code="age-<?= $agencia->ID ?>" data-jstree='{"icon":"glyphicon glyphicon-map-marker"}'><?php echo strtolower($agencia->Nombre) ?></li>
                                                        <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-globe"}'>Fuerza de Venta
                                        <?php
                                        if (count($agencias) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($agencias as $agencia) :
                                                    if ($agencia->TipoAgencia == 3) :
                                                        ?>
                                                        <li data-code="age-<?= $agencia->ID ?>" data-jstree='{"icon":"glyphicon glyphicon-map-marker"}'><?php echo strtolower($agencia->Nombre) ?></li>
                                                        <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-calendar","opened":true}'> Gestionados
                <ul>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-1"> Ayer </li>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-2"> 15 Días </li>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-3"> < 1 Mes</li>
                    <li data-jstree='{"icon":"fa fa-calendar"}' data-code="ant-4"> > 1 Mes</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-edit","opened":true}'> Agendados
                <ul>
                    <li data-jstree='{"icon":"fa fa-edit"}' data-code="agen-1"> Vigentes</li>
                    <li data-jstree='{"icon":"fa fa-edit"}' data-code="agen-2"> Vencidos</li>
                </ul>
            </li>
            <li data-jstree='{"icon":"fa fa-calendar","opened":true}'> Temporalidad
                <ul>
                    <li data-jstree='{"icon":"fa fa-map"}'> 2015
                        <ul>
                            <li data-code="time-2015-01" data-jstree='{"icon":"fa fa-map"}'>Enero</li>
                            <li data-code="time-2015-02" data-jstree='{"icon":"fa fa-map"}'>Febrero</li>
                            <li data-code="time-2015-03" data-jstree='{"icon":"fa fa-map"}'>Marzo</li>
                            <li data-code="time-2015-04" data-jstree='{"icon":"fa fa-map"}'>Abril</li>
                            <li data-code="time-2015-05" data-jstree='{"icon":"fa fa-map"}'>Mayo</li>
                            <li data-code="time-2015-06" data-jstree='{"icon":"fa fa-map"}'>Junio</li>
                            <li data-code="time-2015-07" data-jstree='{"icon":"fa fa-map"}'>Julio</li>
                            <li data-code="time-2015-08" data-jstree='{"icon":"fa fa-map"}'>Agosto</li>
                            <li data-code="time-2015-09" data-jstree='{"icon":"fa fa-map"}'>Septiembre</li>
                            <li data-code="time-2015-10" data-jstree='{"icon":"fa fa-map"}'>Octubre</li>
                            <li data-code="time-2015-11" data-jstree='{"icon":"fa fa-map"}'>Noviembre</li>
                            <li data-code="time-2015-12" data-jstree='{"icon":"fa fa-map"}'>Diciembre</li>
                        </ul>
                    </li>
                    <li data-jstree='{"icon":"fa fa-map"}'> 2016
                        <ul>
                            <li data-code="time-2016-01" data-jstree='{"icon":"fa fa-map"}'>Enero</li>
                            <li data-code="time-2016-02" data-jstree='{"icon":"fa fa-map"}'>Febrero</li>
                            <li data-code="time-2016-03" data-jstree='{"icon":"fa fa-map"}'>Marzo</li>
                            <li data-code="time-2016-04" data-jstree='{"icon":"fa fa-map"}'>Abril</li>
                            <li data-code="time-2016-05" data-jstree='{"icon":"fa fa-map"}'>Mayo</li>
                            <li data-code="time-2016-06" data-jstree='{"icon":"fa fa-map"}'>Junio</li>
                            <li data-code="time-2016-07" data-jstree='{"icon":"fa fa-map"}'>Julio</li>
                            <li data-code="time-2016-08" data-jstree='{"icon":"fa fa-map"}'>Agosto</li>
                            <li data-code="time-2016-09" data-jstree='{"icon":"fa fa-map"}'>Septiembre</li>
                            <li data-code="time-2016-10" data-jstree='{"icon":"fa fa-map"}'>Octubre</li>
                            <li data-code="time-2016-11" data-jstree='{"icon":"fa fa-map"}'>Noviembre</li>
                            <li data-code="time-2016-12" data-jstree='{"icon":"fa fa-map"}'>Diciembre</li>
                        </ul>
                    </li>
                    <li data-jstree='{"icon":"fa fa-map"}'> 2017
                        <ul>
                            <li data-code="time-2017-01" data-jstree='{"icon":"fa fa-map"}'>Enero</li>
                            <li data-code="time-2017-02" data-jstree='{"icon":"fa fa-map"}'>Febrero</li>
                            <li data-code="time-2017-03" data-jstree='{"icon":"fa fa-map"}'>Marzo</li>
                            <li data-code="time-2017-04" data-jstree='{"icon":"fa fa-map"}'>Abril</li>
                            <li data-code="time-2017-05" data-jstree='{"icon":"fa fa-map"}'>Mayo</li>
                            <li data-code="time-2017-06" data-jstree='{"icon":"fa fa-map"}'>Junio</li>
                            <li data-code="time-2017-07" data-jstree='{"icon":"fa fa-map"}'>Julio</li>
                            <li data-code="time-2017-08" data-jstree='{"icon":"fa fa-map"}'>Agosto</li>
                            <li data-code="time-2017-09" data-jstree='{"icon":"fa fa-map"}'>Septiembre</li>
                            <li data-code="time-2017-10" data-jstree='{"icon":"fa fa-map"}'>Octubre</li>
                            <li data-code="time-2017-11" data-jstree='{"icon":"fa fa-map"}'>Noviembre</li>
                            <li data-code="time-2017-12" data-jstree='{"icon":"fa fa-map"}'>Diciembre</li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>