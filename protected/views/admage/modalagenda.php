<?php
if (sizeof($data) > 0) :
    $ids = explode(',', str_replace('[', '', str_replace(']', '', str_replace('"', '', $data))));
    ?>
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Volver a Llamar</h4>
    </div>
    <div class="modal-body" style="font-size: 8pt">
        <p>Tienes (<?= sizeof($ids) ?>) Citas Pendientes para las <?= date('H') ?> hrs </p>
        <table class="table table-bordered">
            <tr>
                <th>TIPO</th>
                <th>RAZON</th>
                <th>ESTADO</th>
                <th>OBSERVACION</th>
            </tr>
            <?PHP
            for ($i = 0; $i < sizeof($ids); $i++) :
                $contacto = Contacto::model()->findByPk($ids[$i]);
                if ($contacto->Tipo_contacto == 1) :
                    $classbtn = 'danger';
                    $classspan = 'fa fa-phone';
                elseif ($contacto->Tipo_contacto == 2) :
                    $classbtn = 'success';
                    $classspan = 'fa fa-folder-open';
                elseif ($contacto->Tipo_contacto == 3) :
                    $classbtn = 'primary';
                    $classspan = 'fa fa-globe';
                elseif ($contacto->Tipo_contacto == 4) :
                    $classbtn = 'warning';
                    $classspan = 'fa fa-user';
                elseif ($contacto->Tipo_contacto == 5) :
                    $classbtn = 'info';
                    $classspan = 'fa fa-wechat';
                endif;

                $link = Yii::app()->createUrl('/admcon/formulario', array('idcontacto' => $contacto->ID, 'tipocontacto' => $contacto->Tipo_contacto));
                ?>
                <tr>
                    <td width="60" onclick="upcontact('<?= $contacto->ID ?>')"><a href="<?= $link ?>" class="btn btn-<?= $classbtn ?>"><span class="<?= $classspan ?>"></span></a></td>
                    <td><?= $contacto->Razon_social ?></td>
                    <td><?= $contacto->estadoProceso->Nombre ?></td>
                    <td><?= $contacto->Observaciones ?></td>
                </tr>
        <?php
    endfor;
    ?>
        </table>
    </div>  
    <?php
endif;
?>