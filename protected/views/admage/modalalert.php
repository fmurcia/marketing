<?php
if ($notificacion == 1) :
    $registros = Contacto::model()->alertasAgendados();
    $titulo = 'Citas';
elseif ($notificacion == 3) :
    $registros = Contacto::model()->alertContactoWebLimit();
    $titulo = 'Campaña Web';
elseif ($notificacion == 4) :
    $registros = Contacto::model()->alertContactoChatLimit();
    $titulo = 'CHAT';
else :
    $registros = Oportunidad::model()->alertasAgendados();
    $titulo = 'Oportunidades';
endif;
?>
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4><?= strtoupper($titulo) ?></h4>
</div>
<div class="modal-body" style="font-size: 8pt">
    <p>Tienes (<?= sizeof($registros) ?>) Pendientes para hoy </p>
    <table class="table table-bordered">
        <tr>
            <th>TIPO</th>
            <th>RAZON</th>
            <th>ESTADO</th>
            <th>OBSERVACION</th>
        </tr>
        <?PHP
        foreach ($registros as $r) :
            if($notificacion == 1) :
                $contacto = Contacto::model()->findByPk($r->ID_Contacto);
                $link = Yii::app()->createUrl('/admcon/formulario', array('idcontacto' => $contacto->ID, 'tipocontacto' => $contacto->Tipo_contacto));
            elseif ($notificacion == 4) :
                $contacto = Contacto::model()->findByPk($r->ID);
                $link = Yii::app()->createUrl('/admcon/formulario', array('idcontacto' => $contacto->ID, 'tipocontacto' => $contacto->Tipo_contacto));
            elseif ($notificacion == 3) :
                $contacto = Contacto::model()->findByPk($r->ID);
                $link = Yii::app()->createUrl('/admcon/formulario', array('idcontacto' => $contacto->ID, 'tipocontacto' => $contacto->Tipo_contacto));
            else :
                $contacto = Oportunidad::model()->findByPk($r->ID_Oportunidad);
                $link = Yii::app()->createUrl('/admopo/formulario', array('idoportunidad' => $r->ID_Oportunidad, 'tipocontacto' => $contacto->Tipo_contacto));
            endif;
        
            if ($contacto->Tipo_contacto == 1) :
                $classbtn = 'danger';
                $classspan = 'fa fa-phone';
            elseif ($contacto->Tipo_contacto == 2) :
                $classbtn = 'success';
                $classspan = 'fa fa-folder-open';
            elseif ($contacto->Tipo_contacto == 3) :
                $classbtn = 'primary';
                $classspan = 'fa fa-globe';
            elseif ($contacto->Tipo_contacto == 4) :
                $classbtn = 'warning';
                $classspan = 'fa fa-user';
            elseif ($contacto->Tipo_contacto == 5) :
                $classbtn = 'info';
                $classspan = 'fa fa-wechat';
            endif;
            
            ?>
            <tr>
                <td width="60" onclick="upcontact('<?= $contacto->ID ?>')"><a href="<?= $link ?>" class="btn btn-<?= $classbtn ?>"><span class="<?= $classspan ?>"></span></a></td>
                <td><?= $contacto->Razon_social ?></td>
                <td><?= $contacto->estadoProceso->Nombre ?></td>
                <td><?= $contacto->Observaciones ?></td>
            </tr>
    <?php
endforeach;
?>
    </table>
</div>