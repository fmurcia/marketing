<nav class="subvar">
    <div class="col-lg-8">
        <br />
        <form class="form-inline" action="javascript:void(0)">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Buscar Por (ID, Razon Social, Cedula, Nombre, Direccion, Email, Celular)" id="searchterm" size="100%">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="buscar"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </form>
    </div>
</nav>

<div class="clearfix"></div>

<div class="col-lg-10">
    <div class="registros">
        <?php $this->renderPartial('tablaTodos', array('limit' => 20)) ?>
    </div>
</div>

<div class="col-lg-2">
    <div class="filtros">
        <?php $this->renderPartial('filtros') ?>
    </div>
</div>