var doc = $(document);
var tree = $('#jstreeage');
tree.bind('loaded.jstree', function () {
    $('#changed').show();
    $('.flwait').hide();
}).on('changed.jstree', function (e, data)
{
    var i, j, r = [];
    for (i = 0, j = data.selected.length; i < j; i++)
    {
        var code = data.instance.get_node(data.selected[i]).data.code;
        if (code !== undefined) {
            r.push(code);
        }
    }
    console.log(data.selected); // newly selected
    console.log(data.deselected); // newly deselected
    actfiltros(r.join(','));
}).jstree({
    'plugins': ['checkbox']
});

doc.on("click", "#buscar", function () {
    buscartexto();
});