<?php
if (!isset($ajax)) :
    $contactos = $this->qryContacto();
    $counter = $this->ttRegContacto();
    $pag = isset($pag) ? $pag : 1; 
    ?>
    <div class="headtable">
        <table class="table table-condensed table-striped table-bordered">
            <tr>
                <th width="50">TIPO</th>
                <th width="60">ID</th>
                <th width="100">RAZON SOCIAL</th>
                <th width="100">CIUDAD</th>
                <th width="100">DIRECCION</th>
                <th width="100">ASESOR</th>
                <th width="100">AGENCIA</th>
                <th width="100">ORIGEN</th>
                <th width="100">ESTADO</th>
                <th width="100">FECHA CREACION</th>
                <th width="100">FECHA GESTION</th>
                <th width="200">Ult. COMENTARIO</th>
            </tr>
        </table>   
    </div>
    <div class="headspace"></div>
    <div class="ctospace">
        <div class="subtable">
            <table class="table table-striped">
                <tbody>
                    <?php
                    if (count($contactos) > 0) :
                        foreach ($contactos as $r) :
                            $classbtn = '';
                            $estado = '';

                            if ($r->iDContacto->Tipo_contacto == 1) :
                                $classbtn = 'danger';
                                $classspan = 'fa fa-phone';
                            elseif ($r->iDContacto->Tipo_contacto == 2) :
                                $classbtn = 'success';
                                $classspan = 'fa fa-folder-open';
                            elseif ($r->iDContacto->Tipo_contacto == 3) :
                                $classbtn = 'primary';
                                $classspan = 'fa fa-globe';
                            elseif ($r->iDContacto->Tipo_contacto == 4) :
                                $classbtn = 'warning';
                                $classspan = 'fa fa-user';
                            elseif ($r->iDContacto->Tipo_contacto == 5) :
                                $classbtn = 'info';
                                $classspan = 'fa fa-wechat';
                            endif;

                            if ($r->iDContacto->Estado_proceso == 20) :
                                $estado = 'Nuevo';
                            elseif ($r->iDContacto->Estado_proceso == 12) :
                                $estado = 'Pendiente';
                            elseif ($r->iDContacto->Estado_proceso == 21) :
                                $estado = 'Sin Asesor';
                            elseif ($r->iDContacto->Estado_proceso == 22) :
                                $estado = 'Asignado';
                            elseif ($r->iDContacto->Estado_proceso == 23) :
                                $estado = 'Cotizacion';
                            elseif ($r->iDContacto->Estado_proceso == 2) :
                                $estado = 'Venta';
                            elseif ($r->iDContacto->Estado_proceso == 19) :
                                $estado = 'Descartado';
                            elseif ($r->iDContacto->Estado_proceso == 28) :
                                $estado = 'Cita';
                            endif;

                            $origen = 'Oportunidad Triangulo';

                            $link = Yii::app()->createUrl('/admcon/formulario', array('idcontacto' => $r->iDContacto->ID, 'tipocontacto' => $r->iDContacto->Tipo_contacto));
                            ?>
                            <tr data-id="<?= $r->ID ?>">
                                <td width="60" onclick="upcontact('<?= $r->ID ?>')"><a href="<?= $link ?>" class="btn btn-<?= $classbtn ?>"><span class="<?= $classspan ?>"></span></a></td>
                                <td width="60"><?= $r->iDContacto->ID ?></td>
                                <td width="100"><?= $r->iDContacto->Razon_social ?></td>
                                <td width="100"><?= $r->iDContacto->ciudad->Nombre ?></td>
                                <td width="100"><?= $r->iDContacto->Direccion ?></td>
                                <td width="100"><?= $r->iDContacto->asesor->Nombre ?></td>
                                <td width="100"><?= $r->iDContacto->agencia->Nombre ?></td>
                                <td width="100"><?= $origen ?></td>
                                <td width="100"><?= $estado ?></td>
                                <td width="100"><?= $r->iDContacto->Fecha_creacion ?></td>
                                <td width="100"><?= $r->iDContacto->Fecha_Ultgestion ?></td>
                                <td width="200"><?= strtolower($r->iDContacto->Observaciones) ?></td>
                            </tr>
                            <?php
                        endforeach;
                    else :
                        ?>
                        <tr>
                            <td colspan="8">
                                <div style="text-align:center">
                                    <b>No se encontraron registros</b>
                                </div>
                            </td>
                        </tr>
                    <?php
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
        <div class="footertable">
            <div class="col-lg-10">
                <div class="counters">
                    <span id="ctocounter"><?php echo $counter ?></span> Registros encontrados
                </div>
            </div>
            <div class="col-lg-1">Pagina: </div>
            <div class="col-lg-1">
                <div class="pages">
                    <?php
                    $pages = ceil($counter / $limit);
                    ?>
                    <select id="ctopages" class="form-control" onchange="paginaCto(this.value)">
                        <?php for ($i = 1; $i <= $pages; $i++) : ?>
                            <option <?= ($pag == $i)  ? 'Selected' : '' ?>><?php echo $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>
    <div class="subtable">
        <table class="table table-striped">
            <tbody>
                <?php
                if (count($contactos) > 0) :
                    foreach ($contactos as $r) :
                        $classbtn = '';
                        $classspan = '';
                        $estado = '';

                        if ($r->iDContacto->Tipo_contacto == 1) :
                            $classbtn = 'danger';
                            $classspan = 'fa fa-phone';
                        elseif ($r->iDContacto->Tipo_contacto == 2) :
                            $classbtn = 'success';
                            $classspan = 'fa fa-folder-open';
                        elseif ($r->iDContacto->Tipo_contacto == 3) :
                            $classbtn = 'primary';
                            $classspan = 'fa fa-globe';
                        elseif ($r->iDContacto->Tipo_contacto == 4) :
                            $classbtn = 'warning';
                            $classspan = 'fa fa-user';
                        elseif ($r->iDContacto->Tipo_contacto == 5) :
                            $classbtn = 'info';
                            $classspan = 'fa fa-wechat';
                        endif;

                        if ($r->iDContacto->Estado_proceso == 20) :
                            $estado = 'Nuevo';
                        elseif ($r->iDContacto->Estado_proceso == 12) :
                            $estado = 'Pendiente';
                        elseif ($r->iDContacto->Estado_proceso == 21) :
                            $estado = 'Sin Asesor';
                        elseif ($r->iDContacto->Estado_proceso == 22) :
                            $estado = 'Asignado';
                        elseif ($r->iDContacto->Estado_proceso == 23) :
                            $estado = 'Cotizacion';
                        elseif ($r->iDContacto->Estado_proceso == 2) :
                            $estado = 'Venta';
                        elseif ($r->iDContacto->Estado_proceso == 19) :
                            $estado = 'Descartado';
                        elseif ($r->iDContacto->Estado_proceso == 28) :
                            $estado = 'Cita';
                        endif;

                        $link = Yii::app()->createUrl('/admage/formulario', array('idcontacto' => $r->iDContacto->ID, 'tipocontacto' => $r->iDContacto->Tipo_contacto));
                        ?>
                        <tr data-id="<?= $r->ID ?>">
                            <td width="60"><a href="<?= $link ?>" class="btn btn-<?= $classbtn ?>"><span class="<?= $classspan ?>"></span></a></td>
                            <td width="60"><?= $r->iDContacto->ID ?></td>
                            <td width="100"><?= $r->iDContacto->Razon_social ?></td>
                            <td width="100"><?= $r->iDContacto->Direccion ?></td>
                            <td width="100"><?= $r->iDContacto->ciudad->Nombre ?></td>
                            <td width="100"><?= $r->iDContacto->asesor->Nombre ?></td>
                            <td width="100"><?= $r->iDContacto->agencia->Nombre ?></td>
                            <td width="100"><?= $estado ?></td>
                            <td width="100"><?= $r->iDContacto->Fecha_creacion ?></td>
                            <td width="100"><?= $r->iDContacto->Fecha_Ultgestion ?></td>
                            <td width="200"><?= strtolower($r->iDContacto->Observaciones) ?></td>
                        </tr>
                        <?php
                    endforeach;
                else :
                    ?>
                    <tr>
                        <td colspan="8">
                            <div style="text-align:center">
                                <b>No se encontraron registros</b>
                            </div>
                        </td>
                    </tr>
                <?php
                endif;
                ?>
            </tbody>
        </table>
    </div>
    <div class="footertable">
        <div class="col-lg-10">
            <div class="counters">
                <span id="ctocounter"><?php echo $counter ?></span> Registros encontrados
            </div>
        </div>
        <div class="col-lg-1">Pagina: </div>
        <div class="col-lg-1">
            <div class="pages">
                <?php
                $pages = ceil($counter / $limit);
                ?>
                <select id="ctopages" class="form-control" onchange="paginaCto(this.value)">
                    <?php for ($i = 1; $i <= $pages; $i++) : ?>
                        <option <?= ($pag == $i)  ? 'Selected' : '' ?>><?php echo $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
    </div>
<?php
endif;