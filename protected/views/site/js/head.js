function cargarDivFecha(tipo, contexto, fecha)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/detallado") ?>',
        'data': 'tipo=' + tipo + '&contexto=' + contexto + '&fecha=' + fecha,
        'success':
                function (respuesta) {
                    $("#detalletipo").modal("show");
                    $('#detalle').html(respuesta);
                    $('#detalle').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function cargarDivMes(tipo, contexto, fecha1, fecha2, mes)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/detalladomes") ?>',
        'data': 'tipo=' + tipo + '&contexto=' + contexto + '&fechainicial=' + fecha1 + '&fechafinal=' + fecha2 + '&mes=' + mes,
        'success':
                function (respuesta) {
                    $("#detalletipo").modal("show");
                    $('#detalle').html(respuesta);
                    $('#detalle').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function cargarDiv(tipo, contexto)
{
    var fecha = $('#datefechainicial').val();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/detallado") ?>',
        'data': 'tipo=' + tipo + '&fecha=' + fecha + '&contexto=' + contexto,
        'success':
                function (respuesta) {
                    $("#detalletipo").modal("show");
                    $('#detalle').html(respuesta);
                    $('#detalle').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function cargarDivdetalle(asesor, contexto)
{
    var fecha = $('#datefechainicial').val();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/detalladoasesor") ?>',
        'data': 'asesor=' + asesor + '&fecha=' + fecha + '&contexto=' + contexto,
        'success':
                function (respuesta) {
                    $("#detalletipo").modal("show");
                    $('#detalle').fadeOut('fast');
                    $('#detalle').html(respuesta);
                    $('#detalle').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function cargarDivdetalleetapa(estado, contexto)
{
    var fecha = $('#datefechainicial').val();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/detalladoestado") ?>',
        'data': 'estado=' + estado + '&fecha=' + fecha + '&contexto=' + contexto,
        'success':
                function (respuesta) {
                    $("#detalletipo").modal("show");
                    $('#detalle').fadeOut('fast');
                    $('#detalle').html(respuesta);
                    $('#detalle').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function change(accion)
{
    var fecha = $('#datefechainicial').val();
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/site") ?>',
        'data': 'accion=' + accion + '&fechainicial=' + fecha + '&fechafinal=' + fecha,
        'success':
                function (respuesta) {
                    $('#etapas').fadeOut('fast');
                    $('#asesores').fadeOut('fast');
                    $('#site').fadeOut('fast');
                    $('#tipos').fadeOut('fast');
                    $('#tipos').html(respuesta);
                    $('#tipos').fadeIn('fast');
                },
        'cache': false
    });
    return false;
}
/*
 * @param {type} accion
 * @param {type} fecha
 * @returns {Boolean}         
 */
function changeMes(accion, fecha)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/sitemes") ?>',
        'data': 'accion=' + accion + '&fecha=' + fecha,
        'success':
                function (respuesta) {
                    $('#paginadorMes').fadeOut('fast');
                    $('#paginadorMes').html(respuesta);
                    $('#paginadorMes').fadeIn('fast');
                    reportsGraphics(accion, fecha);
                    reportsExport(accion, fecha);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function reportsExport(accion, fecha)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/divexport") ?>',
        'data': 'accion=' + accion + '&fecha=' + fecha,
        'success':
                function (respuesta) {
                    $('#paginadorMesExporte').fadeOut('fast');
                    $('#paginadorMesExporte').html(respuesta);
                    $('#paginadorMesExporte').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function reportsGraphics(accion, fecha)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/sitetatics") ?>',
        'data': 'accion=' + accion + '&fecha=' + fecha,
        'success':
                function (respuesta) {
                    $('#site').fadeOut('fast');
                    $('#site').html(respuesta);
                    $('#site').fadeIn('fast');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function cargaDetallada(tipo, contexto, asesor)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/detalladotipo") ?>',
        'data': 'fechainicial=' + $('#datefechainicial').val() + '&fechafinal=' + $('#datefechainicial').val() + '&tipo=' + tipo + '&contexto=' + contexto + '&asesor=' + asesor,
        'beforeSend': function () {
            $("#dt ul li:nth-child(1)").removeClass("active");
            $("#dt ul li:nth-child(2)").addClass("active");
            $('#yw1_tab_1').removeClass('active in');
            $('#yw1_tab_2').addClass('active in');
        },
        'success': function (respuesta) {
            $('#detalle_asesor_tipo').fadeOut('fast');
            $('#detalle_asesor_tipo').html(respuesta);
            $('#detalle_asesor_tipo').fadeIn('fast');
        },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function cargaDetalladaEstado(asesor)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '<?php echo Yii::app()->createUrl("site/detalladoasesorgestion") ?>',
        'data': 'fechainicial=' + $('#datefechainicial').val() + '&fechafinal=' + $('#datefechainicial').val() + '&asesor=' + asesor,
        'beforeSend': function () {
            $("#dt ul li:nth-child(1)").removeClass("active");
            $("#dt ul li:nth-child(2)").addClass("active");
            $('#yw1_tab_1').removeClass('active in');
            $('#yw1_tab_2').addClass('active in');
        },
        'success': function (respuesta) {
            $('#detalle_asesor_estado').fadeOut('fast');
            $('#detalle_asesor_estado').html(respuesta);
            $('#detalle_asesor_estado').fadeIn('fast');
        },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
        },
        'cache': false
    });
    return false;
}

function paginarFecha()
{
    if ($('#datefechafinal').val() >= $('#datefechainicial').val())
    {
        jQuery.ajax({
            'type': 'POST',
            'url': '<?php echo Yii::app()->createUrl("site/site") ?>',
            'data': 'accion=entre&fechainicial=' + $('#datefechainicial').val() + '&fechafinal=' + $('#datefechafinal').val(),
            'success':
                    function (respuesta) {
                        $('#tipos').fadeOut('fast');
                        $('#tipos').html(respuesta);
                        $('#tipos').fadeIn('fast');
                    },
            'cache': false
        });
        return false;
    }
    else
    {
        $('#datefechafinal').val($('#datefechainicial').val());
    }
}

/////////////filtros////////////
var filtros = '';

function filtrocierre()
{
    $.ajax({
        type: "post",
        url: "<?php echo Yii::app()->createUrl('site/updfltr')?>",
        data: {
            fltr: filtros,
            idcierre: $('#qrycierre').val()
        },
        beforeSend: function () {
            $('#modalAlerta').modal('show');
        },
        success: function (data) {
            $('#modalAlerta').modal('hide');
            $(".changeCierre").html(data);
            busquedasync();
        },
        error: function (err) {
            error(err.responseText);
        }
    });
}

function detalleAgencia(estado, nombreagencia, div, divdetalle) {
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createUrl('site/dttpagn')?>",
        data: {agencia: nombreagencia, estado: estado},
        beforeSend: function () {
            $('#' + div).fadeOut('fast');
            $('#' + divdetalle).fadeIn('fast');
            $('#' + divdetalle).html($('#modalwait').html());
            $("#navegacion").append('<li class="active"><a href="#" onclick="detalleAgencia(' + estado + ', ' + nombreagencia + ', ' + div + ', ' + divdetalle + ')">' + nombreagencia + '</a></li>');
        },
        success: function (data) {
            $('#' + divdetalle).html(data);
        }
    });
}

function agendadas(tipo) {
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createUrl('admage/'"+tipo+")?>",
        beforeSend: function () {
            
        },
        success: function (data) {
            
        }
    });
}