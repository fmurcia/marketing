<?php
$arr_estados = array();

$estcontacto = Contacto::model()->getEtapas();
$telemark = Cliente::model()->contratosTelemark();
$totalContactos = Contacto::model()->totalContactos();

//$arr_estados[0] = '<li class="animated flipInY list-group-item" href="' . Yii::app()->createUrl('informes/comisiones') . '"><span class="badge btn btn-success">$' . number_format($telemark) . '</span>Proyección en Ventas</li>';
$z = 0;

foreach ($estcontacto as $t) :
    $arr_estados[$z] = '<li class="animated flipInY list-group-item" style="cursor:pointer">'
            . '<a href="' . Yii::app()->createUrl('admcon/agendames', array('t' => $t->ID)) . '">'
            . '<span class="badge btn btn-primary">' . $this->contadorEstadoProceso($t->ID) . '</span> ' . ucfirst($t->Descripcion) . '</a>'
            . '</li>';
    $z++;
endforeach;
?>

<div style="height: 600px;">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">   
                    <span class="fa fa-signal"></span> Agenda General
                </a>
            </div>
        </div>
        <div style="height: 600px; overflow: auto">
            <ul class="list-group">
                <?php
                for ($i = 0; $i < sizeof($arr_estados); $i++) :
                    echo $arr_estados[$i];
                endfor;
                ?>        
            </ul>
        </div>  
    </nav>
</div>
