<?php

/*Agendados Contacto*/
$agendadosct = Contacto::model()->alertasAgendados(); 
/*Agendados Oportunidades*/
$agendadosop = Oportunidad::model()->alertasAgendados();

/* Alertas Chat y Web en contacto*/
$globe = Contacto::model()->alertContactoWebLimit();
$chat = Contacto::model()->alertContactoChatLimit();

$arr_estados = array();
$arr_estados[0] = '<li class="animated flipInY list-group-item" style="cursor:pointer" onclick="modalAlertas(1)"><span class="fa fa-edit"></span><span class="badge btn btn-danger">'.sizeof($agendadosct).'</span>Citas</a></li>';
$arr_estados[1] = '<li class="animated flipInY list-group-item" style="cursor:pointer" onclick="modalAlertas(2)"><span class="fa fa-user"></span> <span class="badge btn btn-success">'.sizeof($agendadosop).'</span>Oportunidades</li>';
$arr_estados[2] = '<li class="animated flipInY list-group-item" style="cursor:pointer" onclick="modalAlertas(3)"><span class="fa fa-bell"></span> <span class="badge btn btn-primary">'.sizeof($globe).'</span>Campaña Web</li>';
$arr_estados[3] = '<li class="animated flipInY list-group-item" style="cursor:pointer" onclick="modalAlertas(4)"><span class="fa fa-wechat"></span> <span class="badge btn btn-info">'.sizeof($chat).'</span>Chat</li>';

$reg = Contacto::model()->alertasContactoWebUlt();
if ($reg != NULL) :
    $ultweb = $reg->ID;
else :
    $ultweb = 0;
endif;
?>


<div>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">   
                    <span class="fa fa-list"> </span> Alertas
                </a>
            </div>
        </div>
        <div>
            <ul class="list-group">
                <?php
                for ($i = 0; $i < sizeof($arr_estados); $i++) :
                    echo $arr_estados[$i];
                endfor;
                ?>        
            </ul>
        </div>  
    </nav>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        setInterval("notydesktop(<?= $ultweb ?>)", 5000);
        setInterval("mostrarAgenda()", 900000);
        setInterval("mostrarAgendaOp()", 900000);
    });
</script> 