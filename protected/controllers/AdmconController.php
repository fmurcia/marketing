<?php

/**
 * Clase que administra todo el contenido de los contactos y su seguimiento
 *
 * @author scorpio
 */
class AdmconController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array("guardarcontacto", "enviards", "uprdstation"),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'index',
                    'crear',
                    'qrycontacto',
                    'geslistcon',
                    'nomenclatura',
                    'formulario',
                    'mensajeria',
                    'asignarasesor',
                    'detalle',
                    'updfltr',
                    'guardarcontacto',
                    'gestion',
                    'guardargestion',
                    'coincidencias',
                    'srchtrm',
                    'upcontact',
                    'pageslistcon',
                    'direccion',
                    'upcontacto',
                    'enviarcontacto',
                    'agencias',
                    'reenviar',
                    'agendames',
                    'estweb',
                    'asesor'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Carga la lista inicial de los contactos
     */
    public function actionIndex() {
        $model = new Contacto();
        $this->initSearch();
        $this->updfltrs("");
        $this->render('index', array("model" => $model));
    }

    /**
     * Inicializa las busquedas
     */
    private function initSearch() {
        Yii::app()->user->setState("term", "");
        $this->setFltrtxt("");
    }

    /**
     * 
     * @param type $fltrtxt
     */
    private function setFltrtxt($fltrtxt) {
        Yii::app()->user->setState("fltrtxt", $fltrtxt);
    }

    public function actionUpdfltr() {
        $fltrtxt = $_POST["fltr"];
        $this->setFltrtxt($fltrtxt);
        $this->updfltrs($fltrtxt);
        echo "OK";
    }

    /**
     * 
     * @param type $fltrtxt
     */
    private function updfltrs($fltrtxt = "") {
        $this->limpiarFiltros();
        $term = Yii::app()->user->getState("term");
        $this->porTermino($term);
        $this->porComercial();
        if ($fltrtxt != "") :
            $filtros = explode(",", $fltrtxt);
            if (count($filtros) > 0) :
                sort($filtros);
                foreach ($filtros as $filtro) :
                    $split = explode("-", $filtro);
                    $tipo = $split[0];
                    $id = $split[1];
                    switch ($tipo) :
                        case "est":
                            $this->porEstado($id);
                            break;
                        case "tip":
                            $this->porTipo($id);
                            break;
                        case "ant":
                            $this->porAntiguedad($id);
                            break;
                        case "age":
                            $this->porAgencia($id);
                            break;
                        case "agen":
                            $this->porAgenda($id);
                            break;
                        case "cot":
                            $this->porDecision($id);
                            break;
                        case "pro":
                            $this->porPropiedad($id);
                            break;
                        case "time":
                            $this->porTiempo($id, $split[2]);
                            break;
                    endswitch;
                endforeach;
            endif;
        endif;
        $this->fixFilters();
    }

    /**
     * Filtra por asesor
     */
    private function porComercial() {
        $criteria = $this->getFilter();
        if ($criteria != null) {
            $as = Asesor::model()->findByPk(Yii::app()->user->getState("id_usuario"));
            if ($as != NULL) :
                if ($as->Nivel == 3) :
                    $criteria->addCondition("Telemercaderista=" . $as->ID);
                endif;
            endif;
        }
        $this->setFilter($criteria);
    }

    /**
     * Filtra por fecha de creacion
     */
    private function porTiempo($anio, $mes) {

        $fechaini = $anio . "-" . $mes . "-01";
        $fechafin = $anio . "-" . $mes . "-30";

        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            $crtmp = new CDbCriteria();
            $crtmp->addBetweenCondition("Fecha_creacion", $fechaini, $fechafin, "OR");
            $crtxt = $crtmp->condition;
            $crtxt = str_replace("AND", "<AND>", $crtxt);

            if (isset($pos["pro"])) {
                $criteria->addCondition($crtxt, "OR");
                $criteria->params = CMap::mergeArray($criteria->params, $crtmp->params);
            } else {
                $pos["pro"] = true;
                $criteria->addCondition($crtxt, "AND");
                $criteria->params = CMap::mergeArray($criteria->params, $crtmp->params);
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $criteria
     * @return \CDbCriteria
     */
    private function fixFilters($criteria = null) {
        $skip = false;
        if ($criteria == null) {
            $skip = true;
            $criteria = $this->getFilter();
        }
        $critstring = $criteria->condition;
        if ($critstring != "") {
            $params = $criteria->params;
            $critstring = str_replace("(", " ", $critstring);
            $critstring = str_replace(")", " ", $critstring);
            $critstring = trim(preg_replace('/\s+/', ' ', $critstring));

            $critstring = "(" . str_ireplace(" AND ", ") AND (", $critstring) . ")";

            $criteria = new CDbCriteria();
            $criteria->addCondition($critstring);
            $criteria->params = $params;
        }
        if ($skip == true) {
            $this->setFilter($criteria);
        } else {
            return $criteria;
        }
    }

    /**
     * Busca el parametro o consulta por Criterio de Busqueda dado
     */
    public function actionSrchtrm() {
        if (isset($_POST["term"])) {
            Yii::app()->user->setState("term", $_POST["term"]);
            $this->updfltrs($this->getFltrtxt());
        }
        echo "OK";
    }

    /**
     * Limpia los Filtros
     */
    private function limpiarFiltros() {
        Yii::app()->user->setState("fltini", array());
        $filters = new CDbCriteria();
        $this->setFilter($filters);
    }

    /**
     * 
     * @param type $id
     */
    private function porTermino($id = "") {
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if ($id != "") {
                $criteria->compare("ID", $id, true, "OR");
                $criteria->compare("Razon_social", $id, true, "OR");
                $criteria->compare("Direccion", $id, true, "OR");
                $criteria->compare("Nit", $id, true, "OR");
                $criteria->compare("Nombre_completo", $id, true, "OR");
                $criteria->compare("Celular", $id, true, "OR");
                $criteria->compare("Email", $id, true, "OR");
            }
        }
        $this->setFilter($criteria);
    }

    /**
     * 
     * @param type $id
     */
    private function porDecision($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["cot"])) {
                $criteria->addCondition("Decision_compra = '$id'", "OR");
            } else {
                $pos["cot"] = true;
                $criteria->addCondition("Decision_compra = '$id'", "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porPropiedad($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["pro"])) {
                $criteria->addCondition("Horizontal = '$id'", "OR");
            } else {
                $pos["Pro"] = true;
                $criteria->addCondition("Horizontal = '$id'", "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porEstado($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["est"])) {
                $criteria->addCondition("Id_Estadoweb=" . $id, "OR");
            } else {
                $pos["est"] = true;
                $criteria->addCondition("Id_Estadoweb=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porTipo($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["tip"])) {
                $criteria->addCondition("Tipo_contacto=" . $id, "OR");
            } else {
                $pos["tip"] = true;
                $criteria->addCondition("Tipo_contacto=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porAntiguedad($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        $date = date("Y-m-d H:i:s");

        $logico = "OR";

        if ($criteria != null) :
            if (isset($pos["ant"])) :
                $logico = "OR";
            else :
                $logico = "AND";
            endif;
        endif;

        switch ($id) :
            case 1 :
                $ayer = date("Y-m-d", strtotime("-1 day", strtotime($date)));
                $criteria->addCondition("Fecha_Ultgestion >= '$ayer 00:00:00' AND Fecha_Ultgestion <= '$ayer 23:59:59' ", $logico);
                break;
            case 2 :
                $inicio = date("Y-m-d", strtotime("-15 day", strtotime($date)));
                $criteria->addCondition("Fecha_Ultgestion >= '$inicio 00:00:00' AND Fecha_Ultgestion <= '$date' ", $logico);
                break;
            case 3:
                $inicio = date("Y-m-d", strtotime("-1 Month", strtotime($date)));
                $criteria->addCondition("Fecha_Ultgestion >= '$inicio 00:00:00' AND Fecha_Ultgestion <= '$date' ", $logico);
                break;
            case 4:
                $inicio = date("Y-m-d H:i:s", strtotime("-1 Month", strtotime($date)));
                $criteria->addCondition("Fecha_Ultgestion < '$inicio'", $logico);
                break;
        endswitch;
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porAgenda($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        $date = date("Y-m-d H:i:s");

        $logico = "OR";

        if ($criteria != null) :
            if (isset($pos["ant"])) :
                $logico = "OR";
            else :
                $logico = "AND";
            endif;
        endif;

        switch ($id) :
            case 1 :
                $criteria->addCondition("Fecha_Ultagenda > '$date'", $logico);
                break;
            case 2 :
                $criteria->addCondition("Fecha_Ultagenda < '$date'", $logico);
                break;
        endswitch;
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porAgencia($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["age"])) {
                $criteria->addCondition("Agencia=" . $id, "OR");
            } else {
                $pos["age"] = true;
                $criteria->addCondition("Agencia=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $filters
     */
    private function setFilter($filters) {
        Yii::app()->user->setState("filtros", $filters);
    }

    /**
     * 
     * @return \CDbCriteria
     */
    private function getFilter() {
        $filter = Yii::app()->user->getState("filtros");
        if ($filter == null) {
            $filter = new CDbCriteria();
            $this->setFilter($filter);
        }
        return $filter;
    }

    /**
     * 
     * @return string
     */
    private function getFltrtxt() {
        $fltrtxt = Yii::app()->user->getState("fltrtxt");
        if ($fltrtxt == null) {
            $fltrtxt = "";
            $this->setFltrtxt($fltrtxt);
        }
        return $fltrtxt;
    }

    /**
     * 
     */
    public function actionQrycontacto() {
        $limit = 10;
        $pag = (isset($_POST["pag"]) ? ((int) $_POST["pag"]) - 1 : -1);
        $offset = ($pag > 0 ? $pag * $limit : -1);
        $unegs = $this->qryContacto($offset);
        $counter = $this->ttRegContacto();
        $this->renderPartial("tablaTodos", array("contactos" => $unegs, "ajax" => true, 'limit' => $limit, 'counter' => $counter, 'pag' => $pag + 1));
    }

    /**
     * 
     * @param type $offset
     * @param type $tipo
     * @return type
     */
    public function qryContacto($offset = -1) {
        $permitidos = array(2, 3, 4, 7, 15, 16, 17, 18, 20, 21, 22, 23);
        $criteria = $this->getFilter();
        $criteria->addNotInCondition('Id_Estadoweb', $permitidos);
        $criteria->offset = $offset;
        $criteria->limit = 10;
        $criteria->order = "ID DESC";
        $contactos = Contacto::model()->findAll($criteria);

        if (sizeof($contactos) > 0) :
            return $contactos;
        else :
            return array();
        endif;
    }

    public function ttRegContacto() {
        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = array("count(ID) as counter");
        $critreal->limit = -1;
        $critreal->offset = -1;
        $counter = Contacto::model()->find($critreal);
        return $counter->counter;
    }

    public function actionUpcontact() {
        $contacto = Contacto::model()->findByPk($_POST['id']);
        if ($contacto != NULL) :
            $contacto->Fecha_Ultgestion = date('Y-m-d H:i:s');
            $contacto->save();
        endif;
    }

    /**
     * Formulario de Contacto
     */
    public function actionFormulario() {

        Yii::app()->user->setState('encnum', 0);
        Yii::app()->user->setState('gesnum', 0);
        $model = Contacto::model()->findByPk($_REQUEST['idcontacto']);
        $agendas = Agendados::model()->findAllByAttributes(array('ID_Contacto' => $model->ID));
        foreach ($agendas as $a) :
            $time = $a->Fecha . " " . $a->Hora;
            if ($time <= date('Y-m-d H:i:s')) :
                $a->ID_Accion = 19;
            endif;
            $a->update();
        endforeach;
        $this->render('formulario', array('model' => $model));
    }

    /**
     * 
     * @param type $start
     * @param type $block
     */
    function actionPageslistcon($start = 0, $block = 1) {

        if (isset($_POST['start'])) :
            $start = $_POST['start'];
        endif;

        if (isset($_POST['block'])) :
            $block = $_POST['block'];
        endif;

        $pagact = $start;
        $ttreg = $this->ttRegContacto();

        $criteria = $this->getFilter();
        $criteria->limit = $block;
        $criteria->offset = $start;
        $criteria->order = 'ID DESC';
        $model = Contacto::model()->find($criteria);
        $this->renderPartial('formpage', array('model' => $model, 'start' => $start, 'block' => $block, 'pages' => $ttreg, 'pag' => $pagact), false, true);
    }

    function actionGeslistcon($start = 0, $block = 1) {

        if (isset($_POST['start'])) :
            $start = $_POST['start'];
        endif;

        if (isset($_POST['block'])) :
            $block = $_POST['block'];
        endif;

        $pagact = $block;
        $ttreg = $this->ttRegContacto();

        $criteria = $this->getFilter();
        $criteria->limit = $block;
        $criteria->offset = $start;
        $criteria->order = 'ID DESC';
        $contacto = Contacto::model()->findAll($criteria);

        $ultsi = 0;
        if ($contacto != NULL) :
            foreach ($contacto as $o) :
                $ultsi = $o->ID;
            endforeach;
            $this->paginadorFormularios($ultsi, $start, $block, $ttreg, $pagact);
        else :
            $this->render('index');
        endif;
    }

    public function paginadorFormularios($idoportunidad, $start, $block, $ttreg, $pagact) {
        $model = Contacto::model()->findByPk($idoportunidad);
        $this->render('formpage', array('model' => $model, 'start' => $start, 'block' => $block, 'pages' => $ttreg, 'pag' => $pagact));
    }

    /**
     * Crea un Nuevo Contacto
     */
    public function actionCrear() {
        $this->render('crear');
    }

    /**
     * Guarda los Contactos a la BD
     */
    public function actionGuardarcontacto() {
        date_default_timezone_set('America/Bogota');
        if (!empty($_POST['ciudad'])) :
            $ciudad = isset($_POST['ciudad']) ? $_POST['ciudad'] : 11001;
            $tipocontacto = isset($_POST['tipocontacto']) ? $_POST['tipocontacto'] : 5;
            $telefono = isset($_POST['telefono']) ? $_POST['telefono'] : 0;

            $fijo = (strlen($telefono) == 7 ) ? $telefono : 0;
            $celular = (strlen($telefono) == 10) ? $telefono : 0;
            $telemercadeo = $this->getTelemercaderista($ciudad, $tipocontacto);

            $ciu = Ciudad::model()->findByPk($ciudad);

            $arr = array();
            $arr['cf_ciudad'] = $ciu->Nombre;
            $arr['ciudad'] = $ciudad;
            $arr['persona'] = isset($_POST['persona']) ? $_POST['persona'] : '';
            $arr['direccion'] = isset($_POST['direccion']) ? $_POST['direccion'] : 'SIN ASIGNAR';
            $arr['cf_direccion'] = isset($_POST['direccion']) ? $_POST['direccion'] : 'SIN ASIGNAR';
            $arr['email'] = isset($_POST['email']) ? $_POST['email'] : 'SIN ASIGNAR';
            $arr['observaciones'] = isset($_POST['comentarios_direccion']) ? $_POST['comentarios_direccion'] : '';
            $arr['razon'] = isset($_POST['razon']) ? $_POST['razon'] : '';
            $arr['tipocontacto'] = $tipocontacto;
            $arr['mediocontacto'] = isset($_POST['mediocontacto']) ? (!empty($_POST['mediocontacto']) ? $_POST['mediocontacto'] : 'GOOGLE' ) : 'GOOGLE';
            $arr['segmentacion'] = isset($_POST['tippersona']) ? (!empty($_POST['tippersona']) ? $_POST['tippersona'] : '1' ) : '1';

            $arr['servicio'] = isset($_POST['tipservicio']) ? (!empty($_POST['tipservicio']) ? $_POST['tipservicio'] : '1' ) : '1';
            $arr['fijo'] = $fijo;
            $arr['celular'] = $celular;
            $arr['telemercadeo'] = $telemercadeo;
            $arr['cf_tipo_cliente'] = isset($_POST['tippersona']) ? (!empty($_POST['tippersona']) ? $_POST['tippersona'] : 'Residencial' ) : 'Residencial';

            $cot = Contacto::model()->insertarContacto($arr);

            if (!empty($_POST['email'])) :
                $this->Enviards($_POST['email'], $arr, true, false);
            endif;

            $arr_url = array();
            $arr_url['contacto'] = $cot->ID;
            $arr_url['tipo'] = $cot->Tipo_contacto;

            if ($telemercadeo == Yii::app()->user->getState('id_usuario')) :
                $arr_url['start'] = 1;
            else :
                $arr_url['start'] = 0;
            endif;

            print json_encode($arr_url);

        else :
            echo "Error";
        endif;
    }

    public function actionUprdstation() {
        $criteria = new CDbCriteria();
        $criteria->compare('Fecha_creacion','>=2019-11-01',true);
        $contacto = Contacto::model()->findAll($criteria);
        if ($contacto != NULL) {
            foreach ($contacto as $c) {
                $arr_aux = array();
                $arr_aux['name'] = $c->Razon_social;
                $arr_aux['cf_ciudad'] = $c->ciudad->Nombre;
                $arr_aux['personal_phone'] = $c->Celular;
                $arr_aux['cf_mdate'] = $c->Fecha_creacion;
                $arr_aux['cf_estado_del_clientes_general'] = $c->idEstadoweb->Descripcion;
                $arr_aux['cf_servicio'] = $c->Servicio;
                $arr_aux['cf_mediocontacto'] = $c->Servicio;
                $arr_aux['cf_tipo_cliente'] = "Residencial";
                $this->PatchLeadrdstation($c->Email, $arr_aux);
                echo "OK";
            }
        } else {
            echo "No se ejecuto el metodo";
        }
    }

    public function Enviards($email, $datos, $insert, $update) {

        if ($update) :
            $ciudad = Ciudad::model()->findByPk($datos['ciudad']);
            $servicio = Servicios::model()->findByPk($datos['servicio']);
            $arr_aux = array();
            $arr_aux['name'] = $datos['razon'];
            $arr_aux['cf_ciudad'] = $ciudad->Nombre;
            $arr_aux['personal_phone'] = $datos['celular'];
            $arr_aux['cf_mdate'] = date('d/m/y');
            $arr_aux['cf_estado_del_clientes_general'] = "Bienvenido";
            $arr_aux['cf_servicio'] = $servicio->Nombre;
            $this->PostLeadrdstation($email, $arr_aux);
        endif;

        if ($insert):
            $this->PatchLeadrdstation($email, $datos);
        else :
            $this->PostLeadrdstation($email, $datos);
        endif;
    }

    /**
     * Carga el Fomrulario de las Direcciones
     */
    public function actionDireccion() {

        if (isset($_POST['formulario']) && !empty($_POST['formulario'])) :
            $input = substr($_POST['formulario'], -1);
        elseif (isset($_POST['input']) && !empty($_POST['input'])) :
            $input = $_POST['input'];
        else :
            $input = 1;
        endif;

        $tipo = '';
        if (isset($_POST['tipo']) && !empty($_POST['tipo'])) :
            $tipo = $_POST['tipo'];
        endif;

        $this->renderPartial('direcciones', array('idinput' => $input, 'tipo' => $tipo));
    }

    /**
     * Actualiza el Contacto
     */
    public function actionUpcontacto() {
        $model = $_POST['model'];
        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $valor = $_POST['valor'];
        $datos = array();
        if ($model == 'Contacto') :
            $instance = Contacto::model()->actualizar($id, $campo, $valor);
            if ($campo == 'Direccion') :
                $datos['cf_direccion'] = $valor;
            elseif ($campo == 'Telefono' || $campo == 'Celular') :
                $datos['personal_phone'] = $valor;
            elseif ($campo == 'Razon_social') :
                $datos['name'] = $valor;
            elseif ($campo == 'Ciudad') :
                $ciudad = Ciudad::model()->findByPk($valor);
                $datos['cf_ciudad'] = $ciudad->Nombre;
            endif;

            if (!empty($instance->Email)) :
                $this->Enviards($instance->Email, $datos, true, false);
            endif;

        elseif ($model == 'ServicioInteres') :
            $service = explode('-', $valor);
            $opcion = ServicioInteres::model()->findByAttributes(array("ID_Contacto" => $id, "ID_Servicio" => $service[0]));
            if ($opcion == null) :
                $opcion = new ServicioInteres;
                $opcion->ID_Contacto = $id;
                $opcion->ID_Servicio = $service[0];
            endif;
            $opcion->Estado = $service[1];
            if ($campo == 'Equipos') :
                $opcion->Equipos = $service[1];
            else :
                $opcion->Equipos = 0;
            endif;
            $datos['cf_servicio'] = $opcion->iDServicio->Nombre;

            if (!empty($opcion->iDContacto->Email)) :
                $this->Enviards($opcion->iDContacto->Email, $datos, true, false);
            endif;

            $opcion->save();
        endif;
    }

    /**
     * Renderiza las demas Nomenclaturas de las Direcciones
     */
    public function actionNomenclatura() {
        $this->renderPartial('nomenclatura');
    }

    /**
     * Carga el Modal del Seguimiento
     */
    public function actionGestion() {
        $this->renderPartial('gestion', array('idcontacto' => $_REQUEST['idcontacto']));
    }

    /**
     * Guarda la Gestion
     */
    public function actionGuardargestion() {
        date_default_timezone_set('America/Bogota');
        $error = '';

        $contacto = Contacto::model()->findByPk($_POST['idcontacto']);
        $cadena = isset($_POST['comentarios']) ? $_POST['comentarios'] : '';

        $agendados = array(8, 19, 24, 29, 31, 32, 36, 37);

        if (in_array($_POST['accion'], $agendados)) :
            $fechaCita = strtotime($_POST['fecha_agendada']);
            $fechaActual = strtotime(date('Y-m-d H:i:s'));
            $cadena .= isset($_POST['comentarios_agenda']) ? $_POST['comentarios_agenda'] : '';
            if ($fechaCita < $fechaActual) :
                $error .= "La fecha de Agenda no puede ser menor a Hoy";
            else :
                $time = explode(' ', $_POST['fecha_agendada']);
                Agendados::model()->registro($time[0], $time[1], $_POST['idcontacto'], Yii::app()->user->getState('id_usuario'), 2);
                $cadena .= " SE AGENDA PARA EL DIA " . $_POST['fecha_agendada'];
                $contacto->Fecha_Ultagenda = $time[0];
            endif;
        endif;

        if (empty($error)) :
            $contacto->Fecha_Ultgestion = date('Y-m-d H:i:s');
            $contacto->Id_Estadoweb = $_POST['accion'];

            $estadosweb = EstadosWeb::model()->findByPk($_POST['accion']);
            if ($estadosweb != NULL) :
                $contacto->Estado_proceso = 12;
                
                $servicio = Servicios::model()->findByPk($contacto->Servicio);
                $ser = '';

                if($servicio == 'NULL') :
                    $ser = $contacto->Servicio;
                else :
                    $ser = "Monitoreo";
                endif;
                
                if (!empty($contacto->Email)) :
                    $datos['name'] = $contacto->Razon_social;
                    $datos['cf_direccion'] = $contacto->Direccion;
                    $datos['personal_phone'] = $contacto->Celular;
                    $datos['cf_estado_del_clientes_general'] = 'Cita';
                    $datos['cf_ciudad'] = $contacto->ciudad->Nombre;
                    $datos['cf_servicio'] = $ser;
                    $datos['cf_estado_del_clientes_general'] = $estadosweb->Descripcion;
                    $datos['cf_comentarios'] = $cadena;
                    $datos['bio'] = $cadena;
                    $this->Enviards($contacto->Email, $datos, true, false);
                endif;
            endif;

            $contacto->Observaciones = $cadena;

            if (empty($contacto->Medio_contacto)) :
                $contacto->Medio_contacto = 'Adwords';
            endif;

            if (!empty($_POST['cont'])) :
                $contacto->Contrato = $_POST['cont'];
                $cadena .= 'Numero de contrato venta ' . $_POST['cont'];
            endif;

            $contacto->update();

            HistoricoGestion::model()->guardarGestion($contacto->ID, $contacto->Estado_proceso, $cadena, $_POST['accion'], 0, 0);

            $datos = array();

            echo "OK";
        endif;
    }

    public function actionMensajeria() {
        $contacto = Contacto::model()->findByPk($_POST['idcontacto']);
        if ($_POST['tipo'] != '4D') :
            $mensajeria = Mensajeria::model()->findAllByAttributes(array('ID_Contacto' => $_POST['idcontacto'], 'TipoMensaje' => $_POST['tipo']));
            $this->renderPartial('mensajeria', array('mensajeria' => $mensajeria, 'titulo' => $_POST['tipo']));
        else :
            if ($this->sendContacto($contacto->ID, $contacto->Ciudad)) :
                $contacto->Aprobado4D = 1;
            else :
                $contacto->Aprobado4D = 2;
            endif;

            if ($contacto->save()):
                echo "4D";
            else :
                echo "Error";
            endif;
        endif;
    }

    public function actionReenviar() {

        $error = 0;
        if (isset($_POST['id'])) :
            $registro = Mensajeria::model()->findByAttributes(array('ID' => $_POST['id']));

            if ($registro->TipoMensaje == 'SMS') :
                $longitud = strlen($registro->Texto);
                if ($longitud <= 150) :
                    $this->urlsms($registro->Texto, $registro->iDDestinatario->Celular);
                endif;
                if ($longitud >= 151 && $longitud <= 300) :
                    $cadena1 = substr($registro->Texto, 0, 150);
                    $cadena2 = substr($registro->Texto, 151, 300);
                    $this->urlsms($cadena1, $registro->iDDestinatario->Celular);
                    $this->urlsms($cadena2, $registro->iDDestinatario->Celular);
                endif;
                if ($longitud >= 301) :
                    $cadena1 = substr($registro->Texto, 0, 150);
                    $cadena2 = substr($registro->Texto, 151, 300);
                    $cadena3 = substr($registro->Texto, 301, 450);
                    $this->urlsms($cadena1, $registro->iDDestinatario->Celular);
                    $this->urlsms($cadena2, $registro->iDDestinatario->Celular);
                    $this->urlsms($cadena3, $registro->iDDestinatario->Celular);
                endif;
            endif;

            if ($registro->TipoMensaje == 'EMAIL') :
                $this->sendMailReenvio($registro->Texto, $registro->iDDestinatario->Email);
            endif;

            $registro->ContadorEnvios += 1;
            $registro->Fecha = date('Y-m-d');
            $registro->Hora = date('H:i:s');

            if ($registro->save()) :
                $error = 1;
            else :
                $error = 0;
            endif;
        else :
            $error = 0;
        endif;

        echo $error;
    }

    /**
     * Transfiere un contacto al comercial asignado
     */
    public function actionEnviarcontacto() {
        $contacto = Contacto::model()->findByPk($_POST['idcontacto']);
        if (!empty($_POST['comercial']) && !empty($_POST['comentarios'])) :
            $contacto->Telemercaderista = $_POST['comercial'];
            $contacto->Observaciones = $_POST['comentarios'];
            $comentario = 'Se realiza transferencia a ' . $contacto->telemercaderista->Nombre . ' Observacion : ' . $_POST['comentarios'];
            if ($contacto->save()) :
                HistoricoGestion::model()->guardarGestion($contacto->ID, $contacto->Estado_proceso, $comentario, 22);
                echo $contacto->telemercaderista->Nombre;
            endif;
        else :
            echo "Error";
        endif;
    }

    public function actionAgencias() {
        Yii::app()->clientScript->scriptMap["jquery.js"] = false;
        Yii::app()->clientScript->scriptMap["jquery.min.js"] = false;
        $model = new Contacto();
        $this->renderPartial('agencias', array('model' => $model, 'idregional' => $_POST['idregional']), false, true);
    }

    /**
     * Asocia los Asesores segun su Agencia
     */
    public function actionAsesor() {
        Yii::app()->clientScript->scriptMap["jquery.js"] = false;
        Yii::app()->clientScript->scriptMap["jquery.min.js"] = false;
        $model = new Contacto();
        $fecha = isset($_POST['fecha']) ? $_POST['fecha'] : date('Y-m-d');
        $this->renderPartial('asesores', array('model' => $model, 'idagencia' => $_POST['idagencia'], 'fecha' => $fecha), false, true);
    }

    /**
     * Asigana el Asesor comercial
     */
    public function actionAsignarasesor() {
        $error = "";
        $datos = $_POST;
        $asesor = isset($datos['asesor']) ? $datos['asesor'] : 1;
        $sms = isset($datos['sms']) ? $datos['sms'] : 0;
        $agencia = isset($datos['agencia']) ? $datos['agencia'] : 1;
        $fechaasignada = isset($datos['fecha_asignada']) ? $datos['fecha_asignada'] : date('Y-m-d H:i:s');
        $observaciones = !empty($datos['observaciones']) ? $datos['observaciones'] : "Ninguna";
        $ciudad = !empty($datos['ciudad']) ? $datos['ciudad'] : 11001;
        $contacto = Contacto::model()->findByPk($datos['idcontacto']);
        $as = Asesor::model()->findByPk($asesor);
        $time = explode(" ", $fechaasignada);
        $fecha = $time[0];
        $hora = $time[1];
        if ($as != NULL) :
            $arr_aux = array(
                'razon' => $contacto->Razon_social, 'datecita' => $fecha . " " . $hora,
                'telefono' => $contacto->Telefono, 'celular' => $contacto->Celular,
                'direccion' => trim(str_replace("#", "N", $contacto->Direccion) . ";ob:" . $observaciones),
                'nombre' => $contacto->Nombre_completo, 'emailasesor' => $as->Email, 'plan' => $contacto->ID_plan);

            Contacto::model()->actualizar($contacto->ID, 'Estado_proceso', 22);
            Contacto::model()->actualizar($contacto->ID, 'Asesor', $as->ID);
            Contacto::model()->actualizar($contacto->ID, 'Agencia', $agencia);
            Contacto::model()->actualizar($contacto->ID, 'Observaciones', $observaciones);
            Contacto::model()->actualizar($contacto->ID, 'Fecha_Ultagenda', $fecha);
            Contacto::model()->actualizar($contacto->ID, 'Fecha_Ultgestion', date('Y-m-d H:i:s'));
            Contacto::model()->actualizar($contacto->ID, 'Id_Estadoweb', 8);

            $contact = Contacto::model()->findByPk($contacto->ID);
            if ($this->sendContacto($contacto->ID, $ciudad)) :
                Contacto::model()->actualizar($contact->ID, 'Aprobado4D', 1);
            else :
                Contacto::model()->actualizar($contact->ID, 'Aprobado4D', 2);
            endif;

            Citas::model()->registro($contact->ID, $contact->Fecha_Ultagenda, $contact->Asesor);

            Agendados::model()->registro($fecha, $hora, $contact->ID, Yii::app()->user->getState('id_usuario'), 2);

            $comentario = "Se Asigna al Asesor Comercial " . strtoupper($as->Nombre) . " para el dia " . $fecha . " a las " . $hora . " Horas, Observaciones : " . $observaciones;
            HistoricoGestion::model()->guardarGestion($contact->ID, $contact->Estado_proceso, $comentario, 8);
            
            $servicio = Servicios::model()->findByPk($contact->Servicio);
            $ser = '';
            
            if($servicio == 'NULL') :
                $ser = $contact->Servicio;
            else :
                $ser = "SmartSense";
            endif;

            $datos = array();
            $datos['name'] = $contact->Razon_social;
            $datos['cf_mdir'] = $contact->Direccion;
            $datos['personal_phone'] = $contact->Celular;
            $datos['cf_estado_del_clientes_general'] = 'Cita';
            $datos['cf_ciudad'] = $contact->ciudad->Nombre;
            $datos['cf_servicio'] = $ser;
            $datos['cf_mdate'] = $fecha;
            $datos['cf_mtime'] = $hora;
            $datos['cf_mname'] = $contact->asesor->Nombre;
            $datos['cf_micel'] = $contact->asesor->Celular;
            $datos['cf_memail'] = $contact->asesor->Email;
            $datos['cf_mid'] = $contact->asesor->ID;
            $datos['cf_mdir'] = $contact->Direccion;
            $datos['cf_mbar'] = $contact->Barrio;
            $datos['cf_mimage'] = "https://www.telesentinel.com/files/asesores/" . $contact->asesor->ID . ".jpg";

            //Actualiza el contacto segun los campos enviados
            if (!empty($contact->Email)) :
                $this->Enviards($contact->Email, $datos, true, false);
            endif;


            if ($sms == 1):
                $this->enviarCitaSMS($arr_aux, $contact->asesor->ID, $contact->ID, $contact->asesor->Celular);
                $this->sendMail($arr_aux, $contact->asesor->ID, $contact->ID);
                $error .= "OK";
            else :
                $error .= "OK";
            endif;
        else :
            $error .= "Asesor Comercial no encontrado ... Selecciona uno diferente";
        endif;
        echo $error;
    }

    /**
     * Coincidencias del Contacto
     */
    public function actionCoincidencias() {
        if (isset($_POST['idcontacto'])) :
            $model = Contacto::model()->findByPk($_POST['idcontacto']);
            $error = "";
            $mensaje = "";
            $coincidenciacliente = "";

            $campos = array(
                "Razon_social",
                "Direccion",
                "Email",
                "Nit",
                "Telefono",
                "Celular",
                "Contrato"
            );

            foreach ($campos as $campo) :
                $error .= $model->coincidencia($campo);
                $error .= Oportunidad::model()->coincidencia($campo);
            endforeach;

            if (!empty($model->Contrato)) :
                $coincidenciacliente = $model->referido();
            endif;

            $retorno = "";

            if (!empty($error)) :
                $model->Bloqueado = 1;
                $model->save();
                $mensaje = " Este contacto ha sido bloqueado por tener coincidencias con otros registros: <br>Puede desbloquear este contacto en la Opcion Bloqueado en el Estado Actual<br> "
                        . " <div style='height:150px; overflow:auto'>"
                        . " <table class='table table-striped table-bordered'><tr><td style='text-align:center'>CAMPO</td><td style='text-align:center'>CANTIDAD</td><td style='text-align:center'>TABLA</td><td style='text-align:center'>COINCIDEN CON</td></tr>" . $error . "</table></div>";
                $retorno .= $mensaje;
            endif;

            if (!empty($coincidenciacliente)) :
                $retorno .= " <div style='height:150px; overflow:auto'>" . $coincidenciacliente . "</div>";
            endif;

            if (empty($coincidenciacliente) && empty($error)) :
                $retorno .= "OK";
            endif;

            echo $retorno;
        else :
            echo "OK";
        endif;
    }

    /**
     * Carga el detalle de las coincidencias
     */
    public function actionDetalle() {
        $arr = explode(",", $_POST['input']);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('ID', $arr);
        $registros = $_POST['model']::model()->findAll($criteria);
        $this->renderPartial('detalle', array('registros' => $registros, 'model' => $_POST['model']));
    }

    /**
     * 
     */
    public function actionEstweb() {
        $this->renderPartial('enviosmail', array('op' => $_POST['op'], 'titulo' => $_POST['titulo'], 'idcontacto' => $_POST['idcontacto']));
    }

    /**
     * 
     */
    public function actionAgendames() {
        $filtro = "est-" . $_REQUEST['t'];
        $model = new Contacto();
        $this->initSearch();
        $this->updfltrs($filtro);
        $this->render('index', array("model" => $model));
    }

}
