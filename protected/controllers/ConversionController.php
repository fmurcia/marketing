<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConversionController
 *
 * @author scorpio
 */
class ConversionController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array("site", "countphone", "countbutton", "upmchimp", "facebook", "formweb", "upfile", "sac", "rdstation", "tokenrdstation", "refreshtokenrdstation"),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'index',
                ),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Carga los Contactos de las Landing y Paginas Externas
     */
    public function actionSite() {
        header("access-control-allow-origin: *");
        if (isset($_REQUEST['confirmacion']) && $_REQUEST['confirmacion'] == 1) :
            $email = !empty($_REQUEST["email"]) ? $_REQUEST["email"] : "@";
            $nombre = !empty($_REQUEST["nombre"]) ? strtoupper($_REQUEST["nombre"]) : "";
            $dispositivo = !empty($_REQUEST["dispositivo"]) ? $_REQUEST["dispositivo"] : "desktop";
            $landing = !empty($_REQUEST["landing"]) ? $_REQUEST["landing"] : "SITIO OFICIAL";
            $servicio = !empty($_REQUEST["servicio"]) ? $_REQUEST["servicio"] : "smartsense";
            $ciudad = !empty($_REQUEST["ciudad"]) ? $_REQUEST["ciudad"] : 11001;
            $mediocontacto = !empty($_REQUEST["mediocontacto"]) ? $_REQUEST["mediocontacto"] : 'Adwords';
            $fijo = (strlen($_REQUEST['telefono']) == 7 ) ? $_REQUEST['telefono'] : 0;
            $celular = (strlen($_REQUEST['telefono']) == 10) ? $_REQUEST['telefono'] : 0;

            $telemercadeo = Contacto::model()->regionalTelemercadeo($ciudad, 3);

            $contacto = Contacto::model()->registro($telemercadeo, $nombre, $nombre, 'REGISTRO WEB', $fijo, $celular, "Campaña Digital : " . strtoupper($landing), 3, $mediocontacto, 0, $email, $ciudad, $servicio, $dispositivo);
            if ($contacto != NULL) :
                $this->cargarEnviarSms($contacto, $_REQUEST['telefono']);
                $this->cargarMailChimp($contacto, $_REQUEST['telefono']);
            else :
                $datos = array();
                $datos['CADDRESS'] = $contacto->Direccion;
                $datos['CPHONE'] = $contacto->Celular;
                $datos['CFNAME'] = $contacto->Razon_social;
                $datos['CTYPE'] = $contacto->Tipo;
                $datos['PTYPE'] = $contacto->Actividad;
                $datos['CSTATUS'] = 'Seguimiento';
                $this->actualizarMailChimp($contacto->Email, $datos);
            endif;
            $response = "ok";
        else :
            $response = "terminos";
        endif;

        echo $response;
    }

    /**
     * captura el formulario por trabajo del Site Sitio Oficial
     */
    public function actionFormweb() {

        $nombre = isset($_POST['nombre_tr']) ? $_POST['nombre_tr'] : "";
        $celular = isset($_POST['celular_tr']) ? $_POST['celular_tr'] : "";
        $telefono = isset($_POST['telefono_tr']) ? $_POST['telefono_tr'] : "";
        $correoelectronico = isset($_POST['email_tr']) ? $_POST['email_tr'] : "";
        $observaciones = isset($_POST['observacion_tr']) ? $_POST['observacion_tr'] : "";
        $contrato = isset($_POST['contrato_tr']) ? $_POST['contrato_tr'] : "";
        $tform = isset($_POST['tipo']) ? $_POST['tipo'] : "pqr";
        $file = isset($_POST['file_tr']) ? $_POST['file_tr'] : "";

        if ($tform == 'married') :
            $titulo = 'MARRIED OFERTA';
            $adjunto = "";
            $destinatario = 'actualicedatos@telesentinel.com';
            $copia = 'servicioalcliente@telesentinel.com';
            $this->confirmacionCliente('Confirmacion de Datos', $correoelectronico, $celular, $contrato, $nombre, $observaciones);
        endif;

        if ($tform == 'pqr') :
            $titulo = 'PQR Atencion al Usuario';
            $adjunto = "";
            $destinatario = 'webservicioalcliente@telesentinel.com';
            $copia = '';
        endif;

        if ($tform == 'trabajo') :
            $titulo = 'EMPLEO TELE';
            $adjunto = '/efs/archivos/site/' . $file;
            $destinatario = 'ksanchez@telesentinel.com';
            $copia = 'trabajo@telesentinel.com';
        endif;

        $contenido = '<h2>REGISTRO WEB ' . strtoupper($titulo) . '</h2><br />';
        $contenido .= '<b>Nombre : </b>' . $nombre . '<br />';
        $contenido .= '<b>Celular : </b>' . $celular . '<br />';
        $contenido .= '<b>Telefono : </b>' . $telefono . '<br />';
        $contenido .= '<b>Contrato : </b>' . $contrato . '<br />';
        $contenido .= '<b>Email : </b>' . $correoelectronico . '<br />';
        $contenido .= '<b>Comentarios : </b>' . strtolower($observaciones) . '<br />';
        $contenido .= '<br />'
                . '<img src="http://www.telesentinel.com.mx/images2/firma2.jpg" class="block " width="180" height="280"  style="border: 0px; transform-origin: left top 0px; display: block;" alt="">'
                . '<br><b>Departamento de Sistemas </b><br>'
                . 'Tel.: (57-1) 2888788 Ext.: 1603 <br>'
                . '|Brasil|Colombia|Inglaterra|Israel|Mexico|';

        $this->sendMailWeb($destinatario, $titulo, $contenido, $adjunto, $copia);
    }

    /**
     * captura el formulario por trabajo del Site Sitio Oficial
     */
    public function actionSac() {

        $referido = isset($_POST['referido']) ? $_POST['referido'] : "";
        $telreferido = isset($_POST['telreferido']) ? $_POST['telreferido'] : "";
        $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
        $telefono = isset($_POST['telefono']) ? $_POST['telefono'] : "";
        $correoelectronico = isset($_POST['email']) ? $_POST['email'] : "";
        $direccion = isset($_POST['direccion']) ? $_POST['direccion'] : "";
        $formulario = isset($_POST['formulario']) ? $_POST['formulario'] : "";
        $celular = isset($_POST['celular']) ? $_POST['celular'] : "";
        $cargo = isset($_POST['cargo']) ? $_POST['cargo'] : "";
        $empresa = isset($_POST['empresa']) ? $_POST['empresa'] : "";
        $comentarios = isset($_POST['comentarios']) ? $_POST['comentarios'] : "";

        $adjunto = "";
        $destinatario = 'atencionalcliente@telesentinel.com';
        $copia = 'sacoficina@telesentinel.com';

        if (!empty($formulario)) :
            $titulo = 'SOLICITUD EJECUTIVO DE SERVICIO';
            $contenido = '<h2>SOLICITUD EJECUTIVO DE SERVICIO</h2><br />';
            $contenido .= '<b>Empresa : </b>' . $empresa . '<br />';
            $contenido .= '<b>Nombre : </b>' . $nombre . '<br />';
            $contenido .= '<b>Cargo : </b>' . $cargo . '<br />';
            $contenido .= '<b>Teléfono Fijo : </b>' . $telefono . '<br />';
            $contenido .= '<b>Teléfono Celular : </b>' . $celular . '<br />';
            $contenido .= '<b>Comentarios : </b>' . $comentarios . '<br />';
        else :
            $titulo = 'SAC Referido Web';
            $contenido = '<h2>REFERIDOS</h2><br />';
            $contenido .= '<b>Referido por : </b>' . $referido . '<br />';
            $contenido .= '<b>Contacto referido : </b>' . $telreferido . '<br />';
            $contenido .= '<b>Nombre : </b>' . $nombre . '<br />';
            $contenido .= '<b>Telefono : </b>' . $telefono . '<br />';
            $contenido .= '<b>Direccion : </b>' . $direccion . '<br />';
            $contenido .= '<b>Email : </b>' . $correoelectronico . '<br />';
        endif;

        $contenido .= '<br />'
                . '<img src="http://www.telesentinel.com.mx/images2/firma2.jpg" class="block " width="180" height="280"  style="border: 0px; transform-origin: left top 0px; display: block;" alt="">'
                . '<br><b>Departamento Servicio al Cliente </b><br>'
                . 'Tel.: (57-1) 2888788 Ext.: 1861 <br>'
                . '|Brasil|Colombia|Inglaterra|Israel|Mexico|';

        $this->sendMailWeb($destinatario, $titulo, $contenido, $adjunto, $copia);
    }

    /**
     * Genera la Respuesta para el Cliente segun su Seleccion
     * @param String $email Email del Cliente
     * @param int $opcion Tipo de Respuesta Si : 1 ,  No : 2
     * @return boolean Respuesta del  Envio
     */
    public function confirmacionCliente($asunto, $correos, $telefonos, $contrato, $nombre, $comentarios) {

        $destinatario = explode(',', $correos);
        if (sizeof($destinatario) > 0):
            $correo = $destinatario[0];
        else :
            $correo = $correos;
        endif;

        Yii::import('ext.phpmailer.JPhpMailer');
        $mail = new JPhpMailer();
        $mail->isSMTP();                                                  // Set mailer to use SMTP
        $mail->Host = 'email-smtp.us-west-2.amazonaws.com';               // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                           // Enable SMTP authentication
        $mail->Username = 'AKIAIURMRPOHPU32Q7DQ';                         // SMTP username
        $mail->Password = ' AkmTrrcl32b0FIoKrE6/moNqK1fU0jg+eD3CFNKACxEp';                                     // SMTP password
        $mail->SMTPSecure = 'tls';                                        // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 2587;                                               // TCP port to connect to
        $mail->SetFrom('actualicedatos@telesentinel.com', 'EMAIL TELESENTINEL');
        $mail->AddAddress($correo);  // Add a recipient
        $mail->Subject = $asunto;
        $mail->AltBody = 'Este Mensaje es para uso Interno de la Compañia';

        $mail->AddBCC('jzarate@telesentinel.com');  // Add a recipients
        $mail->AddBCC('gcarvajal@telesentinel.com');  // Add a recipients
        $mail->AddBCC('jballen@telesentinel.com');  // Add a recipients

        $html = 'Nombre: ' . $nombre . ' <br><br><br> Contrato: ' . $contrato . ' <br><br><br> Comentarios : ' . $comentarios . '<br><br><br>';
        $html .= 'Apreciado cliente, le informamos que ha inscrito de forma exitosa los correos: ' . $correos . ' <br><br><br> Y ha adquirido el servicio de mensajes de texto para los celulares: ' . $telefonos . ' <br><br><br> Aplican condiciones y restricciones <br><br><br> Muchas gracias, siempre buscamos mejorar para ti.';
        $html .= '<br />'
                . '<img src="http://www.telesentinel.com.mx/images2/firma2.jpg" class="block " width="180" height="280"  style="border: 0px; transform-origin: left top 0px; display: block;" alt="">'
                . '<br><b>Departamento Servicio al Cliente </b><br>'
                . 'Tel.: (57-1) 2888788 Ext.: 1861 <br>'
                . '|Brasil|Colombia|Inglaterra|Israel|Mexico|';

        $mail->MsgHTML(utf8_decode($html));

        try {
            if ($mail->send()) :
                return 0;
            else :
                return 1;
            endif;
        } catch (Exception $e) {
            return 1;
        }
    }

    /**
     * Genera la Respuesta para el Cliente segun su Seleccion
     * @param String $email Email del Cliente
     * @param int $opcion Tipo de Respuesta Si : 1 ,  No : 2
     * @return boolean Respuesta del  Envio
     */
    public function sendMailWeb($destinatario, $asunto, $contenido, $adjunto = '', $copia = '') {

        Yii::import('ext.phpmailer.JPhpMailer');
        $mail = new JPhpMailer();
        $mail->isSMTP();                                                  // Set mailer to use SMTP
        $mail->Host = 'email-smtp.us-west-2.amazonaws.com';               // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                           // Enable SMTP authentication
        $mail->Username = 'AKIAIURMRPOHPU32Q7DQ';                         // SMTP username
        $mail->Password = ' AkmTrrcl32b0FIoKrE6/moNqK1fU0jg+eD3CFNKACxEp';                                     // SMTP password
        $mail->SMTPSecure = 'tls';                                        // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 2587;                                               // TCP port to connect to
        $mail->SetFrom('gcarvajal@telesentinel.com', 'SITIO OFICIAL WEB');
        $mail->AddAddress($destinatario);  // Add a recipient
        $mail->Subject = $asunto;
        $mail->AltBody = 'Este Mensaje es para uso Interno de la Compañia';

        if (!empty($copia)) :
            $mail->AddAddress($copia);  // Add a recipient
        endif;

        if (!empty($adjunto)) :
            $mail->AddAttachment($adjunto);
        endif;

        $html = $contenido;

        $mail->MsgHTML(utf8_decode($html));

        try {
            if ($mail->send()) :
                return 0;
            else :
                return 1;
            endif;
        } catch (Exception $e) {
            return 1;
        }
    }

    /**
     * Metodo que Carga el Archivo adjunto al servidor para enviarlo por email desde la pagina Site de Telesentinel
     */
    public function actionUpfile() {
        if (isset($_FILES["file"])) {
            $file = $_FILES["file"];
            $nombre = $file["name"];
            $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];

            // application/msword ======> Word 2003,
            // application/vnd.openxmlformats-officedocument.wordprocessingml.document ===========> Word 2007 and 2010
            // application/vnd.ms-excel =======> Excel 2003
            // application/vnd.openxmlformats-officedocument.spreadsheetml.sheet  =========> Excel 2007 and 2010
            // Cargar en un arreglo los formatos de archivos permitidos
            $tipos = array("application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/pdf");
            if (in_array($tipo, $tipos)) {
                $ext = explode('.', $nombre);
                $src = '/efs/archivos/site/' . $nombre;
                if (!move_uploaded_file($ruta_provisional, $src)) {
                    echo 1;
                } else {
                    $cryp = MD5($nombre);
                    $arch = $cryp . '.' . $ext[1];
                    rename($src, "/efs/archivos/site/" . $arch);
                    chmod("/efs/archivos/site/" . $arch, 777);
                    echo $arch;
                }
            } else {
                echo 2;
            }
        }
    }

    /**
     * Cuenta los Click al Boton Llamar de las landing
     */
    public function actionCountPhone() {
        $count = RegistroMovil::model()->registro($_POST['promo']);
        echo $count;
    }

    /**
     * Cuenta los Click al Boton Trabajo de las landing
     */
    public function actionCountButton() {
        $count = RegistroLanding::model()->registro($_POST['accion'], $_POST['sitio']);
        echo $count;
    }

    public function actionUpmchimp() {
        $email = !empty($_REQUEST["email"]) ? $_REQUEST["email"] : "@";
        $razon = !empty($_REQUEST["razon"]) ? strtoupper($_REQUEST["razon"]) : "";
        $nombre = !empty($_REQUEST["nombre"]) ? strtoupper($_REQUEST["nombre"]) : "";
        $fijo = (strlen($_REQUEST['telefono']) == 7 ) ? $_REQUEST['telefono'] : 0;
        $celular = (strlen($_REQUEST['telefono']) == 10) ? $_REQUEST['telefono'] : 0;

        $telefono = 0;
        if ($celular > 0) :
            $telefono = $celular;
        elseif ($fijo > 0) :
            $telefono = $fijo;
        endif;

        $datos = array();
        $datos['CPHONE'] = $telefono;
        $datos['CFNAME'] = $razon;
        $datos['CLNAME'] = $nombre;
        $datos['CSTATUS'] = 'Interesado';

        //Actualiza el contacto si existe y actualiza sus datos
        Contacto::model()->setEmail($email, $telefono, $razon, $nombre);

        //Actualiza el contacto segun los campos enviados
        $this->actualizarMailChimp($email, $datos);
        echo "ok";
    }

    /**
     * Carga los Contactos de las Landing y Paginas Externas
     */
    public function actionFacebook() {

        $email = !empty($_REQUEST["email"]) ? $_REQUEST["email"] : "@";
        $nombre = !empty($_REQUEST["nombre"]) ? strtoupper($_REQUEST["nombre"]) : "";
        $servicio = 'SmartSense';
        $mediocontacto = !empty($_REQUEST['formulario']) ? strtolower($_REQUEST['formulario']) : 'facebook';
        $ciudad = !empty($_REQUEST['ciudad']) ? strtolower($_REQUEST['ciudad']) : 11001;

//        if (substr($_REQUEST['telefono'], 0, 1) == '+') :
//            $arr_tel = explode('+5', $_REQUEST['telefono']);
//            if (strlen($arr_tel[1]) > 10) :
                //$celular = $arr_tel[1];
                $celular = $_REQUEST['telefono'];
//            endif;
//        else :
//            $celular = !empty($_REQUEST['telefono']) ? strtolower($_REQUEST['telefono']) : 0;
//        endif;

        $telemercadeo = Contacto::model()->regionalTelemercadeo($ciudad, 3);
        
        $ciu = Ciudad::model()->findByPk($ciudad);
        
        $arr = array();
        
        $arr['ciudad'] = $ciudad;
        $arr['persona'] = isset($_REQUEST['nombre']) ? $_REQUEST['nombre'] : '';
        $arr['direccion'] = '';
        $arr['email'] = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $arr['observaciones'] = isset($_POST['comentarios_direccion']) ? $_POST['comentarios_direccion'] : '';
        $arr['razon'] = isset($_REQUEST['nombre']) ? $_REQUEST['nombre'] : '';
        $arr['tipocontacto'] = 3;
        $arr['mediocontacto'] = isset($_REQUEST['formulario']) ? (!empty($_REQUEST['formulario']) ? $_REQUEST['formulario'] : 'facebook' ) : 'facebook';
        $arr['segmentacion'] = '1';

        $arr['servicio'] = 1;
        $arr['fijo'] = 0;
        $arr['celular'] = $celular;
        $arr['telemercadeo'] = $telemercadeo;
        
        $arr['name'] = isset($_REQUEST['nombre']) ? $_REQUEST['nombre'] : '';
        $arr['personal_phone'] = $celular;
        $arr['cf_mdate'] = date('Y-m-d');
        $arr['cf_estado_del_clientes_general'] = 'Bienvenido';
        $arr['cf_servicio'] = $servicio;
        $arr['cf_mediocontacto'] = $mediocontacto;
        $arr['cf_tipo_cliente'] = "Residencial";
        $arr['cf_ciudad'] = $ciu->Nombre;
        $arr['cf_mediocontacto'] = isset($_REQUEST['formulario']) ? (!empty($_REQUEST['formulario']) ? $_REQUEST['formulario'] : 'facebook' ) : 'facebook';
        $arr['cf_tipo_cliente'] = 'Residencial';
        
        $contacto = Contacto::model()->insertarContacto($arr);

        if ($contacto != NULL) :
            $ciudad_email = Regional::model()->findByPk($ciudad);
            $arr_data = array();
            $arr_data['email'] = $email;
            $arr_data['nombre'] = $nombre;
            $arr_data['ciudad'] = $ciudad_email->Descripcion;
            $arr_data['origen'] = $mediocontacto;
            $arr_data['telefono'] = ($celular > 0) ? $celular : 0;
            /* Envio Sms */
            $this->enviarNotificacionSMS($arr_data);
            $this->PatchLeadrdstation($email, $arr);            
        endif;
        echo "OK";
        $this->_sendResponse(200, 'Se Guardo con Exito');
    }

    public function actionRdstation() {
        // Takes raw data from the request
        $json = file_get_contents('php://input');
        // Converts it into a PHP object
        $data = json_decode($json);
        
        file_put_contents('/tmp/data.txt', print_r($data, true) . PHP_EOL, FILE_APPEND);

        $celular = $data->leads[0]->last_conversion->content->telefone;
        $email = !empty($data->leads[0]->last_conversion->content->email_lead) ? $data->leads[0]->last_conversion->content->email_lead : "@";
        $nombre = !empty($data->leads[0]->last_conversion->content->nome) ? strtoupper($data->leads[0]->last_conversion->content->nome) : "";
        $mediocontacto = !empty($data->leads[0]->last_conversion->conversion_origin->source) ? $data->leads[0]->last_conversion->conversion_origin->source : 'Direct';

        $servicio = 'smartsense';

        $ciudad = 11001;
        if (substr($data->leads[0]->custom_fields->Ciudad, 0, 3) == 'Bog') :
            $ciudad = 11001;
        elseif (substr($data->leads[0]->custom_fields->Ciudad, 0, 3) == 'Med') :
            $ciudad = 5001;
        elseif (substr($data->leads[0]->custom_fields->Ciudad, 0, 3) == 'Cal') :
            $ciudad = 76001;
        elseif (substr($data->leads[0]->custom_fields->Ciudad, 0, 3) == 'Buc') :
            $ciudad = 68001;
        endif;

        $coleccion = array();
        $coleccion['telemercaderista'] = Contacto::model()->regionalTelemercadeo($ciudad, 3);
        $coleccion['razon'] = $nombre;
        $coleccion['persona'] = $nombre;
        $coleccion['direccion'] = "";
        $coleccion['celular'] = $celular;
        $coleccion['observaciones'] = "";
        $coleccion['mediocontacto'] = $mediocontacto;
        $coleccion['servicio'] = $servicio;
        $coleccion['email'] = $email;
        $coleccion['ciudad'] = $ciudad;

        Contacto::model()->registroRds($coleccion);
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html') {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status;
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
        }
        // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>' . $status . '</title>
</head>
<body>
    <h1>Registrado</h1>
    <p>' . $message . '</p>
    <hr />
    <address>' . $signature . '</address>
</body>
</html>';

            echo $body;
        }
        Yii::app()->end();
    }

}
