<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BusquedaController
 *
 * @author scorpio
 */
class BuscarController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
    /**
     * 
     * @return type
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
    /**
     * 
     * @return type
     */
    public function accessRules() {
        return array(
//            array('allow', // allow all users to perform 'index' and 'view' actions
//                'actions' => array('*'),
//                'users' => array('*'),
//            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'resultados'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    /**
     * 
     */
    public function actionIndex() {
        $this->render('index');
    }
    /**
     * 
     */
    public function actionResultados() {

        $criteria = new CDbCriteria();

        if (!empty($_POST['modulador1']) || !empty($_POST['valor1']) || !empty($_POST['criterio1'])) :

            if ($_POST['criterio1'] != 'Contrato') :
                if ($_POST['modulador1'] == 'LIKE') :
                    $criteria->addCondition($_POST['criterio1'] . ' LIKE :tn');
                    $criteria->params = array(':tn' => '%' . $_POST['valor1'] . '%');
                elseif ($_POST['modulador1'] == 'BETWEEN') :
                    $criteria->addBetweenCondition($_POST['criterio1'], $_POST['valor1'], $_POST['valor2']);
                else :
                    $criteria->addCondition($_POST['criterio1'] . ' ' . $_POST['modulador1'] . ' "' . $_POST['valor1'] . '"');
                endif;
            endif;

            if ($_POST['op1'] == 'AND' || $_POST['op1'] == 'OR') :
                if (!empty($_POST['modulador2']) || !empty($_POST['valor2']) || !empty($_POST['criterio2'])) :
                    if ($_POST['criterio2'] != 'Contrato') :
                        if ($_POST['modulador2'] == 'LIKE') :
                            $criteria->addCondition($_POST['criterio2'] . ' LIKE :tn');
                            $criteria->params = array(':tn' => '%' . $_POST['valor2'] . '%');
                        elseif ($_POST['modulador2'] == 'BETWEEN') :
                            $criteria->addBetweenCondition($_POST['criterio2'], $_POST['valor1'], $_POST['valor2']);
                        else :
                            $criteria->addCondition($_POST['criterio2'] . ' ' . $_POST['modulador2'] . ' "' . $_POST['valor2'] . '"');
                        endif;
                    endif;
                else :
                    $error = 2;
                endif;
            endif;

            if (isset($_POST['op2']) && ($_POST['op2'] == 'AND' || $_POST['op2'] == 'OR')) :
                if (!empty($_POST['modulador3']) || !empty($_POST['valor3']) || !empty($_POST['criterio3'])) :
                    if ($_POST['criterio3'] != 'Contrato') :
                        if ($_POST['modulador3'] == 'LIKE') :
                            $criteria->addCondition($_POST['criterio3'] . ' LIKE :tn');
                            $criteria->params = array(':tn' => '%' . $_POST['valor3'] . '%');
                        elseif ($_POST['modulador3'] == 'BETWEEN') :
                            $criteria->addBetweenCondition($_POST['criterio3'], $_POST['valor2'], $_POST['valor3']);
                        else :
                            $criteria->addCondition($_POST['criterio3'] . ' ' . $_POST['modulador3'] . ' "' . $_POST['valor3'] . '"');
                        endif;
                    endif;
                else :
                    $error = 3;
                endif;
            endif;
        else :
            $error = 1;
        endif;
	$criteria->order = 'Fecha_creacion DESC';
        $criteria->limit = 15;
        $sql = $_POST['tabla']::model()->findAll($criteria);
        $this->renderPartial('viewresponse', array('data' => $sql, 'tipo' => $_POST['tabla']));
    }
}
