<?php

/**
 * Controlador de Eventos del Sitio Principal
 * Clase para el Manejo de Controladores Iniciales
 * 
 * @author Gustavo Carvajal <gcarvajal@telesentinel.com>
 * @version 1.0
 * 
 */
class ConfigController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (Yii::app()->user->getState('id_usuario')) :
            // Renderiza la Vista
            $this->render('index');
        else :
            $this->redirect(Yii::app()->createUrl('lock/plugin', array('estado' => 1)));
        endif;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'agencias', 'tipos', 'regionales', 'agencias', 'search', 'resultados', 'loadopor', 'upmodelo', 'upcliente', 'upestadoagencia'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionTipos() {
        $as = AsesorTipocontacto::model()->findByAttributes(array('ID_Asesor' => $_POST['as'], 'ID_Tipo' => $_POST['it']));
        if ($as != NULL) :
            $as->Estado = $_POST['op'];
            $as->update();
        else :
            $ass = new AsesorTipocontacto();
            $ass->ID_Asesor = $_POST['as'];
            $ass->ID_Tipo = $_POST['it'];
            $ass->Estado = $_POST['op'];
            $ass->save();
        endif;
    }

    public function actionRegionales() {

        $as = RegionalAsesor::model()->findByAttributes(array('ID_Asesor' => $_POST['as'], 'ID_Regional' => $_POST['it']));
        if ($as != NULL) :
            $as->Estado = $_POST['op'];
            $as->update();
        else :
            $ass = new RegionalAsesor();
            $ass->ID_Regional = $_POST['it'];
            $ass->ID_Asesor = $_POST['as'];
            $ass->Estado = $_POST['op'];
            $ass->save();
        endif;

        $query = '  SELECT ra.ID_Agencia, ar.ID_Asesor '
                . ' FROM regional_agencia ra, regional_asesor ar '
                . ' WHERE ar.ID_Regional = ra.ID_Regional '
                . ' AND ar.ID_Regional = ' . $_POST['it'] . ' ';

        $command = Yii::app()->db->createCommand($query);
        $dataReader = $command->query();

        foreach ($dataReader as $d) :
            if ($d['ID_Asesor'] == $_POST['as']) :
                $com = ComercialAgencias::model()->findByAttributes(array('ID_Asesor' => $_POST['as'], 'ID_Agencia' => $d['ID_Agencia']));
                if ($com != NULL) :
                    $com->Estado = $_POST['op'];
                    $com->update();
                else :
                    if ($_POST['op'] == 1) :
                        $com2 = new ComercialAgencias();
                        $com2->ID_Asesor = $_POST['as'];
                        $com2->ID_Agencia = $d['ID_Agencia'];
                        $com2->Estado = 1;
                        $com2->save();
                    endif;
                endif;
            endif;
        endforeach;
    }

    public function actionAgencias() {
        $as = ComercialAgencias::model()->findByAttributes(array('ID_Asesor' => $_POST['as'], 'ID_Agencia' => $_POST['it']));
        if ($as != NULL) :
            $as->Estado = $_POST['op'];
            $as->update();
        else :
            $reg = new ComercialAgencias();
            $reg->ID_Agencia = $_POST['it'];
            $reg->ID_Asesor = $_POST['as'];
            $reg->Estado = $_POST['op'];
            $reg->save();
        endif;
    }

    public function actionLoadopor() {
        $loadOpor = AsesorOportunidad::model()->findByAttributes(array('ID_Asesor' => $_POST['idasesor']));
        if ($_POST['tipo'] == 1) :
            $loadOpor->ID_Oportunidad = $_POST['op'];
        else :
            $loadOpor->ID_Horizontal = $_POST['op'];
        endif;

        if ($loadOpor->save()) :
            echo "OK";
        else :
            echo "Error";
        endif;
    }

    public function actionUpmodelo() {
        $modelo = $_POST['modelo'];
        $campo = $_POST['campo'];
        $valor = $_POST['valor'];
        $reg = $modelo::model()->findByPk($_POST['id']);
        if($reg != NULL) :
            $reg->$campo = $valor;
            if($reg->update()):
                echo $this->renderPartial('viewregional', array(), false, true);
            else :
                echo 1;
            endif;
        else :    
            echo 2;    
        endif;   
    }

    public function actionUpcliente() {
        $postdata = file_get_contents("php://input");
        if ($postdata != "") :
            $xml = simplexml_load_string($postdata);
            
            $sg = 1;
            
            $TipoCli = TipoCliente::model()->findByAttributes(array("Nombre" => (string) $xml->TipoCliente));
            if ($TipoCli == null) :
                $TpCli = 1;
            else :
                $TpCli = $TipoCli->ID;
                $sg = $TipoCli->ID_Segmento;
            endif;

            $asesor = Asesor::model()->findByAttributes(array("ID" => (int) $xml->Asesor));
            if ($asesor != null) :
                $asesorID = $asesor->ID;
            else :
                $asesorID = 60;
            endif;

            $agencia = Agencia::model()->findByAttributes(array("ID" => (int) $xml->Agencia));
            if ($agencia != null) :
                $agenciaID = $agencia->ID;
            else :
                $agenciaID = 1;
            endif;

            $cliente = Cliente::model()->findByAttributes(array("Contrato" => (int) $xml->Contrato));

            if ($cliente == null) :
                $cliente = new Cliente();
            endif;
            
            $cliente->Tipo_persona = (int) $xml->TipoPersona;
            $cliente->Razon_social = (string) $xml->RazonSocial;
            $cliente->Nombre_1 = (string) $xml->Nombre1;
            $cliente->Nombre_2 = (string) $xml->Nombre2;
            $cliente->Apellido_1 = (string) $xml->Apellido1;
            $cliente->Apellido_1 = (string) $xml->Apellido2;
            $cliente->Contrato = (int) $xml->Contrato;
            $cliente->Fecha = date("Y-m-d", strtotime($xml->Fecha));
            $cliente->Direccion_instalacion = (string) $xml->DirInstala;
            $cliente->Barrio_instalacion = (string) $xml->BarrioInstala;
            $cliente->Telefono_instalacion = (string) $xml->TelInstala;
            $cliente->Direccion_cobro = (string) $xml->DirCobro;
            $cliente->Barrio_cobro = (string) $xml->BarrioCobro;
            $cliente->Telefono_cobro = (string) $xml->TelCobro;
            $cliente->Num_documento = (int) $xml->NumDocumento;
            $cliente->Digito_verificacion = (int) $xml->DigVerificacion;
            $cliente->Tipo_documento = (string) $xml->TipoDocumento;
            $cliente->Representante_legal = (string) $xml->Representante;
            $cliente->Cedula = (int) $xml->CedulaRep;
            $cliente->Celular = (string) $xml->CelularRep;
            $cliente->Num_cotizacion = (string) $xml->NumCotiza;
            $cliente->Duracion_contrato = (int) $xml->DurContrato;
            $cliente->Tipo_cliente = $TpCli;
            $cliente->Segmento = $sg;
            $cliente->Hora = date("H:i:s");
            $cliente->Asesor = $asesorID;
            $cliente->Agencia = $agenciaID;
            $cliente->Ciudad = (int) $xml->Ciudad;
            $cliente->Fecha_comercial = date("Y-m-d", strtotime($xml->FechaComercial));
            if ($cliente->save())  : 
                echo "Cliente : ".$cliente->ID." Contrato : " . $cliente->Contrato . " Sincronizado con exito";
                $dtc = DetalleServicio::model()->findByPk((int)$xml->IDweb);
                if($dtc != NULL) :
                    $dtc->Valor_servicio = (int)$xml->IDvalor;
                    $dtc->ID_serv4D = (int)$xml->IDzona;
                    if($dtc->update()) :    
                        echo $dtc->ID_serv4D;
                    else :   
                        file_put_contents('/var/www/html/telemark/assets/NoActualizoValores.txt', print_r($cliente->Contrato, true).PHP_EOL, FILE_APPEND);
                    endif;
                else :
                    $dtc2 = DetalleServicio::model()->findByAttributes(array( 'ID_serv4D' => (int)$xml->IDzona));
                    if($dtc2 != NULL) :
                        $dtc2->Valor_servicio = (int)$xml->IDvalor;
                        if($dtc2->update()) :
                            echo $dtc2->ID_serv4D;
                        else :
                            file_put_contents('/var/www/html/telemark/assets/NoActualizoContratos.txt', print_r($cliente->Contrato.' - '.$dtc2->Valor_servicio, true).PHP_EOL, FILE_APPEND);
                        endif;
                    endif;
                endif;
            else :
               echo "No se pudo crear el cliente " . json_encode($cliente->errors);
            endif;
        else :
            echo "XML vacio";
        endif;
    }
    
    public function actionUpestadoagencia() {
        $agencia = Agencia::model()->findByPk($_POST['id']);
        $this->renderPartial('estadoagencia', array('model' => $agencia), false, true);
    }
}
