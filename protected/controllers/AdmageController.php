<?php

/**
 * Description of SeguimientosController
 *
 * @author scorpio
 */
class AdmageController extends Controller {

    public $filtro;

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'index',
                    'qryContacto',
                    "updfltr",
                    "direccion",
                    'pageslist',
                    'srchtrm',
                    'formulario',
                    'upcontact',
                    'upcontacto',
                    'comercial',
                    'enviarcontacto',
                    'guardargestion',
                    'coincidencias',
                    'detalle',
                    'viewage',
                    'viewageop',
                    'notificacion',
                    'geslistage'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * 
     */
    public function actionIndex() {
        $model = new Contacto();
        $this->initSearch();
        $this->updfltrs("");
        $this->render('index', array("model" => $model));
    }

    /**
     * Inicializa las busquedas
     */
    private function initSearch() {
        Yii::app()->user->setState("term", "");
        $this->setFltrtxt("");
    }

    /**
     * 
     * @param type $fltrtxt
     */
    private function setFltrtxt($fltrtxt) {
        Yii::app()->user->setState("fltrtxt", $fltrtxt);
    }

    public function actionUpdfltr() {
        $fltrtxt = $_POST["fltr"];
        $this->setFltrtxt($fltrtxt);
        $this->updfltrs($fltrtxt);
        echo "OK";
    }

    /**
     * 
     * @param type $fltrtxt
     */
    private function updfltrs($fltrtxt = "") {
        $this->limpiarFiltros();
        $term = Yii::app()->user->getState("term");
        Yii::app()->user->setState("status", true);
        $this->porTermino($term);
        $this->porComercial();
        if ($fltrtxt != "") {
            $filtros = explode(",", $fltrtxt);
            if (count($filtros) > 0) {
                sort($filtros);
                foreach ($filtros as $filtro) {
                    $split = explode("-", $filtro);
                    $tipo = $split[0];
                    $id = $split[1];
                    switch ($tipo) {
                        case "est":
                            Yii::app()->user->setState("status", false);
                            $this->porEstado($id);
                            break;
                        case "tip":
                            $this->porTipo($id);
                            break;
                        case "ant":
                            $this->porAntiguedad($id);
                            break;
                        case "age":
                            $this->porAgencia($id);
                            break;
                        case "pro":
                            $this->porPropiedad($id);
                            break;
                        case "time":
                            $this->porTiempo($id, $split[2]);
                            break;
                    }
                }
            }
        }
        $this->fixFilters();
    }

    /**
     * 
     * @param type $id
     */
    private function porPropiedad($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["pro"])) {
                $criteria->addCondition("iDContacto.Horizontal = '$id'", "OR");
            } else {
                $pos["pro"] = true;
                $criteria->addCondition("iDContacto.Horizontal = '$id'", "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     */
    private function porComercial() {
        $criteria = $this->getFilter();
        if ($criteria != null) :
            $criteria->addCondition("ID_Asesor=" . Yii::app()->user->getState("id_usuario"));
        endif;
        $this->setFilter($criteria);
    }

    /**
     * 
     */
    private function porTiempo($anio, $mes) {

        $fechaini = $anio . "-" . $mes . "-01";
        $fechafin = $anio . "-" . $mes . "-30";

        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            $crtmp = new CDbCriteria();
            $crtmp->addBetweenCondition("iDContacto.Fecha_creacion", $fechaini, $fechafin, "OR");
            $crtxt = $crtmp->condition;
            $crtxt = str_replace("AND", "<AND>", $crtxt);

            if (isset($pos["time"])) {
                $criteria->addCondition($crtxt, "OR");
                $criteria->params = CMap::mergeArray($criteria->params, $crtmp->params);
            } else {
                $pos["time"] = true;
                $criteria->addCondition($crtxt, "AND");
                $criteria->params = CMap::mergeArray($criteria->params, $crtmp->params);
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $criteria
     * @return \CDbCriteria
     */
    private function fixFilters($criteria = null) {
        $skip = false;
        if ($criteria == null) {
            $skip = true;
            $criteria = $this->getFilter();
        }
        $critstring = $criteria->condition;
        if ($critstring != "") {
            $params = $criteria->params;
            $critstring = str_replace("(", " ", $critstring);
            $critstring = str_replace(")", " ", $critstring);
            $critstring = trim(preg_replace('/\s+/', ' ', $critstring));

            $critstring = "(" . str_ireplace(" AND ", ") AND (", $critstring) . ")";
            $critstring = str_ireplace("<AND>", "AND", $critstring);

            $criteria = new CDbCriteria();
            $criteria->addCondition($critstring);
            $criteria->params = $params;
        }
        if ($skip == true) {
            $this->setFilter($criteria);
        } else {
            return $criteria;
        }
    }

    /**
     * 
     */
    public function actionSrchtrm() {
        if (isset($_POST["term"])) {
            Yii::app()->user->setState("term", $_POST["term"]);
            $this->updfltrs($this->getFltrtxt());
        }
        echo "OK";
    }

    /**
     * 
     */
    private function limpiarFiltros() {
        Yii::app()->user->setState("fltini", array());
        $filters = new CDbCriteria();
        $this->setFilter($filters);
    }

    /**
     * 
     * @param type $id
     */
    private function porTermino($id = "") {
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if ($id != "") {
                $criteria->compare("iDContacto.ID", $id, true, "OR");
                $criteria->compare("iDContacto.Razon_social", $id, true, "OR");
                $criteria->compare("iDContacto.Direccion", $id, true, "OR");
                $criteria->compare("iDContacto.Nit", $id, true, "OR");
                $criteria->compare("iDContacto.Nombre_completo", $id, true, "OR");
                $criteria->compare("iDContacto.Celular", $id, true, "OR");
                $criteria->compare("iDContacto.Email", $id, true, "OR");
            }
        }
        $this->setFilter($criteria);
    }

    /**
     * 
     * @param type $id
     */
    private function porEstado($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["est"])) {
                $criteria->addCondition("iDContacto.Id_Estadoweb=" . $id, "OR");
            } else {
                $pos["est"] = true;
                $criteria->addCondition("iDContacto.Id_Estadoweb=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porTipo($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["tip"])) {
                $criteria->addCondition("iDContacto.Tipo_contacto=" . $id, "OR");
            } else {
                $pos["tip"] = true;
                $criteria->addCondition("iDContacto.Tipo_contacto=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porAntiguedad($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["ant"])) {
                $criteria->addCondition("iDContacto.Fecha_Ultgestion=" . $id, "OR");
            } else {
                $pos["ant"] = true;
                $criteria->addCondition("iDContacto.Fecha_Ultgestion=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $filters
     */
    private function setFilter($filters) {
        Yii::app()->user->setState("filtros", $filters);
    }

    /**
     * 
     * @return \CDbCriteria
     */
    private function getFilter() {
        $filter = Yii::app()->user->getState("filtros");
        if ($filter == null) {
            $filter = new CDbCriteria();
            $this->setFilter($filter);
        }
        return $filter;
    }

    /**
     * 
     * @return string
     */
    private function getFltrtxt() {
        $fltrtxt = Yii::app()->user->getState("fltrtxt");
        if ($fltrtxt == null) {
            $fltrtxt = "";
            $this->setFltrtxt($fltrtxt);
        }
        return $fltrtxt;
    }

    /**
     * 
     */
    public function actionQrycontacto() {
        $limit = 10;
        $pag = (isset($_POST["pag"]) ? ((int) $_POST["pag"]) - 1 : -1);
        $offset = ($pag > 0 ? $pag * $limit : -1);
        $unegs = $this->qryContacto($offset);
        $counter = $this->ttRegContacto();
        $this->renderPartial("tablaTodos", array("contactos" => $unegs, "ajax" => true, 'limit' => $limit, 'counter' => $counter, 'pag' => $pag + 1));
    }

    /**
     * 
     * @param type $offset
     * @param type $tipo
     * @return type
     */
    public function qryContacto($offset = -1) {
        $criteria = $this->getFilter();
        $criteria->offset = $offset;
        $criteria->limit = 10;
        $criteria->order = "t.ID DESC";
        $contactos = Agendados::model()->with('iDContacto')->findAll($criteria);

        if (sizeof($contactos) > 0) :
            return $contactos;
        else :
            return array();
        endif;
    }

    public function ttRegContacto() {
        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = array("count(iDContacto.ID) as counter");
        $critreal->limit = -1;
        $critreal->offset = -1;
        $confltr = Agendados::model()->with('iDContacto')->find($critreal);
        if($confltr != NULL) :
            return $confltr->counter;
        else :
            return 0;
        endif;
        
    }

    public function actionDebug() {
        Yii::import("ext.httpclient.*");
        $message = urlencode("prueba");
        $destino = 3114695714;
        $sitios = Sitios::model()->findByPk(13);
        $connection = $sitios->Controlador . "?mensaje=" . $message . "&destino=" . $destino;
        $client = new EHttpClient($connection, array(
            "maxredirects" => 3,
            "timeout" => 10
        ));
        $response = $client->request();

        try {
            if ($response->isSuccessful()) :
                if ($response->getBody() == 'OK') :
                    echo "Enviado";
                else :
                    echo "No se envio el SMS Intentelo de Nuevo";
                endif;
            else :
                echo "Error con el Servidor Intentelo mas Tarde";
            endif;
        } catch (Exception $e) {
            echo $e;
        }
    }
    /**
     * 
     * @param type $id
     */
    public function porAgencia($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["age"])) {
                $criteria->addCondition("iDContacto.Agencia=" . $id, "OR");
            } else {
                $pos["age"] = true;
                $criteria->addCondition("iDContacto.Agencia=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    function actionGeslistage($start = 0, $block = 1) {

        if (isset($_POST['start'])) :
            $start = $_POST['start'];
        endif;

        if (isset($_POST['block'])) :
            $block = $_POST['block'];
        endif;

        $pagact = $block;
        $ttreg = $this->ttRegContacto();

        $criteria = $this->getFilter();
        $criteria->limit = $block;
        $criteria->offset = $start;
        $criteria->order = 'iDContacto.ID DESC, iDContacto.Fecha_creacion DESC';
        $oportunidad = Agendados::model()->with('iDContacto')->findAll($criteria);
        
        $ultsi = 0;
        if ($oportunidad != NULL) :
            foreach ($oportunidad as $o) :
                $ultsi = $o->ID;
            endforeach;
            $this->paginadorFormularios($ultsi, $start, $block, $ttreg, $pagact);
        else :
            $this->render('index');
        endif;
    }

    /**
     * 
     * @param type $start
     * @param type $block
     */
    function actionPageslist($start = 0, $block = 1) {

        if (isset($_POST['start'])) :
            $start = $_POST['start'];
        endif;

        if (isset($_POST['block'])) :
            $block = $_POST['block'];
        endif;

        $pagact = $start;
        $ttreg = $this->ttRegContacto();

        $criteria = $this->getFilter();
        $criteria->limit = $block;
        $criteria->offset = $start;
        $criteria->order = 'iDContacto.ID DESC';
        $model = Agendados::model()->find($criteria);
        Yii::app()->clientScript->scriptMap["jquery.js"] = false;
        Yii::app()->clientScript->scriptMap["jquery.min.js"] = false;
        $this->renderPartial('formpage', array('model' => $model, 'start' => $start, 'block' => $block, 'pages' => $ttreg, 'pag' => $pagact), false, true);
    }

    public function paginadorFormularios($idoportunidad, $start, $block, $ttreg, $pagact) {
        $model = Contacto::model()->findByPk($idoportunidad);
        $this->render('formpage', array('model' => $model, 'start' => $start, 'block' => $block, 'pages' => $ttreg, 'pag' => $pagact));
    }

    /**
     * Formulario de Contacto
     */
    public function actionFormulario($carga = 'single') {
        $model = Contacto::model()->findByPk($_REQUEST['idoportunidad']);
        $this->render('formulario', array(
            'model' => $model, 'carga' => $carga
        ));
    }
    /**
     * Formulario de Contacto
     */
    public function actionViewage() {
        $this->renderPartial('modalagenda', array('data' => $_POST['items']));
    }
    /**
     * Formulario de Oportunidades
     */
    public function actionViewageop() {
        $this->renderPartial('modalagendaop', array('data' => $_POST['items']));
    }
    
    public function actionNotificacion() {
        $this->renderPartial('modalalert', array('notificacion' => $_POST['tipo']));
    }
}