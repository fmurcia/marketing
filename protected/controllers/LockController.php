<?php
/**
 * Clase para el Manejo de Bloqueos en la WEB
 * @author Gustavo Carvajal <gcarvajal@telesentinel.com>
 */
class LockController extends Controller
{
    /**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('plugin'),
				'users'=>array('*'),
			)
		);
	}
        /**
         * Carga la Plantilla de Bloqueo en la Pagina
         */
	public function actionPlugin()
	{
            $pagina = Sitios::model()->findByPk(5);
            if($_REQUEST['estado'] == 1) :
                $atributos = array(
                    'heading'=>'UUPS!, Sesion Terminada!',
                    'message' => 'Ha Expirado el Limite de Inactividad de su Sesion, por Favor ingrese Nuevamente', 
                    'button' => 'Login',
                    'href' => $pagina->Controlador
                    );
            elseif($_REQUEST['estado'] == 2) :
                $atributos = array(                    
                    'heading'=> 'UUPS!, Salida Inesperada!', 
                    'message' =>'Inicio sesion en otro dispositivo, por favor cierre e intente nuevamente', 
                    'button' => 'Cerrar Todo!', 
                    'href' => $pagina->Controlador
                );
            elseif($_REQUEST['estado'] == 3) :
                $atributos = array(                    
                    'heading'=> 'Sin Agenda!', 
                    'message' =>'Debe escoger una solicitud en prueba!, por favor intente nuevamente', 
                );
            elseif($_REQUEST['estado'] == 4) :
                $atributos = array(                    
                    'heading'=>'UUPS!, En Mantenimiento!',
                    'message' => 'En este momento estamos ajustando la plataforma para Usted, Intentelo mas Tarde', 
                    'button' => 'Mantenimiento',
                    'href' => $pagina->Controlador
                );
            endif;
            $this->render('bloqueo', $atributos);
	}
}