<?php

/**
 * Controlador de Eventos del Sitio Principal
 * Clase para el Manejo de Controladores Iniciales
 * 
 * @author Gustavo Carvajal <gcarvajal@telesentinel.com>
 * @version 1.0
 * 
 * @method Void MenuSuperior() Carga el Menu Superior de la Vista Inicial
 * @method Void Alertas() Carga las Alertas de la Aplicacion
 * @method Void Pausaactiva() Carga el Menu con la Pausa Seleccionada
 * @method Void Pausas() Carga el Menu de Pausas
 * @method Void Tablacerrados() Carga las Solicitudes Cerradas
 * @method Void Tablacontenido() Tabla de Contenidos
 * @method Void Tablamantenimiento() Carga las Solicitudes en Mantenimiento
 * @method Void Tablapruebas() Tabla de Pruebas Generadas
 * @method Void Tablasitio() Carga los Tecnicos en Sitio
 * @method Void Tablavistas() Carga las Solicitudes Vistas
 */
class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
    
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array("index", 'logout', 'login', 'emailmchimp'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('shownoty', 'divcit', 'notificaciones', 'agendados', 'agendadosop'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    public function actionIndex() {
        if(Yii::app()->user->getState("id_usuario")) :
            $this->redirect('index.php/admcon/crear');
        else :
            $this->redirect('/web/index.php/site/logout');
        endif;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->actionLogout();
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect('/web/index.php/site/logout');
    }

    /**
     * 
     * @param type $nombreagencia
     * @param type $estado
     * @return type
     */
    public function getClientes($nombreagencia, $estado) {
        $idagencia = Agencia::model()->findByAttributes(array('Nombre' => $nombreagencia));
        $criteria = $this->getFilter();
        $criteria->select = 'IDCliente, CodigoServicioDetalle, SUM(ValorServicioDetalle) as Total';
        $criteria->addCondition('AgenciaCliente = ' . $idagencia->ID . ' AND EstadoDetalle = ' . $estado);
        $criteria->group = 'IDCliente, CodigoServicioDetalle';
        $criteria->order = 'Total, Codig    oServicioDetalle ASC';
        return XVentas::model()->findAll($criteria);
    }

    public function actionShownoty() {
        $cot = new Contacto();
        $registros = $cot->alertasContactoWebUlt();
        $id = isset($_POST['ultid']) ? $_POST['ultid'] : 1;
        $nuevos = 0;
        if ($registros != NULL) :
            if ($registros->ID != $id) :
                $nuevos = 1;
            endif;
        endif;
        echo $nuevos;
    }
    
    public function actionShowagenda() {
        $cot = new Agendados();
        echo $cot->alertasAgendados();
    }
    
    public function actionShowagendaOp() {
        $cot = new AgendadosOp();
        echo $cot->alertasAgendados();
    }
    
    public function actionDivcit() {
        $this->renderPartial('divcit');
    }
    
    public function actionNotificaciones(){
        
    }
    /**
     * Carga los registrosd agendados en las citas
     */    
    public function actionAgendados(){
        $agendados = new Agendados();
        $age = $agendados->getAgendados();
        if(count($age) > 0) :
            print json_encode($age);
        else :
            print json_encode(array());
        endif;
    }
    /**
     * Carga los registrosd agendados en las oportunidades
     */
    public function actionAgendadosOp(){
        $agendados = new AgendadosOp();
        $age = $agendados->getAgendados();
        if(count($age) > 0) :
            print json_encode($age);
        else :
            print json_encode(array());
        endif;
    }
    
    public function actionEmailmchimp(){
        if($_POST['email']) :
            return Contacto::actualizarContactoEmail($_POST['email']);
        endif;
    }
}