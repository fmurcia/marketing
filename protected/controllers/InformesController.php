<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChatController
 *
 * @author scorpio
 */
class InformesController extends Controller {

    public function getData() {
        return Yii::app()->user->getState('data');
    }

    public function setData($datos) {
        Yii::app()->user->setState('data', $datos);
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'oportunidades', 
                    'contactos', 
                    'ventas', 
                    'estadisticas', 
                    'informe', 
                    'comisiones', 
                    'divcomisiones', 
                    'export', 
                    'updfltr',
                    'totales',
                    'infoventas', 
                    'exportventas', 
                    'dashboard'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    private function accessRegionales($op = false) {

        if ($op) :
            return Regional::model()->with(array('regionalAgencia'))->findAll();
        else :
            $data = ComercialAgencias::model()->findAll('ID_Asesor = ' . Yii::app()->user->getState('id_usuario') . ' AND Estado = 1');
            if (count($data) > 0) :
                return $data;
            else :
                return array();
            endif;
        endif;
    }

    public function actionOportunidades() {
        $this->render('oportunidad', array('informe' => 'Oportunidad'));
    }

    public function actionContactos() {
        $this->render('contactos', array('informe' => 'Contacto'));
    }

    public function actionInforme() {
        $fechainicial = isset($_POST['fecha1']) ? $_POST['fecha1'] : date('Y-m-d');
        $fechafinal = isset($_POST['fecha2']) ? $_POST['fecha2'] : date('Y-m-d');
        $data = new $_POST['tabla']();                
        if ($this->entrar("tele.informescontactos", true)) :
            $dataProvider = $data->getExcelConvert($fechainicial, $fechafinal, FALSE);
        else :
            $dataProvider = $data->getExcelConvert($fechainicial, $fechafinal);
        endif;

        if ($dataProvider != NULL) :
            $this->renderPartial('excel_data', array('excel_data' => $dataProvider, 'fecha1' => $fechainicial, 'fecha2' => $fechafinal, 'tabla' => $_POST['tabla']));
        else:
            echo "No Se Encontraron Registros!.";
        endif;
    }

    public function actionExport() {

        $fechainicial = isset($_GET['fecha1']) ? $_GET['fecha1'] : date('Y-m-d');
        $fechafinal = isset($_GET['fecha2']) ? $_GET['fecha2'] : date('Y-m-d');
        $data = new $_GET['tabla']();
        if ($this->entrar("tele.informesoportunidades", true)) :
            $dataProvider = $data->getExcelConvert($fechainicial, $fechafinal, FALSE);
        else :
            $dataProvider = $data->getExcelConvert($fechainicial, $fechafinal);
        endif;
        if ($dataProvider != NULL) :
            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;Filename=reporte_" . $fechainicial . "_al_" . $fechafinal . ".xls");
            $this->renderPartial('tabla_excel', array('excel_data' => $dataProvider));
            Yii::app()->end();
        else :
            echo "No se descargo el Archivo!.";
        endif;
    }

    public function actionVentas() {
        $this->render('ventas');
    }

    /**
     * Contador de Citas por rango de Fechas y Asesor
     * @param type $asesor
     * @param type $fechaini
     * @param type $fechafin
     * @return type
     */
    private function contCitVenT($asesor, $cierre) {
        $data = ClienteVenta::model()->findAll('Cierre = ' . $cierre . ' AND Asesor = ' . $asesor);
        return $data;
    }

    public function actionInfoventas() {
        date_default_timezone_set('America/Bogota');
        $dataProvider = array();
        if ($this->entrar("tele.informesventas", true)) :
            $relacion = $this->accessRegionales(true);
            if (count($relacion) > 0) : //Tiene Permisos
                foreach ($relacion as $p) :
                    foreach ($p->regionalAgencia as $ag) :
                        $asesor_agencia = AsesorAgencia::model()->with(array('asesor' => array('condition' => 't.Estado = 1 AND Nivel = 10'), 'agencia' => array('condition' => 'agencia.ID = '.$ag->ID_Agencia)))->findAll();
                        $arr_ct = array();
                        foreach ($asesor_agencia as $aa):
                            $citas = $this->contCitVenT($aa->Asesor, $_POST['idcierre']);
                            $arr_ct = array();
                            if ($citas != NULL) :
                                $z = 0;
                                foreach ($citas as $c) :
                                    $cliente = Cliente::model()->findByPk($c->ID_cliente);
                                    if ($cliente != NULL) :
                                        $reg = '';
                                        $ser = '';
                                        
                                        $regional = RegionalAgencia::model()->findByAttributes(array('ID_Agencia' => $c->Agencia));
                                        if ($regional != NULL) :
                                            $reg = $regional->iDRegional->Descripcion;
                                        endif;
                                        
                                        $servicio = Servicios::model()->findByPk($c->Servicio);
                                        if ($servicio != NULL) :
                                            $ser = $servicio->Nombre;
                                        endif;
                                        
                                        $contacto = Contacto::model()->findByAttributes(array('ID' => $cliente->ID_Contacto, 'Asesor' => $aa->Asesor));
                                        if ($contacto != NULL) :
                                            $estadoventa = EstadoVenta::model()->findByPk($c->Estado);
                                            $arr_ct[$z]['id'] = $contacto->ID;
                                            $arr_ct[$z]['razon_social'] = isset($cliente->Razon_social) ? $cliente->Razon_social : 'Vacio';
                                            $arr_ct[$z]['contrato'] = isset($cliente->Contrato) ? $cliente->Contrato : 'Vacio';
                                            $arr_ct[$z]['fecha_modificacion'] = isset($c->Fecha_cambioEstado) ? $c->Fecha_cambioEstado : 'Vacio';
                                            $arr_ct[$z]['estado'] = isset($estadoventa->Nombre) ? $estadoventa->Nombre : '';
                                            $arr_ct[$z]['comentarios'] = isset($contacto->Observaciones) ? $contacto->Observaciones : '';
                                            $arr_ct[$z]['comercial'] = isset($contacto->telemercaderista->Nombre) ? $contacto->telemercaderista->Nombre : '';
                                            $arr_ct[$z]['regional'] = isset($reg) ? $reg : '';
                                            $arr_ct[$z]['fecha_creacion'] = isset($c->Fecha_creacion) ? $c->Fecha_creacion : '';
                                            $arr_ct[$z]['servicio'] = isset($ser) ? $ser : '';
                                            $arr_ct[$z]['nombre'] = isset($contacto->asesor->Nombre) ? $contacto->asesor->Nombre : '';
                                            $arr_ct[$z]['idasesor'] = isset($contacto->Asesor) ? $contacto->Asesor : '';
                                            $arr_ct[$z]['agencia'] = isset($contacto->agencia->Nombre) ? $contacto->agencia->Nombre : '';
                                            $z++;
                                        endif;
                                    endif;
                                endforeach;
                            endif;
                            $dataProvider[]['citas'] = $arr_ct;
                        endforeach;
                    endforeach;
                endforeach;
                if ($dataProvider != NULL) :
                    $this->setData($dataProvider);
                    $this->renderPartial('excel_ventas', array('data' => $dataProvider, 'idcierre' => $_POST['idcierre']));
                else:
                    echo "No Se Encontraron Registros!.";
                endif;
            endif;
        else :
            $contacto = Contacto::model()->findAll('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Estado != 19 GROUP BY Asesor');
            foreach ($contacto as $co) :
                $citas = $this->contCitVenT($co->Asesor, $_POST['idcierre']);
                $arr_ct = array();
                if ($citas != NULL) :
                    $z = 0;
                    foreach ($citas as $c) :
                        $cliente = Cliente::model()->findByPk($c->ID_cliente);
                        if ($cliente != NULL) :
                            $con = Contacto::model()->findByAttributes(array('ID' => $cliente->ID_Contacto, 'Telemercaderista' => Yii::app()->user->getState('id_usuario'), 'Asesor' => $co->Asesor));
                            if ($con != NULL) :
                                $regional = RegionalAgencia::model()->findByAttributes(array('ID_Agencia' => $c->Agencia));
                                if ($regional != NULL) :
                                    $reg = $regional->iDRegional->Descripcion;
                                else :
                                    $reg = '';
                                endif;
                                $servicio = Servicios::model()->findByPk($c->Servicio);
                                if ($servicio != NULL) :
                                    $ser = $servicio->Nombre;
                                else :
                                    $ser = '';
                                endif;
                                $estadoventa = EstadoVenta::model()->findByPk($c->Estado);
                                $arr_ct[$z]['id'] = $con->ID;
                                $arr_ct[$z]['razon_social'] = isset($cliente->Razon_social) ? $cliente->Razon_social : '';
                                $arr_ct[$z]['contrato'] = isset($cliente->Contrato) ? $cliente->Contrato : '';
                                $arr_ct[$z]['fecha_modificacion'] = isset($c->Fecha_cambioEstado) ? $c->Fecha_cambioEstado : '0000-00-00';
                                $arr_ct[$z]['estado'] = isset($estadoventa->Nombre) ? $estadoventa->Nombre : '';
                                $arr_ct[$z]['comentarios'] = isset($con->Observaciones) ? $con->Observaciones : '';
                                $arr_ct[$z]['comercial'] = isset($con->telemercaderista->Nombre) ? $con->telemercaderista->Nombre : '';
                                $arr_ct[$z]['nombre'] = isset($con->asesor->Nombre) ? $con->asesor->Nombre : '';
                                $arr_ct[$z]['agencia'] = isset($con->agencia->Nombre) ? $con->agencia->Nombre : '';
                                $arr_ct[$z]['idasesor'] = isset($con->Asesor) ? $con->Asesor : '';
                                $arr_ct[$z]['idagencia'] = isset($con->Agencia) ? $con->Agencia : '';
                                $arr_ct[$z]['regional'] = $reg;
                                $arr_ct[$z]['fecha_creacion'] = $c->Fecha_creacion;
                                $arr_ct[$z]['servicio'] = $ser;
                                $z++;
                            endif;
                        endif;
                    endforeach;
                endif;
                $dataProvider[]['citas'] = $arr_ct;
            endforeach;
            if ($dataProvider != NULL) :
                $this->setData($dataProvider);
                $this->renderPartial('excel_ventas', array('data' => $dataProvider, 'idcierre' => $_POST['idcierre']));
            else:
                echo "No Se Encontraron Registros!.";
            endif;
        endif;
    }

    public function actionExportventas() {
        $dataProvider = $this->getData();
        $cierre = Cierres::model()->findByPk($_GET['idcierre']);
        if ($dataProvider != NULL) :
            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;Filename=reporteventas_" . $cierre->Fecha_inicial . "_al_" . $cierre->Fecha_final . ".xls");
            $this->renderPartial('tabla_ventas', array('data' => $dataProvider));
            Yii::app()->end();
        else :
            echo "No se descargo el Archivo!.";
        endif;
    }

    public function actionEstadisticas() {
        $this->render('index');
    }
    
    
    public function actionContadores() {
        Yii::app()->session['contador'] = $_POST['opcion'];
        $this->renderPartial('contadores');
    }

    /**
     * Retorna el Cierre Vigente en la Fecha dada o el ID Parametrizado
     * @param int $intervalo
     * @return array Id y Mes Vigente
     */
    public function cierre($intervalo = '') {
        $idcierre = 1;
        $mes = '';
        if (!empty($intervalo)) :
            $periodo = Cierres::model()->findByPk($intervalo);
            $idcierre = $periodo->ID;
            $mes = $periodo->Mes;
        else :
            $periodo = Cierres::model()->findAll();
            foreach ($periodo as $p) :
                if (Cierres::model()->check_in_range($p->Fecha_inicial, $p->Fecha_final, date('Y-m-d'))) :
                    $idcierre = $p->ID;
                    $mes = $p->Mes;
                endif;
            endforeach;
        endif;
        Yii::app()->user->setState("IdCierre", $idcierre);
        Yii::app()->user->setState("MesCierre", $mes);
    }

    /**
     * Inicializa y /o Actualiza los Filtros de las busquedas
     * @param string $fltrtxt filtro inicial
     * @param bool valida si va el cierre o no
     */
    private function updfltrs($fltrtxt = "") {
        //Limpia los Filtros Iniciales / Busquedas
        $reg = true;
        $this->clearFil();
        //retorna la version de los registros
        $this->porVersion();

//        if (!$this->entrar("admin", true)) :
//            $this->porAsesor();
//        endif;

        if (!empty($fltrtxt)) :
            $filtros = explode(",", $fltrtxt);
            if (count($filtros) > 0) :
                foreach ($filtros as $filtro) :
                    $split = explode("-", $filtro);
                    $tipo = $split[0];
                    $idopcion = $split[1];
                    switch ($tipo) :
                        case "reg":
                            $this->porRegional($idopcion);
                            $reg = false;
                            break;
                        case "age":
                            $this->porAgencia($idopcion);
                            $reg = false;
                            break;
                    endswitch;
                endforeach;
            endif;
        endif;

        //Permisos en la Busqueda de registros
        if ($reg == true && $this->entrar("admin", true) == false) :
            $this->permisosRegional();
        endif;

        $this->fixFilters();
    }

    /**
     * Retorna y concatena la cadena de busqueda para el cierre dado
     * @param int $id ID del Cierrre Buscado
     */
    private function porCierre($id) {
        $this->cierre($id);
        $criteria = $this->getFilter();
        $criteria->addCondition('CierreClienteVenta = ' . Yii::app()->user->getState("IdCierre"));
        $this->setFilter($criteria);
    }

    /**
     * Limpia la variable de session de busqueda en filtros
     * reinicia el criteria de busqueda
     */
    private function clearFil() {
        Yii::app()->user->setState("fltsearch", array());
        $filcriteria = new CDbCriteria();
        $this->setFilter($filcriteria);
    }

    /**
     * Contatena la version de las busquedas para los registros
     */
    private function porVersion() {
        $criteria = $this->getFilter();
        $criteria->addCondition('VersionClienteVenta = 2');
        $this->setFilter($criteria);
    }

    /**
     * Retorna los privilegios de la aplicacion
     */
    private function permisosRegional() {

        $criteria = $this->getFilter();
        if ($criteria != null) :
            $regionales = $this->getRegionalesXNivel();
            if (COUNT($regionales) > 0) :
                $c = 1;
                foreach ($regionales as $regional) :
                    if ($c == 1) :
                        $criteria->addCondition("IDRegionalCliente='" . $regional->ID_Regional . "'", "AND");
                    else :
                        $criteria->addCondition("IDRegionalCliente='" . $regional->ID_Regional . "'", "OR");
                    endif;
                    $c++;
                endforeach;
            else :
                $criteria->addCondition("IDRegionalCliente=-999");
            endif;
        endif;
        $this->setFilter($criteria);
    }

    /**
     * Concatena la cadenas de la(s) regional(es) Seleccionadas
     * @param Int $id de la(s) regional(es) Seleccionadas
     */
    private function porRegional($id) {
        $pos = Yii::app()->user->getState("fltsearch");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["reg"])) {
                $criteria->addCondition("IDRegionalCliente = " . $id, "OR");
            } else {
                $pos["reg"] = true;
                $criteria->addCondition("IDRegionalCliente = " . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltsearch", $pos);
    }

    /**
     * Concatena la cadenas de la(s) Agencias(es) Seleccionadas
     * @param Int $id de la(s) Agencias(es) Seleccionadas
     */
    private function porAgencia($id) {
        $pos = Yii::app()->user->getState("fltsearch");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["age"])) {
                $criteria->addCondition("AgenciaCliente = " . $id, "OR");
            } else {
                $pos["age"] = true;
                $criteria->addCondition("AgenciaCliente = " . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltsearch", $pos);
    }

    /**
     * 
     */
    private function porAsesor() {
        $criteria = $this->getFilter();
        $criteria->addCondition("AsesorCliente=" . Yii::app()->user->getState("id_usuario"));
        $this->setFilter($criteria);
    }

    /**
     * Funcionan que limpia y concatena de manera ordenada las busquedas retornando el objeto criteria
     * @param string $criteria
     * @return Object CDbCriteria
     */
    private function fixFilters($criteria = null) {
        $skip = false;
        if ($criteria == null) {
            $skip = true;
            $criteria = $this->getFilter();
        }
        $critstring = $criteria->condition;
        if ($critstring != "") {
            $params = $criteria->params;
            $critstring = str_replace("(", " ", $critstring);
            $critstring = str_replace(")", " ", $critstring);
            $critstring = trim(preg_replace('/\s+/', ' ', $critstring));
            //$critstring = "(" . str_ireplace(" AND ", ") AND (", $critstring) . ")";
            $criteria = new CDbCriteria();
            $criteria->addCondition($critstring);
            $criteria->params = $params;
        }

        if ($skip == true) {
            $this->setFilter($criteria);
        } else {
            return $criteria;
        }
    }

    /**
     * Setea el criteria de busqueda
     * @param string $filcriteria
     */
    private function setFilter($filcriteria) {
        Yii::app()->user->setState("filcriteria", $filcriteria);
    }

    /**
     * Retorna el tipo de estado a Agrupar en la Busqueda
     * @param String $tipo
     * @return Int Tipo de Agrupado Web
     */
    private function groupStatus4D($tipo) {

        $group = '';
        if ($tipo == 'Venta') :
            $group = '2';
        elseif ($tipo == 'Instalacion') :
            $group = '10';
        elseif ($tipo == 'Facturacion') :
            $group = '14';
        elseif ($tipo == 'Descartado') :
            $group = '19';
        elseif ($tipo == 'Pcomercial') :
            $group = '24';
        elseif ($tipo == 'Pasesor') :
            $group = '4';
        elseif ($tipo == 'Preventa') :
            $group = '1';
        endif;

        return $group;
    }

    /**
     * Retorna el criteria de busqueda o instancia un nuevo objeto si no existe session
     * @return string $filter
     */
    private function getFilter() {
        $filter = Yii::app()->user->getState("filcriteria");
        if ($filter == null) {
            $filter = new CDbCriteria();
            $this->setFilter($filter);
        }
        return $filter;
    }

    /**
     * Actualiza los filtros de busqueda desde Ajax y retorna el Mes Vigente o Buscado
     */
    public function actionUpdfltr() {
        $this->cierre($_POST['idcierre']);
        $this->updfltrs($_POST["fltr"]);
        echo strtoupper(Yii::app()->user->getState("MesCierre"));
    }

    /**
     * Retorna los Contadores Generales del Cierre Vigente
     * @param String $estado Estado a Agrupar
     * @param String $filter si existe Filtro
     * @return Int Total Generado
     */
    private function getContadores($estado) {
        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = 'SUM(ValorServicioDetalle) as Total';
        $critreal->addCondition('CierreClienteVenta = ' . Yii::app()->user->getState("IdCierre") . ' AND EstadoDetalle = ' . $this->groupStatus4D($estado) . ' AND Codigo4DDetalleServicio != 27');
        $registro = XVentas::model()->find($critreal);
        return $registro->Total;
    }

    /**
     * Retorna los Contadores Generales en Proyeccion
     * @param string $estado a Filtrar
     * @param string $filter filtro predeterminado
     * @return int $total Sumatoria Total
     */
    public function getContadoresProyeccion($estado) {
        $cierre = Yii::app()->user->getState("IdCierre");
        $periodo = Cierres::model()->findByAttributes(array('Estado' => 1));
        $total = 0;
        if ($periodo != NULL) :
            if ($periodo->ID == $cierre) :
                $critreal = new CDbCriteria();
                $criteria = $this->getFilter();
                $critreal->mergeWith($criteria);
                $critreal->select = 'SUM(ValorServicioDetalle) as Total';
                $critreal->addCondition('EstadoDetalle = ' . $this->groupStatus4D($estado));
                $registro = XProyeccion::model()->find($critreal);
                $total = $registro->Total;
            endif;
        endif;
        return $total;
    }

    /**
     * Retorna el detallado del estado a consultar
     * @param String $estado Estado a Buscar
     * @param String $filter filtro 
     * @return array $registro Total de Registros
     */
    public function getContadoresDetalleEstado($estado) {
        $cierre = Yii::app()->user->getState("IdCierre");
        $periodo = Cierres::model()->findByAttributes(array('Estado' => 1));
        $registro = array();
        if ($periodo != NULL) :
            if ($periodo->ID == $cierre) :
                $critreal = new CDbCriteria();
                $criteria = $this->getFilter();
                $critreal->mergeWith($criteria);
                $critreal->select = 'IDCliente, AgenciaCliente, NegociacionDetalle, CodigoServicioDetalle, ValorServicioDetalle, Codigo4DDetalleServicio';
                $critreal->addCondition('EstadoDetalle = ' . $this->groupStatus4D($estado));
                $registro = XProyeccion::model()->findAll($critreal);
            endif;
        endif;
        return $registro;
    }

    /**
     * Contadot total del Cierre Vigente de todas las Ciudades
     * @return array
     */
    public function totalCierreVentas($tipo) {
        /* TotalVentas */
        return $this->getContadores($tipo);
    }

    /**
     * Contador total del Cierre Vigente de todas las Ciudades
     * @return array
     */
    public function totalProyeccion($tipo) {
        return $this->getContadoresProyeccion($tipo);
    }

    /**
     * Contador total del Cierre Vigente de todas las Ciudades
     * @param type $tipo
     * @return array Arreglo de Totales por Estado
     */
    public function totalProyeccionDetalle($tipo) {
        return $this->getContadoresDetalleEstado($tipo);
    }

    /**
     * 
     */
    public function actionTotales() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial("contenedores", array('ajax' => true));
    }

    /**
     * Carga las Estadisticas detalladas y Renderiza la Nueva vista
     */
    public function actionStats() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('registros_ventas');
    }

    /**
     * 
     * @return Array 
     */
    public function registrosUnidades() {
        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = 'CodigoServicioDetalle, Codigo4DDetalleServicio, SUM(ValorServicioDetalle) as Total';
        $critreal->addCondition('CierreClienteVenta = ' . Yii::app()->user->getState("IdCierre"));
        $critreal->addInCondition('EstadoDetalle', array(2, 10, 14));
        $critreal->group = 'Codigo4DDetalleServicio';
        $critreal->order = 'Codigo4DDetalleServicio ASC';
        return XVentas::model()->findAll($critreal);
    }

    /**
     * 
     * @return Array
     */
    public function registroAgrupadoUnidad() {

        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = 'CodigoServicioDetalle, Codigo4DDetalleServicio, EstadoDetalle, ValorServicioDetalle';
        $critreal->addCondition('CierreClienteVenta = ' . Yii::app()->user->getState("IdCierre") . ' AND Codigo4DDetalleServicio != 27');
        $critreal->addInCondition('EstadoDetalle', array(2, 10, 14));
        $critreal->order = 'CodigoServicioDetalle ASC';
        return XVentas::model()->findAll($critreal);
    }

    /**
     * 
     * @return Array
     */
    public function registroAgrupadoCiudad() {

        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = 'IDRegionalCliente, SUM(ValorServicioDetalle) as Total';
        $critreal->addCondition('CierreClienteVenta = ' . Yii::app()->user->getState("IdCierre") . ' AND ValorServicioDetalle IS NOT NULL  AND Codigo4DDetalleServicio != 27');
        $critreal->addInCondition('EstadoDetalle', array(2, 10, 14));
        $critreal->group = 'IDRegionalCliente';
        $critreal->order = 'IDRegionalCliente ASC';
        return XVentas::model()->findAll($critreal);
    }

    /**
     * 
     * @param type $estado
     * @return type
     */
    public function unidadNegocioEstado($estado) {

        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = 'CodigoServicioDetalle, Codigo4DDetalleServicio, SUM(ValorServicioDetalle) as Total';
        $critreal->addCondition('CierreClienteVenta = ' . Yii::app()->user->getState("IdCierre") . ' AND EstadoDetalle = ' . $this->groupStatus4D($estado) . ' AND Codigo4DDetalleServicio != 27');
        $critreal->group = 'CodigoServicioDetalle, EstadoDetalle';
        $critreal->order = 'CodigoServicioDetalle ASC';
        return XVentas::model()->findAll($critreal);
    }

    /**
     * 
     * @param type $estado
     * @return type
     */
    public function unidadNegocioAgencia($estado) {

        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = 'IDRegionalCliente, AgenciaCliente, Regional, Agencia, CodigoServicioDetalle, Codigo4DDetalleServicio, SUM(ValorServicioDetalle) as Total';
        $critreal->addCondition('CierreClienteVenta = ' . Yii::app()->user->getState("IdCierre") . ' AND Codigo4DDetalleServicio != 27');
        $critreal->addCondition('EstadoDetalle = ' . $this->groupStatus4D($estado));
        $critreal->group = 'Agencia, CodigoServicioDetalle';
        $critreal->order = 'Agencia ASC';
        return XVentas::model()->findAll($critreal);
    }

    /**
     * 
     * @return Array 
     */
    public function cotizacionesAgencias() {

        $idcierre = Cierres::model()->findByPk(Yii::app()->user->getState("cierre"));
        $criteria = new CDbCriteria();
        $criteria->select = 'ID_Negociacion, SUM(Valor_Total) as Total';
        $criteria->addBetweenCondition('Fecha_creacion', $idcierre->Fecha_inicial, $idcierre->Fecha_final);
        $criteria->addCondition('ID_Negociacion IS NOT NULL');
        $criteria->group = 'ID_Negociacion';
        return XCotizacion::model()->findAll($criteria);
    }

    /**
     * 
     * @return Array 
     */
    public function registrosAgencias() {
        $criteria = $this->getFilter();
        $criteria->select = 'IDRegionalCliente, AgenciaCliente, CodigoServicioDetalle, Codigo4DDetalleServicio, ValorServicioDetalle';
        $criteria->group = 'AgenciaCliente, CodigoServicioDetalle';
        $criteria->order = 'AgenciaCliente ASC';
        return XVentas::model()->findAll($criteria);
    }

    /**
     * 
     */
    public function actionDttpagn() {
        $agencia = $_POST['agencia'];
        $estado = $_POST['estado'];
        $registros = $this->getClientes($agencia, $estado);
        $this->renderPartial('detallado_contratos', array('registros' => $registros));
    }
    
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionDashboard() {
        
        //retorna el Cierre Vigente
        $this->cierre();
        //Inicializa Filtros
        $this->updfltrs();
        // Renderiza la Vista
        $this->render('dashboard');
    }
    
    public function actionComisiones(){
        $telemark = Cliente::model()->informeTelemark();
        if(sizeof($telemark) > 0) :
            $this->render('comisiones', array('telemark' => $telemark));
        else :
            $user = Yii::app()->getComponent('user');
            $user->setFlash(
                    'warning', "<strong>SIN REGISTROS!</strong> Check yourself, you're not looking good."
            );
            // Render them all with single `TbAlert`
            $this->widget('booster.widgets.TbAlert', array(
                'fade' => true,
                'closeText' => '&times;', // false equals no close link
                'events' => array(),
                'htmlOptions' => array(),
                'userComponentId' => 'user',
                'alerts' => array(// configurations per alert type
                    'warning' => array('closeText' => false),
                ),
            ));
        endif;
    }
    
    public function actionDivcomisiones(){
        if(isset($_POST['idcierre'])) :
            $idcierre = $_POST['idcierre'];
            $telemark = Cliente::model()->informeTelemark($idcierre);
            if(sizeof($telemark) > 0) :
                $this->renderPartial('divcomisiones', array('telemark' => $telemark));
            endif;
        else :
            $user = Yii::app()->getComponent('user');
            $user->setFlash(
                    'warning', "<strong>SIN REGISTROS!</strong> Check yourself, you're not looking good."
            );
            // Render them all with single `TbAlert`
            $this->widget('booster.widgets.TbAlert', array(
                'fade' => true,
                'closeText' => '&times;', // false equals no close link
                'events' => array(),
                'htmlOptions' => array(),
                'userComponentId' => 'user',
                'alerts' => array(// configurations per alert type
                    'warning' => array('closeText' => false),
                ),
            ));
        endif;
    }
}