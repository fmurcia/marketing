<?php

/**
 * Description of SeguimientosController
 *
 * @author scorpio
 */
class AdmopoController extends Controller {

    public $filtro;

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'index',
                    'qryContacto',
                    "updfltr",
                    "direccion",
                    'pageslist',
                    'srchtrm',
                    'formulario',
                    'upcontact',
                    'upcontacto',
                    'comercial',
                    'enviarcontacto',
                    'guardargestion',
                    'coincidencias',
                    'detalle',
                    'estweb',
                    'geslistopo'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * 
     */
    public function actionIndex() {
        $model = new Contacto();
        $this->initSearch();
        $this->updfltrs("");
        $this->render('index', array("model" => $model));
    }

    /**
     * Inicializa las busquedas
     */
    private function initSearch() {
        Yii::app()->user->setState("term", "");
        $this->setFltrtxt("");
    }

    /**
     * 
     * @param type $fltrtxt
     */
    private function setFltrtxt($fltrtxt) {
        Yii::app()->user->setState("fltrtxt", $fltrtxt);
    }

    public function actionUpdfltr() {
        $fltrtxt = $_POST["fltr"];
        $this->setFltrtxt($fltrtxt);
        $this->updfltrs($fltrtxt);
        echo "OK";
    }

    /**
     * Actualiza el Contacto
     */
    public function actionUpcontacto() {

        $model = $_POST['model'];
        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $valor = $_POST['valor'];

        if ($model == 'Oportunidad') :
            $instance = $model::model()->findByPk($id);

            if ($instance->Estado_proceso == 20 || $instance->Estado_proceso == 19) :
                $instance->Estado_proceso = 21;
            endif;

            if ($campo == 'Medio_contacto') :
                $medios = MediosContacto::model()->findByAttributes(array('ID' => $valor));
                if ($medios == NULL) :
                    $instance->$campo = $valor;
                else :
                    $instance->$campo = $medios->Nombre;
                endif;
            endif;

            $instance->Fecha_Ultgestion = date('Y-m-d H:i:s');

            if (empty($instance->Medio_contacto) && $instance->Tipo_contacto == 3) :
                if ($instance->Tipo_contacto == 1) :
                    $instance->Medio_contacto = 'GOOGLE';
                elseif ($instance->Tipo_contacto == 2) :
                    $instance->Medio_contacto = 'TRABAJO EN FRIO';
                elseif ($instance->Tipo_contacto == 3) :
                    $instance->Medio_contacto = 'CONTACTO HOME';
                elseif ($instance->Tipo_contacto == 4) :
                    $instance->Medio_contacto = 'BASE DE DATOS';
                elseif ($instance->Tipo_contacto == 5) :
                    $instance->Medio_contacto = "CHAT";
                endif;
            endif;

            if (strlen($valor) == 7 && $campo == 'Telefono'):
                $instance->Telefono = $valor;
            elseif (strlen($valor) == 10 && $campo == 'Celular'):
                $instance->Celular = $valor;
            endif;

            if ($campo != 'Telefono' && $campo != 'Celular' && $campo != 'Medio_contacto') :
                $instance->$campo = $valor;
            endif;

            if (!empty($_POST['observaciones'])) :
                $instance->Observaciones = $_POST['observaciones'];

                HistoricoGestionOp::model()->guardarGestion($instance->ID, 21, $_POST['observaciones'], 1);

            endif;

            if ($instance->save()) :
                echo "OK";
            else :
                echo "Error";
            endif;

        elseif ($model == 'ServicioInteres') :
            $service = explode('-', $valor);

            $opcion = ServicioInteresOportunidad::model()->findByAttributes(array(
                "ID_Contacto" => $id,
                "ID_Servicio" => $service[0]
            ));

            if ($opcion == null) :
                $opcion = new ServicioInteresOportunidad();
                $opcion->ID_Contacto = $id;
                $opcion->ID_Servicio = $service[0];
            endif;

            $opcion->Estado = $service[1];

            if ($campo == 'Equipos') :
                $opcion->Equipos = $service[1];
            else :
                $opcion->Equipos = 0;
            endif;

            $opcion->save();

        endif;
    }

    /**
     * 
     * @param type $fltrtxt
     */
    private function updfltrs($fltrtxt = "") {
        $this->limpiarFiltros();
        $term = Yii::app()->user->getState("term");
        Yii::app()->user->setState("status", true);
        $this->porTermino($term);
        $this->porComercial();
        if ($fltrtxt != "") {
            $filtros = explode(",", $fltrtxt);
            if (count($filtros) > 0) {
                sort($filtros);
                foreach ($filtros as $filtro) {
                    $split = explode("-", $filtro);
                    $tipo = $split[0];
                    $id = $split[1];
                    switch ($tipo) {
                        case "est":
                            Yii::app()->user->setState("status", false);
                            $this->porEstado($id);
                            break;
                        case "tip":
                            $this->porTipo($id);
                            break;
                        case "ant":
                            $this->porAntiguedad($id);
                            break;
                        case "age":
                            $this->porAgencia($id);
                            break;
                        case "pro":
                            $this->porPropiedad($id);
                            break;
                        case "time":
                            $this->porTiempo($id, $split[2]);
                            break;
                    }
                }
            }
        }
        $this->fixFilters();
    }

    /**
     * 
     * @param type $id
     */
    private function porPropiedad($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["pro"])) {
                $criteria->addCondition("Horizontal = '$id'", "OR");
            } else {
                $pos["pro"] = true;
                $criteria->addCondition("Horizontal = '$id'", "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     */
    private function porComercial() {
        $criteria = $this->getFilter();
        if ($criteria != null) {
            $as = Asesor::model()->findByPk(Yii::app()->user->getState("id_usuario"));
            if ($as != NULL) :
                if ($as->Nivel == 3) :
                    $criteria->addCondition("Telemercaderista=" . $as->ID);
                endif;
            endif;
        }
        $this->setFilter($criteria);
    }

    /**
     * 
     */
    private function porTiempo($anio, $mes) {

        $fechaini = $anio . "-" . $mes . "-01";
        $fechafin = $anio . "-" . $mes . "-30";

        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            $crtmp = new CDbCriteria();
            $crtmp->addBetweenCondition("Fecha_creacion", $fechaini, $fechafin, "OR");
            $crtxt = $crtmp->condition;
            $crtxt = str_replace("AND", "<AND>", $crtxt);

            if (isset($pos["time"])) {
                $criteria->addCondition($crtxt, "OR");
                $criteria->params = CMap::mergeArray($criteria->params, $crtmp->params);
            } else {
                $pos["time"] = true;
                $criteria->addCondition($crtxt, "AND");
                $criteria->params = CMap::mergeArray($criteria->params, $crtmp->params);
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $criteria
     * @return \CDbCriteria
     */
    private function fixFilters($criteria = null) {
        $skip = false;
        if ($criteria == null) {
            $skip = true;
            $criteria = $this->getFilter();
        }
        $critstring = $criteria->condition;
        if ($critstring != "") {
            $params = $criteria->params;
            $critstring = str_replace("(", " ", $critstring);
            $critstring = str_replace(")", " ", $critstring);
            $critstring = trim(preg_replace('/\s+/', ' ', $critstring));

            $critstring = "(" . str_ireplace(" AND ", ") AND (", $critstring) . ")";
            $critstring = str_ireplace("<AND>", "AND", $critstring);

            $criteria = new CDbCriteria();
            $criteria->addCondition($critstring);
            $criteria->params = $params;
        }
        if ($skip == true) {
            $this->setFilter($criteria);
        } else {
            return $criteria;
        }
    }

    /**
     * 
     */
    public function actionSrchtrm() {
        if (isset($_POST["term"])) {
            Yii::app()->user->setState("term", $_POST["term"]);
            $this->updfltrs($this->getFltrtxt());
        }
        echo "OK";
    }

    /**
     * 
     */
    private function limpiarFiltros() {
        Yii::app()->user->setState("fltini", array());
        $filters = new CDbCriteria();
        $this->setFilter($filters);
    }

    /**
     * 
     * @param type $id
     */
    private function porTermino($id = "") {
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if ($id != "") {
                $criteria->compare("ID", $id, true, "OR");
                $criteria->compare("Razon_social", $id, true, "OR");
                $criteria->compare("Direccion", $id, true, "OR");
                $criteria->compare("Nit", $id, true, "OR");
                $criteria->compare("Nombre_completo", $id, true, "OR");
                $criteria->compare("Celular", $id, true, "OR");
                $criteria->compare("Email", $id, true, "OR");
            }
        }
        $this->setFilter($criteria);
    }

    /**
     * 
     * @param type $id
     */
    private function porEstado($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["est"])) {
                $criteria->addCondition("Id_Estadoweb=" . $id, "OR");
            } else {
                $pos["est"] = true;
                $criteria->addCondition("Id_Estadoweb=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porTipo($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["tip"])) {
                $criteria->addCondition("Tipo_contacto=" . $id, "OR");
            } else {
                $pos["tip"] = true;
                $criteria->addCondition("Tipo_contacto=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $id
     */
    private function porAntiguedad($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["ant"])) {
                $criteria->addCondition("Fecha_Ultgestion=" . $id, "OR");
            } else {
                $pos["ant"] = true;
                $criteria->addCondition("Fecha_Ultgestion=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    /**
     * 
     * @param type $filters
     */
    private function setFilter($filters) {
        Yii::app()->user->setState("filtros", $filters);
    }

    /**
     * 
     * @return \CDbCriteria
     */
    private function getFilter() {
        $filter = Yii::app()->user->getState("filtros");
        if ($filter == null) {
            $filter = new CDbCriteria();
            $this->setFilter($filter);
        }
        return $filter;
    }

    /**
     * 
     * @return string
     */
    private function getFltrtxt() {
        $fltrtxt = Yii::app()->user->getState("fltrtxt");
        if ($fltrtxt == null) {
            $fltrtxt = "";
            $this->setFltrtxt($fltrtxt);
        }
        return $fltrtxt;
    }

    /**
     * 
     */
    public function actionQrycontacto() {
        $limit = 10;
        $pag = (isset($_POST["pag"]) ? ((int) $_POST["pag"]) - 1 : -1);
        $offset = ($pag > 0 ? $pag * $limit : -1);
        $unegs = $this->qryContacto($offset);
        $counter = $this->ttRegContacto();
        $this->renderPartial("tablaTodos", array("contactos" => $unegs, "ajax" => true, 'limit' => $limit, 'counter' => $counter, 'pag' => $pag + 1));
    }

    /**
     * 
     * @param type $offset
     * @param type $tipo
     * @return type
     */
    public function qryContacto($offset = -1) {
        $criteria = $this->getFilter();
        if (Yii::app()->user->getState("status")) :
            $criteria->addCondition('Estado_proceso NOT IN (2, 19, 28)');
        else :
            $criteria->addCondition('Estado_proceso IN (20, 21)');
        endif;
        $criteria->offset = $offset;
        $criteria->limit = 10;
        $criteria->order = "ID DESC";
        $contactos = Oportunidad::model()->findAll($criteria);

        if (sizeof($contactos) > 0) :
            return $contactos;
        else :
            return array();
        endif;
    }

    public function ttRegContacto() {
        $critreal = new CDbCriteria();
        $criteria = $this->getFilter();
        $critreal->mergeWith($criteria);
        $critreal->select = array("count(ID) as counter");
        $critreal->limit = -1;
        $critreal->offset = -1;
        $confltr = Oportunidad::model()->find($critreal);
        return $confltr->counter;
    }

    public function actionDebug() {
        Yii::import("ext.httpclient.*");
        $message = urlencode("prueba");
        $destino = 3114695714;
        $sitios = Sitios::model()->findByPk(13);
        $connection = $sitios->Controlador . "?mensaje=" . $message . "&destino=" . $destino;
        $client = new EHttpClient($connection, array(
            "maxredirects" => 3,
            "timeout" => 10
        ));
        $response = $client->request();

        try {
            if ($response->isSuccessful()) :
                if ($response->getBody() == 'OK') :
                    echo "Enviado";
                else :
                    echo "No se envio el SMS Intentelo de Nuevo";
                endif;
            else :
                echo "Error con el Servidor Intentelo mas Tarde";
            endif;
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function actionUpcontact() {
        $contacto = Oportunidad::model()->findByPk($_POST['id']);
        if ($contacto != NULL) :
            $contacto->Fecha_Ultgestion = date('Y-m-d H:i:s');
            $contacto->save();
        endif;
    }

    /**
     * 
     * @param type $id
     */
    public function porAgencia($id) {
        $pos = Yii::app()->user->getState("fltini");
        $criteria = $this->getFilter();
        if ($criteria != null) {
            if (isset($pos["age"])) {
                $criteria->addCondition("Agencia=" . $id, "OR");
            } else {
                $pos["age"] = true;
                $criteria->addCondition("Agencia=" . $id, "AND");
            }
        }
        $this->setFilter($criteria);
        Yii::app()->user->setState("fltini", $pos);
    }

    function actionGeslistopo($start = 0, $block = 1) {

        if (isset($_POST['start'])) :
            $start = $_POST['start'];
        endif;

        if (isset($_POST['block'])) :
            $block = $_POST['block'];
        endif;

        $pagact = $block;
        $ttreg = $this->ttRegContacto();

        $criteria = $this->getFilter();
        $criteria->limit = $block;
        $criteria->offset = $start;
        $criteria->order = 'ID DESC, Fecha_creacion DESC';
        $oportunidad = Oportunidad::model()->findAll($criteria);
        
        $ultsi = 0;
        if ($oportunidad != NULL) :
            foreach ($oportunidad as $o) :
                $ultsi = $o->ID;
            endforeach;
            $this->paginadorFormularios($ultsi, $start, $block, $ttreg, $pagact);
        else :
            $this->render('index');
        endif;
    }

    /**
     * 
     * @param type $start
     * @param type $block
     */
    function actionPageslist($start = 0, $block = 1) {

        if (isset($_POST['start'])) :
            $start = $_POST['start'];
        endif;

        if (isset($_POST['block'])) :
            $block = $_POST['block'];
        endif;

        $pagact = $start;
        $ttreg = $this->ttRegContacto();

        $criteria = $this->getFilter();
        $criteria->limit = $block;
        $criteria->offset = $start;
        $criteria->order = 'ID DESC';
        $model = Oportunidad::model()->find($criteria);
        Yii::app()->clientScript->scriptMap["jquery.js"] = false;
        Yii::app()->clientScript->scriptMap["jquery.min.js"] = false;
        $this->renderPartial('formpage', array('model' => $model, 'start' => $start, 'block' => $block, 'pages' => $ttreg, 'pag' => $pagact), false, true);
    }

    public function paginadorFormularios($idoportunidad, $start, $block, $ttreg, $pagact) {
        $model = Oportunidad::model()->findByPk($idoportunidad);
        $this->render('formpage', array('model' => $model, 'start' => $start, 'block' => $block, 'pages' => $ttreg, 'pag' => $pagact));
    }

    /**
     * Formulario de Contacto
     */
    public function actionFormulario($carga = 'single') {
        $model = Oportunidad::model()->findByPk($_REQUEST['idoportunidad']);
        $agendas = AgendadosOp::model()->findAllByAttributes(array('ID_Oportunidad' => $model->ID));
        foreach ($agendas as $a) :
            $time = $a->Fecha ." ". $a->Hora;
            if($time <= date('Y-m-d H:i:s')) :
                $a->ID_Accion = 19;
            endif;
            $a->update();
        endforeach;
        $this->render('formulario', array(
            'model' => $model, 'carga' => $carga
        ));
    }

    public function actionDireccion() {
        if (isset($_POST['formulario'])) :
            $input = substr($_POST['formulario'], -1);
        endif;

        if (isset($_POST['input'])) :
            $input = $_POST['input'];
        endif;

        $this->renderPartial('direcciones', array('idinput' => $input));
    }

    /**
     * Guarda la Gestion de la Oportunidad
     */
    public function actionGuardargestion() {
        $error = '';
        $permitidos = array(1, 2);
        $oportunidad = Oportunidad::model()->findByPk($_POST['idoportunidad']);
        $cadena = isset($_POST['comentarios']) ? $_POST['comentarios'] : 'Ningun Comentario';
        $cadena_agendamiento = isset($_POST['comentarios_agenda']) ? $_POST['comentarios_agenda'] : 'Ningun Comentario de Agendamiento';
        if ($_POST['accion'] == 19) :
            $fechaCita = strtotime($_POST['fecha_agendada']);
            $fechaActual = strtotime(date('Y-m-d H:i:s'));
            if ($fechaCita < $fechaActual) :
                $error .= "La fecha de Agenda no puede ser menor a Hoy";
            else :
                $time = explode(' ', $_POST['fecha_agendada']);
                AgendadosOp::model()->registro($time[0], $time[1], $_POST['idoportunidad'], Yii::app()->user->getState('id_usuario'), 2);
                $cadena .= " SE AGENDA PARA EL DIA " . $_POST['fecha_agendada'] . " - ". $cadena_agendamiento;
                $oportunidad->Fecha_Ultagenda = $time[0];
            endif;
        else :
            $oportunidad->Fecha_Ultagenda = date('Y-m-d');
        endif;
        if(empty($error)) :
            if (!in_array($oportunidad->Estado_proceso, $permitidos)) :
//                $datos = array();
//                $estadosweb = EstadosWeb::model()->findByPk($_POST['accion']);
                $invalido = array(2, 3, 4, 7);
                $descartados = array(16, 17, 18, 20);
                $cotizados = array(9,10,11,12);
                if(in_array($_POST['accion'], $descartados)) :
                    $oportunidad->Estado_proceso = 19;
                elseif(in_array($_POST['accion'], $cotizados)) :
                    $oportunidad->Estado_proceso = 23;
                elseif(in_array($_POST['accion'], $invalido)) :
                    $oportunidad->Estado_proceso = 19;
                else :
                    $oportunidad->Estado_proceso = 12;
                endif;
                $oportunidad->Fecha_Ultgestion = date('Y-m-d H:i:s');
                $oportunidad->Id_Estadoweb = $_POST['accion'];
                $oportunidad->Observaciones = $cadena;
                $oportunidad->update();
                
                HistoricoGestionOp::model()->guardarGestion($oportunidad->ID, $oportunidad->Estado_proceso, $oportunidad->Observaciones, $_POST['accion'], 0, 0);
                OportunidadEstado::model()->registro($oportunidad->ID, $oportunidad->Estado_proceso,  $_POST['accion']);
                echo "OK";
            else :
                echo "No se puede ingresar Gestion ya esta en " . $oportunidad->estadoProceso->Nombre;
            endif;
        else :
            echo $error;
        endif;
    }

    /**
     * Envia  el Registro a Contactos y actualiza la oportunida en Cita
     */
    public function actionComercial() {
        $idoportunidad = isset($_POST['idoportunidad']) ? $_POST['idoportunidad'] : 1;
        $cit = isset($_POST['fecha_asignada']) ? $_POST['fecha_asignada'] : date('Y-m-d H:i:s');
        $error = "";
        $oportunidad = Oportunidad::model()->findByPk($idoportunidad);
        $observaciones = isset($_POST['comentarios_cita']) ? $_POST['comentarios_cita'] : "Ninguna";
        $fecha = explode(" ", $cit);
        if ($oportunidad != NULL) :

            $oportunidad->Observaciones = $observaciones;
            $oportunidad->Estado_proceso = 28;
            $oportunidad->Id_Estadoweb = 5;
            $oportunidad->save();
            
            HistoricoGestionOp::model()->guardarGestion($oportunidad->ID, 22, $observaciones, 5);
            $contacto = Contacto::model()->actualizarContactoOportunidad($oportunidad, $observaciones, $cit);
            
            $historico = HistoricoGestionOp::model()->findAll('ID_Contacto = ' . $oportunidad->ID);
            if ($historico != NULL) :
                foreach ($historico as $h) :
                    HistoricoGestion::model()->guardarGestion($contacto->ID, $h->ID_Seguimiento, $h->Memo, $h->ID_Accion);
                endforeach;
            endif;
            
            $comentario = "Se Asigna al Asesor Comercial " . strtoupper($contacto->asesor->Nombre) . " para el dia " . $cit . " Horas, Observaciones : " . $observaciones;
            HistoricoGestion::model()->guardarGestion($contacto->ID, $contacto->Estado_proceso, $comentario, 10);

            $serv = ServicioInteresOportunidad::model()->findAll('ID_Contacto = ' . $oportunidad->ID);
            if ($serv != NULL) :
                foreach ($serv as $s) :
                    $servl = ServicioInteres::model()->findByAttributes(array('ID_Contacto' => $contacto->ID));
                    if ($servl == NULL) :
                        $servl = new ServicioInteres();
                    endif;
                    $servl->ID_Contacto = $contacto->ID;
                    $servl->ID_Servicio = $s->ID_Servicio;
                    $servl->Equipos = $s->Equipos;
                    $servl->Estado = $s->Estado;
                    $servl->save();
                endforeach;
            endif;

            Citas::model()->registro(0, $fecha[0], $oportunidad->Asesor, $_POST['idoportunidad']);
//
//            /*$arr_aux = array('razon' => $oportunidad->Razon_social, 'datecita' => $cit . ":00:00",
//                'telefono' => $oportunidad->Telefono, 'celular' => $oportunidad->Celular,
//                'direccion' => trim(str_replace("#", "N", $oportunidad->Direccion) . ";ob:" . $observaciones),
//                'nombre' => $oportunidad->Nombre_completo, 'emailasesor' => $oportunidad->asesor->Email, 'plan' => 1);*/
//
//            /*$this->enviarCitaSMS($arr_aux, $contacto->asesor->ID, $contacto->ID, $contacto->asesor->Celular);
//            $this->sendMail($arr_aux, $contacto->asesor->ID, $contacto->ID);*/
//            
//            /* Crear el nuevo Registro Mchimp */
            $datos = array();
            $datos['CFNAME'] = $contacto->Razon_social;
            $datos['CLNAME'] = $contacto->Nombre_completo;
            $datos['CPHONE'] = $contacto->Telefono;
            $datos['CCITY'] = $contacto->ciudad->Nombre;
            $datos['AGNAME'] = $contacto->telemercaderista->Nombre;
            $datos['AGPHONE'] = '2888788';
            $datos['AGEMAIL'] = $contacto->telemercaderista->Email;
            $datos['CORIGIN'] = $contacto->tipoContacto->Descripcion;
            $datos['CADDRESS'] = $contacto->Direccion;
            $datos['CSTATUS'] = 'Cita';
            $datos['MDATE'] = $fecha[0];
            $datos['MTIME'] = $fecha[1].":00:00";
            $datos['MNAME'] = $contacto->asesor->Nombre;
            $datos['MID'] = $contacto->asesor->Celular;
            $datos['ACITY'] = $contacto->ciudad->Nombre;
            $this->crearMailChimp($contacto->Email,$datos);

            $error = "OK";
        else : 
            $error = 'Error';
        endif;
        
        echo $error;
    }

    /**
     * 
     */
    public function actionEnviarcontacto() {
        $oportunidad = Oportunidad::model()->findByPk($_POST['idoportunidad']);
        if (!empty($_POST['comentarios']) && !empty($_POST['comercial'])) :
            $asesor = Asesor::model()->findByPk($_POST['comercial']);
            $oportunidad->Telemercaderista = $asesor->ID;
            $oportunidad->Observaciones = $_POST['comentarios'];
            if ($oportunidad->save()) :
                HistoricoGestionOp::model()->guardarGestion($oportunidad->ID, $oportunidad->Estado_proceso, $_POST['comentarios'], 2);
            endif;
            echo $asesor->Nombre;
        else :
            echo "Error";
        endif;
    }

    /**
     * Coincidencias del Contacto
     */
    public function actionCoincidencias() {
        if (isset($_POST['idoportunidad'])) :
            $model = Oportunidad::model()->findByPk($_POST['idoportunidad']);
            $error = "";
            $mensaje = "";
            $coincidenciacliente = "";

            $campos = array(
                "Razon_social",
                "Direccion",
                "Email",
                "Nit",
                "Telefono",
                "Celular",
                "Contrato"
            );

            foreach ($campos as $campo) :
                $error .= $model->coincidencia($campo);
                $error .= Contacto::model()->coincidencia($campo);
            endforeach;

            if (!empty($model->Contrato)) :
                $coincidenciacliente = $model->referido();
            endif;

            $retorno = "";

            if (!empty($error)) :
                $model->Bloqueado = 1;
                $model->save();
                $mensaje = " Este contacto ha sido bloqueado por tener coincidencias con otros registros: <br>Puede desbloquear este contacto en la Opcion Bloqueado en el Estado Actual<br> "
                        . " <div style='height:150px; overflow:auto'>"
                        . " <table class='table table-striped table-bordered'><tr><td style='text-align:center'>CAMPO</td><td style='text-align:center'>CANTIDAD</td><td style='text-align:center'>TIPO</td><td style='text-align:center'>COINCIDEN CON</td></tr>" . $error . "</table></div>";
                $retorno .= $mensaje;
            endif;

            if (!empty($coincidenciacliente)) :
                $retorno .= " <div style='height:150px; overflow:auto'>" . $coincidenciacliente . "</div>";
            endif;

            if (empty($coincidenciacliente) && empty($error)) :
                $retorno .= "OK";
            endif;
            
            echo $retorno;
        else :
            echo "OK";
        endif;
    }

    /**
     * Carga el detalle de las coincidencias
     */
    public function actionDetalle() {
        $arr = explode(",", $_POST['input']);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('ID', $arr);
        $registros = $_POST['model']::model()->findAll($criteria);
        $this->renderPartial('detalle', array('registros' => $registros, 'model' => $_POST['model']));
    }
    
    /**
     * 
     */
    public function actionEstweb() {
        $this->renderPartial('enviosmail', array('op' => $_POST['op'], 'titulo' => $_POST['titulo'], 'idoportunidad' => $_POST['idoportunidad']));
    }
}
