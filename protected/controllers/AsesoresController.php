<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChatController
 *
 * @author scorpio
 */
class AsesoresController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
//            array('allow', // allow all users to perform 'index' and 'view' actions
//                'actions' => array('*'),
//                'users' => array('*'),
//            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('citas', 'detalle', 'citassemana', 'citasrango', 'exporte', 'exporteventas', 'exporteventasgeneral'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    private function accessRegionales($op = false) {

        if ($op) :
            return Regional::model()->with(
                            array(
                                'regionalAgencia'
                            )
                    )->findAll();
        else :
            return Regional::model()->with(
                            array(
                                'regionalAsesor' =>
                                array(
                                    'condition' => 'ID_Asesor = ' . Yii::app()->user->getState('id_usuario') . ' AND regionalAsesor.Estado = 1'
                                ),
                                'regionalAgencia'
                            )
                    )->findAll();
        endif;
    }

    /**
     * Contador de Oportunidades por rango de Fechas y Asesor
     * @param type $asesor
     * @param type $fechaini
     * @param type $fechafin
     * @return type
     */
    private function contOport($asesor, $fechaini, $fechafin) {
        $criterio = new CDbCriteria();
        $criterio->select = 'Fecha_creacion, COUNT(*) as Total';
        $criterio->addBetweenCondition('Fecha_creacion', $fechaini, $fechafin);
        $criterio->addCondition('Asesor =' . $asesor);
        $criterio->group = 'Fecha_creacion';
        $criterio->order = 'Fecha_creacion ASC';
        return Oportunidad::model()->findAll($criterio);
    }

    /**
     * Contador de Oportunidades en Cita por rango de Fechas y Asesor
     * @param type $asesor
     * @param type $fechaini
     * @param type $fechafin
     * @return type
     */
    private function contCitOport($asesor, $fechaini, $fechafin) {
        $criterio = new CDbCriteria();
        $criterio->select = 'Fecha, COUNT(*) as Total';
        $criterio->addBetweenCondition('Fecha', $fechaini, $fechafin);
        $criterio->addCondition('ID_Asesor =' . $asesor . ' AND ID_Oportunidad != 0');
        $criterio->group = 'Fecha';
        $criterio->order = 'Fecha ASC';
        return Citas::model()->findAll($criterio);
    }

    /**
     * Contador de Citas por rango de Fechas y Asesor
     * @param type $asesor
     * @param type $fechaini
     * @param type $fechafin
     * @return type
     */
    private function contCit($asesor, $fechaini, $fechafin) {
        $criterio = new CDbCriteria();
        $criterio->select = 'Fecha, COUNT(*) as Total';
        $criterio->addBetweenCondition('Fecha', $fechaini, $fechafin);
        $criterio->addCondition('ID_Asesor =' . $asesor . ' AND ID_Contacto != 0');
        $criterio->group = 'ID_Contacto';
        $criterio->order = 'Fecha ASC';
        return Citas::model()->findAll($criterio);
    }

    /**
     * Contador de Citas por rango de Fechas y Asesor
     * @param type $asesor
     * @param type $fechaini
     * @param type $fechafin
     * @return type
     */
    private function contCitVen($asesor, $fechaini, $fechafin) {
        $criterio = new CDbCriteria();
        $criterio->addBetweenCondition('Fecha', $fechaini, $fechafin);
        $criterio->addCondition('ID_Asesor =' . $asesor);
        $criterio->order = 'Fecha ASC';
        return Citas::model()->findAll($criterio);
    }

    /**
     * Contador de Citas por rango de Fechas y Asesor
     * @param type $asesor
     * @param type $fechaini
     * @param type $fechafin
     * @return type
     */
    private function contCitVenT($asesor, $fechaini) {
        $cierre = $this->IDCierres($fechaini);
        $data = ClienteVenta::model()->findAll('Cierre = ' . $cierre . ' AND Asesor = ' . $asesor);
        return $data;
    }

    /**
     * Contador de Citas por rango de Fechas y Asesor
     * @param type $asesor
     * @return type
     */
    private function contCitVenGen($asesor) {
        $criterio = new CDbCriteria();
        $criterio->addCondition('ID_Asesor =' . $asesor);
        $criterio->order = 'Fecha ASC';
        return Citas::model()->findAll($criterio);
    }

    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionCitas() {

        date_default_timezone_set('America/Bogota');
        $fecha = date('Y-m-d');
        $dias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

        $semana_dia = explode("-", $fecha);

        # Obtenemos el día de la semana de la fecha dada
        $diaSemana = date("w", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2], $semana_dia[0]));
        # el 0 equivale al domingo...
        if ($diaSemana == 0) :
            $diaSemana = 7;
        endif;

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $firstDia = date("d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] - $diaSemana + 1, $semana_dia[0]));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $secondDia = date("d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] + (7 - $diaSemana), $semana_dia[0]));

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $primerDia = date("Y-m-d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] - $diaSemana + 1, $semana_dia[0]));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $ultimoDia = date("Y-m-d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] + (7 - $diaSemana), $semana_dia[0]));

        if(Yii::app()->user->getState('id_usuario') == 427 || Yii::app()->user->getState('id_usuario') == 508 || Yii::app()->user->getState('id_usuario') == 61 || Yii::app()->user->getState('id_usuario') == 484) :
            $relacion = $this->accessRegionales(true);
        else :
            $relacion = $this->accessRegionales();
        endif;

        $mes = Contacto::model()->Mes($semana_dia[1]);

        $i = 0;
        $personal_acargo = array();

        $ttoportunidad = 0;
        $ttcitas = 0;
        $ttoportunidadcita = 0;

        foreach ($relacion as $p) :
            foreach ($p->regionalAgencia as $r) :
                $asesor_agencia = AsesorAgencia::model()->findAll('Agencia = ' . $r->ID_Agencia);
                $arr_op = array();
                $arr_ct = array();
                $arr_co = array();
                foreach ($asesor_agencia as $aa):
                    $tto = 0;
                    $ttc = 0;
                    $ttco = 0;
                    if ($aa->asesor->Nivel == 10 && $aa->asesor->Estado == 1) :
                        $oportunidad = $this->contOport($aa->Asesor, $primerDia, $ultimoDia);
                        $citas = $this->contCit($aa->Asesor, $primerDia, $ultimoDia);
                        $citaop = $this->contCitOport($aa->Asesor, $primerDia, $ultimoDia);
                        if ($citas != NULL) :
                            $z = 0;
                            foreach ($citas as $c) :
                                $arr_ct[$z]['dia'] = date("d", strtotime($c->Fecha));
                                $arr_ct[$z]['cantidad'] = $c->Total;
                                $ttc += $c->Total;
                                $ttcitas += $c->Total;
                                $z++;
                            endforeach;
                        endif;

                        if ($citaop != NULL) :
                            $x = 0;
                            foreach ($citaop as $o) :
                                $arr_co[$x]['dia'] = date("d", strtotime($o->Fecha));
                                $arr_co[$x]['cantidad'] = $o->Total;
                                $ttco += $o->Total;
                                $ttoportunidadcita += $o->Total;
                                $x++;
                            endforeach;
                        endif;

                        if ($oportunidad != NULL) :
                            $j = 0;
                            foreach ($oportunidad as $o) :
                                $arr_op[$j]['dia'] = date("d", strtotime($o->Fecha_creacion));
                                $arr_op[$j]['cantidad'] = $o->Total;
                                $tto += $o->Total;
                                $ttoportunidad += $o->Total;
                                $j++;
                            endforeach;
                        endif;

                        $personal_acargo[$i]['nombre'] = $aa->asesor->Nombre;
                        $personal_acargo[$i]['oport'] = $arr_op;
                        $personal_acargo[$i]['citas'] = $arr_ct;
                        $personal_acargo[$i]['oprtcit'] = $arr_co;
                        $personal_acargo[$i]['agencia'] = $r->iDAgencia->Nombre;
                        $personal_acargo[$i]['idasesor'] = $aa->Asesor;
                        $personal_acargo[$i]['idagencia'] = $aa->Agencia;
                        $personal_acargo[$i]['tcitas'] = $ttc;
                        $personal_acargo[$i]['toport'] = $tto;
                        $personal_acargo[$i]['tco'] = $ttco;
                        $i++;
                    endif;
                endforeach;
            endforeach;
        endforeach;

        $this->render('index', array('personal' => $personal_acargo, 'mes' => $mes, 'fecha' => $fecha, 'dias' => $dias,
            'totalo' => $ttoportunidad, 'totalc' => $ttcitas, 'totalco' => $ttoportunidadcita,
            'primerdia' => $firstDia, 'ultimodia' => $secondDia, 'iniciosemana' => $primerDia, 'finsemana' => $ultimoDia));
    }

    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionCitasSemana() {

        date_default_timezone_set('America/Bogota');

        $dias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

        /* Carga la Semana Actual */
        if (isset($_POST['accion']) && $_POST['accion'] == 'atras') :
            $fecha = date('Y-m-d', strtotime('-1 day', strtotime($_POST['fecha'])));
        endif;
        if (isset($_POST['accion']) && $_POST['accion'] == 'siguiente') :
            $fecha = date('Y-m-d', strtotime('+1 day', strtotime($_POST['fecha'])));
        endif;

        $semana_dia = explode("-", $fecha);

        # Obtenemos el día de la semana de la fecha dada
        $diaSemana = date("w", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2], $semana_dia[0]));
        # el 0 equivale al domingo...
        if ($diaSemana == 0) :
            $diaSemana = 7;
        endif;

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $firstDia = date("d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] - $diaSemana + 1, $semana_dia[0]));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $secondDia = date("d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] + (7 - $diaSemana), $semana_dia[0]));

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $primerDia = date("Y-m-d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] - $diaSemana + 1, $semana_dia[0]));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $ultimoDia = date("Y-m-d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] + (7 - $diaSemana), $semana_dia[0]));

        if(Yii::app()->user->getState('id_usuario') == 427 || Yii::app()->user->getState('id_usuario') == 508 || Yii::app()->user->getState('id_usuario') == 61 || Yii::app()->user->getState('id_usuario') == 484) :
            $relacion = $this->accessRegionales(true);
        else :
            $relacion = $this->accessRegionales();
        endif;

        $mes = Contacto::model()->Mes($semana_dia[1]);

        $i = 0;
        $personal_acargo = array();
        $ttoportunidad = 0;
        $ttcitas = 0;
        $ttoportunidadcita = 0;

        if ($_POST['funcionario'] == 'all') :
            foreach ($relacion as $p) :
                foreach ($p->regionalAgencia as $r) :
                    $asesor_agencia = AsesorAgencia::model()->findAll('Agencia = ' . $r->ID_Agencia);
                    foreach ($asesor_agencia as $aa):
                        if ($aa->asesor->Nivel == 10 && $aa->asesor->Estado == 1) :
                            $oportunidad = $this->contOport($aa->Asesor, $primerDia, $ultimoDia);
                            $citas = $this->contCit($aa->Asesor, $primerDia, $ultimoDia);
                            $citaop = $this->contCitOport($aa->Asesor, $primerDia, $ultimoDia);
                            $tto = 0;
                            $ttc = 0;
                            $ttco = 0;
                            $arr_op = array();
                            $arr_ct = array();
                            $arr_co = array();
                            if ($citas != NULL) :
                                $z = 0;
                                foreach ($citas as $c) :
                                    $arr_ct[$z]['dia'] = date("d", strtotime($c->Fecha));
                                    $arr_ct[$z]['cantidad'] = $c->Total;
                                    $ttc += $c->Total;
                                    $ttcitas += $c->Total;
                                    $z++;
                                endforeach;
                            endif;

                            if ($citaop != NULL) :
                                $x = 0;
                                foreach ($citaop as $o) :
                                    $arr_co[$x]['dia'] = date("d", strtotime($o->Fecha));
                                    $arr_co[$x]['cantidad'] = $o->Total;
                                    $ttco += $o->Total;
                                    $ttoportunidadcita += $o->Total;
                                    $x++;
                                endforeach;
                            endif;

                            if ($oportunidad != NULL) :
                                $j = 0;
                                foreach ($oportunidad as $o) :
                                    $arr_op[$j]['dia'] = date("d", strtotime($o->Fecha_creacion));
                                    $arr_op[$j]['cantidad'] = $o->Total;
                                    $tto += $o->Total;
                                    $ttoportunidad += $o->Total;
                                    $j++;
                                endforeach;
                            endif;

                            $personal_acargo[$i]['nombre'] = $aa->asesor->Nombre;
                            $personal_acargo[$i]['oport'] = $arr_op;
                            $personal_acargo[$i]['citas'] = $arr_ct;
                            $personal_acargo[$i]['oprtcit'] = $arr_co;
                            $personal_acargo[$i]['agencia'] = $r->iDAgencia->Nombre;
                            $personal_acargo[$i]['idasesor'] = $aa->Asesor;
                            $personal_acargo[$i]['idagencia'] = $aa->Agencia;
                            $personal_acargo[$i]['tcitas'] = $ttc;
                            $personal_acargo[$i]['toport'] = $tto;
                            $personal_acargo[$i]['tco'] = $ttco;
                            $i++;
                        endif;
                    endforeach;
                endforeach;
            endforeach;
        else :

            $asesor = Asesor::model()->findByPk($_POST['funcionario']);
            $agencia = AsesorAgencia::model()->findByAttributes(array('Asesor' => $asesor->ID));
            $oportunidad = $this->contOport($_POST['funcionario'], $_POST['fecha1'], $_POST['fecha2']);
            $citas = $this->contCit($_POST['funcionario'], $_POST['fecha1'], $_POST['fecha2']);
            $citaop = $this->contCitOport($_POST['funcionario'], $_POST['fecha1'], $_POST['fecha2']);
            $tto = 0;
            $ttc = 0;
            $ttco = 0;
            $arr_op = array();
            $arr_ct = array();
            $arr_co = array();

            if ($citas != NULL) :
                $z = 0;
                foreach ($citas as $c) :
                    $arr_ct[$z]['dia'] = date("d", strtotime($c->Fecha));
                    $arr_ct[$z]['cantidad'] = $c->Total;
                    $ttc += $c->Total;
                    $ttcitas += $c->Total;
                    $z++;
                endforeach;
            endif;

            if ($oportunidad != NULL) :
                $j = 0;
                foreach ($oportunidad as $o) :
                    $arr_op[$j]['dia'] = date("d", strtotime($o->Fecha));
                    $arr_op[$j]['cantidad'] = $o->Total;
                    $tto += $o->Total;
                    $ttoportunidad += $o->Total;
                    $j++;
                endforeach;
            endif;

            if ($citaop != NULL) :
                $x = 0;
                foreach ($citaop as $o) :
                    $arr_co[$x]['dia'] = date("d", strtotime($o->Fecha));
                    $arr_co[$x]['cantidad'] = $o->Total;
                    $ttco += $o->Total;
                    $ttoportunidadcita += $o->Total;
                    $x++;
                endforeach;
            endif;


            $personal_acargo[$i]['nombre'] = $asesor->Nombre;
            $personal_acargo[$i]['oport'] = $arr_op;
            $personal_acargo[$i]['citas'] = $arr_ct;
            $personal_acargo[$i]['agencia'] = $agencia->agencia->Nombre;
            $personal_acargo[$i]['idasesor'] = $asesor->ID;
            $personal_acargo[$i]['idagencia'] = $agencia->Agencia;
            $personal_acargo[$i]['tcitas'] = $ttc;
            $personal_acargo[$i]['toport'] = $tto;
            $personal_acargo[$i]['tco'] = $ttco;
        endif;


        $this->renderPartial('paginador', array('personal' => $personal_acargo, 'mes' => $mes, 'fecha' => $fecha, 'dias' => $dias,
            'totalo' => $ttoportunidad, 'totalc' => $ttcitas, 'totalco' => $ttoportunidadcita, 'fecha1' => $primerDia, 'fecha2' => $ultimoDia,
            'primerdia' => $firstDia, 'ultimodia' => $secondDia, 'iniciosemana' => $primerDia, 'finsemana' => $ultimoDia));
    }

    /**
     * Retorna el Cierre Vigente en la Fecha dada o el ID Parametrizado
     * @param string $date
     * @return array Id y Mes Vigente
     */
    public function cierres($date) {

        $periodo = Cierres::model()->findAll();
        foreach ($periodo as $p) :
            if (Cierres::model()->check_in_range($p->Fecha_inicial, $p->Fecha_final, $date)) :
                $fecha['inicial'] = $p->Fecha_inicial;
                $fecha['final'] = $p->Fecha_final;
            endif;
        endforeach;

        return $fecha;
    }

    /**
     * Retorna el Cierre Vigente en la Fecha dada o el ID Parametrizado
     * @param string $date
     * @return array Id y Mes Vigente
     */
    public function IDCierres($date) {

        $periodo = Cierres::model()->findAll();
        $id = 0;
        foreach ($periodo as $p) :
            if (Cierres::model()->check_in_range($p->Fecha_inicial, $p->Fecha_final, $date)) :
                $id = $p->ID;
            endif;
        endforeach;

        return $id;
    }

    /**
     * Detallado por dia o por Mes del Asesor Seleccionado
     */
    public function actionDetalle() {

        $criteria = new CDbCriteria();
        $date = explode('-', $_POST['fechainicial']);

        if ('Oportunidad' == $_POST['tabla']) :
            $criteria->addCondition('Asesor = ' . $_POST['idasesor']);
            if ($_POST['accion'] == 'diario') :
                $fecha = $date[0] . "-" . $date[1] . "-" . $_POST['dia'];
                $criteria->addCondition('Fecha_creacion = "' . $fecha . '"');
            else :
                $criteria->addBetweenCondition('Fecha_creacion', $_POST['fechainicial'], $_POST['fechafinal']);
            endif;
            $registros = Oportunidad::model()->findAll($criteria);
        else :
            if ($_POST['accion'] == 'diario') :
                $fecha = $date[0] . "-" . $date[1] . "-" . $_POST['dia'];
                $criteria->addCondition('Fecha = "' . $fecha . '"');
            else :
                $criteria->addBetweenCondition('Fecha', $_POST['fechainicial'], $_POST['fechafinal']);
            endif;
            if ($_POST['tabla'] == 'Citao') :
                $criteria->addCondition('ID_Asesor = ' . $_POST['idasesor'] . ' AND ID_Oportunidad != 0');
                $criteria->group = 'ID_Oportunidad';
            else :
                $criteria->addCondition('ID_Asesor = ' . $_POST['idasesor'] . ' AND ID_Contacto != 0');
                $criteria->group = 'ID_Contacto';
            endif;

            $registros = Citas::model()->findAll($criteria);
        endif;

        $this->renderPartial('detalle', array('registros' => $registros, 'tabla' => $_POST['tabla']));
    }

    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionCitasRango() {

        date_default_timezone_set('America/Bogota');
        $fecha = date('Y-m-d');
        $dias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

        /* Carga la Semana Actual */
        //$fecha1 = date('Y-m-d', strtotime('0 day', strtotime($_POST['fecha1'])));
        $semana_dia = explode("-", $_POST['fecha1']);
        # Obtenemos el día de la semana de la fecha dada
        $diaSemana = date("w", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2], $semana_dia[0]));
        # el 0 equivale al domingo...
        if ($diaSemana == 0) :
            $diaSemana = 7;
        endif;

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $firstDia = date("d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] - $diaSemana + 1, $semana_dia[0]));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $secondDia = date("d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] + (7 - $diaSemana), $semana_dia[0]));

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $primerDia = date("Y-m-d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] - $diaSemana + 1, $semana_dia[0]));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $ultimoDia = date("Y-m-d", mktime(0, 0, 0, $semana_dia[1], $semana_dia[2] + (7 - $diaSemana), $semana_dia[0]));

        if(Yii::app()->user->getState('id_usuario') == 427 || Yii::app()->user->getState('id_usuario') == 508 || Yii::app()->user->getState('id_usuario') == 61 || Yii::app()->user->getState('id_usuario') == 484) :
            $relacion = $this->accessRegionales(true);
        else :
            $relacion = $this->accessRegionales();
        endif;
        

        $mes = Contacto::model()->Mes($semana_dia[1]);

        $i = 0;
        $personal_acargo = array();

        $ttoportunidad = 0;
        $ttcitas = 0;
        $ttoportunidadcita = 0;

        if ($_POST['funcionario'] == 'all') :
            foreach ($relacion as $p) :
                foreach ($p->regionalAgencia as $r) :
                    $asesor_agencia = AsesorAgencia::model()->findAll('Agencia = ' . $r->ID_Agencia);
                    foreach ($asesor_agencia as $aa):
                        if ($aa->asesor->Nivel == 10 && $aa->asesor->Estado == 1) :
                            $oportunidad = $this->contOport($aa->Asesor, $_POST['fecha1'], $_POST['fecha2']);
                            $citas = $this->contCit($aa->Asesor, $_POST['fecha1'], $_POST['fecha2']);
                            $citaop = $this->contCitOport($aa->Asesor, $_POST['fecha1'], $_POST['fecha2']);
                            $tto = 0;
                            $ttc = 0;
                            $ttco = 0;
                            $arr_op = array();
                            $arr_ct = array();
                            $arr_co = array();
                            if ($citas != NULL) :
                                $z = 0;
                                foreach ($citas as $c) :
                                    $arr_ct[$z]['dia'] = date("d", strtotime($c->Fecha));
                                    $arr_ct[$z]['cantidad'] = $c->Total;
                                    $ttc += $c->Total;
                                    $ttcitas += $c->Total;
                                    $z++;
                                endforeach;
                            endif;

                            if ($citaop != NULL) :
                                $x = 0;
                                foreach ($citaop as $o) :
                                    $arr_co[$x]['dia'] = date("d", strtotime($o->Fecha));
                                    $arr_co[$x]['cantidad'] = $o->Total;
                                    $ttco += $o->Total;
                                    $ttoportunidadcita += $o->Total;
                                    $x++;
                                endforeach;
                            endif;

                            if ($oportunidad != NULL) :
                                $j = 0;
                                foreach ($oportunidad as $o) :
                                    $arr_op[$j]['dia'] = date("d", strtotime($o->Fecha_creacion));
                                    $arr_op[$j]['cantidad'] = $o->Total;
                                    $tto += $o->Total;
                                    $ttoportunidad += $o->Total;
                                    $j++;
                                endforeach;
                            endif;

                            $personal_acargo[$i]['nombre'] = $aa->asesor->Nombre;
                            $personal_acargo[$i]['oport'] = $arr_op;
                            $personal_acargo[$i]['citas'] = $arr_ct;
                            $personal_acargo[$i]['oprtcit'] = $arr_co;
                            $personal_acargo[$i]['agencia'] = $r->iDAgencia->Nombre;
                            $personal_acargo[$i]['idasesor'] = $aa->Asesor;
                            $personal_acargo[$i]['idagencia'] = $aa->Agencia;
                            $personal_acargo[$i]['tcitas'] = $ttc;
                            $personal_acargo[$i]['toport'] = $tto;
                            $personal_acargo[$i]['tco'] = $ttco;
                            $i++;
                        endif;
                    endforeach;
                endforeach;
            endforeach;
        else :

            if (isset($_POST['funcionario']) && $_POST['funcionario'] != 'undefined') :
                $asesor = Asesor::model()->findByPk($_POST['funcionario']);
                $agencia = AsesorAgencia::model()->findByAttributes(array('Asesor' => $asesor->ID));
                $oportunidad = $this->contOport($_POST['funcionario'], $_POST['fecha1'], $_POST['fecha2']);
                $citas = $this->contCit($_POST['funcionario'], $_POST['fecha1'], $_POST['fecha2']);
                $citaop = $this->contCitOport($_POST['funcionario'], $_POST['fecha1'], $_POST['fecha2']);
                $tto = 0;
                $ttc = 0;
                $ttco = 0;
                $arr_op = array();
                $arr_ct = array();
                $arr_co = array();
                if ($citas != NULL) :
                    $z = 0;
                    foreach ($citas as $c) :
                        $arr_ct[$z]['dia'] = date("d", strtotime($c->Fecha));
                        $arr_ct[$z]['cantidad'] = $c->Total;
                        $ttc += $c->Total;
                        $ttcitas += $c->Total;
                        $z++;
                    endforeach;
                endif;

                if ($oportunidad != NULL) :
                    $j = 0;
                    foreach ($oportunidad as $o) :
                        $arr_op[$j]['dia'] = date("d", strtotime($o->Fecha_creacion));
                        $arr_op[$j]['cantidad'] = $o->Total;
                        $tto += $o->Total;
                        $ttoportunidad += $o->Total;
                        $j++;
                    endforeach;
                endif;

                if ($citaop != NULL) :
                    $x = 0;
                    foreach ($citaop as $o) :
                        $arr_co[$x]['dia'] = date("d", strtotime($o->Fecha));
                        $arr_co[$x]['cantidad'] = $o->Total;
                        $ttco += $o->Total;
                        $ttoportunidadcita += $o->Total;
                        $x++;
                    endforeach;
                endif;

                $personal_acargo[$i]['nombre'] = $asesor->Nombre;
                $personal_acargo[$i]['oport'] = $arr_op;
                $personal_acargo[$i]['citas'] = $arr_ct;
                $personal_acargo[$i]['oprtcit'] = $arr_co;
                $personal_acargo[$i]['agencia'] = $agencia->agencia->Nombre;
                $personal_acargo[$i]['idasesor'] = $asesor->ID;
                $personal_acargo[$i]['idagencia'] = $agencia->Agencia;
                $personal_acargo[$i]['tcitas'] = $ttc;
                $personal_acargo[$i]['toport'] = $tto;
                $personal_acargo[$i]['tco'] = $ttco;
            else :
                echo "Sin Funcionario";
            endif;
        endif;

        $this->renderPartial('paginador', array('personal' => $personal_acargo, 'mes' => $mes, 'fecha' => $fecha, 'dias' => $dias,
            'totalo' => $ttoportunidad, 'totalc' => $ttcitas, 'totalco' => $ttoportunidadcita, 'fecha1' => $_POST['fecha1'], 'fecha2' => $_POST['fecha2'],
            'primerdia' => $firstDia, 'ultimodia' => $secondDia, 'iniciosemana' => $primerDia, 'finsemana' => $ultimoDia));
    }

    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionExporte() {

        date_default_timezone_set('America/Bogota');

        $relacion = $this->accessRegionales();

        $i = 0;
        $personal_acargo = array();

        foreach ($relacion as $p) :
            foreach ($p->regionalAgencia as $r) :
                $asesor_agencia = AsesorAgencia::model()->findAll('Agencia = ' . $r->ID_Agencia);
                foreach ($asesor_agencia as $aa):
                    if ($aa->asesor->Nivel == 10 && $aa->asesor->Estado == 1) :
                        $oportunidad = $this->contOport($aa->Asesor, $_GET['fecha1'], $_GET['fecha2']);
                        $citas = $this->contCit($aa->Asesor, $_GET['fecha1'], $_GET['fecha2']);
                        $citaop = $this->contCitOport($aa->Asesor, $_GET['fecha1'], $_GET['fecha2']);
                        $tto = 0;
                        $ttc = 0;
                        $ttco = 0;
                        $arr_op = array();
                        $arr_ct = array();
                        $arr_co = array();
                        if ($citas != NULL) :
                            $z = 0;
                            foreach ($citas as $c) :
                                $arr_ct[$z]['dia'] = $c->Fecha;
                                $arr_ct[$z]['cantidad'] = $c->Total;
                                $ttc += $c->Total;
                                $z++;
                            endforeach;
                        endif;

                        if ($citaop != NULL) :
                            $x = 0;
                            foreach ($citaop as $o) :
                                $arr_co[$x]['dia'] = $o->Fecha;
                                $arr_co[$x]['cantidad'] = $o->Total;
                                $ttco += $o->Total;
                                $x++;
                            endforeach;
                        endif;

                        if ($oportunidad != NULL) :
                            $j = 0;
                            foreach ($oportunidad as $o) :
                                $arr_op[$j]['dia'] = $o->Fecha_creacion;
                                $arr_op[$j]['cantidad'] = $o->Total;
                                $tto += $o->Total;
                                $j++;
                            endforeach;
                        endif;

                        $personal_acargo[$i]['nombre'] = $aa->asesor->Nombre;
                        $personal_acargo[$i]['oport'] = $arr_op;
                        $personal_acargo[$i]['citas'] = $arr_ct;
                        $personal_acargo[$i]['oprtcit'] = $arr_co;
                        $personal_acargo[$i]['agencia'] = $r->iDAgencia->Nombre;
                        $personal_acargo[$i]['idasesor'] = $aa->Asesor;
                        $personal_acargo[$i]['idagencia'] = $aa->Agencia;
                        $personal_acargo[$i]['tcitas'] = $ttc;
                        $personal_acargo[$i]['toport'] = $tto;
                        $personal_acargo[$i]['tco'] = $ttco;
                        $i++;
                    endif;
                endforeach;
            endforeach;
        endforeach;

        if ($personal_acargo != NULL) :
            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;Filename=reporte_" . $_GET['fecha1'] . "_al_" . $_GET['fecha2'] . ".xls");
            $this->renderPartial('exporte', array('personal' => $personal_acargo, 'iniciosemana' => $primerDia));
            Yii::app()->end();
        else :
            echo "No se descargo el Archivo!.";
        endif;
    }

    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionExporteVentas() {

        date_default_timezone_set('America/Bogota');

        $relacion = $this->accessRegionales();

        $i = 0;
        $personal_acargo = array();

        foreach ($relacion as $p) :
            foreach ($p->regionalAgencia as $r) :
                $asesor_agencia = AsesorAgencia::model()->findAll('Agencia = ' . $r->ID_Agencia);
                foreach ($asesor_agencia as $aa):
                    if ($aa->asesor->Nivel == 10 && $aa->asesor->Estado == 1) :
                        $citas = $this->contCitVenT($aa->Asesor, $_GET['fecha1']);
                        $arr_ct = array();
                        if ($citas != NULL) :
                            $z = 0;
                            foreach ($citas as $c) :
                                $cliente = Cliente::model()->findByPk($c->ID_cliente);
                                if ($cliente != NULL) :
                                    $contacto = Contacto::model()->findByPk($cliente->ID_Contacto);
                                    $estadoventa = EstadoVenta::model()->findByPk($c->Estado);
                                    $arr_ct[$z]['id'] = $contacto->ID;
                                    $arr_ct[$z]['razon_social'] = $cliente->Razon_social;
                                    $arr_ct[$z]['contrato'] = $cliente->Contrato;
                                    $arr_ct[$z]['dia'] = $c->Fecha_cambioEstado;
                                    $arr_ct[$z]['estado'] = $estadoventa->Nombre;
                                    $arr_ct[$z]['comentarios'] = $contacto->Observaciones;
                                    $arr_ct[$z]['comercial'] = $contacto->telemercaderista->Nombre;
                                    $z++;
                                endif;
                            endforeach;
                        endif;
                        $personal_acargo[$i]['nombre'] = $aa->asesor->Nombre;
                        $personal_acargo[$i]['citas'] = $arr_ct;
                        $personal_acargo[$i]['agencia'] = $r->iDAgencia->Nombre;
                        $personal_acargo[$i]['idasesor'] = $aa->Asesor;
                        $personal_acargo[$i]['idagencia'] = $aa->Agencia;
                        $i++;
                    endif;
                endforeach;
            endforeach;
        endforeach;

        if ($personal_acargo != NULL) :
            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;Filename=reporteVentas_" . $_GET['fecha1'] . "_al_" . $_GET['fecha2'] . ".xls");
            $this->renderPartial('exporteventas', array('personal' => $personal_acargo));
            Yii::app()->end();
        else :
            echo "No se descargo el Archivo!.";
        endif;
    }

    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionExporteVentasGeneral() {

        date_default_timezone_set('America/Bogota');

        $relacion = $this->accessRegionales(true);

        $i = 0;
        $personal_acargo = array();

        foreach ($relacion as $p) :
            foreach ($p->regionalAgencia as $r) :
                $asesor_agencia = AsesorAgencia::model()->findAll('Agencia = ' . $r->ID_Agencia);
                foreach ($asesor_agencia as $aa):
                    $citas = $this->contCitVenT($aa->Asesor, $_GET['fecha1']);
                    $arr_ct = array();
                    if ($citas != NULL) :
                        $z = 0;
                        foreach ($citas as $c) :
                            $cliente = Cliente::model()->findByPk($c->ID_cliente);
                            if ($cliente != NULL) :
                                $contacto = Contacto::model()->findByPk($cliente->ID_Contacto);
                                if ($contacto != NULL) :
                                    $estadoventa = EstadoVenta::model()->findByPk($c->Estado);
                                    $arr_ct[$z]['id'] = $contacto->ID;
                                    $arr_ct[$z]['razon_social'] = $cliente->Razon_social;
                                    $arr_ct[$z]['contrato'] = $cliente->Contrato;
                                    $arr_ct[$z]['dia'] = $c->Fecha_cambioEstado;
                                    $arr_ct[$z]['estado'] = $estadoventa->Nombre;
                                    $arr_ct[$z]['comentarios'] = $contacto->Observaciones;
                                    $arr_ct[$z]['comercial'] = $contacto->telemercaderista->Nombre;
                                    $z++;
                                endif;
                            endif;
                        endforeach;
                    endif;
                    $personal_acargo[$i]['nombre'] = $aa->asesor->Nombre;
                    $personal_acargo[$i]['citas'] = $arr_ct;
                    $personal_acargo[$i]['agencia'] = $r->iDAgencia->Nombre;
                    $personal_acargo[$i]['idasesor'] = $aa->Asesor;
                    $personal_acargo[$i]['idagencia'] = $aa->Agencia;
                    $i++;
                endforeach;
            endforeach;
        endforeach;

        if ($personal_acargo != NULL) :
            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;Filename=reporteVentas_" . $_GET['fecha1'] . "_al_" . $_GET['fecha2'] . ".xls");
            $this->renderPartial('exporteventasgeneral', array('personal' => $personal_acargo));
            Yii::app()->end();
        else :
            echo "No se descargo el Archivo!.";
        endif;
    }

    public function actionVentas() {

        date_default_timezone_set('America/Bogota');

        $i = 0;
        $personal_acargo = array();

        if (Yii::app()->user->getState('id_usuario') == 568) :
            $relacion = $this->accessRegionales(true);
        else :
            $relacion = $this->accessRegionales();
        endif;

        foreach ($relacion as $p) :
            foreach ($p->regionalAgencia as $r) :
                $asesor_agencia = AsesorAgencia::model()->findAll('Agencia = ' . $r->ID_Agencia);
                foreach ($asesor_agencia as $aa) :
                    if ($aa->asesor->Nivel == 10 && $aa->asesor->Estado == 1) :
                        $citas = $this->contCitVen($aa->Asesor);
                        $arr_ct = array();
                        if ($citas != NULL) :
                            $z = 0;
                            foreach ($citas as $c) :
                                $contacto = Contacto::model()->findByPk($c->ID_Contacto);
                                if ($contacto != NULL) :
                                    $cliente = Cliente::model()->findByAttributes(array('ID_Contacto' => $contacto->ID));
                                    if ($cliente != NULL) :
                                        $contrato = $cliente->Contrato;
                                    else :
                                        $contrato = '';
                                    endif;
                                    $arr_ct[$z]['id'] = $contacto->ID;
                                    $arr_ct[$z]['razon_social'] = $contacto->Razon_social;
                                    $arr_ct[$z]['contrato'] = $contrato;
                                    $arr_ct[$z]['dia'] = $c->Fecha;
                                    $arr_ct[$z]['estado'] = "";
                                    $z++;
                                endif;
                            endforeach;
                        endif;
                        $personal_acargo[$i]['nombre'] = $aa->asesor->Nombre;
                        $personal_acargo[$i]['citas'] = $arr_ct;
                        $personal_acargo[$i]['agencia'] = $r->iDAgencia->Nombre;
                        $personal_acargo[$i]['idasesor'] = $aa->Asesor;
                        $personal_acargo[$i]['idagencia'] = $aa->Agencia;
                        $i++;
                    endif;
                endforeach;
            endforeach;
        endforeach;

        $this->renderPartial('ventas');
    }
}