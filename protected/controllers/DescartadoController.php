<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChatController
 *
 * @author scorpio
 */
class DescartadoController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
    
     public function accessRules() {
        return array(
//            array('allow', // allow all users to perform 'index' and 'view' actions
//                'actions' => array('*'),
//                'users' => array('*'),
//            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('presupuesto', 'competencia' , 'otros', 'error', 'marketing'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionPresupuesto() {
        $model = new Contacto('search');
        $model->setEstado(19);
        $model->setDescartado(12);
        $model->unsetAttributes();
        if (Yii::app()->getRequest()->getIsAjaxRequest()) :
            if (isset($_GET['Contacto'])) :
                $model->attributes = $_GET['Contacto'];
            endif;
            header('Content-type: application/json');
            $this->renderPartial('tablaDescartados', array('model' => $model));
            Yii::app()->end();
        endif;
        $this->render('index', array('model' => $model));
    }
    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionCompetencia() {
        
        $model = new Contacto('search');
        $model->setEstado(19);
        $model->setDescartado(13);
        $model->unsetAttributes();
        if (Yii::app()->getRequest()->getIsAjaxRequest()) :
            if (isset($_GET['Contacto'])) :
                $model->attributes = $_GET['Contacto'];
            endif;
            header('Content-type: application/json');
            $this->renderPartial('tablaDescartados', array('model' => $model));
            Yii::app()->end();
        endif;
        $this->render('index', array('model' => $model));
    }
    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionOtros() {
        
        $model = new Contacto('search');
        $model->setEstado(19);
        $model->setDescartado(14);
        $model->unsetAttributes();
        if (Yii::app()->getRequest()->getIsAjaxRequest()) :
            if (isset($_GET['Contacto'])) :
                $model->attributes = $_GET['Contacto'];
            endif;
            header('Content-type: application/json');
            $this->renderPartial('tablaDescartados', array('model' => $model));
            Yii::app()->end();
        endif;
        $this->render('index', array('model' => $model));
    }
    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionError() {
        
        $model = new Contacto('search');
        $model->setEstado(19);
        $model->setDescartado(15);
        $model->unsetAttributes();
        if (Yii::app()->getRequest()->getIsAjaxRequest()) :
            if (isset($_GET['Contacto'])) :
                $model->attributes = $_GET['Contacto'];
            endif;
            header('Content-type: application/json');
            $this->renderPartial('tablaDescartados', array('model' => $model));
            Yii::app()->end();
        endif;
        $this->render('tablaDescartados', array('model' => $model, 'view' => 'tablaDescartados'));
    }
    /**
     * Carga el DashBoard del Seguimiento
     */
    public function actionMarketing() {
        
        $model = new Contacto('search');
        $model->setEstado(19);
        $model->setDescartado(16);
        $model->unsetAttributes();
        if (Yii::app()->getRequest()->getIsAjaxRequest()) :
            if (isset($_GET['Contacto'])) :
                $model->attributes = $_GET['Contacto'];
            endif;
            header('Content-type: application/json');
            $this->renderPartial('tablaDescartados', array('model' => $model));
            Yii::app()->end();
        endif;
        $this->render('tablaDescartados', array('model' => $model, 'view' => 'tablaDescartados'));
    }
}
