<?php

class ToolsCommand extends CConsoleCommand {

    
    public function actionTokenrdstation() {
        
        Yii::import("ext.httpclient.*");
        $tkn = Rdstation::model()->findByPk(1);
        $url = "https://api.rd.services/auth/token";
        $client = new EHttpClient($url);
        $client->setParameterPost('client_id', $tkn->client_id);
        $client->setParameterPost('client_secret', $tkn->client_secret);
        $client->setParameterPost('code', $tkn->code);
        $response = $client->request('POST');
        print_r($response);
        try {
            if ($response->isSuccessful()) :
                $data = json_decode($response->getBody());
                
                if ($tkn == NULL) :
                    $tkn = new Rdstation();
                    $tkn->client_id = '37b9ddd8-80a2-47e5-8dff-c997dbfc2bb3';
                    $tkn->client_secret = '08345a029aa543e6a1c30204972208cc';
                    $tkn->code = 'c765abbfac831b412e2e33f69fe975a4';
                endif;
                $tkn->access_token = $data->access_token;
                $tkn->expires_in = $data->expires_in;
                $tkn->refresh_token = $data->refresh_token;
                if(!$tkn->save()) :
                    print_r($tkn->getErrors());
                endif;
                return true;
            else :
                return false;
            endif;
        } catch (Exception $e) {
            return false;
        }
    }

    public function actionRefreshtokenrdstation() {
        
        $tkn = Rdstation::model()->findByPk(1);
        Yii::import("ext.httpclient.*");
        $url = "https://api.rd.services/auth/token";
        $client = new EHttpClient($url);
        $client->setParameterPost('client_id', $tkn->client_id);
        $client->setParameterPost('client_secret', $tkn->client_secret);
        $client->setParameterPost('refresh_token', $tkn->refresh_token);
        $response = $client->request('POST');
        try {
            if ($response->isSuccessful()) :
                $data = json_decode($response->getBody());
                $tkn = Rdstation::model()->findByPk(1);
                if ($tkn == NULL) :
                    $tkn = new Rdstation();
                    $tkn->client_id = '37b9ddd8-80a2-47e5-8dff-c997dbfc2bb3';
                    $tkn->client_secret = '08345a029aa543e6a1c30204972208cc';
                    $tkn->code = 'c765abbfac831b412e2e33f69fe975a4';
                endif;
                $tkn->access_token = $data->access_token;
                $tkn->expires_in = $data->expires_in;
                $tkn->refresh_token = $data->refresh_token;
                $tkn->save();
                return true;
            else :
                return false;
            endif;
        } catch (Exception $e) {
            return false;
        }
    }
}
