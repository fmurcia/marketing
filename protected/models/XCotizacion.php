<?php

/**
 * This is the model class for table "x_cotizacion".
 *
 * The followings are the available columns in table 'x_cotizacion':
 * @property integer $ID
 * @property string $Codigo
 * @property integer $ID_Contacto
 * @property integer $Asesor
 * @property integer $Agencia
 * @property integer $Contrato
 * @property string $Decision_compra
 * @property string $Fecha_creacion
 * @property integer $Ciudad
 * @property string $Razon_social
 * @property string $Direccion
 * @property string $Telefono
 * @property string $Celular
 * @property string $Email
 * @property string $Tipo_cotizacion
 * @property integer $Servicio
 * @property integer $Plazo
 * @property string $clasificacion
 * @property double $Valor_total
 * @property integer $ID_Negociacion
 */
class XCotizacion extends ActiveRecord {

    public $counter;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'x_cotizacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Codigo', 'required'),
            array('ID, ID_Contacto, Asesor, Agencia, Contrato, Ciudad, Servicio, Plazo, ID_Negociacion', 'numerical', 'integerOnly' => true),
            array('Valor_total', 'numerical'),
            array('Codigo, Celular', 'length', 'max' => 40),
            array('Decision_compra, clasificacion', 'length', 'max' => 100),
            array('Razon_social', 'length', 'max' => 500),
            array('Telefono', 'length', 'max' => 50),
            array('Email', 'length', 'max' => 200),
            array('Tipo_cotizacion', 'length', 'max' => 45),
            array('Fecha_creacion, Direccion, counter', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Codigo, ID_Contacto, Asesor, Agencia, Contrato, Decision_compra, Fecha_creacion, Ciudad, Razon_social, Direccion, Telefono, Celular, Email, Tipo_cotizacion, Servicio, Plazo, clasificacion, Valor_total, ID_Negociacion', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Codigo' => 'Codigo',
            'ID_Contacto' => 'Id Contacto',
            'Asesor' => 'Asesor',
            'Agencia' => 'Agencia',
            'Contrato' => 'Contrato',
            'Decision_compra' => 'Decision Compra',
            'Fecha_creacion' => 'Fecha Creacion',
            'Ciudad' => 'Ciudad',
            'Razon_social' => 'Razon Social',
            'Direccion' => 'Direccion',
            'Telefono' => 'Telefono',
            'Celular' => 'Celular',
            'Email' => 'Email',
            'Tipo_cotizacion' => 'Tipo Cotizacion',
            'Servicio' => 'Servicio',
            'Plazo' => 'Plazo',
            'clasificacion' => 'Clasificacion',
            'Valor_total' => 'Valor Total',
            'ID_Negociacion' => 'Id Negociacion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('Asesor', $this->Asesor);
        $criteria->compare('Agencia', $this->Agencia);
        $criteria->compare('Contrato', $this->Contrato);
        $criteria->compare('Decision_compra', $this->Decision_compra, true);
        $criteria->compare('Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Ciudad', $this->Ciudad);
        $criteria->compare('Razon_social', $this->Razon_social, true);
        $criteria->compare('Direccion', $this->Direccion, true);
        $criteria->compare('Telefono', $this->Telefono, true);
        $criteria->compare('Celular', $this->Celular, true);
        $criteria->compare('Email', $this->Email, true);
        $criteria->compare('Tipo_cotizacion', $this->Tipo_cotizacion, true);
        $criteria->compare('Servicio', $this->Servicio);
        $criteria->compare('Plazo', $this->Plazo);
        $criteria->compare('clasificacion', $this->clasificacion, true);
        $criteria->compare('Valor_total', $this->Valor_total);
        $criteria->compare('ID_Negociacion', $this->ID_Negociacion);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return XCotizacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
