<?php

/**
 * This is the model class for table "Grupo_cotizacion".
 *
 * The followings are the available columns in table 'Grupo_cotizacion':
 * @property integer $ID
 * @property string $Codigo
 * @property integer $ID_Contacto
 * @property integer $Asesor
 * @property integer $Agencia
 * @property integer $Contrato
 * @property string $Decision_compra
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 * @property string $Fecha_creacion
 * @property integer $Reuso
 * @property integer $Estado
 * @property integer $Editable
 * @property string $Observaciones
 * @property integer $Enviado_4D
 * @property integer $ID_cierre
 * @property integer $Ciudad
 *
 * The followings are the available model relations:
 * @property Cotizacion[] $cotizacions
 * @property Agencia $agencia
 * @property Asesor $asesor
 * @property Contacto $iDContacto
 */
class GrupoCotizacion extends ActiveRecord {

    public $redondeo = 0;
    public $attr = array(
        "Estado"
    );

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Grupo_cotizacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('Codigo, ID_cierre', 'required'),
            array('ID_Contacto, ID_Encargado,  Asesor, Agencia, Contrato, Reuso, Estado, Editable, Enviado_4D, ID_cierre, Ciudad', 'numerical', 'integerOnly' => true),
            array('Codigo', 'length', 'max' => 40),
            array('Decision_compra', 'length', 'max' => 100),
            array('Observaciones', 'length', 'max' => 200),
            array('Fecha_inicial, Fecha_final, Fecha_creacion,Version', 'safe'),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('ID, Codigo, ID_Contacto, Asesor, Agencia, Contrato, Decision_compra, Fecha_inicial, Fecha_final, Fecha_creacion, Reuso, Estado, Editable, Observaciones, Enviado_4D, ID_cierre, Ciudad', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'cotizaciones' => array(self::HAS_MANY, 'Cotizacion', 'ID_Grupo'),
            'agencia' => array(self::BELONGS_TO, 'Agencia', 'Agencia'),
            'asesor' => array(self::BELONGS_TO, 'Asesor', 'Asesor'),
            'contacto' => array(self::BELONGS_TO, 'Contacto', 'ID_Contacto'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Codigo' => 'Codigo',
            'ID_Contacto' => 'Id Contacto',
            'Asesor' => 'Asesor',
            'Agencia' => 'Agencia',
            'Contrato' => 'Contrato',
            'Decision_compra' => 'Decision Compra',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
            'Fecha_creacion' => 'Fecha Creacion',
            'Reuso' => 'Reuso',
            'Estado' => 'Estado',
            'Editable' => 'Editable',
            'Observaciones' => 'Observaciones',
            'Enviado_4D' => 'Enviado 4 D',
            'ID_cierre' => 'Id Cierre',
            'Ciudad' => 'Ciudad',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('Asesor', $this->Asesor);
        $criteria->compare('Agencia', $this->Agencia);
        $criteria->compare('Contrato', $this->Contrato);
        $criteria->compare('Decision_compra', $this->Decision_compra, true);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);
        $criteria->compare('Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Reuso', $this->Reuso);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Editable', $this->Editable);
        $criteria->compare('Observaciones', $this->Observaciones, true);
        $criteria->compare('Enviado_4D', $this->Enviado_4D);
        $criteria->compare('ID_cierre', $this->ID_cierre);
        $criteria->compare('Ciudad', $this->Ciudad);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return GrupoCotizacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function calculaCodigo() {
        if ($this->Codigo == "") {
            $cons = Parametros::model()->getParam("consgcot");
            $code = "G" . str_pad($cons, 7, "0", STR_PAD_LEFT);
            $cons = $cons + 1;
            Parametros::model()->setParam("consgcot", $cons);
        } else {
            $code = $this->Codigo;
        }
        return $code;
    }

    public function getElementos() {
        $elementos = Yii::app()->db->createCommand()
                ->setFetchMode(PDO::FETCH_OBJ)
                ->select('e.ID as ID, e.Nombre as Nombre, e.Categoria as Categoria')
                ->from('Elemento e')
                ->where("e.Clasificacion='Elemento' and e.Servicio=1 and e.Estado=1")
                ->queryAll();
        return CHtml::listData($elementos, "ID", "Nombre", 'Categoria');
    }

    public function getNegociaciones() {
        return Yii::app()->db->createCommand()
                        ->setFetchMode(PDO::FETCH_OBJ)
                        ->select('n.ID, n.Nombre')
                        ->from('Negociacion n')
                        ->queryAll();
    }

    public function getServicios() {
        return Yii::app()->db->createCommand()
                        ->setFetchMode(PDO::FETCH_OBJ)
                        ->select('s.ID, s.Nombre')
                        ->from('Servicios s')
                        ->where("s.Tipo=1 and s.Estado=1")
                        ->queryAll();
    }

    public function getContacto() {
        $contacto = Yii::app()->user->getState("ctzcontacto");
        $encargado = Yii::app()->user->getState("ctzencargado");

        if ($contacto != null && $encargado != null) {
            if ((int) $encargado > 0) {
                return PersonaContacto::model()->findByPk($encargado);
            } else {
                return Contacto::model()->findByPk($contacto);
            }
        } else {
            throw new CHttpException(500, "No hay informacion del contacto");
        }
    }

    public function getUnidadesNegocio($text = false) {
        $cotids = array();
        $cottext = "";
        $cotizas = $this->cotizaciones;
        if ($cotizas != null) {
            if (count($cotizas) > 0) {
                foreach ($cotizas as $cotiza) {
                    $serv = Servicios::model()->findByPk($cotiza->Servicio);
                    $nomserv = ($serv != null ? $serv->Nombre : "[Error en servicio]");
                    if ($text == false) {
                        $cotids[] = array(
                            "ID" => $cotiza->ID,
                            "UNegocio" => $cotiza->Servicio,
                            "Nombre" => $nomserv,
                            "Descripcion" => "", //Pendiente cambiar con la descripcion
                            "Multiple" => $serv->Multiple,
                        );
                    } else {
                        $cottext.=$nomserv . ", ";
                    }
                }
            }
        }
        if ($text == false) {
            return $cotids;
        } else {
            $cottext = substr($cottext, 0, strlen($cottext) - 2);
            return $cottext;
        }
    }

    public function getTotalCotizacion() {
        $totres = array(
            "equinst" => array(),
            "arriendos" => array(),
            "servicios" => array(),
            "nocargo" => array(),
        );
        $eqbase = array(
            "nombre" => "",
            "stotal" => 0,
            "dcto" => 0,
            "dctoval" => 0,
            "totalniva" => 0,
            "iva" => 0,
            "totaliva" => 0,
            "ddi" => 0,
            "apagar" => 0,
        );
        $arrbase = array(
            "plazo" => 0,
            "valor" => 0,
            "cuotaniva" => 0,
            "iva" => 0,
            "cuotaiva" => 0,
        );
        $ncbase = array(
            "nombre" => "",
            "plazo" => 0,
            "valor" => 0
        );
        $servbase = array(
            "nombre" => "",
            "totniva" => 0,
            "piva" => 0,
            "iva" => 0,
            "totiva" => 0
        );


        ///Traigo los totales///


        $cotizas = $this->cotizaciones;
        if (!empty($cotizas)) {
            foreach ($cotizas as $cotiza) {
                $totlist = $cotiza->totales;
                if (!empty($totlist)) {
                    foreach ($totlist as $tot) {
                        $totales["Cotizacion"][] = $tot->ID_Cotizacion;
                        $totales["UNegocio"][] = $tot->ID_Servicio;
                        $totales["Negociacion"][] = $tot->ID_Negociacion;
                        $totales["Clasificacion"][] = $tot->Clasificacion;
                        $totales["Plazo"][] = $tot->Plazo;
                        $totales["Valor_proveedor"][] = $tot->Precio_proveedor;
                        $totales["Valor_CIF"][] = $tot->Precio_CIF;
                        $totales["Valor_venta"][] = $tot->Precio_venta;
                        $totales["Cuota"][] = $tot->Valor_cuota;
                        $totales["Descuento"][] = $tot->Descuento;
                        $totales["DDI"][] = $tot->Dcto_desp_iva;
                        $totales["Sumar"][] = $tot->Sumar;
                        $totales["Obsequio"][] = $tot->Obsequio;
                        $totales["Borrar"][] = 0;
                    }
                }

                if (!empty($totales)) {
//Primero equipos
                    $pos1 = queryArray("Elemento", $totales["Clasificacion"]);
//Segundo ventas
                    $pos2 = querySelectionArray(1, $totales["Negociacion"], $pos1);
                    if (count($pos2) > 0) {
                        foreach ($pos2 as $pos) {
                            if (!isset($totres["equinst"]["Equipos"])) {
                                $totres["equinst"]["Equipos"] = $eqbase;
                                $totres["equinst"]["Equipos"]["nombre"] = "Equipos";
                            }
                            $totres["equinst"]["Equipos"]["stotal"]+=$totales["Valor_venta"][$pos];
                            $totres["equinst"]["Equipos"]["dcto"] = ($totales["Descuento"][$pos] > $totres["equinst"]["Equipos"]["dcto"] ? $totales["Descuento"][$pos] : $totres["equinst"]["Equipos"]["dcto"]);
                            $totres["equinst"]["Equipos"]["ddi"]+=$totales["DDI"][$pos];
                        }
                    }
//Tercero arriendos
                    $pos3 = querySelectionArray(2, $totales["Negociacion"], $pos1);

                    if (count($pos3) > 0) {
                        foreach ($pos3 as $pos) {
                            if (!isset($totres["arriendos"][$totales["Plazo"][$pos]])) {
                                $totres["arriendos"][$totales["Plazo"][$pos]] = $arrbase;
                                $totres["arriendos"][$totales["Plazo"][$pos]]["plazo"] = $totales["Plazo"][$pos];
                            }
                            $totres["arriendos"][$totales["Plazo"][$pos]]["valor"]+=$totales["Valor_venta"][$pos];
                            $totres["arriendos"][$totales["Plazo"][$pos]]["cuotaniva"]+=$totales["Cuota"][$pos];
                        }
                    }
//Cuarto comodatos
                    $pos4 = querySelectionArray(3, $totales["Negociacion"], $pos1);

                    if (count($pos4) > 0) {
                        foreach ($pos4 as $pos) {
                            if (!isset($totres["nocargo"]["Comodato"])) {
                                $totres["nocargo"]["Comodato"] = $ncbase;
                                $totres["nocargo"]["Comodato"]["nombre"] = "Comodato";
                            }
                            $totres["nocargo"]["Comodato"]["plazo"] = ($totres["nocargo"]["Comodato"]["plazo"] > $totales["Plazo"][$pos] ? $totres["nocargo"]["Comodato"]["plazo"] : $totales["Plazo"][$pos]);
                            $totres["nocargo"]["Comodato"]["valor"]+=$totales["Valor_venta"][$pos];
                        }
                    }
//Quinto obsequios

                    $pos5 = querySelectionArray(4, $totales["Negociacion"], $pos1);

                    if (count($pos5) > 0) {
                        foreach ($pos5 as $pos) {
                            if (!isset($totres["nocargo"]["Obsequio"])) {
                                $totres["nocargo"]["Obsequio"] = $ncbase;
                                $totres["nocargo"]["Obsequio"]["nombre"] = "Obsequio";
                            }
                            $totres["nocargo"]["Obsequio"]["plazo"] = 0;
                            $totres["nocargo"]["Obsequio"]["valor"]+=$totales["Valor_venta"][$pos];
                        }
                    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Traer instalacion

                    $pos1 = queryArray("Instalacion", $totales["Clasificacion"]);

                    if (count($pos1) > 0) {
                        foreach ($pos1 as $pos) {
                            if (!isset($totres["equinst"]["Instalacion"])) {
                                $totres["equinst"]["Instalacion"] = $eqbase;
                                $totres["equinst"]["Instalacion"]["nombre"] = "Instalacion";
                            }
                            if ($totales["Sumar"][$pos] == 1) {
                                $totres["equinst"]["Instalacion"]["stotal"]+=$totales["Valor_venta"][$pos];
                            }
                            $totres["equinst"]["Instalacion"]["dcto"] = ($totales["Descuento"][$pos] > $totres["equinst"]["Instalacion"]["dcto"] ? $totales["Descuento"][$pos] : $totres["equinst"]["Instalacion"]["dcto"]);
                            $totres["equinst"]["Instalacion"]["ddi"]+=$totales["DDI"][$pos];
                        }
                    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Traer servicios


                    $servs = DetalleCotizacion::model()->findAllByAttributes(array(
                        "ID_Cotizacion" => $cotiza->ID,
                        "Clasificacion" => "Servicio"
                    ));

                    if (count($servs) > 0) {
                        foreach ($servs as $serv) {
                            if (!isset($totres["servicios"][$serv->Codigo])) {
                                $totres["servicios"][$serv->Codigo] = $servbase;
                            }
                            $ele = DetalleLista::model()->findByPk($serv->ID_ElementoLP);
                            if ($ele != null) {
                                $totres["servicios"][$serv->Codigo]["nombre"] = $ele->getNombreElemento();
                                $totres["servicios"][$serv->Codigo]["totniva"] += $serv->Valor_venta;
                                $totres["servicios"][$serv->Codigo]["piva"] = $ele->getIVA();
                            }
                        }
                    }
///////////FINALIZANDO CON LOS TOTALES AHORA A CALCULAR FALTANTES////////////



                    $equinst = $totres["equinst"];
                    $arriendos = $totres["arriendos"];
                    $servicios = $totres["servicios"];

//Primero equipos + instalacion
                    $n = count($equinst);
                    if ($n > 0) {
                        foreach ($equinst as $id => $info) {
                            $info["dctoval"] = round(($info["stotal"] * $info["dcto"]) / 100, $this->redondeo, PHP_ROUND_HALF_UP);
                            $info ["totalniva"] = $info["stotal"] - $info["dctoval"];
                            $info["iva"] = round(($info["totalniva"] * 16) / 100, $this->redondeo, PHP_ROUND_HALF_UP);
                            $info["totaliva"] = $info["totalniva"] + $info["iva"];
                            $info["apagar"] = $info["totaliva"] - $info["ddi"];
                            $equinst[$id] = $info;
                        }
                    }

                    $n = count($arriendos);

                    if ($n > 0) {
                        foreach ($arriendos as $id => $info) {
                            $info["iva"] = round(($info["cuotaniva"] * 16) / 100, $this->redondeo, PHP_ROUND_HALF_UP);
                            $info["cuotaiva"] = $info["cuotaniva"] + $info["iva"];
                            $arriendos[$id] = $info;
                        }
                    }

                    $n = count($servicios);

                    if ($n > 0) {
                        foreach ($servicios as $id => $info) {
                            $info["iva"] = round(($info["totniva"] * $info["piva"]) / 100, $this->redondeo, PHP_ROUND_HALF_UP);
                            $info["totiva"] = $info["totniva"] + $info["iva"];
                            $servicios[$id] = $info;
                        }
                    }

                    $totres["equinst"] = $equinst;
                    $totres["arriendos"] = $arriendos;

                    $totres["servicios"] = $servicios;
                }
            }
        }

        return $totres;
    }

    public function sumTotales() {
        $gtotal = 0;
        $totales = $this->getTotalCotizacion();
        if ($totales != null) {

            $equinst = $totales["equinst"];
            $arriendos = $totales["arriendos"];
            $servicios = $totales["servicios"];
            $nocargo = $totales["nocargo"];

            $totequinst = 0;
            $totarriendos = 0;
            $totservicios = 0;
            $totnocargo = 0;

            if (count($equinst) > 0) {
                foreach ($equinst as $id => $info) {
                    $totequinst+=$info["apagar"];
                }
            }

            if (count($arriendos) > 0) {
                foreach ($arriendos as $id => $info) {
                    $totarriendos+=$info["cuotaiva"];
                }
            }

            if (count($servicios) > 0) {
                foreach ($servicios as $id => $info) {
                    $totservicios+=$info["totiva"];
                }
            }

            if (count($nocargo) > 0) {
                foreach ($nocargo as $id => $info) {
                    $totnocargo+=$info["valor"];
                }
            }

            $gtotal = $totequinst + $totarriendos + $totservicios;
        }
        return $gtotal;
    }

    public function sumServicios() {
        $totservicios = 0;
        $totales = $this->getTotalCotizacion();
        if ($totales != null) {
            $servicios = $totales["servicios"];
            if (count($servicios) > 0) {
                foreach ($servicios as $id => $info) {
                    $totservicios+=$info["totiva"];
                }
            }
        }
        return $totservicios;
    }

    private function createTotales() {
        return array(
            "Cotizacion" => array(),
            "UNegocio" => array(),
            "Negociacion" => array(),
            "Clasificacion" => array(),
            "Plazo" => array(),
            "Valor_proveedor" => array(),
            "Valor_CIF" => array(),
            "Valor_venta" => array(),
            "Cuota" => array(),
            "Descuento" => array(),
            "DDI" => array(),
            "Sumar" => array(),
            "Obsequio" => array(),
            "Borrar" => array(),
        );
    }

    public function getNegoPrinc($texto = true) {
        $negopri = 99;
        $negoid = 0;
        $cotizas = $this->cotizaciones;
        if (!empty($cotizas)) {
            foreach ($cotizas as $cotiza) {
                $neg = $cotiza->getNegoPrinc();
                if ($neg > 0) {
                    $nego = Negociacion::model()->findByPk($neg);
                    if ($nego != null) {
                        if ($nego->Prioridad < $negopri) {
                            $negopri = $nego->Prioridad;
                            $negoid = $nego->ID;
                        }
                    }
                }
            }
        }
        if ($negoid > 0) {
            if ($texto == true) {
                $nego = Negociacion::model()->findByPk($negoid);
                if ($nego != null) {
                    return $nego->Nombre;
                }
            } else {
                return $nego->ID;
            }
        }
        return "Sin negociación";
    }

    public function getDetServicios() {
        $serv = array();

        if (!empty($this->cotizaciones)) {
            $cotizas = $this->cotizaciones;
            foreach ($cotizas as $cotiza) {
                $servlist = $cotiza->getServicios();
                $serv = $serv + $servlist;
            }
        }
        return $servlist;
    }

    public function getUnico() {
        $unegs = $this->getUnidadesNegocio();
        if (count($unegs) > 0) {
            foreach ($unegs as $uneg) {
                if ($uneg["Multiple"] == 1) {
                    return 0;
                }
            }
        }
        return 1;
    }

    public function getPLDocs() {
        $docidlist = array();
        $cotizas = $this->cotizaciones;
        if (!empty($cotizas)) {
            foreach ($cotizas as $cotiza) {
                $dlist = $cotiza->getPLDocs();
                if (count($dlist) > 0) {
                    foreach ($dlist as $d) {
                        $docidlist[$d] = $d;
                    }
                }
            }
        }
        return $docidlist;
    }

    public function getEditable() {
        $asesor = Yii::app()->user->getState("id_usuario");
        if ($this->Editable == 1) {
            if ($this->Asesor == $asesor) {
                return 1;
            }
        }
        return 0;
    }

}
