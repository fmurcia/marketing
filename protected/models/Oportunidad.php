<?php

/**
 * This is the model class for table "Oportunidad".
 *
 * The followings are the available columns in table 'Oportunidad':
 * @property integer $ID
 * @property integer $Telemercaderista
 * @property integer $Tipo_cliente
 * @property string $Razon_social
 * @property string $Nit
 * @property string $Nombre_completo
 * @property string $Direccion
 * @property string $Telefono
 * @property string $Celular
 * @property string $Email
 * @property integer $Ciudad
 * @property integer $Agencia
 * @property integer $Asesor
 * @property string $Barrio
 * @property string $Medio_contacto
 * @property string $Documento
 * @property string $Observaciones
 * @property integer $Estado
 * @property string $Fecha_creacion
 * @property string $Hora_creacion
 * @property string $Actividad
 * @property integer $Bloqueado
 * @property integer $Estado_proceso
 * @property integer $Tipo_contacto
 * @property integer $TipoDescartado
 * @property string $Fecha_Ultgestion
 * @property string $Fecha_Ultagenda
 * @property integer $Id_Estadoweb
 *
 * The followings are the available model relations:
 * @property Asesor $telemercaderista
 * @property Ciudad $ciudad
 * @property Agencia $agencia
 * @property Asesor $asesor
 * @property EstadoVenta $estadoProceso
 * @property TipoContacto $tipoContacto
 * @property TipoCliente $tipoCliente
 */
class Oportunidad extends CActiveRecord {

    public $searchasesor;
    public $searchagencia;
    public $searchciudad;
    public $searchtipocontacto;
    public $EstadoProceso;
    public $Total;
    public $counter;
    public $Contrato;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Oportunidad';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Razon_social', 'required'),
            array('Telemercaderista, Tipo_cliente, Ciudad, Agencia, Asesor, Estado, Estado_proceso, Tipo_contacto, TipoDescartado, Id_Estadoweb', 'numerical', 'integerOnly' => true),
            array('Razon_social, Nombre_completo', 'length', 'max' => 500),
            array('Nit', 'length', 'max' => 100),
            array('Telefono', 'length', 'max' => 50),
            array('Celular', 'length', 'max' => 40),
            array('Email, Medio_contacto, Actividad', 'length', 'max' => 200),
            array('Barrio', 'length', 'max' => 45),
            array('Documento', 'length', 'max' => 20),
            array('Direccion, Observaciones, Fecha_creacion, Fecha_Ultgestion, Hora_creacion, Fecha_Ultagenda', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Telemercaderista, Tipo_cliente, Razon_social, Nit, Nombre_completo, Direccion, Telefono, Celular, Email, Ciudad, Agencia, Asesor, Barrio, Medio_contacto, Documento, Observaciones, Estado, Fecha_creacion, Hora_creacion, Actividad, Bloqueado, Estado_proceso, Tipo_contacto, TipoDescartado, searchtipocontacto, searchciudad, searchagencia, searchasesor, Id_Estadoweb', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'telemercaderista' => array(self::BELONGS_TO, 'Asesor', 'Telemercaderista'),
            'ciudad' => array(self::BELONGS_TO, 'Ciudad', 'Ciudad'),
            'agencia' => array(self::BELONGS_TO, 'Agencia', 'Agencia'),
            'asesor' => array(self::BELONGS_TO, 'Asesor', 'Asesor'),
            'estadoProceso' => array(self::BELONGS_TO, 'EstadoVenta', 'Estado_proceso'),
            'tipoContacto' => array(self::BELONGS_TO, 'TipoContacto', 'Tipo_contacto'),
            'tipoCliente' => array(self::BELONGS_TO, 'TipoCliente', 'Tipo_cliente'),
            'historicoGestions' => array(self::HAS_MANY, 'HistoricoGestionOp', 'ID_Contacto'),
            'idEstadoweb' => array(self::BELONGS_TO, 'EstadosWeb', 'Id_Estadoweb')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Telemercaderista' => 'Telemercaderista',
            'Tipo_cliente' => 'Tipo Cliente',
            'Razon_social' => 'Razon Social',
            'Nit' => 'Nit',
            'Nombre_completo' => 'Con Quien se Habla',
            'Direccion' => 'Direccion',
            'Telefono' => 'Telefono',
            'Celular' => 'Celular',
            'Email' => 'Email',
            'Ciudad' => 'Ciudad',
            'Agencia' => 'Agencia',
            'Asesor' => 'Asesor',
            'Barrio' => 'Barrio',
            'Medio_contacto' => 'Medio Contacto',
            'Documento' => 'Documento',
            'Observaciones' => 'Observaciones',
            'Estado' => 'Estado',
            'Fecha_creacion' => 'Fecha Creacion',
            'Hora_creacion' => 'Hora Creacion',
            'Actividad' => 'Actividad',
            'Bloqueado' => 'Bloqueado',
            'Estado_proceso' => 'Estado Proceso',
            'Tipo_contacto' => 'Tipo Contacto',
            'TipoDescartado' => 'Tipo Descartado',
            'searchasesor' => 'Funcionario',
            'searchagencia' => 'Agencia',
            'searchciudad' => 'Ciudad',
            'searchtipocontacto' => 'Tipo Contacto',
            'Id_Estadoweb' => 'Id Estadoweb'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.ID', $this->ID);
        $criteria->compare('Telemercaderista', $this->Telemercaderista);
        $criteria->compare('Tipo_cliente', $this->Tipo_cliente);
        $criteria->compare('Razon_social', $this->Razon_social, true);
        $criteria->compare('t.Nit', $this->Nit, true);
        $criteria->compare('Nombre_completo', $this->Nombre_completo, true);
        $criteria->compare('t.Direccion', $this->Direccion, true);
        $criteria->compare('t.Telefono', $this->Telefono, true);
        $criteria->compare('t.Celular', $this->Celular, true);
        $criteria->compare('t.Email', $this->Email, true);
        $criteria->compare('t.Ciudad', $this->Ciudad);
        $criteria->compare('Agencia', $this->Agencia);
        $criteria->compare('Asesor', $this->Asesor);
        $criteria->compare('Barrio', $this->Barrio, true);
        $criteria->compare('Medio_contacto', $this->Medio_contacto, true);
        $criteria->compare('Documento', $this->Documento, true);
        $criteria->compare('Observaciones', $this->Observaciones, true);
        $criteria->compare('t.Estado', $this->Estado);
        $criteria->compare('t.Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Hora_creacion', $this->Hora_creacion, true);
        $criteria->compare('Actividad', $this->Actividad, true);
        $criteria->compare('Bloqueado', $this->Bloqueado);
        $criteria->compare('Estado_proceso', $this->Estado_proceso);
        $criteria->compare('Tipo_contacto', $this->Tipo_contacto);
        $criteria->compare('TipoDescartado', $this->TipoDescartado);
        $criteria->compare('asesor.Nombre', $this->searchasesor, true);
        $criteria->compare('agencia.Nombre', $this->searchagencia, true);
        $criteria->compare('ciudad.Nombre', $this->searchciudad, true); 
        
        $criteria->with = array(
            'asesor' => array('Nombre LIKE "%' . $this->searchasesor . '%"'),
            'agencia' => array('Nombre LIKE "%' . $this->searchagencia . '%"'),
            'tipoContacto' => array('Descripcion LIKE "%' . $this->searchtipocontacto . '%"'),
            'ciudad' => array('Nombre LIKE "%' . $this->searchciudad . '%"'),
        );
                
        $criteria->addCondition('Estado_proceso = ' . $this->getEstado());
        $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario'));
        
        $criteria->order = 't.ID DESC';
        
        return new CActiveDataProvider(
                $this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10
            )
                )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Oportunidad the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function getEstado() {
        return $this->EstadoProceso;
    }

    public function setEstado($estado) {
        $this->EstadoProceso = $estado;
    }

    public function getCiudades() {
        $ciudades = Yii::app()->db->createCommand()
                ->setFetchMode(PDO::FETCH_OBJ)
                ->select('c.ID as ID, c.Nombre as Nombre, d.Nombre as Dpto')
                ->from('Ciudad c,Departamento d')
                ->where("d.ID=c.Departamento")
                ->queryAll();
        return CHtml::listData($ciudades, "ID", "Nombre", 'Dpto');
    }

    public function getBarrios() {
        $barrios = Barrios::model()->findAll();
        $data = CHtml::listData($barrios, "Nombre", "Nombre");
        return $data;
    }

    public function getActividades() {
        $activs = Actividades::model()->findAll();
        return CHtml::listData($activs, "ID", "Nombre");
    }

    public function getTipoCliente() {
        $tcli = TipoCliente::model()->findAll();
        return CHtml::listData($tcli, "ID", "Nombre");
    }

    public function getMedioContacto() {
        $mcto = MediosContacto::model()->findAllByAttributes(array(
            "Estado" => 1
        ));
        return CHtml::listData($mcto, "Nombre", "Nombre");
    }

    public function getCiudad() {
        $ciudad = Ciudad::model()->findByPk($this->Ciudad);
        if ($ciudad != null) {
            return $ciudad->Nombre;
        }
        return "";
    }

    public function getServicios() {
        $servicios = Servicios::model()->findAllByAttributes(array(
            "Tipo" => 1,
            "Estado" => 1
        ));
        return $servicios;
    }

    public function getOpcion($service = 0, $type = "") {
        if (!$this->isNewRecord) {
            if ($service > 0 && $type != "") {
                $opcion = ServicioInteresOportunidad::model()->findByAttributes(array(
                    "ID_Contacto" => $this->ID,
                    "ID_Servicio" => $service
                ));
                if ($opcion != null) {
                    switch ($type) {
                        case "Estado":
                            return $opcion->Estado;
                            break;
                        case "Equipos":
                            return $opcion->Equipos;
                            break;
                        case "ID":
                            return $opcion->ID;
                            break;
                        default:
                            return false;
                            break;
                    }
                }
            }
        }
        return false;
    }

    public function setOption($service = 0, $status = 0, $eq = 0, &$id = null) {
        if (!$this->isNewRecord) {
            if ($service > 0) {
                $opcion = ServicioInteresOportunidad::model()->findByAttributes(array(
                    "ID_Contacto" => $this->ID,
                    "ID_Servicio" => $service
                ));
                if ($opcion == null) {
                    $opcion = new ServicioInteresOportunidad;
                    $opcion->ID_Contacto = $this->ID;
                    $opcion->ID_Servicio = $service;
                }
                $opcion->Estado = $status;
                $opcion->Equipos = $eq;
                if ($opcion->save()) {
                    $id = $opcion->ID;
                }
                return true;
            }
        }
        return false;
    }

    public function getAntiguedad() {
        $fechaHora = strtotime($this->Fecha_creacion . " " . $this->Hora_creacion);
        return $this->th($fechaHora);
    }

    function th($time) {

        $time = time() - $time; // to get the time since that moment

        $tokens = array(
            31536000 => 'año',
            2592000 => 'mes',
            86400 => 'dia',
            3600 => 'hora',
            60 => 'minuto',
            1 => 'segundo'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit)
                continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? ($text == "mes" ? "e" : '') . 's' : '');
        }
    }
    
    /**
     * Retorna las Ciudades Predeterminadas
     */
    public function getActividadeconomica() {
        return CHtml::listData(Actividades::model()->findAll(array('order' => 'Nombre')), 'Nombre', 'Nombre');
    }
    
    public function getTotalRegistros() {
        return Oportunidad::model()->count('Telemercaderista=:id AND Estado_proceso = 20 ', array(':id' => Yii::app()->user->getState('id_usuario')));
    }
    
    public function alertasAgendados() {
        $criteria = new CDbCriteria();
        $criteria->addCondition(' ID_Asesor = ' . Yii::app()->user->getState('id_usuario') . ' AND Fecha = "' . date('Y-m-d') . '" AND ID_Accion = 2 ');
        $criteria->order = 'Hora ASC';
        return AgendadosOp::model()->findAll($criteria);
    }
    
    public function coincidencia($attribute) {

        $error = "";
        if ($attribute != "") :
            $criteria = new CDbCriteria();
            $criteria->compare("Razon_social", $attribute, false, "OR");
            $criteria->compare("Direccion", $attribute, false, "OR");
            $criteria->compare("Email", $attribute, false, "OR");
            $criteria->compare("Telefono", $attribute, false, "OR");
            $criteria->compare("Celular", $attribute, false, "OR");
            $oportunidades = Oportunidad::model()->findAll($criteria);

            if (count($oportunidades) > 1) :
                $arr_aux = array();
                foreach ($oportunidades as $cl) :
                    $arr_aux[] = $cl->ID;
                endforeach;
                $error .= "<tr><td>" . $this->getAttributeLabel($attribute) . "</td><td style='color:red; text-align:center'><a href='#' onclick='verDetalle(" . json_encode($arr_aux) . ", \"Contacto\")'><span class='badge'>" . count($oportunidades) . "</span></a></td><td>OPORTUNIDAD</td><td>" . $this->$attribute . "</td></tr>";
            endif;
            
            if (!empty($error)) :
                return $error;
            endif;
        endif;
    }
    
    /**
     * Exporta a Excel los Registros del Mes Actual
     * @param type $fecha1
     * @param type $fecha2
     * @return \CActiveDataProvider
     */
    public function getExcelConvert($fecha1, $fecha2, $excepcion = true)
    {
        if(!$excepcion) :
            return Yii::app()->db->createCommand()
                ->select('co.ID, co.Razon_social, co.Email, co.Medio_contacto, ci.Nombre as Ciudad, ev.Nombre as Estado_proceso,  co.Fecha_creacion, tc.Descripcion as Tipo_contacto, co.TipoDescartado, co.Horizontal as Servicio, co.Falencias as Tipo, a.Nombre as Asesor, aa.Nombre as Comercial, co.Fecha_Ultgestion as Fecha_gestion, co.Observaciones as Comentarios, es.Descripcion as EstadoWeb, co.Telefono, co.Celular, co.Direccion')
                ->from('Oportunidad co, Ciudad ci, Tipo_contacto tc, Estado_venta ev, Asesor a, Asesor aa, Estados_web es')
                ->where('es.ID = co.Id_Estadoweb AND co.Fecha_creacion BETWEEN :fecha AND :fecha2 AND ci.ID = co.Ciudad AND tc.ID = co.Tipo_contacto AND ev.ID = co.Estado_proceso AND a.ID = co.Asesor AND aa.ID = co.Telemercaderista',
                        array(':fecha' => $fecha1, ':fecha2' => $fecha2))
                ->order('co.Fecha_creacion ASC')
                ->queryAll();
        else :
            return Yii::app()->db->createCommand()
                ->select('co.ID, co.Razon_social, co.Email, co.Medio_contacto, ci.Nombre as Ciudad, ev.Nombre as Estado_proceso,  co.Fecha_creacion, tc.Descripcion as Tipo_contacto, co.TipoDescartado, co.Horizontal as Servicio, co.Falencias as Tipo, a.Nombre as Asesor, aa.Nombre as Comercial, co.Fecha_Ultgestion as Fecha_gestion, co.Observaciones as Comentarios, es.Descripcion as EstadoWeb, co.Telefono, co.Celular, co.Direccion')
                ->from('Oportunidad co, Ciudad ci, Tipo_contacto tc, Estado_venta ev, Asesor a, Asesor aa, Estados_web es')
                ->where('es.ID = co.Id_Estadoweb AND co.Fecha_creacion BETWEEN :fecha AND :fecha2 AND ci.ID = co.Ciudad AND tc.ID = co.Tipo_contacto AND ev.ID = co.Estado_proceso AND co.Telemercaderista = :comercial AND a.ID = co.Asesor AND aa.ID = co.Telemercaderista',
                        array(':fecha' => $fecha1, ':fecha2' => $fecha2, ':comercial' => Yii::app()->user->getState('id_usuario')))
                ->order('co.Fecha_creacion ASC')
                ->queryAll();
        endif;
    }
}
