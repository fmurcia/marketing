<?php

/**
 * This is the model class for table "Documentos".
 *
 * The followings are the available columns in table 'Documentos':
 * @property integer $ID
 * @property string $Hora
 * @property integer $ID_Tipodoc
 * @property integer $ID_cliente
 * @property string $Ruta
 * @property string $Titulo
 * @property string $Fecha
 * @property string $Clasificacion
 * @property integer $Paginas
 * @property string $Doc_pdf
 * @property string $Doc_4D
 * @property integer $Creado_por
 * @property integer $Enviado_4D
 * @property string $Observaciones
 * @property string $Thumbnail
 * @property string $Preview
 *
 * The followings are the available model relations:
 * @property TipoDocumento $iDTipodoc
 */
class Documentos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Documentos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Doc_pdf, Doc_4D, Creado_por, Enviado_4D, Observaciones, Thumbnail, Preview', 'required'),
			array('ID_Tipodoc, ID_cliente, Paginas, Creado_por, Enviado_4D', 'numerical', 'integerOnly'=>true),
			array('Ruta, Doc_pdf, Thumbnail, Preview', 'length', 'max'=>200),
			array('Titulo, Clasificacion', 'length', 'max'=>100),
			array('Doc_4D', 'length', 'max'=>50),
			array('Observaciones', 'length', 'max'=>1000),
			array('Hora, Fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Hora, ID_Tipodoc, ID_cliente, Ruta, Titulo, Fecha, Clasificacion, Paginas, Doc_pdf, Doc_4D, Creado_por, Enviado_4D, Observaciones, Thumbnail, Preview', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDTipodoc' => array(self::BELONGS_TO, 'TipoDocumento', 'ID_Tipodoc'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Hora' => 'Hora',
			'ID_Tipodoc' => 'Id Tipodoc',
			'ID_cliente' => 'Id Cliente',
			'Ruta' => 'Ruta',
			'Titulo' => 'Titulo',
			'Fecha' => 'Fecha',
			'Clasificacion' => 'Clasificacion',
			'Paginas' => 'Paginas',
			'Doc_pdf' => 'Doc Pdf',
			'Doc_4D' => 'Doc 4 D',
			'Creado_por' => 'Creado Por',
			'Enviado_4D' => 'Enviado 4 D',
			'Observaciones' => 'Observaciones',
			'Thumbnail' => 'Thumbnail',
			'Preview' => 'Preview',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Hora',$this->Hora,true);
		$criteria->compare('ID_Tipodoc',$this->ID_Tipodoc);
		$criteria->compare('ID_cliente',$this->ID_cliente);
		$criteria->compare('Ruta',$this->Ruta,true);
		$criteria->compare('Titulo',$this->Titulo,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Clasificacion',$this->Clasificacion,true);
		$criteria->compare('Paginas',$this->Paginas);
		$criteria->compare('Doc_pdf',$this->Doc_pdf,true);
		$criteria->compare('Doc_4D',$this->Doc_4D,true);
		$criteria->compare('Creado_por',$this->Creado_por);
		$criteria->compare('Enviado_4D',$this->Enviado_4D);
		$criteria->compare('Observaciones',$this->Observaciones,true);
		$criteria->compare('Thumbnail',$this->Thumbnail,true);
		$criteria->compare('Preview',$this->Preview,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Documentos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
