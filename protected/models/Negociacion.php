<?php

/**
 * This is the model class for table "Negociacion".
 *
 * The followings are the available columns in table 'Negociacion':
 * @property integer $ID
 * @property string $Codigo
 * @property string $Nombre
 * @property string $Prefijo
 * @property integer $Comision_equipos
 * @property integer $Comision_servicio
 * @property integer $Recurrente
 * @property integer $Obsequio
 * @property integer $Forzar_plazo
 * @property integer $Prioridad
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 *
 * The followings are the available model relations:
 * @property ComisionGrupo[] $comisionGrupos
 * @property Cotizacion[] $cotizacions
 * @property DetalleCotizacion[] $detalleCotizacions
 * @property DetalleElemento[] $detalleElementos
 * @property ServicioOpcion[] $servicioOpcions
 */
class Negociacion extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Negociacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Comision_equipos, Comision_servicio, Recurrente, Obsequio, Forzar_plazo, Prioridad, Estado', 'numerical', 'integerOnly' => true),
            array('Codigo', 'length', 'max' => 30),
            array('Nombre', 'length', 'max' => 300),
            array('Prefijo', 'length', 'max' => 5),
            array('Fecha_inicial, Fecha_final', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Codigo, Nombre, Prefijo, Comision_equipos, Comision_servicio, Recurrente, Obsequio, Forzar_plazo, Prioridad, Estado, Fecha_inicial, Fecha_final', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'comisionGrupos' => array(self::HAS_MANY, 'ComisionGrupo', 'ID_Negociacion'),
            'cotizacions' => array(self::HAS_MANY, 'Cotizacion', 'ID_Negociacion'),
            'detalleCotizacions' => array(self::HAS_MANY, 'DetalleCotizacion', 'ID_Negociacion'),
            'detalleElementos' => array(self::HAS_MANY, 'DetalleElemento', 'ID_Negociacion'),
            'servicioOpcions' => array(self::HAS_MANY, 'ServicioOpcion', 'ID_Negociacion'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Codigo' => 'Codigo',
            'Nombre' => 'Nombre',
            'Prefijo' => 'Prefijo',
            'Comision_equipos' => 'Comision Equipos',
            'Comision_servicio' => 'Comision Servicio',
            'Recurrente' => 'Recurrente',
            'Obsequio' => 'Obsequio',
            'Forzar_plazo' => 'Forzar Plazo',
            'Prioridad' => 'Prioridad',
            'Estado' => 'Estado',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Prefijo', $this->Prefijo, true);
        $criteria->compare('Comision_equipos', $this->Comision_equipos);
        $criteria->compare('Comision_servicio', $this->Comision_servicio);
        $criteria->compare('Recurrente', $this->Recurrente);
        $criteria->compare('Obsequio', $this->Obsequio);
        $criteria->compare('Forzar_plazo', $this->Forzar_plazo);
        $criteria->compare('Prioridad', $this->Prioridad);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Negociacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNegociaciones() {
        return Yii::app()->db->createCommand()
                        ->setFetchMode(PDO::FETCH_OBJ)
                        ->select('n.ID, n.Nombre')
                        ->from('Negociacion n')
                        ->order("n.Prioridad ASC")
                        ->queryAll();
    }

}
