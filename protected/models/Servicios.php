<?php

/**
 * This is the model class for table "Servicios".
 *
 * The followings are the available columns in table 'Servicios':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $Contenido
 * @property string $Imagen
 * @property integer $Tipo
 * @property integer $Usa_mtrans
 * @property integer $Codigo_4D
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 * @property integer $Multiple
 * @property string $Formato
 * @property integer $Obligatorio
 * @property string $Descr_campo
 *
 * The followings are the available model relations:
 * @property Cotizacion[] $cotizacions
 * @property Elemento[] $elementos
 * @property AsesorServicio[] $asesorServicios
 * @property ServicioElemento[] $servicioElementos
 * @property ServicioFormapago[] $servicioFormapagos
 * @property ServicioOpcion[] $servicioOpcions
 * @property ServicioPrueba[] $servicioPruebas
 * @property ServicioServicio[] $servicioServicios
 * @property ServicioServicio[] $servicioServicios1
 */
class Servicios extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Servicios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tipo, Usa_mtrans, Codigo_4D, Estado, Multiple, Obligatorio', 'numerical', 'integerOnly'=>true),
			array('Nombre', 'length', 'max'=>200),
			array('Imagen', 'length', 'max'=>50),
			array('Formato, Descr_campo', 'length', 'max'=>100),
			array('Descripcion, Contenido, Fecha_inicial, Fecha_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, Contenido, Imagen, Tipo, Usa_mtrans, Codigo_4D, Estado, Fecha_inicial, Fecha_final, Multiple, Formato, Obligatorio, Descr_campo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cotizacions' => array(self::HAS_MANY, 'Cotizacion', 'Servicio'),
			'elementos' => array(self::HAS_MANY, 'Elemento', 'Servicio'),
			'asesorServicios' => array(self::HAS_MANY, 'AsesorServicio', 'Servicio'),
			'servicioElementos' => array(self::HAS_MANY, 'ServicioElemento', 'ID_Servicio'),
			'servicioFormapagos' => array(self::HAS_MANY, 'ServicioFormapago', 'ID_Servicio'),
			'servicioOpcions' => array(self::HAS_MANY, 'ServicioOpcion', 'ID_Servicio'),
			'servicioPruebas' => array(self::HAS_MANY, 'ServicioPrueba', 'ID_Servicio'),
			'servicioServicios' => array(self::HAS_MANY, 'ServicioServicio', 'ID_ServicioH'),
			'servicioServicios1' => array(self::HAS_MANY, 'ServicioServicio', 'ID_ServicioP'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripcion',
			'Contenido' => 'Contenido',
			'Imagen' => 'Imagen',
			'Tipo' => 'Tipo',
			'Usa_mtrans' => 'Usa Mtrans',
			'Codigo_4D' => 'Codigo 4 D',
			'Estado' => 'Estado',
			'Fecha_inicial' => 'Fecha Inicial',
			'Fecha_final' => 'Fecha Final',
			'Multiple' => 'Multiple',
			'Formato' => 'Formato',
			'Obligatorio' => 'Obligatorio',
			'Descr_campo' => 'Descr Campo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('Contenido',$this->Contenido,true);
		$criteria->compare('Imagen',$this->Imagen,true);
		$criteria->compare('Tipo',$this->Tipo);
		$criteria->compare('Usa_mtrans',$this->Usa_mtrans);
		$criteria->compare('Codigo_4D',$this->Codigo_4D);
		$criteria->compare('Estado',$this->Estado);
		$criteria->compare('Fecha_inicial',$this->Fecha_inicial,true);
		$criteria->compare('Fecha_final',$this->Fecha_final,true);
		$criteria->compare('Multiple',$this->Multiple);
		$criteria->compare('Formato',$this->Formato,true);
		$criteria->compare('Obligatorio',$this->Obligatorio);
		$criteria->compare('Descr_campo',$this->Descr_campo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Servicios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
