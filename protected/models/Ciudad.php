<?php

/**
 * This is the model class for table "Ciudad".
 *
 * The followings are the available columns in table 'Ciudad':
 * @property integer $ID
 * @property string $Nombre
 * @property integer $Departamento
 * @property string $NomDep
 * @property double $Latitud
 * @property double $Longitud
 * @property integer $Principal
 *
 * The followings are the available model relations:
 * @property Calendario[] $calendarios
 * @property Departamento $departamento
 * @property Cliente[] $clientes
 * @property Contacto[] $contactos
 * @property AgenciaCiudad[] $agenciaCiudads
 * @property RegionalCiudad[] $regionalCiudads
 */
class Ciudad extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Ciudad';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID, Departamento', 'required'),
            array('ID, Departamento, Principal', 'numerical', 'integerOnly' => true),
            array('Latitud, Longitud', 'numerical'),
            array('Nombre', 'length', 'max' => 500),
            array('NomDep', 'length', 'max' => 200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Nombre, Departamento, NomDep, Latitud, Longitud, Principal', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'calendarios' => array(self::HAS_MANY, 'Calendario', 'Ciudad'),
            'departamento' => array(self::BELONGS_TO, 'Departamento', 'Departamento'),
            'clientes' => array(self::HAS_MANY, 'Cliente', 'Ciudad'),
            'contactos' => array(self::HAS_MANY, 'Contacto', 'Ciudad'),
            'agenciaCiudads' => array(self::HAS_MANY, 'AgenciaCiudad', 'Ciudad'),
            'regionalCiudads' => array(self::HAS_MANY, 'RegionalCiudad', 'ID_Ciudad'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Departamento' => 'Departamento',
            'NomDep' => 'Nom Dep',
            'Latitud' => 'Latitud',
            'Longitud' => 'Longitud',
            'Principal' => 'Principal',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Departamento', $this->Departamento);
        $criteria->compare('NomDep', $this->NomDep, true);
        $criteria->compare('Latitud', $this->Latitud);
        $criteria->compare('Longitud', $this->Longitud);
        $criteria->compare('Principal', $this->Principal);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Ciudad the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
}