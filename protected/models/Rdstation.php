<?php

/**
 * This is the model class for table "rdstation".
 *
 * The followings are the available columns in table 'rdstation':
 * @property integer $idrdstation
 * @property string $access_token
 * @property string $expires_in
 * @property string $refresh_token
 * @property string $client_id
 * @property string $client_secret
 * @property string $code
 */
class Rdstation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rdstation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('expires_in, client_id, client_secret, code', 'length', 'max'=>45),
			array('refresh_token', 'length', 'max'=>255),
			array('access_token', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idrdstation, access_token, expires_in, refresh_token, client_id, client_secret, code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idrdstation' => 'Idrdstation',
			'access_token' => 'Access Token',
			'expires_in' => 'Expires In',
			'refresh_token' => 'Refresh Token',
			'client_id' => 'Client',
			'client_secret' => 'Client Secret',
			'code' => 'Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idrdstation',$this->idrdstation);
		$criteria->compare('access_token',$this->access_token,true);
		$criteria->compare('expires_in',$this->expires_in,true);
		$criteria->compare('refresh_token',$this->refresh_token,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('client_secret',$this->client_secret,true);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rdstation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
