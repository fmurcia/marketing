<?php

/**
 * This is the model class for table "Gestion_comercial".
 *
 * The followings are the available columns in table 'Gestion_comercial':
 * @property double $ID
 * @property integer $ID_Contacto
 * @property integer $Contrato
 * @property integer $Servicio
 * @property double $Valor_equipos
 * @property double $Valor_servicio
 * @property double $Valor_instalacion
 * @property string $Decision_compra
 * @property string $Comentarios
 * @property string $Fecha_ingreso
 * @property string $Fecha_visita
 * @property integer $Descartar
 * @property integer $Posicion
 *
 * The followings are the available model relations:
 * @property Contacto $iDContacto
 * @property Servicios $servicio
 */
class GestionComercial extends CActiveRecord {

    public $decision;

    public function getEstado() {
        return $this->decision;
    }

    public function setEstado($opcion) {
        $this->decision = $opcion;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Gestion_comercial';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Contacto, Servicio', 'required'),
            array('ID_Contacto, Contrato, Servicio, Descartar, Posicion, ID_Asesor', 'numerical', 'integerOnly' => true),
            array('Valor_equipos, Valor_servicio, Valor_instalacion', 'numerical'),
            array('Decision_compra', 'length', 'max' => 5),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Contacto, Contrato, Servicio, Valor_equipos, Valor_servicio, Valor_instalacion, Decision_compra, Comentarios, Fecha_ingreso, Fecha_visita, Descartar, Posicion, ID_Asesor', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDContacto' => array(self::BELONGS_TO, 'Contacto', 'ID_Contacto'),
            'servicio' => array(self::BELONGS_TO, 'Servicios', 'Servicio'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Contacto' => 'ID Contacto',
            'Contrato' => 'Contrato',
            'Servicio' => 'Servicio',
            'Valor_equipos' => 'Valor Equipos',
            'Valor_servicio' => 'Valor Servicio',
            'Valor_instalacion' => 'Valor Instalacion',
            'Decision_compra' => 'Decision Compra',
            'Comentarios' => 'Comentarios',
            'Fecha_ingreso' => 'Fecha Ingreso',
            'Fecha_visita' => 'Fecha Visita',
            'Descartar' => 'Estado',
            'Posicion' => 'Posicion',
            'ID_Asesor' => 'Id Asesor',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('Contrato', $this->Contrato);
        $criteria->compare('Servicio', $this->Servicio);
        $criteria->compare('Valor_equipos', $this->Valor_equipos);
        $criteria->compare('Valor_servicio', $this->Valor_servicio);
        $criteria->compare('Valor_instalacion', $this->Valor_instalacion);
        $criteria->compare('Decision_compra', $this->Decision_compra, true);
        $criteria->compare('Comentarios', $this->Comentarios, true);
        $criteria->compare('Fecha_ingreso', $this->Fecha_ingreso, true);
        $criteria->compare('Fecha_visita', $this->Fecha_visita, true);
        $criteria->compare('Descartar', $this->Descartar);
        $criteria->compare('Posicion', $this->Posicion);
        $criteria->compare('ID_Asesor',$this->ID_Asesor);

        $criteria->addCondition('Decision_compra = "'.$this->getEstado().'" AND ID_Asesor = "'.Yii::app()->user->getState('id_usuario').'"');

        return new CActiveDataProvider(
                $this, array(
            'criteria' => $criteria,
            "pagination" => array(
                'pageSize' => 10
            )
                )
        );
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return GestionComercial the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function totalCotizados($opcion){
        $criteria = new CDbCriteria;
        if($opcion != 't') :
            $criteria->addCondition('Decision_compra = "'.$opcion.'" AND ID_Asesor = "'.Yii::app()->user->getState('id_usuario').'"');
        endif;
        return GestionComercial::model()->count($criteria);
    }
}
