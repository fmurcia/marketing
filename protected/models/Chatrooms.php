<?php

/**
 * This is the model class for table "Chatrooms".
 *
 * The followings are the available columns in table 'Chatrooms':
 * @property integer $ID_Chat_Room
 * @property integer $Area
 * @property string $Nombre
 * @property string $Fecha
 * @property string $Hora
 *
 * The followings are the available model relations:
 * @property Messages[] $messages
 */
class Chatrooms extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Chatrooms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_Chat_Room, Nombre, Fecha, Hora', 'required'),
			array('ID_Chat_Room, Area', 'numerical', 'integerOnly'=>true),
			array('Nombre', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID_Chat_Room, Area, Nombre, Fecha, Hora', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'Messages', 'ID_Chat_Room'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_Chat_Room' => 'Id Chat Room',
			'Area' => 'Area',
			'Nombre' => 'Nombre',
			'Fecha' => 'Fecha',
			'Hora' => 'Hora',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_Chat_Room',$this->ID_Chat_Room);
		$criteria->compare('Area',$this->Area);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Hora',$this->Hora,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Chatrooms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getChatRooms()
        {
            return $this->model()->with(
                    array(
                        'messages' => 
                        array(
                                'order'=>'created_at DESC'
                            )
                        )
                    )->findAll('t.ID_Chat_Room = 239 AND Area = 10');
        }
}
