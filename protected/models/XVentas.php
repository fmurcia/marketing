<?php

/**
 * This is the model class for table "x_ventas".
 *
 * The followings are the available columns in table 'x_ventas':
 * @property integer $IDCliente
 * @property integer $IDClienteVenta
 * @property integer $IDDetalle
 * @property string $FechaCreacionDetalle
 * @property integer $ValorServicioDetalle
 * @property string $EstadoDetalle
 * @property integer $NegociacionDetalle
 * @property string $CodigoServicioDetalle
 * @property integer $IDRegionalCliente
 * @property integer $AsesorCliente
 * @property integer $AgenciaCliente
 * @property integer $CierreClienteVenta
 * @property integer $VersionClienteVenta
 * @property integer $Codigo4DDetalleServicio
 * @property integer $ContratoCliente
 * @property integer $Enviado4DDetalle
 * @property string $DescripcionDetalleServicio
 * @property string $Regional
 * @property string $Agencia
 */
class XVentas extends CActiveRecord
{
        public $Total;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'x_ventas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
                return array(
			array('CierreClienteVenta, Codigo4DDetalleServicio, Regional', 'required'),
			array('IDCliente, IDClienteVenta, IDDetalle, ValorServicioDetalle, EstadoDetalle, NegociacionDetalle, CodigoServicioDetalle, IDRegionalCliente, AsesorCliente, AgenciaCliente, CierreClienteVenta, VersionClienteVenta, Codigo4DDetalleServicio, ContratoCliente, Enviado4DDetalle', 'numerical', 'integerOnly'=>true),
			array('DescripcionDetalleServicio', 'length', 'max'=>300),
			array('Regional', 'length', 'max'=>50),
			array('Agencia', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('IDCliente, IDClienteVenta, IDDetalle, ValorServicioDetalle, EstadoDetalle, NegociacionDetalle, CodigoServicioDetalle, IDRegionalCliente, AsesorCliente, AgenciaCliente, CierreClienteVenta, VersionClienteVenta, Codigo4DDetalleServicio, ContratoCliente, Enviado4DDetalle, DescripcionDetalleServicio, Regional, Agencia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'IDCliente' => 'Idcliente',
			'IDClienteVenta' => 'Idcliente Venta',
			'IDDetalle' => 'Iddetalle',
			'FechaCreacionDetalle' => 'Fecha Creacion Detalle',
			'ValorServicioDetalle' => 'Valor Servicio Detalle',
			'EstadoDetalle' => 'Estado Detalle',
			'NegociacionDetalle' => 'Negociacion Detalle',
			'CodigoServicioDetalle' => 'Codigo Servicio Detalle',
			'IDRegionalCliente' => 'Idregional Cliente',
			'AsesorCliente' => 'Asesor Cliente',
			'AgenciaCliente' => 'Agencia Cliente',
			'CierreClienteVenta' => 'Cierre Cliente Venta',
			'VersionClienteVenta' => 'Version Cliente Venta',
			'Codigo4DDetalleServicio' => 'Codigo 4D Detalle Servicio',
                        'ContratoCliente' => 'Contrato Cliente',
			'Enviado4DDetalle' => 'Enviado4 Ddetalle',
                        'DescripcionDetalleServicio' => 'Descripcion Detalle Servicio',
			'Regional' => 'Regional',
			'Agencia' => 'Agencia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('IDCliente',$this->IDCliente);
		$criteria->compare('IDClienteVenta',$this->IDClienteVenta);
		$criteria->compare('IDDetalle',$this->IDDetalle);
		$criteria->compare('FechaCreacionDetalle',$this->FechaCreacionDetalle,true);
		$criteria->compare('ValorServicioDetalle',$this->ValorServicioDetalle);
		$criteria->compare('EstadoDetalle',$this->EstadoDetalle,true);
		$criteria->compare('NegociacionDetalle',$this->NegociacionDetalle);
		$criteria->compare('CodigoServicioDetalle',$this->CodigoServicioDetalle,true);
		$criteria->compare('IDRegionalCliente',$this->IDRegionalCliente);
		$criteria->compare('AsesorCliente',$this->AsesorCliente);
		$criteria->compare('AgenciaCliente',$this->AgenciaCliente);
		$criteria->compare('CierreClienteVenta',$this->CierreClienteVenta);
                $criteria->compare('ContratoCliente',$this->ContratoCliente);
		$criteria->compare('Enviado4DDetalle',$this->Enviado4DDetalle);
                $criteria->compare('DescripcionDetalleServicio',$this->DescripcionDetalleServicio,true);
		$criteria->compare('Regional',$this->Regional,true);
		$criteria->compare('Agencia',$this->Agencia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return XVentas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
