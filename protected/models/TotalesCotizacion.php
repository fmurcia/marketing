<?php

/**
 * This is the model class for table "totales_cotizacion".
 *
 * The followings are the available columns in table 'totales_cotizacion':
 * @property integer $ID
 * @property integer $ID_Cotizacion
 * @property integer $ID_Servicio
 * @property integer $ID_Negociacion
 * @property string $Clasificacion
 * @property integer $Plazo
 * @property double $Precio_proveedor
 * @property double $Precio_CIF
 * @property double $Precio_venta
 * @property integer $Descuento
 * @property double $Valor_cuota
 * @property integer $Tipo
 * @property integer $Sumar
 * @property integer $Obsequio
 *
 * The followings are the available model relations:
 * @property Cotizacion $iDCotizacion
 * @property Negociacion $iDNegociacion
 * @property Servicios $iDServicio
 */
class TotalesCotizacion extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Totales_cotizacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Cotizacion, ID_Servicio, ID_Negociacion, Plazo, Descuento, Tipo, Sumar, Obsequio', 'numerical', 'integerOnly' => true),
            array('Precio_proveedor, Precio_CIF, Precio_venta, Valor_cuota', 'numerical'),
            array('Clasificacion', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Cotizacion, ID_Servicio, ID_Negociacion, Clasificacion, Plazo, Precio_proveedor, Precio_CIF, Precio_venta, Descuento, Valor_cuota, Tipo, Sumar, Obsequio', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDCotizacion' => array(self::BELONGS_TO, 'Cotizacion', 'ID_Cotizacion'),
            'iDNegociacion' => array(self::BELONGS_TO, 'Negociacion', 'ID_Negociacion'),
            'iDServicio' => array(self::BELONGS_TO, 'Servicios', 'ID_Servicio'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Cotizacion' => 'Id Cotizacion',
            'ID_Servicio' => 'Id Servicio',
            'ID_Negociacion' => 'Id Negociacion',
            'Clasificacion' => 'Clasificacion',
            'Plazo' => 'Plazo',
            'Precio_proveedor' => 'Precio Proveedor',
            'Precio_CIF' => 'Precio Cif',
            'Precio_venta' => 'Precio Venta',
            'Descuento' => 'Descuento',
            'Valor_cuota' => 'Valor Cuota',
            'Tipo' => 'Tipo',
            'Sumar' => 'Sumar',
            'Obsequio' => 'Obsequio',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Cotizacion', $this->ID_Cotizacion);
        $criteria->compare('ID_Servicio', $this->ID_Servicio);
        $criteria->compare('ID_Negociacion', $this->ID_Negociacion);
        $criteria->compare('Clasificacion', $this->Clasificacion, true);
        $criteria->compare('Plazo', $this->Plazo);
        $criteria->compare('Precio_proveedor', $this->Precio_proveedor);
        $criteria->compare('Precio_CIF', $this->Precio_CIF);
        $criteria->compare('Precio_venta', $this->Precio_venta);
        $criteria->compare('Descuento', $this->Descuento);
        $criteria->compare('Valor_cuota', $this->Valor_cuota);
        $criteria->compare('Tipo', $this->Tipo);
        $criteria->compare('Sumar', $this->Sumar);
        $criteria->compare('Obsequio', $this->Obsequio);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TotalesCotizacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getDescuento($cotiza, $un, $nego, $clas, $plazo) {
        $total = $this->findAllByAttributes(array(
            "ID_Cotizacion" => $cotiza,
            "ID_Servicio" => $un,
            "ID_Negociacion" => $nego,
            "Clasificacion" => $clas,
            "Plazo" => $plazo
        ));
        if (!empty($total)) {
            return $total[0]->Descuento;
        }
        return 0;
    }

}
