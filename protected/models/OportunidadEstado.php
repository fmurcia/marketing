<?php

/**
 * This is the model class for table "Oportunidad_estado".
 *
 * The followings are the available columns in table 'Oportunidad_estado':
 * @property double $ID
 * @property integer $ID_Oportunidad
 * @property integer $ID_Asesor
 * @property integer $ID_Estadoproceso
 * @property integer $ID_Estadoweb
 * @property string $Fecha
 *
 * The followings are the available model relations:
 * @property Asesor $iDAsesor
 * @property EstadosWeb $iDEstadoweb
 * @property Oportunidad $iDOportunidad
 * @property EstadoVenta $iDEstadoproceso
 */
class OportunidadEstado extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Oportunidad_estado';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Oportunidad, ID_Asesor, ID_Estadoproceso, ID_Estadoweb, Fecha', 'required'),
            array('ID_Oportunidad, ID_Asesor, ID_Estadoproceso, ID_Estadoweb', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Oportunidad, ID_Asesor, ID_Estadoproceso, ID_Estadoweb, Fecha', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
            'iDEstadoweb' => array(self::BELONGS_TO, 'EstadosWeb', 'ID_Estadoweb'),
            'iDOportunidad' => array(self::BELONGS_TO, 'Oportunidad', 'ID_Oportunidad'),
            'iDEstadoproceso' => array(self::BELONGS_TO, 'EstadoVenta', 'ID_Estadoproceso'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Oportunidad' => 'Id Oportunidad',
            'ID_Asesor' => 'Id Asesor',
            'ID_Estadoproceso' => 'Id Estadoproceso',
            'ID_Estadoweb' => 'Id Estadoweb',
            'Fecha' => 'Fecha',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Oportunidad', $this->ID_Oportunidad);
        $criteria->compare('ID_Asesor', $this->ID_Asesor);
        $criteria->compare('ID_Estadoproceso', $this->ID_Estadoproceso);
        $criteria->compare('ID_Estadoweb', $this->ID_Estadoweb);
        $criteria->compare('Fecha', $this->Fecha, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OportunidadEstado the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @param type $idcontacto
     * @param type $idestado
     */
    public function registro($idcontacto, $idestadoproceso, $idestadoweb) {
        $contacto = new OportunidadEstado();
        $contacto->ID_Asesor = Yii::app()->user->getState('id_usuario');
        $contacto->ID_Oportunidad = $idcontacto;
        $contacto->ID_Estadoproceso = $idestadoproceso;
        $contacto->ID_Estadoweb = $idestadoweb;
        $contacto->Fecha = date('Y-m-d H:i:s');
        $contacto->save();
    }

    /**
     * 
     * @param type $idcontacto
     */
    public function getEstadoWeb($idcontacto) {
        $criteria = new CDbCriteria();
        $criteria->select = 'ID_Estadoweb';
        $criteria->addCondition('ID_Oportunidad = ' . $idcontacto);
        $criteria->group = 'ID_Estadoweb';
        $criteria->order = 'Fecha ASC';
        return $this->model()->findAll($criteria);
    }

    public function getEstadoProceso($idcontacto) {
        $criteria = new CDbCriteria();
        $criteria->select = 'ID_Estadoweb';
        $criteria->addCondition('ID_Oportunidad = ' . $idcontacto);
        $criteria->order = 'Fecha DESC';
        return $this->model()->find($criteria);
    }
}