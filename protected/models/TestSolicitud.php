<?php

/**
 * This is the model class for table "Test_solicitud".
 *
 * The followings are the available columns in table 'Test_solicitud':
 * @property integer $ID
 * @property string $Zona
 * @property integer $Evento
 * @property integer $ID_Solicitud
 * @property integer $ID_Prueba
 * @property integer $ID_Medio
 * @property string $Fecha
 * @property string $Hora
 * @property integer $ID_Estado
 * @property integer $Verificacion
 * @property integer $Lote
 * @property double $Operador
 * @property double $Tecnico
 *
 * The followings are the available model relations:
 * @property Solicitud $iDSolicitud
 * @property Estados $iDEstado
 * @property TipoPrueba $evento
 * @property TipoAlarma $iDMedio
 */
class TestSolicitud extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Test_solicitud';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Zona, Evento, ID_Solicitud, ID_Prueba, ID_Medio, Fecha, Hora, Lote, Operador, Tecnico', 'required'),
			array('Evento, ID_Solicitud, ID_Prueba, ID_Medio, ID_Estado, Verificacion, Lote', 'numerical', 'integerOnly'=>true),
			array('Operador, Tecnico', 'numerical'),
			array('Zona', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Zona, Evento, ID_Solicitud, ID_Prueba, ID_Medio, Fecha, Hora, ID_Estado, Verificacion, Lote, Operador, Tecnico', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDSolicitud' => array(self::BELONGS_TO, 'Solicitud', 'ID_Solicitud'),
			'iDEstado' => array(self::BELONGS_TO, 'Estados', 'ID_Estado'),
			'evento' => array(self::BELONGS_TO, 'TipoPrueba', 'Evento'),
			'iDMedio' => array(self::BELONGS_TO, 'TipoAlarma', 'ID_Medio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Zona' => 'Zona',
			'Evento' => 'Evento',
			'ID_Solicitud' => 'Id Solicitud',
			'ID_Prueba' => 'Id Prueba',
			'ID_Medio' => 'Id Medio',
			'Fecha' => 'Fecha',
			'Hora' => 'Hora',
			'ID_Estado' => 'Id Estado',
			'Verificacion' => 'Verificacion',
			'Lote' => 'Lote',
			'Operador' => 'Operador',
			'Tecnico' => 'Tecnico',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Zona',$this->Zona,true);
		$criteria->compare('Evento',$this->Evento);
		$criteria->compare('ID_Solicitud',$this->ID_Solicitud);
		$criteria->compare('ID_Prueba',$this->ID_Prueba);
		$criteria->compare('ID_Medio',$this->ID_Medio);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Hora',$this->Hora,true);
		$criteria->compare('ID_Estado',$this->ID_Estado);
		$criteria->compare('Verificacion',$this->Verificacion);
		$criteria->compare('Lote',$this->Lote);
		$criteria->compare('Operador',$this->Operador);
		$criteria->compare('Tecnico',$this->Tecnico);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TestSolicitud the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
