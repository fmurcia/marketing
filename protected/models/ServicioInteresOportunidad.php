<?php

/**
 * This is the model class for table "servicio_interes_oportunidad".
 *
 * The followings are the available columns in table 'servicio_interes_oportunidad':
 * @property double $ID
 * @property integer $ID_Contacto
 * @property integer $ID_Servicio
 * @property integer $Equipos
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Servicios $iDServicio
 * @property Oportunidad $iDContacto
 */
class ServicioInteresOportunidad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'servicio_interes_oportunidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_Contacto, ID_Servicio, Equipos, Estado', 'required'),
			array('ID_Contacto, ID_Servicio, Equipos, Estado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, ID_Contacto, ID_Servicio, Equipos, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDServicio' => array(self::BELONGS_TO, 'Servicios', 'ID_Servicio'),
			'iDContacto' => array(self::BELONGS_TO, 'Oportunidad', 'ID_Contacto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_Contacto' => 'Id Contacto',
			'ID_Servicio' => 'Id Servicio',
			'Equipos' => 'Equipos',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_Contacto',$this->ID_Contacto);
		$criteria->compare('ID_Servicio',$this->ID_Servicio);
		$criteria->compare('Equipos',$this->Equipos);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ServicioInteresOportunidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
