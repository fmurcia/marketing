<?php

/**
 * This is the model class for table "Detalle_servicio".
 *
 * The followings are the available columns in table 'Detalle_servicio':
 * @property integer $ID
 * @property integer $ID_servicio
 * @property string $Codigo_servicio
 * @property string $Descripcion
 * @property integer $Valor_servicio
 * @property string $Fecha_creacion
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 * @property integer $Estado
 * @property integer $ID_serv4D
 * @property integer $Codigo4D
 * @property integer $Cierre
 * @property integer $Plazo
 * @property integer $Negociacion
 * @property integer $Contrato
 * @property integer $Agencia
 * @property integer $Asesor
 * @property integer $Cuotas_pendientes
 * @property integer $Cuotas_facturadas
 * @property string $Fecha_ingreso
 * @property string $Fecha_servicio
 * @property integer $Estado_servicio
 * @property integer $Contrato_franquicia
 * @property integer $ATS
 *
 * The followings are the available model relations:
 * @property ClienteVenta $iDServicio
 */
class DetalleServicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Detalle_servicio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_serv4D, Codigo4D, Cierre', 'required'),
			array('ID_servicio, Valor_servicio, Estado, ID_serv4D, Codigo4D, Cierre, Plazo, Negociacion, Contrato, Agencia, Asesor, Cuotas_pendientes, Cuotas_facturadas, Estado_servicio, Contrato_franquicia, ATS', 'numerical', 'integerOnly'=>true),
			array('Codigo_servicio', 'length', 'max'=>50),
			array('Descripcion', 'length', 'max'=>300),
			array('Fecha_creacion, Fecha_inicial, Fecha_final, Fecha_ingreso, Fecha_servicio', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, ID_servicio, Codigo_servicio, Descripcion, Valor_servicio, Fecha_creacion, Fecha_inicial, Fecha_final, Estado, ID_serv4D, Codigo4D, Cierre, Plazo, Negociacion, Contrato, Agencia, Asesor, Cuotas_pendientes, Cuotas_facturadas, Fecha_ingreso, Fecha_servicio, Estado_servicio, Contrato_franquicia, ATS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDServicio' => array(self::BELONGS_TO, 'ClienteVenta', 'ID_servicio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_servicio' => 'Id Servicio',
			'Codigo_servicio' => 'Codigo Servicio',
			'Descripcion' => 'Descripcion',
			'Valor_servicio' => 'Valor Servicio',
			'Fecha_creacion' => 'Fecha Creacion',
			'Fecha_inicial' => 'Fecha Inicial',
			'Fecha_final' => 'Fecha Final',
			'Estado' => 'Estado',
			'ID_serv4D' => 'Id Serv4 D',
			'Codigo4D' => 'Codigo4 D',
			'Cierre' => 'Cierre',
			'Plazo' => 'Plazo',
			'Negociacion' => 'Negociacion',
			'Contrato' => 'Contrato',
			'Agencia' => 'Agencia',
			'Asesor' => 'Asesor',
			'Cuotas_pendientes' => 'Cuotas Pendientes',
			'Cuotas_facturadas' => 'Cuotas Facturadas',
			'Fecha_ingreso' => 'Fecha Ingreso',
			'Fecha_servicio' => 'Fecha Servicio',
			'Estado_servicio' => 'Estado Servicio',
			'Contrato_franquicia' => 'Contrato Franquicia',
			'ATS' => 'Ats',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_servicio',$this->ID_servicio);
		$criteria->compare('Codigo_servicio',$this->Codigo_servicio,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('Valor_servicio',$this->Valor_servicio);
		$criteria->compare('Fecha_creacion',$this->Fecha_creacion,true);
		$criteria->compare('Fecha_inicial',$this->Fecha_inicial,true);
		$criteria->compare('Fecha_final',$this->Fecha_final,true);
		$criteria->compare('Estado',$this->Estado);
		$criteria->compare('ID_serv4D',$this->ID_serv4D);
		$criteria->compare('Codigo4D',$this->Codigo4D);
		$criteria->compare('Cierre',$this->Cierre);
		$criteria->compare('Plazo',$this->Plazo);
		$criteria->compare('Negociacion',$this->Negociacion);
		$criteria->compare('Contrato',$this->Contrato);
		$criteria->compare('Agencia',$this->Agencia);
		$criteria->compare('Asesor',$this->Asesor);
		$criteria->compare('Cuotas_pendientes',$this->Cuotas_pendientes);
		$criteria->compare('Cuotas_facturadas',$this->Cuotas_facturadas);
		$criteria->compare('Fecha_ingreso',$this->Fecha_ingreso,true);
		$criteria->compare('Fecha_servicio',$this->Fecha_servicio,true);
		$criteria->compare('Estado_servicio',$this->Estado_servicio);
		$criteria->compare('Contrato_franquicia',$this->Contrato_franquicia);
		$criteria->compare('ATS',$this->ATS);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetalleServicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
