<?php

/**
 * This is the model class for table "Agendados".
 *
 * The followings are the available columns in table 'Agendados':
 * @property integer $ID
 * @property integer $ID_Asesor
 * @property integer $ID_Contacto
 * @property integer $ID_Accion
 * @property string $Fecha
 * @property string $Hora
 *
 * The followings are the available model relations:
 * @property Contacto $iDContacto
 * @property AccionGestion $iDAccion
 * @property Asesor $iDAsesor
 */
class Agendados extends CActiveRecord {

    private $intervalo;
    
    public $counter;
    
    private $estadocontacto = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Agendados';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Asesor, ID_Contacto, ID_Accion, Fecha, Hora', 'required'),
            array('ID_Asesor, ID_Contacto, ID_Accion', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Asesor, ID_Contacto, ID_Accion, Fecha, Hora, intervalo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDContacto' => array(self::BELONGS_TO, 'Contacto', 'ID_Contacto'),
            'iDAccion' => array(self::BELONGS_TO, 'AccionGestion', 'ID_Accion'),
            'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Asesor' => 'Id Asesor',
            'ID_Contacto' => 'Id Contacto',
            'ID_Accion' => 'Id Accion',
            'Fecha' => 'Fecha',
            'Hora' => 'Hora',
        );
    }

    public function setIntervalo($opcion) {
        $this->intervalo = $opcion;
    }
    
    public function getIntervalo() {
        return $this->intervalo;
    }
    
    public function setEstadoContacto($opcion) {
        $this->estadocontacto = $opcion;
    }
    
    public function getEstadoContacto() {
        return $this->estadocontacto;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Asesor', $this->ID_Asesor);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('ID_Accion', $this->ID_Accion);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Hora', $this->Hora, true);

        if ($this->intervalo == 1) :
            $criteria->addCondition('Fecha = "' . date('Y-m-d') . '"');
        endif;
        if ($this->intervalo == 2) :
            $criteria->addCondition('Fecha >= "' . date('Y-m-d') . '"');
        endif;
        if ($this->intervalo == 3) :
            $criteria->addCondition('Fecha < "' . date('Y-m-d') . '"');
        endif;

        $criteria->addCondition('ID_Asesor = ' . Yii::app()->user->getState('id_usuario'));

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Agendados the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function totalAgendados($intervalo){
        $criteria = new CDbCriteria();
        
        if ($intervalo == 1) :
            $criteria->addCondition('Fecha = "' . date('Y-m-d') . '"');
        endif;
        
        if ($intervalo == 2) :
            $criteria->addCondition('Fecha >= "' . date('Y-m-d') . '"');
        endif;
        
        if ($intervalo == 3) :
            $criteria->addCondition('Fecha < "' . date('Y-m-d') . '"');
        endif;
        
        if ($intervalo == 0) :
            
        endif;

        $criteria->addCondition('ID_Asesor = ' . Yii::app()->user->getState('id_usuario'));
        
        return Agendados::model()->count($criteria);
    }
    
    public function alertaAgendados() {
        $criteria = new CDbCriteria();
        $criteria->addCondition(' ID_Asesor = "' . Yii::app()->user->getState('id_usuario') . '" AND Fecha = "' . date('Y-m-d') . '" AND ID_Accion != 19 AND Hora > "'.date('H:i:s').'" ');
        $criteria->limit = '5';
        return Agendados::model()->findAll($criteria);
    }

    public function alertasCitas() {
        
        $intervalo = date('Y-m-d H:i:s', strtotime('+2 hours', strtotime(date('Y-m-d H:i:s'))));
        $hora = date_format(date_create($intervalo), 'H:i:s');
        $criteria = new CDbCriteria();
        $criteria->addCondition('ID_Asesor = "' . Yii::app()->user->getState('id_usuario') . '" AND Fecha = "' . date('Y-m-d') . '" AND Hora >= "' . $hora . '" AND ID_Accion != 19');
        $criteria->limit = '5';
        return Agendados::model()->findAll($criteria);
    }
    
    public function alertasAgendados() {
        $time =  strtotime(date('Y-m-d H:i:s'));
        $criteria = new CDbCriteria();
        $criteria->addCondition('ID_Asesor = "' . Yii::app()->user->getState('id_usuario') .  '" AND Fecha = "' . date('Y-m-d') . '" AND ID_Accion = 2');
        $criteria->order = 'Hora ASC';
        $resumen = Agendados::model()->findAll($criteria);
        $contador = 0;
        foreach($resumen as $r) :
            $cita = strtotime($r->Fecha.' '.$r->Hora);
            if($time < $cita) :
                $contador += 1;
            endif;
        endforeach;
        return $contador;
    }
    /**
     * Ingresa un nuevo registro o actualiza uno existente
     * @param type $fecha
     * @param type $hora
     * @param type $idcontacto
     * @param type $idusuario
     * @param type $accion
     */
    public function registro($fecha, $hora, $idcontacto, $idusuario, $accion){
        $agenda = $this->model()->findByAttributes(array('ID_Contacto' => $idcontacto));
        if($agenda == NULL) :
            $agenda = new Agendados();
        endif;
        $agenda->ID_Accion = $accion;
        $agenda->Fecha = $fecha;
        $agenda->Hora = $hora;
        $agenda->ID_Contacto = $idcontacto;
        $agenda->ID_Asesor = $idusuario;
        $agenda->save();
    }
    /**
     * 
     * @return type
     */
    public function getAgendados() {
        date_default_timezone_set('America/Bogota');
        $agenda = $this->model()->findAllByAttributes(array('ID_Asesor' => Yii::app()->user->getState("id_usuario"), 'Fecha' => date('Y-m-d'),'ID_Accion' => 2));
        $arr = array();
        if($agenda != NULL) :
            $time =  strtotime(date('Y-m-d H:i:s'));
            $i = 0;
            foreach($agenda as $a) :
                $hr = explode(':', $a->Hora);
                $tt =  $a->Fecha.' '.$hr[0].':'.$hr[1];
                $dif = strtotime ( '+1 hour' , strtotime($tt)) ;
                $intervalo = (int)$time - (int)$dif;
                if($intervalo >= 0) :
                    $arr[$i] = $a->ID_Contacto;
                    $i++;
                endif;
            endforeach;
        endif;
        return $arr;
    }
}