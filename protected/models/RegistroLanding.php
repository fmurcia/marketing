<?php

/**
 * This is the model class for table "Registro_landing".
 *
 * The followings are the available columns in table 'Registro_landing':
 * @property double $ID
 * @property integer $Contador
 * @property string $Fecha
 * @property string $Accion
 * @property string $Sitio
 */
class RegistroLanding extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Registro_landing';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Contador, Fecha, Accion, Sitio', 'required'),
            array('Contador', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Contador, Fecha, Accion, Sitio', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Contador' => 'Contador',
            'Fecha' => 'Fecha',
            'Accion' => 'Accion',
            'Sitio' => 'Sitio',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Contador', $this->Contador);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Accion', $this->Accion, true);
        $criteria->compare('Sitio', $this->Sitio, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RegistroLanding the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function registro($accion, $sitio) {
        $retorno = "";
        $count = $this->findByAttributes(array('Fecha' => date('Y-m-d'), 'Accion' => $accion, 'Sitio' => strtoupper($sitio)));
        if ($count == NULL) :
            $count = new RegistroLanding();
            $count->Contador = 1;
        else :
            $count->Contador = $count->Contador + 1;
        endif;
        
        $count->Fecha = date('Y-m-d');
        $count->Accion = $accion;
        $count->Sitio = strtoupper($sitio);

        if ($count->save()) :
            $retorno = "OK";
        else :
            $retorno = $count->getErrors();
        endif;
        
        return $retorno;
    }
}