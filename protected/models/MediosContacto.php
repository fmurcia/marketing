<?php

/**
 * This is the model class for table "Medios_contacto".
 *
 * The followings are the available columns in table 'Medios_contacto':
 * @property integer $ID
 * @property string $Nombre
 * @property integer $Principal
 * @property integer $Estado
 */
class MediosContacto extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Medios_contacto';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Nombre, Estado', 'required'),
            array('Principal, Estado', 'numerical', 'integerOnly' => true),
            array('Nombre', 'length', 'max' => 200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Nombre, Principal, Estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Principal' => 'Principal',
            'Estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('ID', $this->ID);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Principal', $this->Principal);
        $criteria->compare('Estado', $this->Estado);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MediosContacto the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getMedioTipo($tipocontacto, $mediocontacto) {
        if ($tipocontacto == 2) :
            $medio_contacto = 'TRABAJO EN FRIO';
        elseif ($tipocontacto == 4) :
            $medio_contacto = 'BASE DE DATOS';
        elseif ($tipocontacto == 5) :
            $medio_contacto = 'CHAT';
        else :
            $medios = MediosContacto::model()->findByAttributes(array('ID' => $mediocontacto));
            $medio_contacto = $medios->Nombre;
        endif;
        
        return $medio_contacto;
    }
}