<?php

/**
 * This is the model class for table "segmentacion".
 *
 * The followings are the available columns in table 'segmentacion':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Cliente[] $clientes
 * @property ServicioOpcion[] $servicioOpcions
 * @property TipoCliente[] $tipoClientes
 */
class Segmentacion extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Segmentacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Estado', 'numerical', 'integerOnly' => true),
            array('Nombre', 'length', 'max' => 300),
            array('Descripcion', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Nombre, Descripcion, Estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'clientes' => array(self::HAS_MANY, 'Cliente', 'Segmento'),
            'servicioOpcions' => array(self::HAS_MANY, 'ServicioOpcion', 'ID_Segmento'),
            'tipoClientes' => array(self::HAS_MANY, 'TipoCliente', 'ID_Segmento'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Descripcion' => 'Descripcion',
            'Estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Descripcion', $this->Descripcion, true);
        $criteria->compare('Estado', $this->Estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Segmentacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
