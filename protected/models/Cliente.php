<?php

/**
 * This is the model class for table "Cliente".
 *
 * The followings are the available columns in table 'Cliente':
 * @property integer $ID
 * @property integer $Tipo_persona
 * @property string $Razon_social
 * @property string $Nombre_1
 * @property string $Nombre_2
 * @property string $Apellido_1
 * @property string $Apellido_2
 * @property integer $Contrato
 * @property string $Tipo_contrato
 * @property string $Fecha
 * @property integer $Ciudad_instalacion
 * @property string $Direccion_instalacion
 * @property string $Barrio_instalacion
 * @property string $Telefono_instalacion
 * @property integer $Ciudad_cobro
 * @property string $Direccion_cobro
 * @property string $Barrio_cobro
 * @property string $Telefono_cobro
 * @property integer $Num_documento
 * @property integer $Digito_verificacion
 * @property string $Tipo_documento
 * @property string $Representante_legal
 * @property integer $Cedula
 * @property string $Celular
 * @property string $Num_cotizacion
 * @property integer $Duracion_contrato
 * @property integer $Tipo_cliente
 * @property integer $Segmento
 * @property string $Hora
 * @property integer $Documentos
 * @property integer $Estado
 * @property integer $Editable
 * @property string $Observacion
 * @property integer $OrigenObser
 * @property string $Fecha_cambioEstado
 * @property integer $Completo
 * @property integer $Asesor
 * @property integer $Agencia
 * @property integer $Ciudad
 * @property string $Titular_debito
 * @property string $Email_debito
 * @property integer $Numero_cuenta
 * @property integer $Banco
 * @property string $Tipo_cuenta
 * @property string $ConsCliente4D
 * @property integer $ProgramacionEqui
 * @property string $Fecha_comercial
 * @property integer $ID_negociacion
 * @property string $Placa
 * @property string $cadena4D
 * @property integer $ID_cierre
 *
 * The followings are the available model relations:
 * @property Agencia $agencia
 * @property Asesor $asesor
 * @property Ciudad $ciudad
 * @property TipoCliente $tipoCliente
 * @property Segmentacion $segmento
 * @property ClienteVenta[] $clienteVentas
 * @property Solicitud[] $solicituds
 */
class Cliente extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Cliente';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Direccion_cobro', 'required'),
            array('Tipo_persona, Contrato, Ciudad_instalacion, Ciudad_cobro, Num_documento, Digito_verificacion, Cedula, Duracion_contrato, Tipo_cliente, Segmento, Documentos, Estado, Editable, OrigenObser, Completo, Asesor, Agencia, Ciudad, Numero_cuenta, Banco, ProgramacionEqui, ID_negociacion, ID_cierre', 'numerical', 'integerOnly' => true),
            array('Razon_social', 'length', 'max' => 1000),
            array('Nombre_1, Nombre_2, Apellido_1, Apellido_2, Direccion_instalacion, Barrio_instalacion, Direccion_cobro, Barrio_cobro, Tipo_documento, Representante_legal, Placa', 'length', 'max' => 200),
            array('Tipo_contrato', 'length', 'max' => 45),
            array('Telefono_instalacion, Telefono_cobro, Celular', 'length', 'max' => 20),
            array('Num_cotizacion', 'length', 'max' => 25),
            array('Titular_debito, Tipo_cuenta', 'length', 'max' => 150),
            array('Email_debito', 'length', 'max' => 100),
            array('ConsCliente4D', 'length', 'max' => 50),
            array('Fecha, Hora, Observacion, Fecha_cambioEstado, Fecha_comercial, cadena4D', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Tipo_persona, Razon_social, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Contrato, Tipo_contrato, Fecha, Ciudad_instalacion, Direccion_instalacion, Barrio_instalacion, Telefono_instalacion, Ciudad_cobro, Direccion_cobro, Barrio_cobro, Telefono_cobro, Num_documento, Digito_verificacion, Tipo_documento, Representante_legal, Cedula, Celular, Num_cotizacion, Duracion_contrato, Tipo_cliente, Segmento, Hora, Documentos, Estado, Editable, Observacion, OrigenObser, Fecha_cambioEstado, Completo, Asesor, Agencia, Ciudad, Titular_debito, Email_debito, Numero_cuenta, Banco, Tipo_cuenta, ConsCliente4D, ProgramacionEqui, Fecha_comercial, ID_negociacion, Placa, cadena4D, ID_cierre', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'agencia' => array(self::BELONGS_TO, 'Agencia', 'Agencia'),
            'asesor' => array(self::BELONGS_TO, 'Asesor', 'Asesor'),
            'ciudad' => array(self::BELONGS_TO, 'Ciudad', 'Ciudad'),
            'tipoCliente' => array(self::BELONGS_TO, 'TipoCliente', 'Tipo_cliente'),
            'segmento' => array(self::BELONGS_TO, 'Segmentacion', 'Segmento'),
            'clienteVentas' => array(self::HAS_MANY, 'ClienteVenta', 'ID_cliente'),
            'solicituds' => array(self::HAS_MANY, 'Solicitud', 'ID_Cliente'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Tipo_persona' => 'Tipo Persona',
            'Razon_social' => 'Razon Social',
            'Nombre_1' => 'Nombre 1',
            'Nombre_2' => 'Nombre 2',
            'Apellido_1' => 'Apellido 1',
            'Apellido_2' => 'Apellido 2',
            'Contrato' => 'Contrato',
            'Tipo_contrato' => 'Tipo Contrato',
            'Fecha' => 'Fecha',
            'Ciudad_instalacion' => 'Ciudad Instalacion',
            'Direccion_instalacion' => 'Direccion Instalacion',
            'Barrio_instalacion' => 'Barrio Instalacion',
            'Telefono_instalacion' => 'Telefono Instalacion',
            'Ciudad_cobro' => 'Ciudad Cobro',
            'Direccion_cobro' => 'Direccion Cobro',
            'Barrio_cobro' => 'Barrio Cobro',
            'Telefono_cobro' => 'Telefono Cobro',
            'Num_documento' => 'Num Documento',
            'Digito_verificacion' => 'Digito Verificacion',
            'Tipo_documento' => 'Tipo Documento',
            'Representante_legal' => 'Representante Legal',
            'Cedula' => 'Cedula',
            'Celular' => 'Celular',
            'Num_cotizacion' => 'Num Cotizacion',
            'Duracion_contrato' => 'Duracion Contrato',
            'Tipo_cliente' => 'Tipo Cliente',
            'Segmento' => 'Segmento',
            'Hora' => 'Hora',
            'Documentos' => 'Documentos',
            'Estado' => 'Estado',
            'Editable' => 'Editable',
            'Observacion' => 'Observacion',
            'OrigenObser' => 'Origen Obser',
            'Fecha_cambioEstado' => 'Fecha Cambio Estado',
            'Completo' => 'Completo',
            'Asesor' => 'Asesor',
            'Agencia' => 'Agencia',
            'Ciudad' => 'Ciudad',
            'Titular_debito' => 'Titular Debito',
            'Email_debito' => 'Email Debito',
            'Numero_cuenta' => 'Numero Cuenta',
            'Banco' => 'Banco',
            'Tipo_cuenta' => 'Tipo Cuenta',
            'ConsCliente4D' => 'Cons Cliente4 D',
            'ProgramacionEqui' => 'Programacion Equi',
            'Fecha_comercial' => 'Fecha Comercial',
            'ID_negociacion' => 'Id Negociacion',
            'Placa' => 'Placa',
            'cadena4D' => 'Cadena4 D',
            'ID_cierre' => 'Id Cierre',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Tipo_persona', $this->Tipo_persona);
        $criteria->compare('Razon_social', $this->Razon_social, true);
        $criteria->compare('Nombre_1', $this->Nombre_1, true);
        $criteria->compare('Nombre_2', $this->Nombre_2, true);
        $criteria->compare('Apellido_1', $this->Apellido_1, true);
        $criteria->compare('Apellido_2', $this->Apellido_2, true);
        $criteria->compare('Contrato', $this->Contrato);
        $criteria->compare('Tipo_contrato', $this->Tipo_contrato, true);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Ciudad_instalacion', $this->Ciudad_instalacion);
        $criteria->compare('Direccion_instalacion', $this->Direccion_instalacion, true);
        $criteria->compare('Barrio_instalacion', $this->Barrio_instalacion, true);
        $criteria->compare('Telefono_instalacion', $this->Telefono_instalacion, true);
        $criteria->compare('Ciudad_cobro', $this->Ciudad_cobro);
        $criteria->compare('Direccion_cobro', $this->Direccion_cobro, true);
        $criteria->compare('Barrio_cobro', $this->Barrio_cobro, true);
        $criteria->compare('Telefono_cobro', $this->Telefono_cobro, true);
        $criteria->compare('Num_documento', $this->Num_documento);
        $criteria->compare('Digito_verificacion', $this->Digito_verificacion);
        $criteria->compare('Tipo_documento', $this->Tipo_documento, true);
        $criteria->compare('Representante_legal', $this->Representante_legal, true);
        $criteria->compare('Cedula', $this->Cedula);
        $criteria->compare('Celular', $this->Celular, true);
        $criteria->compare('Num_cotizacion', $this->Num_cotizacion, true);
        $criteria->compare('Duracion_contrato', $this->Duracion_contrato);
        $criteria->compare('Tipo_cliente', $this->Tipo_cliente);
        $criteria->compare('Segmento', $this->Segmento);
        $criteria->compare('Hora', $this->Hora, true);
        $criteria->compare('Documentos', $this->Documentos);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Editable', $this->Editable);
        $criteria->compare('Observacion', $this->Observacion, true);
        $criteria->compare('OrigenObser', $this->OrigenObser);
        $criteria->compare('Fecha_cambioEstado', $this->Fecha_cambioEstado, true);
        $criteria->compare('Completo', $this->Completo);
        $criteria->compare('Asesor', $this->Asesor);
        $criteria->compare('Agencia', $this->Agencia);
        $criteria->compare('Ciudad', $this->Ciudad);
        $criteria->compare('Titular_debito', $this->Titular_debito, true);
        $criteria->compare('Email_debito', $this->Email_debito, true);
        $criteria->compare('Numero_cuenta', $this->Numero_cuenta);
        $criteria->compare('Banco', $this->Banco);
        $criteria->compare('Tipo_cuenta', $this->Tipo_cuenta, true);
        $criteria->compare('ConsCliente4D', $this->ConsCliente4D, true);
        $criteria->compare('ProgramacionEqui', $this->ProgramacionEqui);
        $criteria->compare('Fecha_comercial', $this->Fecha_comercial, true);
        $criteria->compare('ID_negociacion', $this->ID_negociacion);
        $criteria->compare('Placa', $this->Placa, true);
        $criteria->compare('cadena4D', $this->cadena4D, true);
        $criteria->compare('ID_cierre', $this->ID_cierre);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cliente the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function sqlventa($idcierre=''){
        
        if (empty($idcierre)) :
            $c = $this->cierre();
            $cierre = $c['ID'];
        else :
            $cierre = $idcierre;
        endif;

        $sql = 'SELECT
                `co`.`ID` AS `IDContacto`,
                `co`.`Razon_social` AS `RazonSocialContacto`,
                `a`.`Nombre` AS `AsesorCliente`,
                `cl`.`Razon_social` AS `RazonSocialCliente`,
                `cl`.`Contrato` AS `ContratoCliente`,
                `ag`.`Nombre` AS `AgenciaCliente`, 
                `ct`.`Codigo` AS `IDCotizacion`,
                `es`.`Descripcion` AS `EstadoDetalle`,
                `es`.`Codigo` AS `Codigo`,
                `ds`.`Valor_servicio` AS `ValorServicioDetalle`,
                `co`.`Fecha_creacion` AS `FechaCreacionContacto`,
                `co`.`Fecha_Ultgestion` AS `FechaUltGestionContacto`,
                `cv`.`Fecha_creacion` AS `FechaCreacionClienteVenta`,
                `se`.`Nombre` AS `ServicioClienteVenta`

                FROM    `teleweb`.`Cliente` `cl` 
                         join `teleweb`.`Cliente_venta` `cv`
                         join `teleweb`.`Detalle_servicio` `ds`
                         join `teleweb`.`Estado_4D` `es`
                         join `teleweb`.`Agencia` `ag`
                         join `teleweb`.`Contacto` `co`
                         join `teleweb`.`Cotizacion` `ct`
                         join `teleweb`.`Asesor` `a`
                         join `teleweb`.`Asesor` `a1`
                         join `teleweb`.`Servicios` `se`

                WHERE (`cl`.`ID` = `cv`.`ID_cliente`) 
                       and (`cv`.`ID` = `ds`.`ID_servicio`) 
                       and (`es`.`Codigo` = `ds`.`Estado_servicio`) 
                       and (`cl`.`Agencia` = `ag`.`ID`) 
                       and (`co`.`ID` = `cl`.`ID_Contacto`)
                       and (`co`.`Telemercaderista` = ' . Yii::app()->user->getState("id_usuario") . ')
                       and (`cv`.`ID_Cotizacion` = ct.ID)
                       and (`cv`.`Cierre` = ' . $cierre . ')
                       and (`a`.`ID` = cl.Asesor)
                       and (`se`.`ID` = cv.Servicio)
                       and (`a1`.`ID` = co.Telemercaderista)';
        
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        return $command->query();
        
    }

    /**
     * 
     * @param type $idcierre
     * @param type $telemercaderista
     * @return type
     */
    public function contratosTelemark() {
        
        $data = $this->sqlventa();
        $total = 0;
        foreach ($data as $d) :
            //activo en facturacion
            if ($d['Codigo'] == 8) :
                $count = HistoricoGestion::model()->totalRegistrado($d['IDContacto']);
                if (sizeof($count) > 2) :
                    $total += $d['ValorServicioDetalle'];
                endif;
            endif;
        endforeach;

        return $total;
    }
    
    public function informeTelemark($idcierre = '') {
        return $this->sqlventa($idcierre);
    }

    public function cierre() {
        $periodo = Cierres::model()->findAll();
        $cierre = array();
        foreach ($periodo as $p) :
            if (strtotime($p->Fecha_inicial) <= strtotime(date('Y-m-d')) && strtotime(date('Y-m-d')) <= strtotime($p->Fecha_final)) :
                $cierre = $p->attributes;
            endif;
        endforeach;
        return $cierre;
    }

}
