<?php

/**
 * This is the model class for table "Estado_venta".
 *
 * The followings are the available columns in table 'Estado_venta':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Sig_paso
 * @property integer $ID_4D
 * @property integer $Orden
 *
 * The followings are the available model relations:
 * @property HistoricoGestion[] $historicoGestions
 */
class EstadoVenta extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Estado_venta';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID, Nombre, Sig_paso, ID_4D', 'required'),
            array('ID, ID_4D, Orden', 'numerical', 'integerOnly' => true),
            array('Nombre', 'length', 'max' => 50),
            array('Sig_paso', 'length', 'max' => 200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Nombre, Sig_paso, ID_4D, Orden', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'historicoGestions' => array(self::HAS_MANY, 'HistoricoGestion', 'ID_Seguimiento'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Sig_paso' => 'Sig Paso',
            'ID_4D' => 'Id 4 D',
            'Orden' => 'Orden',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Sig_paso', $this->Sig_paso, true);
        $criteria->compare('ID_4D', $this->ID_4D);
        $criteria->compare('Orden', $this->Orden);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EstadoVenta the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
}