<?php

/**
 * This is the model class for table "Agencia".
 *
 * The followings are the available columns in table 'Agencia':
 * @property integer $ID
 * @property integer $Tipo
 * @property string $Codigo
 * @property string $Nit
 * @property integer $Digito
 * @property integer $Contrato
 * @property string $Nombre
 * @property string $Direccion
 * @property string $Telefono
 * @property string $Celular
 * @property string $Email
 * @property string $Gerente
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 * @property integer $Ciudad
 * @property string $Fecha_creacion
 * @property string $Imagen
 * @property integer $TipoAgencia
 *
 * The followings are the available model relations:
 * @property TipoAgencia $tipo
 * @property Calendario[] $calendarios
 * @property Cliente[] $clientes
 * @property Contacto[] $contactos
 * @property GrupoCotizacion[] $grupoCotizacions
 * @property Rutero[] $ruteros
 * @property AgenciaAgencia[] $agenciaAgencias
 * @property AgenciaAgencia[] $agenciaAgencias1
 * @property AgenciaCiudad[] $agenciaCiudads
 * @property AreaAgencia[] $areaAgencias
 * @property AsesorAgencia[] $asesorAgencias
 */
class Agencia extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Agencia';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID, Ciudad, Fecha_creacion', 'required'),
            array('ID, Tipo, Digito, Contrato, Estado, Ciudad, TipoAgencia', 'numerical', 'integerOnly' => true),
            array('Codigo', 'length', 'max' => 10),
            array('Nit', 'length', 'max' => 25),
            array('Nombre, Email, Imagen', 'length', 'max' => 200),
            array('Telefono', 'length', 'max' => 30),
            array('Celular', 'length', 'max' => 50),
            array('Gerente', 'length', 'max' => 150),
            array('Direccion, Fecha_inicial, Fecha_final', 'safe'),
            array('ID, Tipo, Codigo, Nit, Digito, Contrato, Nombre, Direccion, Telefono, Celular, Email, Gerente, Estado, Fecha_inicial, Fecha_final, Ciudad, Fecha_creacion, Imagen, TipoAgencia', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tipo' => array(self::BELONGS_TO, 'TipoAgencia', 'Tipo'),
            'calendarios' => array(self::HAS_MANY, 'Calendario', 'Agencia'),
            'clientes' => array(self::HAS_MANY, 'Cliente', 'Agencia'),
            'contactos' => array(self::HAS_MANY, 'Contacto', 'Agencia'),
            'grupoCotizacions' => array(self::HAS_MANY, 'GrupoCotizacion', 'Agencia'),
            'ruteros' => array(self::HAS_MANY, 'Rutero', 'ID_ATS'),
            'agenciaAgencias' => array(self::HAS_MANY, 'AgenciaAgencia', 'ID_AgenciaH'),
            'agenciaAgencias1' => array(self::HAS_MANY, 'AgenciaAgencia', 'ID_AgenciaP'),
            'agenciaCiudads' => array(self::HAS_MANY, 'AgenciaCiudad', 'Agencia'),
            'areaAgencias' => array(self::HAS_MANY, 'AreaAgencia', 'ID_Agencia'),
            'asesorAgencias' => array(self::HAS_MANY, 'AsesorAgencia', 'Agencia'),
            'regionalAgencias' => array(self::HAS_MANY, 'RegionalAgencia', 'ID_Agencia'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Tipo' => 'Tipo',
            'Codigo' => 'Codigo',
            'Nit' => 'Nit',
            'Digito' => 'Digito',
            'Contrato' => 'Contrato',
            'Nombre' => 'Nombre',
            'Direccion' => 'Direccion',
            'Telefono' => 'Telefono',
            'Celular' => 'Celular',
            'Email' => 'Email',
            'Gerente' => 'Gerente',
            'Estado' => 'Estado',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
            'Ciudad' => 'Ciudad',
            'Fecha_creacion' => 'Fecha Creacion',
            'Imagen' => 'Imagen',
            'TipoAgencia' => 'Tipo Agencia',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Tipo', $this->Tipo);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('Nit', $this->Nit, true);
        $criteria->compare('Digito', $this->Digito);
        $criteria->compare('Contrato', $this->Contrato);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Direccion', $this->Direccion, true);
        $criteria->compare('Telefono', $this->Telefono, true);
        $criteria->compare('Celular', $this->Celular, true);
        $criteria->compare('Email', $this->Email, true);
        $criteria->compare('Gerente', $this->Gerente, true);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);
        $criteria->compare('Ciudad', $this->Ciudad);
        $criteria->compare('Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Imagen', $this->Imagen, true);
        $criteria->compare('TipoAgencia', $this->TipoAgencia, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Agencia the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Agencias / ATS de Gestion
     */
    public function getAgencias() {
        $criterio = new CDbCriteria;
        $criterio->condition = 'Tipo=:tipo';
        $criterio->order = 'Nombre ASC';
        $criterio->params = array(':tipo' => 1);
        return CHtml::listData(Agencia::model()->findAll($criterio), 'ID', 'Nombre');
    }
}
