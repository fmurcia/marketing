<?php

/**
 * This is the model class for table "Contacto".
 *
 * The followings are the available columns in table 'Contacto':
 * @property integer $ID
 * @property integer $Telemercaderista
 * @property integer $Tipo
 * @property string $Razon_social
 * @property string $Nit
 * @property string $Nombre_completo
 * @property string $Direccion
 * @property string $Direccion_encargado
 * @property string $Telefono
 * @property string $Celular
 * @property string $Email
 * @property integer $Cliente_actual
 * @property integer $Ciudad
 * @property integer $Agencia
 * @property integer $Asesor
 * @property string $Barrio
 * @property string $Persona_contacto
 * @property string $Medio_contacto
 * @property string $Documento
 * @property integer $Digito_verificacion
 * @property string $Tipodoc
 * @property string $Observaciones
 * @property integer $Contrato
 * @property integer $Estado
 * @property string $Busqueda
 * @property string $Fecha_creacion
 * @property string $Hora_creacion
 * @property integer $Tipo_cliente
 * @property string $Segmentacion
 * @property integer $Producto_interes
 * @property string $Actividad
 * @property string $Servicio
 * @property string $pruebas
 * @property integer $Consecutivo4D
 * @property integer $Bloqueado
 * @property integer $Aprobado4D
 * @property integer $Estado_proceso
 * @property integer $ID_cierre
 * @property integer $Tipo_contacto
 * @property integer $Tiene_equipos
 * @property string $Clase_vehiculo
 * @property integer $Cantidad_vehiculos
 * @property integer $TipoDescartado
 * @property integer $Sincronizado
 * @property integer $ID_plan
 * @property integer $Oportunidad
 * @property String $Fecha_Ultgestion
 * @property String $Fecha_Ultagenda
 * @property String $Decision_compra
 * @property integer $Horizontal
 * @property integer $Id_Estadoweb

 *
 * The followings are the available model relations:
 * @property Agendados[] $agendadoses
 * @property Asesor $telemercaderista
 * @property TipoContacto $tipoContacto
 * @property Planes $iDPlan
 * @property ProductoInteres $productoInteres
 * @property Agencia $agencia
 * @property Asesor $asesor
 * @property Ciudad $ciudad
 * @property TipoCliente $tipoCliente
 * @property Cotizacion[] $cotizacions
 * @property GestionComercial[] $gestionComercials
 * @property GrupoCotizacion[] $grupoCotizacions
 * @property HistoricoGestion[] $historicoGestions
 * @property Mensajeria[] $mensajerias
 * @property PersonaContacto[] $personaContactos
 * @property ServicioInteres[] $servicioInteres
 * @property RuteroComercial[] $ruteroComercials
 * @property DetalleCalendario[] $detalleCalendarios
 */
class Contacto extends CActiveRecord {

    public $Contador;
    public $EstadoProceso;
    public $MedioContacto;
    public $tipCont = 0;
    public $searchasesor;
    public $searchagencia;
    public $searchciudad;
    public $searchtipocontacto;
    public $tipDesc = 0;
    public $counter;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Contacto';
    }

    public function getEstado() {
        return $this->EstadoProceso;
    }

    public function setEstado($estado) {
        $this->EstadoProceso = $estado;
    }

    public function setMedio($medio) {
        $this->MedioContacto = $medio;
    }

    public function getMedio() {
        return $this->MedioContacto;
    }

    public function getDescartado() {
        return $this->tipDesc;
    }

    public function setDescartado($opcion) {
        $this->tipDesc = $opcion;
    }

    public function getContacto() {
        return $this->tipCont;
    }

    public function setContacto($opcion) {
        $this->tipCont = $opcion;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Razon_social, Ciudad', 'required'),
            array('Telemercaderista, Tipo, Cliente_actual, Ciudad, Agencia, Asesor, Digito_verificacion, Contrato, Estado, Tipo_cliente, Producto_interes, Consecutivo4D, Bloqueado, Aprobado4D, Estado_proceso, ID_cierre, Tipo_contacto, Tiene_equipos, Cantidad_vehiculos, TipoDescartado, Sincronizado, ID_plan, Oportunidad, Horizontal, Id_Estadoweb', 'numerical', 'integerOnly' => true),
            array('Razon_social, Nombre_completo', 'length', 'max' => 500),
            array('Nit, Direccion_encargado, Clase_vehiculo, Decision_compra', 'length', 'max' => 100),
            array('Telefono', 'length', 'max' => 10),
            array('Celular', 'length', 'max' => 14),
            array('Email, Persona_contacto, Medio_contacto, Actividad', 'length', 'max' => 200),
            array('Barrio, Tipodoc', 'length', 'max' => 45),
            array('Documento, Segmentacion', 'length', 'max' => 20),
            array('Servicio', 'length', 'max' => 150),
            array('Direccion, Observaciones, Busqueda, Fecha_creacion, Fecha_Ultgestion, Hora_creacion, pruebas, Decision_compra', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Telemercaderista, Tipo, Razon_social, Nit, Nombre_completo, Direccion, Direccion_encargado, '
                . 'Telefono, Celular, Email, Cliente_actual, Ciudad, Agencia, Asesor, Barrio, Persona_contacto, Medio_contacto, '
                . 'Documento, Digito_verificacion, Tipodoc, Observaciones, Contrato, Estado, Busqueda, Fecha_creacion, Hora_creacion, Fecha_Ultgestion, Fecha_Ultagenda, '
                . 'Tipo_cliente, Segmentacion, Producto_interes, Actividad, Servicio, pruebas, Consecutivo4D, Bloqueado,'
                . 'Aprobado4D, ID_cierre, Tipo_contacto, Tiene_equipos, Clase_vehiculo, Cantidad_vehiculos, searchtipocontacto, '
                . 'searchciudad, searchagencia, searchasesor, ID_plan, Oportunidad, Decision_compra, Id_Estadoweb', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'agendadoses' => array(self::HAS_MANY, 'Agendados', 'ID_Contacto'),
            'telemercaderista' => array(self::BELONGS_TO, 'Asesor', 'Telemercaderista'),
            'tipoContacto' => array(self::BELONGS_TO, 'TipoContacto', 'Tipo_contacto'),
            'estadoProceso' => array(self::BELONGS_TO, 'EstadoVenta', 'Estado_proceso'),
            'iDPlan' => array(self::BELONGS_TO, 'Planes', 'ID_plan'),
            'productoInteres' => array(self::BELONGS_TO, 'ProductoInteres', 'Producto_interes'),
            'agencia' => array(self::BELONGS_TO, 'Agencia', 'Agencia'),
            'asesor' => array(self::BELONGS_TO, 'Asesor', 'Asesor'),
            'ciudad' => array(self::BELONGS_TO, 'Ciudad', 'Ciudad'),
            'tipoCliente' => array(self::BELONGS_TO, 'TipoCliente', 'Tipo_cliente'),
            'cotizacions' => array(self::HAS_MANY, 'Cotizacion', 'Contacto'),
            'gestionComercials' => array(self::HAS_MANY, 'GestionComercial', 'ID_Contacto'),
            'grupoCotizacions' => array(self::HAS_MANY, 'GrupoCotizacion', 'ID_Contacto'),
            'historicoGestions' => array(self::HAS_MANY, 'HistoricoGestion', 'ID_Contacto'),
            'mensajerias' => array(self::HAS_MANY, 'Mensajeria', 'ID_Contacto'),
            'personaContactos' => array(self::HAS_MANY, 'PersonaContacto', 'Contacto'),
            'servicioInteres' => array(self::HAS_MANY, 'ServicioInteres', 'ID_Contacto'),
            'ruteroComercials' => array(self::HAS_MANY, 'Rutero', 'ID_Contacto'),
            'detalleCalendarios' => array(self::HAS_MANY, 'DetalleCalendario', 'ID_Contacto'),
            'idEstadoweb' => array(self::BELONGS_TO, 'EstadosWeb', 'Id_Estadoweb')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Telemercaderista' => 'Telemercaderista',
            'Tipo' => 'Tipo',
            'Razon_social' => 'Razon Social',
            'Nit' => 'Nit',
            'Nombre_completo' => 'Nombre Completo',
            'Direccion' => 'Direccion',
            'Direccion_encargado' => 'Direccion Encargado',
            'Telefono' => 'Telefono',
            'Celular' => 'Celular',
            'Email' => 'Email',
            'Cliente_actual' => 'Cliente Actual',
            'Ciudad' => 'Ciudad',
            'Agencia' => 'Agencia',
            'Asesor' => 'Asesor',
            'Barrio' => 'Barrio',
            'Persona_contacto' => 'Persona Contacto',
            'Medio_contacto' => 'Medio Contacto',
            'Documento' => 'Documento',
            'Digito_verificacion' => 'Digito Verificacion',
            'Tipodoc' => 'Tipodoc',
            'Observaciones' => 'Observaciones',
            'Contrato' => '# Contrato Referido',
            'Estado' => 'Estado',
            'Busqueda' => 'Busqueda',
            'Fecha_creacion' => 'Fecha Creacion',
            'Fecha_Ultgestion' => 'Fecha Ult Gestion',
            'Hora_creacion' => 'Hora Creacion',
            'Tipo_cliente' => 'Tipo Cliente',
            'Segmentacion' => 'Segmentacion',
            'Producto_interes' => 'Producto Interes',
            'Actividad' => 'Actividad',
            'Servicio' => 'Servicio',
            'pruebas' => 'Pruebas',
            'Consecutivo4D' => 'Consecutivo4 D',
            'Bloqueado' => 'Bloqueado',
            'Aprobado4D' => 'Aprobado4 D',
            'Estado_proceso' => 'Estado Proceso',
            'ID_cierre' => 'Id Cierre',
            'Tipo_contacto' => 'Tipo Contacto',
            'Tiene_equipos' => 'Tiene Equipos',
            'Clase_vehiculo' => 'Clase Vehiculo',
            'Cantidad_vehiculos' => 'Cantidad Vehiculos',
            'searchasesor' => 'Funcionario',
            'searchagencia' => 'Agencia',
            'searchciudad' => 'Ciudad',
            'searchtipocontacto' => 'Tipo Contacto',
            'TipoDescartado' => 'Tipo Descartado',
            'Sincronizado' => 'Sincronizado',
            'ID_plan' => 'Planes de Interes',
            'Oportunidad' => 'Oportunidad Triangulo',
            'Decision_compra' => 'Decision de Compra',
            'Horizontal' => 'Propiedad Horizontal',
            'Id_Estadoweb' => 'Id Estadoweb'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.ID', $this->ID);
        $criteria->compare('Telemercaderista', $this->Telemercaderista);
        $criteria->compare('Tipo', $this->Tipo);
        $criteria->compare('t.Razon_social', $this->Razon_social, true);
        $criteria->compare('t.Nit', $this->Nit, true);
        $criteria->compare('Nombre_completo', $this->Nombre_completo, true);
        $criteria->compare('t.Direccion', $this->Direccion, true);
        $criteria->compare('t.Direccion_encargado', $this->Direccion_encargado, true);
        $criteria->compare('t.Telefono', $this->Telefono, true);
        $criteria->compare('t.Celular', $this->Celular, true);
        $criteria->compare('t.Email', $this->Email, true);
        $criteria->compare('Cliente_actual', $this->Cliente_actual);
        $criteria->compare('Ciudad', $this->Ciudad);
        $criteria->compare('Agencia', $this->Agencia);
        $criteria->compare('Asesor', $this->Asesor);
        $criteria->compare('Barrio', $this->Barrio, true);
        $criteria->compare('Persona_contacto', $this->Persona_contacto, true);
        $criteria->compare('Medio_contacto', $this->Medio_contacto, true);
        $criteria->compare('Documento', $this->Documento, true);
        $criteria->compare('Digito_verificacion', $this->Digito_verificacion);
        $criteria->compare('Tipodoc', $this->Tipodoc, true);
        $criteria->compare('Observaciones', $this->Observaciones, true);
        $criteria->compare('t.Contrato', $this->Contrato);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Busqueda', $this->Busqueda, true);
        $criteria->compare('t.Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Hora_creacion', $this->Hora_creacion, true);
        $criteria->compare('Tipo_cliente', $this->Tipo_cliente);
        $criteria->compare('Segmentacion', $this->Segmentacion, true);
        $criteria->compare('Producto_interes', $this->Producto_interes);
        $criteria->compare('Actividad', $this->Actividad, true);
        $criteria->compare('Servicio', $this->Servicio, true);
        $criteria->compare('pruebas', $this->pruebas, true);
        $criteria->compare('Consecutivo4D', $this->Consecutivo4D);
        $criteria->compare('Bloqueado', $this->Bloqueado);
        $criteria->compare('Aprobado4D', $this->Aprobado4D);
        $criteria->compare('Estado_proceso', $this->Estado_proceso);
        $criteria->compare('ID_cierre', $this->ID_cierre);
        $criteria->compare('Tipo_contacto', $this->Tipo_contacto);
        $criteria->compare('Tiene_equipos', $this->Tiene_equipos);
        $criteria->compare('Clase_vehiculo', $this->Clase_vehiculo, true);
        $criteria->compare('Cantidad_vehiculos', $this->Cantidad_vehiculos);
        $criteria->compare('asesor.Nombre', $this->searchasesor, true);
        $criteria->compare('agencia.Nombre', $this->searchagencia, true);
        $criteria->compare('ciudad.Nombre', $this->searchciudad, true);
        $criteria->compare('tipoContacto.Descripcion', $this->searchtipocontacto, true);
        $criteria->compare('TipoDescartado', $this->TipoDescartado, true);
        $criteria->compare('Id_Estadoweb', $this->Id_Estadoweb);

        $criteria->with = array(
            'asesor' => array('Nombre LIKE "%' . $this->searchasesor . '%"'),
            'agencia' => array('Nombre LIKE "%' . $this->searchagencia . '%"'),
            'tipoContacto' => array('Descripcion LIKE "%' . $this->searchtipocontacto . '%"'),
            'ciudad' => array('Nombre LIKE "%' . $this->searchciudad . '%"'),
        );

        if ($this->getEstado() == 0) :
            $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Estado_proceso != 19 ');
        else :

            $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Estado_proceso = ' . $this->getEstado());

            if ($this->tipCont != 0) :
                $criteria->addCondition('Tipo_contacto = ' . $this->getContacto());
            endif;

            if ($this->tipDesc != 0) :
                $criteria->addCondition('TipoDescartado = ' . $this->getDescartado());
            endif;

        endif;

        if ($this->getMedio() == 'OPORTUNIDAD TRIANGULO') :
            $criteria->addCondition('Medio_contacto = "' . $this->getMedio() . '"');
        endif;

        $criteria->order = 't.Fecha_creacion DESC, t.Hora_creacion DESC';

        return new CActiveDataProvider(
                $this, array(
            'criteria' => $criteria,
            "pagination" => array(
                'pageSize' => 10
            )
                )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Contacto the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Retorna los Tipo de Contacto
     */
    public function getTipocontacto() {
        $tipocontacto = AsesorTipocontacto::model()->with(array('iDTipo'))->findAllByAttributes(array('ID_Asesor' => Yii::app()->user->getState('id_usuario'), 'Estado' => 1));
        $i = 0;
        $list = array();
        if ($tipocontacto != NULL) :
            foreach ($tipocontacto as $t) :
                if ($t->ID_Tipo != 3) :
                    $list[$i]['ID'] = $t->iDTipo->ID;
                    $list[$i]['Descripcion'] = $t->iDTipo->Descripcion;
                    $i++;
                endif;
            endforeach;
        endif;
        return CHtml::listData($list, 'ID', 'Descripcion');
    }

    /**
     * Retorna los Tipo de Contacto
     */
    public function getMediocontacto() {
        $data = MediosContacto::model()->findAllByAttributes(array('Estado' => 1));
        return CHtml::listData($data, 'Nombre', 'Nombre');
    }

    /**
     * Retorna los Medios de Contacto
     */
    public function getMedioscontacto($tpcontacto) {
        $estado = 1;
        if ($tpcontacto->Tipo_contacto == 5) :
            $estado = 69;
        endif;
        $data = MediosContacto::model()->findAllByAttributes(array('Estado' => 1));
        return CHtml::listData($data, 'Nombre', 'Nombre');
    }

    /**
     * Retorna los Servicios Asociados a la Compañia
     */
    public function getServicios() {
        $servicios = Servicios::model()->findAllByAttributes(array("Tipo" => 1, "Estado" => 1));
        return $servicios;
    }

    /**
     * Retorna los Servicios Asociados a la Compañia
     */
    public function getServiciosList() {
        $servicios = Servicios::model()->findAllByAttributes(array("Tipo" => 1, "Estado" => 1));
        return CHtml::listData($servicios, 'ID', 'Nombre');
    }
    /**
     * Retorna la Segmentacion de los clientes
     */
    public function getSegmentacionList() {
        $segmentacion = Segmentacion::model()->findAll();
        return CHtml::listData($segmentacion, 'ID', 'Nombre');
    }

    public function getOpcion($service = 0, $type = "") {
        if (!$this->isNewRecord) {
            if ($service > 0 && $type != "") {
                $opcion = ServicioInteres::model()->findByAttributes(array(
                    "ID_Contacto" => $this->ID,
                    "ID_Servicio" => $service
                ));
                if ($opcion != null) {
                    switch ($type) {
                        case "Estado":
                            return $opcion->Estado;
                            break;
                        case "Equipos":
                            return $opcion->Equipos;
                            break;
                        case "ID":
                            return $opcion->ID;
                            break;
                        default:
                            return false;
                            break;
                    }
                }
            }
        }
        return false;
    }

    public function setOption($service = 0, $status = 0, $eq = 0) {
        if (!$this->isNewRecord) {
            if ($service > 0) {
                $opcion = ServicioInteres::model()->findByAttributes(array(
                    "ID_Contacto" => $this->ID,
                    "ID_Servicio" => $service
                ));
                if ($opcion == null) {
                    $opcion = new ServicioInteres;
                    $opcion->ID_Contacto = $this->ID;
                    $opcion->ID_Servicio = $service;
                }
                $opcion->Estado = $status;
                $opcion->Equipos = $eq;
                $opcion->save();
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna los tipos de cliente para el servicio
     */
    public function getTipocliente() {
        return CHtml::listData(TipoCliente::model()->findAll(), 'ID', 'Nombre');
    }

    /**
     * Retorna las Ciudades Predeterminadas
     */
    public function getCiudades() {
        return CHtml::listData(Ciudad::model()->findAll(), 'ID', 'Nombre');
    }

    /**
     * Retorna las Ciudades Predeterminadas
     */
    public function getBarrios() {
        $data = Barrios::model()->findAll(array('order' => 'Nombre'));
        return CHtml::listData($data, 'Nombre', 'Nombre');
    }

    public function getActividades() {
        $activs = Actividades::model()->findAll();
        return CHtml::listData($activs, "ID", "Nombre");
    }

    /**
     * Retorna las Ciudades Predeterminadas
     */
    public function getActividadeconomica() {
        return CHtml::listData(Actividades::model()->findAll(array('order' => 'Nombre')), 'Nombre', 'Nombre');
    }

    /**
     * Retorna las Ciudades Predeterminadas
     */
    public function getRegionales() {

        if (Yii::app()->user->getState("id_cargo") == 3) :
            $reg = Regional::model()->with(
                            array(
                                'regionalAsesor' => array(
                                    'condition' => 'regionalAsesor.ID_Asesor = "' . Yii::app()->user->getState('id_usuario') . '" AND regionalAsesor.Estado = 1'
                                )
                            )
                    )->findAll('t.Estado = 1');
        else :
            $reg = Regional::model()->findAll('Estado = 1');
        endif;

        return CHtml::listData($reg, 'ID_Regional', 'Descripcion');
    }

    /**
     * Acciones de Gestion
     */
    public function getAcciones() {
        return CHtml::listData(EstadosWeb::model()->findAll('Mostrar = 1'), 'ID', 'Descripcion');
    }

    /**
     * Planes Comerciales
     */
    public function getPlanes() {
        return CHtml::listData(Planes::model()->findAll('Estado = 1'), 'ID', 'Descripcion');
    }

    /**
     * Acciones de Gestion
     */
    public function getAccionesDescartadas() {
        return CHtml::listData(AccionGestion::model()->findAll('Estado = 0'), 'ID', 'Descripcion');
    }

    /**
     * Agencias / ATS de Gestion
     */
    public function getAgencias() {
        $criterio = new CDbCriteria;
        $criterio->condition = 'Tipo=:tipo';
        $criterio->order = 'Nombre ASC';
        $criterio->params = array(':tipo' => 1);
        return CHtml::listData(Agencia::model()->findAll($criterio), 'ID', 'Nombre');
    }

    public function getAgenciasRegional($idregional) {

        $agencias = Agencia::model()->with(array(
                    'regionalAgencias' =>
                    array(
                        'condition' => 'ID_Regional = ' . $idregional
                    )
                        )
                )->findAll('Tipo != 2');

        return CHtml::listData($agencias, 'ID', 'Nombre');
    }

    /**
     * Asesor vs Agencia
     */
    public function getAsesor($idagencia, $fecha) {
        $asesor = Asesor::model()->with(
                        array('asesorAgencias' => array(
                                'condition' => 'Agencia = ' . $idagencia
                            ))
                )
                ->findAll('Nivel = 10');

        $time = explode(" ", $fecha);
        $asesores = array();
        foreach ($asesor as $p) :
            $ocupado = Disponibilidad::model()->findByAttributes(array('Asesor' => $p->ID, 'Fecha_disponible' => $time[0], 'Hora_inicial' => $time[1] . ":00:00"));
            if ($ocupado != null):
                $asesor_libre = "OCUPADO";
            else :
                $asesor_libre = "LIBRE";
            endif;
            $asesores[$p->ID] = $p->Nombre . '- (' . $asesor_libre . ')';
        endforeach;

        return $asesores;
    }

    public function getAntiguedad() {
        $fechaHora = strtotime($this->Fecha_creacion . " " . $this->Hora_creacion);
        return $this->th($fechaHora);
    }

    function th($time) {
        date_default_timezone_set('America/Bogota');
        $time = time() - $time; // to get the time since that moment

        $tokens = array(
            31536000 => 'Año',
            2592000 => 'Mes',
            86400 => 'Dia',
            3600 => 'Hora',
            60 => 'Minuto',
            1 => 'Segundo'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit)
                continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? ($text == "Mes" ? "e" : '') . 's' : '');
        }
    }

    public function coincidencia($attribute) {

        $error = "";

        if ($this->$attribute != "" && $this->$attribute != 'REGISTRO WEB' && $this->$attribute != '0') :

            $criteria = new CDbCriteria();
            $criteria->compare("Razon_social", $this->$attribute, false, "OR");
            $criteria->compare("Direccion", $this->$attribute, false, "OR");
            $criteria->compare("Email", $this->$attribute, false, "OR");
            $criteria->compare("Nit", $this->$attribute, false, "OR");
            $criteria->compare("Telefono", $this->$attribute, false, "OR");
            $criteria->compare("Celular", $this->$attribute, false, "OR");
            $contactos = $this->findAll($criteria);

            $criteria1 = new CDbCriteria();
            $criteria1->compare("Nombre_completo", $this->$attribute, false, "OR");
            $criteria1->compare("Direccion", $this->$attribute, false, "OR");
            $criteria1->compare("Email", $this->$attribute, false, "OR");
            $criteria1->compare("Documento", $this->$attribute, false, "OR");
            $criteria1->compare("Telefono", $this->$attribute, false, "OR");
            $criteria1->compare("Celular", $this->$attribute, false, "OR");
            $encargados = PersonaContacto::model()->findAll($criteria1);

            if ($this->$attribute == 'Razon_social' || $this->$attribute == 'Direccion' || $this->$attribute == 'Telefono' || $this->$attribute == 'Contrato' || $this->$attribute == 'Nit') :
                $criteria2 = new CDbCriteria();
                if ($this->$attribute == 'Razon_social') :
                    $criteria2->compare("Razon_social", $this->$attribute, true, "OR");
                    $criteria2->compare("Representante_legal", $this->$attribute, true, "OR");
                elseif ($this->$attribute == 'Direccion') :
                    $criteria2->compare("Direccion_cobro", $this->$attribute, true, "OR");
                    $criteria2->compare("Direccion_instalacion", $this->$attribute, true, "OR");
                elseif ($this->$attribute == 'Telefono') :
                    $criteria2->compare("Telefono_instalacion", $this->$attribute, true, "OR");
                    $criteria2->compare("Telefono_cobro", $this->$attribute, true, "OR");
                elseif ($this->$attribute == 'Contrato') :
                    $criteria2->compare("Contrato", $this->$attribute, true, "OR");
                elseif ($this->$attribute == 'Nit') :
                    $criteria2->compare("Num_documento", $this->$attribute, true, "OR");
                endif;
                $cliente = Cliente::model()->findAll($criteria2);
            else :
                $cliente = array();
            endif;


            if (count($contactos) > 1) :
                $arr_aux = array();
                foreach ($contactos as $cl) :
                    $arr_aux[] = $cl->ID;
                endforeach;
                $error .= "<tr><td>" . $this->getAttributeLabel($attribute) . "</td><td style='color:red; text-align:center'><a href='#' onclick='verDetalle(" . json_encode($arr_aux) . ", \"Contacto\")'><span class='badge'>" . count($contactos) . "</span></a></td><td>CONTACTO</td><td>" . $this->$attribute . "</td></tr>";
            endif;

            if (count($encargados) > 1) :
                $arr_aux = array();
                foreach ($encargados as $cl) :
                    $arr_aux[] = $cl->ID;
                endforeach;
                $error .= "<tr><td>" . $this->getAttributeLabel($attribute) . "</td><td style='color:red; text-align:center'><a href='#' onclick='verDetalle(" . json_encode($arr_aux) . ", \"PersonaContacto\")'><span class='badge'>" . count($encargados) . "</span></a></td><td>ENCARGADO</td><td>" . $this->$attribute . "</td></tr>";
            endif;

            if (count($cliente) > 0) :
                $arr_aux = array();
                foreach ($cliente as $cl) :
                    $arr_aux[] = $cl->ID;
                endforeach;
                if ($this->getAttributeLabel($attribute) != 'Email') :
                    $error .= "<tr><td>" . $this->getAttributeLabel($attribute) . "</td><td style='color:red; text-align:center'><a href='#'  onclick='verDetalle(" . json_encode($arr_aux) . ",\"Cliente\")'><span class='badge'>" . count($cliente) . "</span></a></td><td>CLIENTE</td><td>" . $this->$attribute . "</td></tr>";
                else :
                    $error .= '';
                endif;

            endif;

            if (!empty($error)) :
                return $error;
            endif;
        endif;
    }

    public function referido() {
        $cliente = Cliente::model()->findByAttributes(array("Contrato" => $this->Contrato));
        $error = "";
        if ($cliente != NULL) :
            $error .= "<table class='table table-striped table-bordered'>";
            $error .= "<tr><td>CLIENTE REDIFERIDO</td><td colspan='3'>" . strtoupper($cliente->Razon_social) . "</td></tr>";
            $error .= "<tr><td>AGENCIA</td><td colspan='3'> " . $cliente->agencia->Nombre . " </td></tr>";
            $error .= "<tr><td>ASESOR</td><td colspan='3'>" . $cliente->asesor->Nombre . " </td></tr>";
            $error .= "<tr><td>CONTRATO</td><td colspan='3'>" . $cliente->Contrato . " </td></tr>";
            $clienteventa = ClienteVenta::model()->findAllByAttributes(array('ID_cliente' => $cliente->ID));
            if ($clienteventa != NULL) :
                $arr_aux1 = array();
                foreach ($clienteventa as $cl) :
                    $arr_aux1[] = $cl->ID;
                endforeach;
                $criteria = new CDbCriteria();
                $criteria->addInCondition('ID_servicio', $arr_aux1);
                $detalleventa = DetalleServicio::model()->findAll($criteria);
                $error .= "<tr><td class='info'>SERVICIO</td><td class='info'>FECHA</td><td class='info'>ESTADO</td><td class='info'>VALOR SERVICIO</td></tr>";
                foreach ($detalleventa as $cl) :
                    $error .= "<tr><td>" . $cl->Codigo_servicio . "</td><td>" . $cl->Fecha_creacion . "</td><td>ACTIVO</td><td style='text-align:right'>$" . number_format($cl->Valor_servicio, 0, ',', ' ') . "</td></tr>";
                endforeach;
            endif;
            $error . "</table>";
        endif;
        return $error;
    }

    public function getBloqueo() {
        if ($this->Bloqueado == 1) {
            return "<span class='glyphicon glyphicon-lock'></span>";
        }
        return "";
    }

    public function estado($estado = "", $oper = "and") {
        $statlist = explode(",", $estado);
        if (count($statlist) > 0) {
            $count = 1;
            foreach ($statlist as $stat) {
                if ($count == 1) {
                    switch (trim($stat)) {
                        case "bloqueado":
                            $status = ($this->Bloqueado == 1 ? true : false);
                            break;
                        case "descartado":
                            $status = ($this->Estado_proceso == 19 ? true : false);
                            break;
                        default:
                            $status = false;
                            break;
                    }
                } else {
                    switch (trim($stat)) {
                        case "bloqueado":
                            if ($oper == "and") {
                                $status = $status && ($this->Bloqueado == 1 ? true : false);
                            } else {
                                $status = $status || ($this->Bloqueado == 1 ? true : false);
                            }
                            break;
                        case "descartado":
                            if ($oper == "and") {
                                $status = $status && ($this->Estado_proceso == 19 ? true : false);
                            } else {
                                $status = $status || ($this->Estado_proceso == 19 ? true : false);
                            }
                            break;
                        default:
                            if ($oper == "and") {
                                $status = $status && false;
                            } else {
                                $status = $status || false;
                            }
                            break;
                    }
                }
                $count++;
            }
        } else {
            $status = false;
        }
        return $status;
    }

    public function getEtapas() {
        $criteria = new CDbCriteria();
        $criteria->order = 'ID ASC';
        return EstadosWeb::model()->findAll($criteria);
    }

    public function getEtapasContacto($tipo) {
        $criteria = new CDbCriteria();
        $criteria->select = 'COUNT(*) as Contador';
        $criteria->addCondition('Estado_proceso = "' . $tipo . '" AND Telemercaderista = ' . Yii::app()->user->getState('id_usuario'));
        $criteria->group = 'Estado_proceso';
        $criteria->order = 'Contador DESC';
        return Contacto::model()->count($criteria);
    }

    public function getEtapasContactoTemporalidad() {
        $criteria = new CDbCriteria();
        $criteria->select = 'Estado_proceso, COUNT(*) as Contador';
        $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario'));
        $criteria->group = 'Estado_proceso';
        return Contacto::model()->findAll($criteria);
    }

    public function contadorTiposContacto($id) {
        $query = '';
        if ($id != 0) :
            $query = " Tipo_contacto = $id AND ";
        endif;

        $criteria = new CDbCriteria();
        $criteria->addCondition($query . ' Telemercaderista = ' . Yii::app()->user->getState('id_usuario'));
        return Contacto::model()->count($criteria);
    }

    public function getAgenciasAsignadas() {
        $criteria = new CDbCriteria();
        $criteria->select = 'Agencia, COUNT(*) as Contador';
        $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario'));
        //$criteria->addBetweenCondition('Fecha_creacion', $this->Primerdia(), $this->Ultimodia());
        $criteria->group = 'Agencia';
        $criteria->order = 'Contador DESC';
        $criteria->limit = 5;
        return Contacto::model()->findAll($criteria);
    }

    /**
     * 
     */
    public function getSemanario($semana) {
        $semanario = $semana;

        switch ($semanario) :
            case "Mon": $dia_esp = "L";
                break;
            case "Tue": $dia_esp = "M";
                break;
            case "Wed": $dia_esp = "M";
                break;
            case "Thu": $dia_esp = "J";
                break;
            case "Fri": $dia_esp = "V";
                break;
            case "Sat": $dia_esp = "S";
                break;
            case "Sun": $dia_esp = "D";
                break;
        endswitch;

        return $dia_esp;
    }

    /**
     * Retorna el Mes Actual
     * @return string
     */
    function getMes($m = '') {
        $month = explode('-', $m);
        $mes = (!empty($month[0])) ? $month[0] : date("F");
        if ($mes == "Jan" || $mes == '01')
            $mes = "Ene - " . $month[1];
        if ($mes == "Feb" || $mes == '02')
            $mes = "Feb - " . $month[1];
        if ($mes == "Mar" || $mes == '03')
            $mes = "Mar - " . $month[1];
        if ($mes == "Apr" || $mes == '04')
            $mes = "Abr - " . $month[1];
        if ($mes == "May" || $mes == '05')
            $mes = "May - " . $month[1];
        if ($mes == "Jun" || $mes == '06')
            $mes = "Jun - " . $month[1];
        if ($mes == "Jul" || $mes == '07')
            $mes = "Jul - " . $month[1];
        if ($mes == "Aug" || $mes == '08')
            $mes = "Ago - " . $month[1];
        if ($mes == "Sep" || $mes == '09')
            $mes = "Sep - " . $month[1];
        if ($mes == "Oct" || $mes == '10')
            $mes = "Oct - " . $month[1];
        if ($mes == "Nov" || $mes == '11')
            $mes = "Nov - " . $month[1];
        if ($mes == "Dec" || $mes == '12')
            $mes = "Dic - " . $month[1];
        return $mes;
    }

    /**
     * Retorna el Mes Actual
     * @return string
     */
    function Mes($m = '') {
        $mes = (!empty($m)) ? $m : date("F");
        if ($mes == "January" || $mes == '01')
            $mes = "Enero";
        if ($mes == "February" || $mes == '02')
            $mes = "Febrero";
        if ($mes == "March" || $mes == '03')
            $mes = "Marzo";
        if ($mes == "April" || $mes == '04')
            $mes = "Abril";
        if ($mes == "May" || $mes == '05')
            $mes = "Mayo";
        if ($mes == "June" || $mes == '06')
            $mes = "Junio";
        if ($mes == "July" || $mes == '07')
            $mes = "Julio";
        if ($mes == "August" || $mes == '08')
            $mes = "Agosto";
        if ($mes == "September" || $mes == '09')
            $mes = "Septiembre";
        if ($mes == "October" || $mes == '10')
            $mes = "Octubre";
        if ($mes == "November" || $mes == '11')
            $mes = "Noviembre";
        if ($mes == "December" || $mes == '12')
            $mes = "Diciembre";
        return $mes;
    }

    /**
     * Retorna todos los Agendados en Cita para el mismo dia
     * @return type
     */
    public function alertasAgendados() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ID_Asesor = ' . Yii::app()->user->getState('id_usuario') . ' AND Fecha = "' . date('Y-m-d') . '" AND ID_Accion = 2');
        $criteria->order = 'Hora ASC';
        return Agendados::model()->findAll($criteria);
    }

    public function alertasCitas() {

        $intervalo = date('Y-m-d H:i:s', strtotime('+2 hours', strtotime(date('Y-m-d H:i:s'))));
        $hora = date_format(date_create($intervalo), 'Y-m-d H:i:s');
        $criteria = new CDbCriteria();
        $criteria->addCondition('ID_Asesor = ' . Yii::app()->user->getState('id_usuario') . ' AND Fecha = "' . date('Y-m-d') . '" AND Hora >= "' . $hora . '" AND ID_Accion IN (2,10)');
        $criteria->limit = '5';
        return Agendados::model()->findAll($criteria);
    }

    public function alertContactoWebLimit() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Tipo_contacto = 3 AND Id_Estadoweb = 1 AND Estado_proceso = 20');
        $criteria->order = 'Fecha_creacion DESC, Hora_creacion DESC';
        return Contacto::model()->findAll($criteria);
    }

    public function alertContactoChatLimit() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Tipo_contacto = 5 AND Id_Estadoweb = 1 AND Estado_proceso = 20');
        $criteria->order = 'Fecha_creacion DESC, Hora_creacion DESC';
        return Contacto::model()->findAll($criteria);
    }

    public function alertasContactoWebUlt() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Tipo_contacto = 3 AND Id_Estadoweb = 20');
        $criteria->order = 'Fecha_creacion DESC';
        return Contacto::model()->find($criteria);
    }

    public function alertWeb() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Tipo_contacto = 3 AND Fecha_creacion = "' . date('Y-m-d') . '"');
        return Contacto::model()->findAll($criteria);
    }

    public function totalContactos() {
        return Contacto::model()->count('Telemercaderista=:id AND Estado_proceso != 19', array(':id' => Yii::app()->user->getState('id_usuario')));
    }

    public function getDescartados() {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('ID', array(12, 13, 14, 15, 16));
        return AccionGestion::model()->findAll($criteria);
    }

    public function totalDescartados($opcion = 0) {
        $criterio = new CDbCriteria();
        $criterio->addCondition('Estado_proceso = 19');
        if ($opcion != 0):
            $criterio->addCondition('TipoDescartado = ' . $opcion);
        endif;
        $criterio->addCondition('Telemercaderista =  ' . Yii::app()->user->getState('id_usuario'));
        return Contacto::model()->count($criterio);
    }

    /**
     * Exporta a Excel los Registros del Mes Actual
     * @param type $fecha1
     * @param type $fecha2
     * @return \CActiveDataProvider
     */
    public function getExcelConvert($fecha1, $fecha2, $excepcion = true) {
//        if (!$excepcion) :
        return Yii::app()->db->createCommand()
                        ->select('co.ID, co.Razon_social, co.Email, co.Medio_contacto, ci.Nombre as Ciudad, co.Ciudad as ID_Ciudad, co.Servicio, ev.Nombre as Estado_proceso, co.Tipo, co.Fecha_creacion, tc.Descripcion as Tipo_contacto, co.TipoDescartado, a.Nombre as Asesor, aa.Nombre as Comercial, co.Fecha_Ultgestion as Fecha_gestion, co.Observaciones as Comentarios, es.Descripcion as EstadoWeb, co.Telefono, co.Celular, co.Direccion')
                        ->from('Contacto co, Ciudad ci, Tipo_contacto tc, Estado_venta ev, Asesor a, Asesor aa, estados_web es')
                        ->where('es.ID = co.Id_Estadoweb AND co.Fecha_creacion BETWEEN :fecha AND :fecha2 AND ci.ID = co.Ciudad AND tc.ID = co.Tipo_contacto AND ev.ID = co.Estado_proceso AND a.ID = co.Asesor AND aa.ID = co.Telemercaderista', array(':fecha' => $fecha1, ':fecha2' => $fecha2))
                        ->order('co.Fecha_creacion ASC')
                        ->queryAll();
//        else :
//            return Yii::app()->db->createCommand()
//                            ->select('co.ID, co.Razon_social, co.Email, co.Medio_contacto, ci.Nombre as Ciudad, co.Servicio, ev.Nombre as Estado_proceso, co.Tipo, co.Fecha_creacion, tc.Descripcion as Tipo_contacto, co.TipoDescartado, a.Nombre as Asesor, aa.Nombre as Comercial, co.Fecha_Ultgestion as Fecha_gestion, co.Observaciones as Comentarios, es.Descripcion as EstadoWeb, co.Telefono, co.Celular, co.Direccion')
//                            ->from('Contacto co, Ciudad ci, Tipo_contacto tc, Estado_venta ev, Asesor a, Asesor aa, estados_web es')
//                            ->where('es.ID = co.Id_Estadoweb AND co.Fecha_creacion BETWEEN :fecha AND :fecha2 AND ci.ID = co.Ciudad AND tc.ID = co.Tipo_contacto AND ev.ID = co.Estado_proceso AND co.Telemercaderista = :comercial  AND a.ID = co.Asesor  AND aa.ID = co.Telemercaderista', array(':fecha' => $fecha1, ':fecha2' => $fecha2, ':comercial' => Yii::app()->user->getState('id_usuario')))
//                            ->order('co.Fecha_creacion ASC')
//                            ->queryAll();
//        endif;
    }

    /**
     * 
     * @param type $idciudad
     * @param type $idtipo
     * @return int
     */
    public function regionalTelemercadeo($idciudad, $idtipo) {

        $telemercadeo = Yii::app()->user->getState('id_usuario');
        $reg = RegionalAsesor::model()->findAllByAttributes(array('ID_Regional' => $idciudad, 'Estado' => 1));

        $prsl = array();
        if ($reg != NULL) :
            foreach ($reg as $r) :
                $prsl[] = $r->ID_Asesor;
            endforeach;
        endif;

        $criteria = new CDbCriteria();
        $criteria->addInCondition('ID_Asesor', $prsl);
        $criteria->addCondition('ID_Tipo = ' . $idtipo . ' AND Estado = 1');
        $criteria->order = 'RAND()';
        $criteria->limit = 1;

        $tp = AsesorTipocontacto::model()->find($criteria);

        if ($tp != NULL):
            $telemercadeo = $tp->ID_Asesor;
        endif;

        return $telemercadeo;
    }
    
    /**
     * @param array $data coleccion para insertar
     */
    public function insertarContacto($data) {
        
        if(!empty($data['email'])) :
            $contacto = Contacto::model()->findByAttributes(array('Email' => $data['email']));
            if ($contacto == NULL) :
                $contacto = new Contacto();
            endif;
        else :
            $contacto = new Contacto();
        endif;
              
        $contacto->Telemercaderista = $data['telemercadeo'];
        $contacto->Tipo = $data['segmentacion'];
        $contacto->Razon_social = $data['razon'];
        $contacto->Nombre_completo = $data['persona'];
        $contacto->Direccion = $data['direccion'];
        $contacto->Telefono = $data['fijo'];
        $contacto->Celular = $data['celular'];
        $contacto->Email = trim($data['email']);
        $contacto->Ciudad = $data['ciudad'];
        $contacto->Agencia = 1;
        $contacto->Asesor = 327;
        $contacto->Persona_contacto = $data['persona'];
        $contacto->Observaciones = $data['observaciones'];
        $contacto->Estado = 1;
        $contacto->Fecha_creacion = date('Y-m-d');
        $contacto->Hora_creacion = date('H:i:s');
        $contacto->Tipo_cliente = 2;
        $contacto->Estado_proceso = 20;
        $contacto->Tipo_contacto = $data['tipocontacto'];
        $contacto->Direccion_encargado = $data['direccion'];
        $contacto->Producto_interes = 1;
        $contacto->Fecha_Ultgestion = date('Y-m-d H:i:s');
        $contacto->Nit = "";
        $contacto->Direccion_encargado = $data['direccion'];
        $contacto->Clase_vehiculo = '';
        $contacto->Cantidad_vehiculos = 0;
        $contacto->Decision_compra = 'P';
        $contacto->Fecha_Ultagenda = date('Y-m-d');

        if ($data['tipocontacto'] == 2 && $data['mediocontacto'] == 'GOOGLE') :
            $contacto->Medio_contacto = 'TRABAJO EN FRIO';
        elseif ($data['tipocontacto'] == 4 && $data['mediocontacto'] == 'GOOGLE') :
            $contacto->Medio_contacto = 'BASE DE DATOS';
        elseif ($data['tipocontacto'] == 5) : 
            $contacto->Medio_contacto = 'CHAT';
        else :
            $contacto->Medio_contacto = $data['mediocontacto'];
        endif;

        $contacto->Horizontal = 0;
        $contacto->Servicio = $data['servicio'];
        $contacto->pruebas = '';
        $contacto->Id_Estadoweb = 1;
        $contacto->save();

        PersonaContacto::model()->crearEncargado($contacto->Razon_social, $contacto->Direccion, $contacto->Ciudad, $contacto->Email, $contacto->ID, $contacto->Telefono, $contacto->Celular);
        
        $comentario = "Contacto Registrado " . $contacto->Observaciones;
        
        ServicioInteres::model()->registro($contacto->ID, $contacto->Servicio);
        HistoricoGestion::model()->guardarGestion($contacto->ID, 20, $comentario, 1);

        return $contacto;
    }

    /**
     * 
     * @param type $telemercadeo
     * @param type $razon
     * @param type $persona
     * @param type $direccion
     * @param type $fijo
     * @param type $celular
     * @param type $observaciones
     * @param type $tipocontacto
     * @param type $mediocontacto
     * @param type $horizontal
     * @param type $email
     * @param type $ciudad
     * @param type $servicio
     * @param type $dispositivo
     */
    public function registro($telemercadeo, $razon, $persona, $direccion, $fijo, $celular, $observaciones, $tipocontacto, $mediocontacto, $horizontal, $email, $ciudad, $servicio = 'MONITOREO', $dispositivo = 'desktop') {
        $contacto = new Contacto();
        $contacto->Telemercaderista = $telemercadeo;
        $contacto->Tipo = 2;
        $contacto->Razon_social = $razon;
        $contacto->Nombre_completo = $persona;
        $contacto->Direccion = $direccion;
        $contacto->Telefono = $fijo;
        $contacto->Celular = $celular;
        $contacto->Email = $email;
        $contacto->Ciudad = $ciudad;
        $contacto->Agencia = 1;
        $contacto->Asesor = 327;
        $contacto->Persona_contacto = $persona;
        $contacto->Observaciones = $observaciones;
        $contacto->Estado = 1;
        $contacto->Fecha_creacion = date('Y-m-d');
        $contacto->Hora_creacion = date('H:i:s');
        $contacto->Tipo_cliente = 2;
        $contacto->Estado_proceso = 20;
        $contacto->Tipo_contacto = $tipocontacto;
        $contacto->Direccion_encargado = $direccion;
        $contacto->Producto_interes = 1;
        $contacto->Fecha_Ultgestion = date('Y-m-d H:i:s');
        $contacto->Nit = "";
        $contacto->Direccion_encargado = $direccion;
        $contacto->Clase_vehiculo = '';
        $contacto->Cantidad_vehiculos = 0;
        $contacto->Decision_compra = 'P';
        $contacto->Fecha_Ultagenda = date('Y-m-d');

        if ($tipocontacto == 2 && $mediocontacto == 'GOOGLE') :
            $contacto->Medio_contacto = 'TRABAJO EN FRIO';
        elseif ($tipocontacto == 4 && $mediocontacto == 'GOOGLE') :
            $contacto->Medio_contacto = 'BASE DE DATOS';
        elseif ($tipocontacto == 5) :
            $contacto->Medio_contacto = 'CHAT';
        else :
            $contacto->Medio_contacto = $mediocontacto;
        endif;

        $contacto->Horizontal = $horizontal;
        $contacto->Servicio = $servicio;
        $contacto->pruebas = $dispositivo;
        $contacto->Id_Estadoweb = 1;
        $contacto->save();

        PersonaContacto::model()->crearEncargado($contacto->Razon_social, $contacto->Direccion, $contacto->Ciudad, $contacto->Email, $contacto->ID, $contacto->Telefono, $contacto->Celular);
        $comentario = "Contacto Registrado" . $contacto->Observaciones;
        HistoricoGestion::model()->guardarGestion($contacto->ID, 20, $comentario, 1);

        return $contacto;
    }

    public function registroRds($coleccion) {

        $contacto = Contacto::model()->findByAttributes(array('Email' => trim($coleccion['email'])));

        if ($contacto == NULL) :
            $contacto = new Contacto();
        endif;

        $contacto->Telemercaderista = $coleccion['telemercaderista'];
        $contacto->Tipo = 2;
        $contacto->Razon_social = $coleccion['razon'];
        $contacto->Nombre_completo = $coleccion['persona'];
        $contacto->Direccion = $coleccion['direccion'];
        $contacto->Telefono = 0;
        $contacto->Celular = $coleccion['celular'];
        $contacto->Email = $coleccion['email'];
        $contacto->Ciudad = $coleccion['ciudad'];
        $contacto->Agencia = 1;
        $contacto->Asesor = 327;
        $contacto->Persona_contacto = $coleccion['persona'];
        $contacto->Observaciones = $coleccion['observaciones'];
        $contacto->Estado = 1;
        $contacto->Fecha_creacion = date('Y-m-d');
        $contacto->Hora_creacion = date('H:i:s');
        $contacto->Tipo_cliente = 2;
        $contacto->Estado_proceso = 20;
        $contacto->Tipo_contacto = 3;
        $contacto->Direccion_encargado = $coleccion['direccion'];
        $contacto->Producto_interes = 1;
        $contacto->Fecha_Ultgestion = date('Y-m-d H:i:s');
        $contacto->Nit = "";
        $contacto->Direccion_encargado = $coleccion['direccion'];
        $contacto->Clase_vehiculo = '';
        $contacto->Cantidad_vehiculos = 0;
        $contacto->Decision_compra = 'P';
        $contacto->Fecha_Ultagenda = date('Y-m-d');
        $contacto->Medio_contacto = $coleccion['mediocontacto'];
        $contacto->Horizontal = 0;
        $contacto->Servicio = 'SmartSense';
        $contacto->pruebas = 'Desktop';
        $contacto->Id_Estadoweb = 1;

        if (!$contacto->save()) :
            file_put_contents('/tmp/data.txt', print_r('Error : ' . $contacto->getErrors(), true) . PHP_EOL, FILE_APPEND);
        else :
            PersonaContacto::model()->crearEncargado($contacto->Razon_social, $contacto->Direccion, $contacto->Ciudad, $contacto->Email, $contacto->ID, $contacto->Telefono, $contacto->Celular);
            $comentario = "Contacto Registrado" . $contacto->Observaciones;
            HistoricoGestion::model()->guardarGestion($contacto->ID, 20, $comentario, 1);
        endif;

        return $contacto;
    }

    /**
     * Actualiza el registro campo a campo de la Tabla Contactos
     * @param type $idcontacto
     * @param type $campo
     * @param type $valor
     * @return type
     */
    public function actualizar($idcontacto, $campo, $valor) {
        $instance = $this->model()->findByPk($idcontacto);
        if ($instance->Estado_proceso == 20 || $instance->Estado_proceso == 19) :
            $instance->Estado_proceso = 21;
            $instance->Id_Estadoweb = 5;
        endif;
        $instance->Fecha_Ultgestion = date('Y-m-d H:i:s');

        if ($campo == 'Medio_contacto') :
            //$medios = MediosContacto::model()->findByAttributes(array('ID' => $valor));
            $instance->$campo = $valor;
        endif;

        if (strlen($valor) == 7 && $campo == 'Telefono'):
            $instance->Telefono = $valor;
        elseif (strlen($valor) == 10 && $campo == 'Celular'):
            $instance->Celular = $valor;
        endif;

        if ($campo != 'Telefono' && $campo != 'Celular' && $campo != 'Medio_contacto') :
            $instance->$campo = $valor;
        endif;

        $instance->update();

        return $instance;
    }

    /**
     * 
     * @param type $oportunidad
     * @param type $observaciones
     * @return \Contacto
     */
    public function actualizarContactoOportunidad($oportunidad, $observaciones, $fecha) {
        $contacto = Contacto::model()->findByAttributes(array('Oportunidad' => $oportunidad->ID));
        if ($contacto == NULL) :
            $contacto = new Contacto();
        endif;
        $contacto->Telemercaderista = $oportunidad->Telemercaderista;
        $contacto->Tipo = $oportunidad->Tipo_cliente;
        $contacto->Razon_social = $oportunidad->Razon_social;
        $contacto->Nit = $oportunidad->Nit;
        $contacto->Nombre_completo = $oportunidad->Nombre_completo;
        $contacto->Direccion = $oportunidad->Direccion;
        $contacto->Direccion_encargado = $oportunidad->Direccion;
        $contacto->Telefono = $oportunidad->Telefono;
        $contacto->Celular = $oportunidad->Celular;
        $contacto->Email = $oportunidad->Email;
        $contacto->Medio_contacto = $oportunidad->Medio_contacto;
        $contacto->Estado_proceso = 22;
        $contacto->Servicio = 'MONITOREO';
        $contacto->Asesor = $oportunidad->Asesor;
        $contacto->Agencia = $oportunidad->Agencia;
        $contacto->Observaciones = $observaciones;
        $contacto->Fecha_creacion = $oportunidad->Fecha_creacion;
        $contacto->Hora_creacion = $oportunidad->Hora_creacion;
        $contacto->Hora_creacion = $oportunidad->Hora_creacion;
        $contacto->Actividad = $oportunidad->Actividad;
        $contacto->Fecha_Ultagenda = $fecha;
        $contacto->Oportunidad = $oportunidad->ID;
        $contacto->Clase_vehiculo = '';
        $contacto->Cantidad_vehiculos = 0;
        $contacto->Id_Estadoweb = 8;
        $contacto->Decision_compra = 'P';
        $contacto->Horizontal = 0;
        $contacto->save();

        return $contacto;
    }

    /**
     * Busqueda de Comparaciones en Modelo
     * @param type $campo
     * @param type $valor
     * @return boolean si esta o no
     */
    public function existe($campo, $valor) {
        $criteria = new CDbCriteria();
        $criteria->compare($campo, $valor);
        $busqueda = $this->model()->find($criteria);
        if ($busqueda == NULL) :
            return 0;
        else :
            return 1;
        endif;
    }

    /**
     * Metodo que actualiza el cliente si el Email existe y actualiza su informacion para ser interesado
     * @param type $email
     * @param type $telefono
     * @param type $razon
     * @param type $nombre
     */
    public function setEmail($email, $telefono, $razon, $nombre) {
        $contacto = Contacto::model()->findByAttributes(array('Email' => $email));
        if ($contacto != NULL) :
            $contacto->Razon_social = $razon;
            $contacto->Nombre_completo = $nombre;
            if (strlen($telefono) == 7) :
                $contacto->Telefono = $telefono;
            elseif (strlen($telefono) == 10) :
                $contacto->Celular = $telefono;
            endif;
            $contacto->Agencia = 1;
            $contacto->Asesor = 327;
            $contacto->Estado_proceso = 20;
            $contacto->TipoDescartado = 0;
            $contacto->Id_Estadoweb = 1;
            $contacto->save();
        endif;
    }

    public function actualizarContactoEmail($email) {
        $contacto = Contacto::model()->findByAttributes(array('Email' => $email));
        if ($contacto != NULL) :
            $contacto->Id_Estadoweb = 1;
            $contacto->Estado_proceso = 20;
            $contacto->TipoDescartado = 0;
            $contacto->Observaciones = 'El Cliente esta interesado en servicios Telesentinel';
            $contacto->Fecha_Ultgestion = date('Y-m-d H:i:s');
            if ($contacto->update()) :
                echo "Cliente Interesado en Telesentinel";
            else :
                echo "Error";
            endif;
        endif;
    }

}
