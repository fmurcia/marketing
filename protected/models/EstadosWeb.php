<?php

/**
 * This is the model class for table "estados_web".
 *
 * The followings are the available columns in table 'estados_web':
 * @property integer $ID
 * @property string $Descripcion
 * @property integer $Padre
 * @property integer $Hijo
 * @property integer $Estado
 * @property string $Icono
 * @property string $Color
 * @property integer $Orden
 */
class EstadosWeb extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'estados_web';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Descripcion, Padre, Hijo, Icono', 'required'),
			array('Padre, Hijo, Estado, Orden', 'numerical', 'integerOnly'=>true),
			array('Descripcion, Icono, Color', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Descripcion, Padre, Hijo, Estado, Icono, Color, Orden', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Descripcion' => 'Descripcion',
			'Padre' => 'Padre',
			'Hijo' => 'Hijo',
			'Estado' => 'Estado',
			'Icono' => 'Icono',
			'Color' => 'Color',
			'Orden' => 'Orden',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('Padre',$this->Padre);
		$criteria->compare('Hijo',$this->Hijo);
		$criteria->compare('Estado',$this->Estado);
		$criteria->compare('Icono',$this->Icono,true);
		$criteria->compare('Color',$this->Color,true);
		$criteria->compare('Orden',$this->Orden);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EstadosWeb the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
