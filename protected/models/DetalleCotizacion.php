<?php

/**
 * This is the model class for table "Detalle_cotizacion".
 *
 * The followings are the available columns in table 'Detalle_cotizacion':
 * @property integer $ID
 * @property integer $ID_Cotizacion
 * @property string $Codigo
 * @property string $Nombre
 * @property string $Clasificacion
 * @property integer $Cantidad
 * @property integer $Valor_CIF
 * @property double $Valor_base
 * @property double $Valor_venta
 * @property double $Valor_comision
 * @property double $Valor_total
 * @property integer $ID_Comision
 * @property integer $ID_Negociacion
 * @property integer $ID_Opcion
 * @property integer $ID_Promocion
 * @property integer $ID_ElementoP
 * @property integer $Cuotas
 * @property integer $Porc_ganancia
 * @property string $Incluido_en
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 *
 * The followings are the available model relations:
 * @property Cotizacion $iDCotizacion
 * @property Negociacion $iDNegociacion
 * @property ServicioOpcion $iDOpcion
 */
class DetalleCotizacion extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Detalle_cotizacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Cotizacion, Cantidad, Valor_CIF, ID_Comision, ID_Negociacion, ID_Opcion, ID_Promocion, ID_ElementoP,ID_ElementoLP, Cuotas, Porc_ganancia, Estado', 'numerical', 'integerOnly' => true),
            array('Valor_base, Valor_venta, Valor_comision, Valor_total', 'numerical'),
            array('Codigo, Incluido_en', 'length', 'max' => 30),
            array('Nombre', 'length', 'max' => 200),
            array('Clasificacion', 'length', 'max' => 100),
            array('Fecha_inicial, Fecha_final', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Cotizacion, Codigo, Nombre, Clasificacion, Cantidad, Valor_CIF, Valor_base, Valor_venta, Valor_comision, Valor_total, ID_Comision, ID_Negociacion, ID_Opcion, ID_Promocion, ID_ElementoP, Cuotas, Porc_ganancia, Incluido_en, Estado, Fecha_inicial, Fecha_final', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cotizacion' => array(self::BELONGS_TO, 'Cotizacion', 'ID_Cotizacion'),
            'negociacion' => array(self::BELONGS_TO, 'Negociacion', 'ID_Negociacion'),
            'opcion' => array(self::BELONGS_TO, 'ServicioOpcion', 'ID_Opcion'),
            'detallelista' => array(self::BELONGS_TO, 'DetalleLista', 'ID_ElementoLP'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Cotizacion' => 'Id Cotizacion',
            'Codigo' => 'Codigo',
            'Nombre' => 'Nombre',
            'Clasificacion' => 'Clasificacion',
            'Cantidad' => 'Cantidad',
            'Valor_CIF' => 'Valor Cif',
            'Valor_base' => 'Valor Base',
            'Valor_venta' => 'Valor Venta',
            'Valor_comision' => 'Valor Comision',
            'Valor_total' => 'Valor Total',
            'ID_Comision' => 'Id Comision',
            'ID_Negociacion' => 'Id Negociacion',
            'ID_Opcion' => 'Id Opcion',
            'ID_Promocion' => 'Id Promocion',
            'ID_ElementoP' => 'Id Elemento P',
            'Cuotas' => 'Cuotas',
            'Porc_ganancia' => 'Porc Ganancia',
            'Incluido_en' => 'Incluido En',
            'Estado' => 'Estado',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Cotizacion', $this->ID_Cotizacion);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Clasificacion', $this->Clasificacion, true);
        $criteria->compare('Cantidad', $this->Cantidad);
        $criteria->compare('Valor_CIF', $this->Valor_CIF);
        $criteria->compare('Valor_base', $this->Valor_base);
        $criteria->compare('Valor_venta', $this->Valor_venta);
        $criteria->compare('Valor_comision', $this->Valor_comision);
        $criteria->compare('Valor_total', $this->Valor_total);
        $criteria->compare('ID_Comision', $this->ID_Comision);
        $criteria->compare('ID_Negociacion', $this->ID_Negociacion);
        $criteria->compare('ID_Opcion', $this->ID_Opcion);
        $criteria->compare('ID_Promocion', $this->ID_Promocion);
        $criteria->compare('ID_ElementoP', $this->ID_ElementoP);
        $criteria->compare('Cuotas', $this->Cuotas);
        $criteria->compare('Porc_ganancia', $this->Porc_ganancia);
        $criteria->compare('Incluido_en', $this->Incluido_en, true);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DetalleCotizacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNombreElemento() {
        $nombre = Elemento::model()->findByAttributes(array('Codigo' => $this->Codigo));
        if ($nombre != NULL) :
            return $nombre->Nombre;
        else :
            return "No Existe";
        endif;
    }

    public function getMultiplicar() {

        $rel = $this->getRelacionado();
        if ($rel != null) {
            $elep = Elemento::model()->findByAttributes(array(
                "Codigo" => $rel->Codigo
            ));
            $eleh = Elemento::model()->findByAttributes(array(
                "Codigo" => $this->Codigo
            ));
            if ($elep != null && $eleh != null) {
                $det = DetalleElemento::model()->findByAttributes(array(
                    "ID_ElementoP" => $elep->ID,
                    "ID_ElementoH" => $eleh->ID,
                ));
                if ($det != null) {
                    return $det->Cantidad;
                }
            }
        }
        return 1;
    }

    public function getRelacionado() {
        if ($this->Incluido_en != "") {
            return $this->findByPk($this->Incluido_en);
        }
        return null;
    }

    public function getNegociacion() {
        $nego = Negociacion::model()->findByPk($this->ID_Negociacion);
        if ($nego != null) {
            return $nego->Nombre;
        } else {
            return "Sin negociación";
        }
    }

    public function getRefElemento() {
        $elemento = $this->getElemento();
        return $elemento->Referencia;
    }

    public function getElemento() {
        $dlist = $this->detallelista;
        if ($dlist != null) {
            $ele = $dlist->elemento;
            if ($ele == null) {
                $ele = Elemento::model()->findByAttributes(array(
                    "Codigo" => $this->Codigo
                ));
            }
        } else {
            $ele = Elemento::model()->findByAttributes(array(
                "Codigo" => $this->Codigo
            ));
        }
        return $ele;
    }

    public function getDDI() {
        $ddi = 0;
        $nego = $this->negociacion;
        if ($nego != null) {
            if ($nego->Obsequio == 1) {
                $ddi = round(($this->Valor_venta * $this->Cantidad) * 1.16, 0);
            }
        }
        return $ddi;
    }

    public function getCodigo4D() {
        $elemento = $this->getElemento();
        if ($elemento != null) {
            return $elemento->Codigo4D;
        }
        return "";
    }

    public function getIVA() {
        $ele = $this->detallelista;
        if ($ele != null) {
            return $ele->getIVA();
        }
        return 0;
    }

}
