<?php

/**
 * This is the model class for table "rutero".
 *
 * The followings are the available columns in table 'rutero':
 * @property integer $ID
 * @property integer $ID_Solicitud
 * @property integer $ID_Contacto
 * @property integer $ID_Tecnico
 * @property integer $ID_ATS
 * @property string $Fecha
 * @property string $Hora
 * @property string $Horafin
 * @property integer $Estado
 * @property string $Bloque
 *
 * The followings are the available model relations:
 * @property Agencia $iDATS
 * @property Asesor $iDTecnico
 * @property Solicitud $iDSolicitud
 */
class Rutero extends CActiveRecord {

    public $numsolicitud;
    public $contrato;
    public $unidad;
    public $area;
    public $motivo;
    public $fechasolicitud;
    public $horasolicitud;
    public $tecnico;
    public $agencia;
    public $estado;
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Rutero';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Tecnico', 'required'),
            array('ID_Solicitud, ID_Contacto, ID_Tecnico, ID_ATS, Estado', 'numerical', 'integerOnly' => true),
            array('Bloque', 'length', 'max' => 2),
            array('Fecha, Hora', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Solicitud, ID_Contacto, ID_Tecnico, ID_ATS, '
                . 'Fecha, Hora, Horafin, Estado, Bloque, numsolicitud, tecnico, agencia, '
                . 'area, motivo, fechasolicitud, horasolicitud, unidad, estado, contrato', 
                'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDATS' => array(self::BELONGS_TO, 'Agencia', 'ID_ATS'),
            'iDTecnico' => array(self::BELONGS_TO, 'Asesor', 'ID_Tecnico'),
            'iDSolicitud' => array(self::BELONGS_TO, 'Solicitud', 'ID_Solicitud'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Solicitud' => 'Id Solicitud',
            'ID_Contacto' => 'Id Contacto',
            'ID_Tecnico' => 'Id Tecnico',
            'ID_ATS' => 'Id Ats',
            'Fecha' => 'Fecha',
            'Hora' => 'Hora',
            'Horafin' => 'Horafin',
            'Estado' => 'Estado',
            'Bloque' => 'Bloque',
            
            'iDSolicitud.Num_solicitud' => 'Numero Solicitud',
            'iDSolicitud.Unidad' => 'Unidad',
            'iDSolicitud.Fecha' => 'Fecha Solicitud',
            'iDSolicitud.Hora' => 'Hora Solicitud',
            'iDTecnico.Nombre' => 'Tecnico',
            'iDATS.Nombre' => 'Agencia',
            
            'iDCliente.Contrato' => 'Contrato',
            'iDEstado.Descripcion' => 'Estado Solicitud',
            'areaSolicitud.Nombre' => 'Area',
            'motivoSolicitud.Nombre' => 'Motivo Solicitud'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.
        
        /* Agencias con Permisos */
        $permisos = AsesorArea::model()->findAll('Asesor = ' . Yii::app()->user->getState('id_usuario'));
        /* Agencias vs Asesores */
        $i=0;
        foreach ($permisos as $p) :
            $areas[$i] = $p->Area;
            $i++;
        endforeach;
        
        $crit2 = new CDbCriteria();
        $crit2->addCondition('ID_Estado != 20');
        $crit2->addInCondition('Area_solicitud', $areas);
        $solicitudes = Solicitud::model()->findAll($crit2);
        $sol = array();
        $j = 0;
        foreach($solicitudes as $s):
            $sol[$j] = $s->ID;
            $j++;
        endforeach;
        
        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID, true);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Hora', $this->Hora, true);
        $criteria->compare('Horafin', $this->Horafin, true);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Bloque', $this->Bloque, true);
        
        $criteria->compare('iDSolicitud.Num_solicitud', $this->numsolicitud, true);
        $criteria->compare('iDSolicitud.Unidad', $this->unidad, true);
        $criteria->compare('iDSolicitud.Fecha', $this->fechasolicitud, true);
        $criteria->compare('iDSolicitud.Hora', $this->horasolicitud, true);
        $criteria->compare('iDATS.Nombre', $this->agencia, true);
        $criteria->compare('iDTecnico.Nombre', $this->tecnico, true);
        
        $criteria->compare('iDCliente.Contrato', $this->contrato, true);
        $criteria->compare('areaSolicitud.Nombre', $this->area, true);
        $criteria->compare('motivoSolicitud.Nombre', $this->motivo, true);
        $criteria->compare('iDEstado.Descripcion', $this->estado, true);

        $criteria->with = array(
                'iDSolicitud', 
                'iDSolicitud.iDCliente' => array('Contrato LIKE "%'.$this->contrato.'%"'), 
                'iDSolicitud.areaSolicitud' => array('Nombre LIKE "%'.$this->area.'%"'), 
                'iDSolicitud.motivoSolicitud' => array('Nombre LIKE "%'.$this->motivo.'%"'), 
                'iDSolicitud.iDEstado' => array('Descripcion LIKE "%'.$this->estado.'%"'), 
                'iDTecnico' => array('Nombre LIKE "%'.$this->tecnico.'%"'), 
                'iDATS' => array('Nombre LIKE "%'.$this->agencia.'%"')
            );
        
        $criteria->addInCondition('ID_Solicitud', $sol);
        
        $criteria->order = 't.Fecha DESC';
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Rutero the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * Registros para las Graficas del DashBoard
     * @return type
     */
    public function Dashboard() {
        $fecha = date('Y-m-d');
        $rutas = Rutero::model()->findAll("Fecha = '$fecha'");
        $redchart = array();
        $piechart = array();
        if($rutas != null) :
            $spider = array();
            $arrayContador = array();
            foreach($rutas as $r) :
                if(array_key_exists($r->iDATS->Nombre, $arrayContador)) :
                    $arrayContador[$r->iDATS->Nombre] += 1;
                else :
                    $arrayContador[$r->iDATS->Nombre] = 1;
                endif;
                $spider[$r->iDATS->Nombre][] = date("G", strtotime($r->Hora));
            endforeach;
            
            $total = sizeof($arrayContador);
            $z = 0;
            $piechart = array();
            foreach($arrayContador as $key => $value) :
                $piechart[$z] = array('name' => $key , 'y' => (int)(($value/$total) * 100));
                $redchart[$z] = array('name' => $key , 'y' => (int)(($value/$total) * 100));
                $z++;
            endforeach;         
        endif;
        $arr_aux['piechart'] = $piechart;
        $arr_aux['redchart'] = $redchart;
        return $arr_aux;
    }
}