<?php

/**
 * This is the model class for table "Asesor".
 *
 * The followings are the available columns in table 'Asesor':
 * @property integer $ID
 * @property string $Codigo
 * @property string $Nombre
 * @property string $Email
 * @property string $Password
 * @property string $Celular
 * @property integer $Activo
 * @property string $Usuario
 * @property integer $Nivel
 * @property integer $Reset
 * @property string $Foto
 * @property string $Color
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 * @property string $Iniciales
 * @property integer $Tipo_asesor
 *
 * The followings are the available model relations:
 * @property Estados $estado
 * @property Calendario[] $calendarios
 * @property Cliente[] $clientes
 * @property Contacto[] $contactos
 * @property Cupo[] $cupos
 * @property Disponibilidad[] $disponibilidads
 * @property GrupoCotizacion[] $grupoCotizacions
 * @property Pruebas[] $pruebases
 * @property Pruebas[] $pruebases1
 * @property Rutero[] $ruteros
 * @property Seguimiento[] $seguimientos
 * @property AsesorAgencia[] $asesorAgencias
 * @property AsesorServicio[] $asesorServicios
 */
class Asesor extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Asesor';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Activo, Nivel, Reset, Estado, Tipo_asesor', 'numerical', 'integerOnly' => true),
            array('Nombre, Email, Nivel, Celular, Estado', 'required'),
            array('Codigo', 'length', 'max' => 10),
            array('Nombre, Foto', 'length', 'max' => 300),
            array('Email', 'length', 'max' => 200),
            array('Password, Usuario', 'length', 'max' => 100),
            array('Celular', 'length', 'max' => 50),
            array('Color', 'length', 'max' => 7),
            array('Iniciales', 'length', 'max' => 11),
            array('Fecha_inicial, Fecha_final', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Codigo, Nombre, Email, Password, Celular, Activo, Usuario, Nivel, Reset, Foto, Color, Estado, Fecha_inicial, Fecha_final, Iniciales, Tipo_asesor', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'estado' => array(self::BELONGS_TO, 'Estados', 'Estado'),
            'nivel' => array(self::BELONGS_TO, 'Cargos', 'Nivel'),
            'calendarios' => array(self::HAS_MANY, 'Calendario', 'Asesor'),
            'clientes' => array(self::HAS_MANY, 'Cliente', 'Asesor'),
            'contactos' => array(self::HAS_MANY, 'Contacto', 'Asesor'),
            'cupos' => array(self::HAS_MANY, 'Cupo', 'Asesor'),
            'disponibilidads' => array(self::HAS_MANY, 'Disponibilidad', 'Asesor'),
            'grupoCotizacions' => array(self::HAS_MANY, 'GrupoCotizacion', 'Asesor'),
            'pruebases' => array(self::HAS_MANY, 'Pruebas', 'ID_Tecnico'),
            'pruebases1' => array(self::HAS_MANY, 'Pruebas', 'ID_Usuario'),
            'ruteros' => array(self::HAS_MANY, 'Rutero', 'ID_Tecnico'),
            'seguimientos' => array(self::HAS_MANY, 'Seguimiento', 'ID_Usuario'),
            'asesorAgencias' => array(self::HAS_MANY, 'AsesorAgencia', 'Asesor'),
            'asesorServicios' => array(self::HAS_MANY, 'AsesorServicio', 'Asesor'),
            'asesorAreas' => array(self::HAS_MANY, 'AsesorArea', 'Asesor'),
            'regionalAsesor' => array(self::HAS_MANY, 'RegionalAsesor', 'ID_Asesor'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Codigo' => 'Codigo',
            'Nombre' => 'Nombre',
            'Email' => 'Email',
            'Password' => 'Password',
            'Celular' => 'Celular',
            'Activo' => 'Activo',
            'Usuario' => 'Usuario',
            'Nivel' => 'Nivel',
            'Reset' => 'Reset',
            'Foto' => 'Foto',
            'Color' => 'Color',
            'Estado' => 'Estado',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
            'Iniciales' => 'Iniciales',
            'Tipo_asesor' => 'Tipo Asesor',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Email', $this->Email, true);
        $criteria->compare('Password', $this->Password, true);
        $criteria->compare('Celular', $this->Celular, true);
        $criteria->compare('Activo', $this->Activo);
        $criteria->compare('Usuario', $this->Usuario, true);
        $criteria->compare('Nivel', $this->Nivel);
        $criteria->compare('Reset', $this->Reset);
        $criteria->compare('Foto', $this->Foto, true);
        $criteria->compare('Color', $this->Color, true);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);
        $criteria->compare('Iniciales', $this->Iniciales, true);
        $criteria->compare('Tipo_asesor', $this->Tipo_asesor);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Asesor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return type
     */
    public function getAreas() {
        $areas = Area::model()->findAll();
        return CHtml::listData($areas, 'ID', 'Nombre');
    }

    /**
     * 
     * @return type
     */
    public function getAgencias() {
        $agencias = Agencia::model()->findAll();
        return CHtml::listData($agencias, 'ID', 'Nombre');
    }

    /**
     * 
     * @return type
     */
    public function getCargos() {
        $cargos = Cargos::model()->findAll();
        return CHtml::listData($cargos, 'id', 'descripcion');
    }
    /**
     * Lista las telemercaderistas
     * @return type
     */
    public function getTelemercaderistas(){
        $registros = Asesor::model()->findAll('Nivel = 3 AND Estado = 1 AND Activo = 1');
        return CHtml::listData($registros, 'ID', 'Nombre');
    }
    /**
     * Retorna las Telemercaderistas
     * @return type
     */
    public function getListTelemercaderistas(){
        $registros = Asesor::model()->findAll('Nivel = 3 AND Estado = 1 AND Activo = 1');
        return $registros;
    }
}