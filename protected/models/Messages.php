<?php

/**
 * This is the model class for table "Messages".
 *
 * The followings are the available columns in table 'Messages':
 * @property integer $ID_Message
 * @property integer $ID_Chat_Room
 * @property integer $ID_Asesor
 * @property string $Messages
 * @property string $created_at
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Estados $estado
 * @property Asesor $iDAsesor
 * @property Chatrooms $iDChatRoom
 */
class Messages extends CActiveRecord
{
        public $maxFecha;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Messages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_Chat_Room, ID_Asesor, Messages, created_at', 'required'),
			array('ID_Chat_Room, ID_Asesor, Estado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID_Message, ID_Chat_Room, ID_Asesor, Messages, created_at, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estado' => array(self::BELONGS_TO, 'Estados', 'Estado'),
			'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
			'iDChatRoom' => array(self::BELONGS_TO, 'Chatrooms', 'ID_Chat_Room'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_Message' => 'Id Message',
			'ID_Chat_Room' => 'Id Chat Room',
			'ID_Asesor' => 'Id Asesor',
			'Messages' => 'Messages',
			'created_at' => 'Created At',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_Message',$this->ID_Message);
		$criteria->compare('ID_Chat_Room',$this->ID_Chat_Room);
		$criteria->compare('ID_Asesor',$this->ID_Asesor);
		$criteria->compare('Messages',$this->Messages,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Messages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getChatRooms($idchatroom)
        {
            $criteria = new CDbCriteria();
            $criteria->addCondition('ID_Chat_Room=:idchatroom');
            $criteria->params = array(':idchatroom' => $idchatroom);
            return $this->model()->findAll($criteria);
        }
        
        public function getMessagesChatroom($id)
        {
            return $this->model()->count(array('condition' => 'ID_Chat_Room=:id AND Estado=:estado', 'params' => array(':id' => $id, ':estado' => 10)));
        }
        
        public function getTimeChatRoom($chat_room_id)
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'max(created_at) AS maxFecha';
            $criteria->condition = 'ID_Chat_Room=:idchatroom';
            $criteria->params = array(':idchatroom' => $chat_room_id);
            $fecha = $this->model()->find($criteria);
            if($fecha != NULL) :
                $UltimoMensaje = $fecha['maxFecha'];
            else :
                $UltimoMensaje = date('Y-m-d H:i:s');
            endif;
            return $UltimoMensaje;
        }
        
        public function upChatRoom($idchatroom){
            $upmessage = $this->model()->findAll('ID_Chat_Room = ' . $idchatroom . ' AND Estado = 10 AND ID_Asesor = ' . $idchatroom);
            foreach ($upmessage as $up) :
                $up->Estado = 15;
                $up->save();
            endforeach;
        }
}
