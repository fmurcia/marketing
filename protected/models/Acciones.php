<?php

/**
 * This is the model class for table "Acciones".
 *
 * The followings are the available columns in table 'Acciones':
 * @property integer $ID
 * @property string $Descripcion
 * @property string $Hash
 */
class Acciones extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Acciones';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Descripcion, Hash', 'required'),
            array('Descripcion, Hash', 'length', 'max' => 100),
            array('Medio, Tipo', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Descripcion, Hash', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            "accionesWeb" => array(self::MANY_MANY, "Acciones", "accion_accion(ID_AccionP,ID_AccionH)","condition"=>"accionesWeb.Medio='Web' and accionesWeb_accionesWeb.Estado=1"),
            "accionesMovil" => array(self::MANY_MANY, "Acciones", "accion_accion(ID_AccionP,ID_AccionH)","condition"=>"accionesMovil.Medio='Movil' and accionesMovil_accionesMovil.Estado=1"),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'Hash' => 'Hash',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Descripcion', $this->Descripcion, true);
        $criteria->compare('Hash', $this->Hash, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Acciones the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getCadena() {
        $chainlist = array();
        if ($this->Tipo == "Rol") {
            $chain = $this->accionesWeb;
            if (count($chain) > 0) {
                foreach ($chain as $c) {
                    if ($c->Tipo == "Tarea") {
                        $chainlist["P".$c->ID] = $c->ID;
                    } else {
                        $chainrel=$c->getCadena();
                        $chainlist = array_merge($chainlist, $chainrel);
                    }
                }
            }
        } else {
            $chainlist[$this->ID] = $this->ID;
        }
        return $chainlist;
    }

}
