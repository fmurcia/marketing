<?php

/**
 * This is the model class for table "asesor_oportunidad".
 *
 * The followings are the available columns in table 'asesor_oportunidad':
 * @property integer $ID
 * @property integer $ID_Asesor
 * @property integer $ID_Oportunidad
 * @property integer $ID_Area
 * @property integer $ID_Horizontal
 * @property integer $Sms
 *
 * The followings are the available model relations:
 * @property Asesor $iDAsesor
 */
class AsesorOportunidad extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'asesor_oportunidad';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Asesor, ID_Oportunidad, ID_Area, ID_Horizontal', 'required'),
            array('ID_Asesor, ID_Oportunidad, ID_Area, ID_Horizontal, Sms', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Asesor, ID_Oportunidad, ID_Area, ID_Horizontal, Sms', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Asesor' => 'Id Asesor',
            'ID_Oportunidad' => 'Id Oportunidad',
            'ID_Area' => 'Id Area',
            'ID_Horizontal' => 'Id Horizontal',
            'Sms' => 'Sms',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Asesor', $this->ID_Asesor);
        $criteria->compare('ID_Oportunidad', $this->ID_Oportunidad);
        $criteria->compare('ID_Area', $this->ID_Area);
        $criteria->compare('ID_Horizontal', $this->ID_Horizontal);
        $criteria->compare('Sms', $this->Sms);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AsesorOportunidad the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Retorna el ID de la telemercaderista Designada para Propiedad Horizontal
     * @return type
     */
    public function teleProHorizontal() {
        $asp = $this->model()->findAll('ID_Horizontal = 1');
        if ($asp != NULL) :
            $ids = array();
            $i = 0;
            foreach ($asp as $d) :
                $ids[$i] = $d->ID_Asesor;
                $i++;
            endforeach;
            $claves_aleatorias = array_rand($ids, 1);
            return $ids[$claves_aleatorias];
        else :
            return 1;
        endif;
    }
    
     /**
     * Retorna el # Celular de la telemercaderista Designada para Propiedad Horizontal
     * @return array();
     */
    public function teleSms() {
        $asp = $this->model()->findAll('Sms = 1');
        if ($asp != NULL) :
            $ids = array();
            $i = 0;
            foreach ($asp as $d) :
                $ids[$i] = $d->iDAsesor->Celular;
                $i++;
            endforeach;
            
            return $ids;
        else :
            return 1;
        endif;
    }
}