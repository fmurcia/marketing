<?php

/**
 * This is the model class for table "regional_agencia".
 *
 * The followings are the available columns in table 'regional_agencia':
 * @property integer $ID
 * @property integer $ID_Regional
 * @property integer $ID_Agencia
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 *
 * The followings are the available model relations:
 * @property Regional $iDRegional
 * @property Agencia $iDAgencia
 */
class RegionalAgencia extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'regional_agencia';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Regional, ID_Agencia, Estado', 'numerical', 'integerOnly' => true),
            array('Fecha_inicial, Fecha_final', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Regional, ID_Agencia, Estado, Fecha_inicial, Fecha_final', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDRegional' => array(self::BELONGS_TO, 'Regional', 'ID_Regional'),
            'iDAgencia' => array(self::BELONGS_TO, 'Agencia', 'ID_Agencia'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Regional' => 'Id Regional',
            'ID_Agencia' => 'Id Agencia',
            'Estado' => 'Estado',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Regional', $this->ID_Regional);
        $criteria->compare('ID_Agencia', $this->ID_Agencia);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RegionalAgencia the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getAgencias() {
        return RegionalAgencia::model()->with(array('iDAgencia' => array('condition' => 'iDAgencia.Estado = 1')))->findAll();
    }
}
