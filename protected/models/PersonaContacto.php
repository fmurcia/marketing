<?php

/**
 * This is the model class for table "Persona_contacto".
 *
 * The followings are the available columns in table 'Persona_contacto':
 * @property integer $ID
 * @property string $Nombre_completo
 * @property string $Tipodocumento
 * @property string $Documento
 * @property string $Direccion
 * @property string $Barrio
 * @property integer $Ciudad
 * @property string $Email
 * @property string $Telefono
 * @property string $Celular
 * @property string $Observaciones
 * @property integer $Contacto
 * @property string $Fecha_creacion
 * @property string $Hora_creacion
 * @property integer $Estado
 */
class PersonaContacto extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Persona_contacto';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Nombre_completo', 'required'),
            array('Ciudad, Contacto, Estado', 'numerical', 'integerOnly' => true),
            array('Nombre_completo', 'length', 'max' => 150),
            array('Tipodocumento, Barrio', 'length', 'max' => 45),
            array('Documento', 'length', 'max' => 20),
            array('Email', 'length', 'max' => 200),
            array('Telefono', 'length', 'max' => 50),
            array('Celular', 'length', 'max' => 40),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Nombre_completo, Tipodocumento, Documento, Direccion, Barrio, Ciudad, Email, Telefono, Celular, Observaciones, Contacto, Fecha_creacion, Hora_creacion, Estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contacto' => array(self::BELONGS_TO, 'Contacto', 'Contacto'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Nombre_completo' => 'Nombre Completo',
            'Tipodocumento' => 'Tipodocumento',
            'Documento' => 'Documento',
            'Direccion' => 'Direccion',
            'Barrio' => 'Barrio',
            'Ciudad' => 'Ciudad',
            'Email' => 'Email',
            'Telefono' => 'Telefono',
            'Celular' => 'Celular',
            'Observaciones' => 'Observaciones',
            'Contacto' => 'Contacto',
            'Fecha_creacion' => 'Fecha Creacion',
            'Hora_creacion' => 'Hora Creacion',
            'Estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Nombre_completo', $this->Nombre_completo, true);
        $criteria->compare('Tipodocumento', $this->Tipodocumento, true);
        $criteria->compare('Documento', $this->Documento, true);
        $criteria->compare('Direccion', $this->Direccion, true);
        $criteria->compare('Barrio', $this->Barrio, true);
        $criteria->compare('Ciudad', $this->Ciudad);
        $criteria->compare('Email', $this->Email, true);
        $criteria->compare('Telefono', $this->Telefono, true);
        $criteria->compare('Celular', $this->Celular, true);
        $criteria->compare('Observaciones', $this->Observaciones, true);
        $criteria->compare('Contacto', $this->Contacto);
        $criteria->compare('Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Hora_creacion', $this->Hora_creacion, true);
        $criteria->compare('Estado', $this->Estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PersonaContacto the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Se compara los parametros para encontrar coincidencias
     * @param type $campo1 Nombre o Razon Social
     * @param type $campo2 # Documento
     * @param type $campo3 Direccion
     * @param type $campo4 Telefonos
     */
    public function getCoincidencias($campo1, $campo2, $campo3, $campo4, $condicion) {
        $criterio = new CDbCriteria();
        $criterio->compare('Nombre_completo', $campo1, true, 'OR');
        $criterio->compare('Direccion', $campo3, true, 'OR');
        if ($campo2 != 0) :
            $criterio->compare('Documento', $campo2, true, 'OR');
        endif;
        if ($campo4 != 0) :
            $criterio->compare('Telefono', $campo4, true, 'OR');
            $criterio->compare('Celular', $campo4, true, 'OR');
        endif;
        $criterio->addCondition('Contacto != ' . $condicion);
        return $this->findAll($criterio);
    }
    /**
     * 
     * @param type $nombre
     * @param type $direccion
     * @param type $ciudad
     * @param type $email
     * @param type $idcontacto
     * @param type $fijo
     * @param type $celular
     */
    public function crearEncargado($nombre, $direccion, $ciudad, $email, $idcontacto, $fijo, $celular) {
        $encargados = PersonaContacto::model()->findByAttributes(array('Nombre_completo' => $nombre));

        if ($encargados == NULL) :
            $encargados = new PersonaContacto();
        endif;

        $encargados->Nombre_completo = $nombre;
        $encargados->Tipodocumento = 3;
        $encargados->Documento = 0;
        $encargados->Direccion = $direccion;
        $encargados->Ciudad = $ciudad;
        $encargados->Email = $email;
        $encargados->Observaciones = "Encargado";
        $encargados->Fecha_creacion = date('Y-m-d');
        $encargados->Hora_creacion = date('H:i:s');
        $encargados->Contacto = $idcontacto;
        $encargados->Estado = 1;
        $encargados->Barrio = '';
        $encargados->Telefono = $fijo;
        $encargados->Celular = $celular;
        $encargados->save();
    }
}
