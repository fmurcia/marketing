<?php

/**
 * This is the model class for table "Count_phone".
 *
 * The followings are the available columns in table 'Count_phone':
 * @property double $ID
 * @property integer $Contador
 * @property string $Fecha
 * @property string $promo
 */
class RegistroMovil extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Registro_movil';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Contador, Fecha, promo', 'required'),
            array('Contador', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Contador, Fecha, promo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Contador' => 'Contador',
            'Fecha' => 'Fecha',
            'promo' => 'Promo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Contador', $this->Contador);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('promo', $this->promo, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CountPhone the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * Registra los Click en el btn Llamar
     * @param type $promo
     * @return type
     */
    public function registro($promo) {
        $retorno = "";
        $count = $this->findByAttributes(array('Fecha' => date('Y-m-d'), 'promo' => strtoupper($promo)));
        if ($count == NULL) :
            $count = new RegistroMovil();
            $count->Fecha = date('Y-m-d');
            $count->Contador = 1;
            $count->promo = strtoupper($promo);
        else :
            $sumador = $count->Contador + 1;
            $count->Contador = $sumador;
        endif;

        if ($count->save()) :
            $retorno = "OK";
        else :
            $retorno = $count->getErrors();
        endif;
        
        return $retorno;
    }
}