<?php

/**
 * This is the model class for table "Cliente_venta".
 *
 * The followings are the available columns in table 'Cliente_venta':
 * @property integer $ID
 * @property integer $ID_cliente
 * @property string $Codigo
 * @property integer $Servicio
 * @property string $Estado
 * @property string $Fecha_creacion
 * @property string $Fecha_cambioEstado
 * @property string $Hora_cambioEstado
 *
 * The followings are the available model relations:
 * @property Cliente $iDCliente
 * @property DetalleServicio[] $detalleServicios
 */
class ClienteVenta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cliente_venta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_cliente, Codigo, Servicio, Estado, Fecha_creacion, Fecha_cambioEstado, Hora_cambioEstado', 'required'),
			array('ID_cliente, Servicio', 'numerical', 'integerOnly'=>true),
			array('Codigo, Estado', 'length', 'max'=>11),
			array('ID, ID_cliente, Codigo, Servicio, Estado, Fecha_creacion, Fecha_cambioEstado, Hora_cambioEstado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDCliente' => array(self::BELONGS_TO, 'Cliente', 'ID_cliente'),
			'detalleServicios' => array(self::HAS_MANY, 'DetalleServicio', 'ID_servicio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_cliente' => 'Id Cliente',
			'Codigo' => 'Codigo',
			'Servicio' => 'Servicio',
			'Estado' => 'Estado',
			'Fecha_creacion' => 'Fecha Creacion',
			'Fecha_cambioEstado' => 'Fecha Cambio Estado',
			'Hora_cambioEstado' => 'Hora Cambio Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_cliente',$this->ID_cliente);
		$criteria->compare('Codigo',$this->Codigo,true);
		$criteria->compare('Servicio',$this->Servicio);
		$criteria->compare('Estado',$this->Estado,true);
		$criteria->compare('Fecha_creacion',$this->Fecha_creacion,true);
		$criteria->compare('Fecha_cambioEstado',$this->Fecha_cambioEstado,true);
		$criteria->compare('Hora_cambioEstado',$this->Hora_cambioEstado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClienteVenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
