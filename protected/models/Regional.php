<?php

/**
 * This is the model class for table "regional".
 *
 * The followings are the available columns in table 'regional':
 * @property integer $ID_Regional
 * @property string $Descripcion
 */
class Regional extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Regional';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Regional, Descripcion', 'required'),
            array('ID_Regional, Estado', 'numerical', 'integerOnly' => true),
            array('Descripcion', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID_Regional, Descripcion', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'regionalAsesor' => array(self::HAS_MANY, 'RegionalAsesor', 'ID_Regional'),
            'regionalAgencia' => array(self::HAS_MANY, 'RegionalAgencia', 'ID_Regional'),
            "agencias" => array(self::MANY_MANY, "Agencia", "regional_agencia(ID_Regional,ID_Agencia)", "order" => "agencias.Nombre ASC"),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID_Regional' => 'Id Regional',
            'Descripcion' => 'Descripcion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID_Regional', $this->ID_Regional);
        $criteria->compare('Descripcion', $this->Descripcion, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Regional the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getRegionales() {
        return Yii::app()->db->createCommand()
                        ->setFetchMode(PDO::FETCH_OBJ)
                        ->select('r.ID_Regional as ID, r.Descripcion')
                        ->from('Regional r')
                        ->order("orden")
                        ->queryAll();
    }

    public function getAgenciasComerciales($regional) {
        $criteria = new CDbCriteria();
        if (is_array($regional)) :
            $criteria->addInCondition('ID_Regional', $regional);
        endif;
        $criteria->order = 'Descripcion ASC';
        return Regional::model()->with(array('regionalAgencias'))->findAll($criteria);
    }

    public function getAgencias() {
        $agencias = $this->agencias;
        if ($agencias != null) {
            return $agencias;
        }
        return array();
    }
}
