<?php

/**
 * This is the model class for table "Solicitud".
 *
 * The followings are the available columns in table 'Solicitud':
 * @property integer $ID
 * @property string $Num_solicitud
 * @property integer $ID_Cliente
 * @property string $Unidad
 * @property integer $Area_solicitud
 * @property integer $Motivo_solicitud
 * @property integer $Ats
 * @property string $Tipo_unidad
 * @property string $Zona_mantenimiento
 * @property integer $Adicional
 * @property string $Quien_solicita
 * @property string $Fecha
 * @property string $Hora
 * @property string $Comentarios
 * @property integer $Prioridad
 * @property integer $ID_Estado
 *
 * The followings are the available model relations:
 * @property Pruebas[] $pruebases
 * @property Rutero[] $ruteros
 * @property Estados $iDEstado
 * @property Area $areaSolicitud
 * @property Cliente $iDCliente
 * @property MotivoSolicitud $motivoSolicitud
 * @property Zonificacion[] $zonificacions
 */
class Solicitud extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Solicitud';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Ats', 'required'),
            array('ID_Cliente, Area_solicitud, Motivo_solicitud, Ats, Adicional, Prioridad, ID_Estado', 'numerical', 'integerOnly' => true),
            array('Num_solicitud', 'length', 'max' => 20),
            array('Unidad', 'length', 'max' => 18),
            array('Tipo_unidad', 'length', 'max' => 30),
            array('Zona_mantenimiento', 'length', 'max' => 10),
            array('Quien_solicita', 'length', 'max' => 200),
            array('Fecha, Hora, Comentarios', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Num_solicitud, ID_Cliente, Unidad, Area_solicitud, Motivo_solicitud, Ats, Tipo_unidad, Zona_mantenimiento, Adicional, Quien_solicita, Fecha, Hora, Comentarios, Prioridad, ID_Estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pruebases' => array(self::HAS_MANY, 'Pruebas', 'ID_Solicitud'),
            'ruteros' => array(self::HAS_MANY, 'Rutero', 'ID_Solicitud'),
            'iDEstado' => array(self::BELONGS_TO, 'Estados', 'ID_Estado'),
            'areaSolicitud' => array(self::BELONGS_TO, 'Area', 'Area_solicitud'),
            'agenciaSolicitud' => array(self::BELONGS_TO, 'Agencia', 'Ats'),
            'iDCliente' => array(self::BELONGS_TO, 'Cliente', 'ID_Cliente'),
            'motivoSolicitud' => array(self::BELONGS_TO, 'MotivoSolicitud', 'Motivo_solicitud'),
            'zonificacions' => array(self::HAS_MANY, 'Zonificacion', 'ID_Solicitud'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Num_solicitud' => 'Num Solicitud',
            'ID_Cliente' => 'Id Cliente',
            'Unidad' => 'Unidad',
            'Area_solicitud' => 'Area Solicitud',
            'Motivo_solicitud' => 'Motivo Solicitud',
            'Ats' => 'Ats',
            'Tipo_unidad' => 'Tipo Unidad',
            'Zona_mantenimiento' => 'Zona Mantenimiento',
            'Adicional' => 'Adicional',
            'Quien_solicita' => 'Quien Solicita',
            'Fecha' => 'Fecha',
            'Hora' => 'Hora',
            'Comentarios' => 'Comentarios',
            'Prioridad' => 'Prioridad',
            'ID_Estado' => 'Id Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Num_solicitud', $this->Num_solicitud, true);
        $criteria->compare('ID_Cliente', $this->ID_Cliente);
        $criteria->compare('Unidad', $this->Unidad, true);
        $criteria->compare('Area_solicitud', $this->Area_solicitud);
        $criteria->compare('Motivo_solicitud', $this->Motivo_solicitud);
        $criteria->compare('Ats', $this->Ats);
        $criteria->compare('Tipo_unidad', $this->Tipo_unidad, true);
        $criteria->compare('Zona_mantenimiento', $this->Zona_mantenimiento, true);
        $criteria->compare('Adicional', $this->Adicional);
        $criteria->compare('Quien_solicita', $this->Quien_solicita, true);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Hora', $this->Hora, true);
        $criteria->compare('Comentarios', $this->Comentarios, true);
        $criteria->compare('Prioridad', $this->Prioridad);
        $criteria->compare('ID_Estado', $this->ID_Estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Solicitud the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Retorna los Tipos y estados de Alertas
     * @param type $estado
     * @return array
     */
    public function estadosAlerta($tabla, $estado) {
        $registros = array();
        if ($tabla == 'TestSolicitud') :
            /* Tecnico en Sitio */
            $criterio = new CDbCriteria;
            $criterio->group = 'ID_Solicitud';
            $criterio->condition = 'ID_Estado=:ID_Estado';
            $criterio->params = array(':ID_Estado' => $estado);
            $registros = $tabla::model()->findAll($criterio);
        else :
            /* Agencias con Permisos */
            $areas = Area::model()->with(
                            array(
                                'areaAsesor' => array(
                                    'condition' => 'Asesor = '. Yii::app()->user->getState('id_usuario')
                                )
                            )
                    )->findAll();
            $arsol = array();

            foreach ($areas as $a):
                $arsol[] = $a->ID;
            endforeach;

            $criterio = new CDbCriteria();
            $criterio->addInCondition('Area_solicitud', $arsol);
            $criterio->addCondition('ID_Estado = ' . $estado);
            $registros = Solicitud::model()->findAll($criterio);

        endif;

        return $registros;
    }

    /**
     * Registro y Notificaciones de Alertas
     * @return type
     */
    public function registrosAlertas() {
        /* Tecnico en Sitio */
        $sitio = $this->estadosAlerta('Solicitud', 4);
        /* Solicitud en Pruebas */
        $pruebas = $this->estadosAlerta('TestSolicitud', 11);
        /* Solicitudes Finalizadas */
        $finalizadas = $this->estadosAlerta('Solicitud', 5);
        /* Solicitudes Vistas en Pruebas */
        $vistas = $this->estadosAlerta('TestSolicitud', 15);
        /* Solicitud en Mantenimiento */
        $mantenimiento = $this->estadosAlerta('Solicitud', 17);

        $registros['totalensitio'] = sizeof($sitio);
        $registros['totalenmantenimiento'] = sizeof($mantenimiento);
        $registros['totalenpruebas'] = sizeof($pruebas);
        $registros['totalvisto'] = sizeof($vistas);
        $registros['totalfinalizado'] = sizeof($finalizadas);
        $registros['total'] = sizeof($finalizadas) + sizeof($vistas) + sizeof($pruebas) + sizeof($mantenimiento) + sizeof($sitio);

        $registros['registrosensitio'] = $sitio;
        $registros['registrosenmantenimiento'] = $mantenimiento;
        $registros['registrosenpruebas'] = $pruebas;
        $registros['registrovisto'] = $vistas;
        $registros['registrofinalizado'] = $finalizadas;

        return $registros;
    }
    
    /**
     * Carga las Solicitudes segun el Area Encargada
     */
    public function getSolicitudes($id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'Area_solicitud =:area AND ID_Estado =:estado ';
        $criteria->params = array(':area' => $id, ':estado' => 0);
        $criteria->order = 'Fecha ASC';       
        return Solicitud::model()->findAll($criteria);
    }
}
