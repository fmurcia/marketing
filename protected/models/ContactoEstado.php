<?php

/**
 * This is the model class for table "Contacto_estado".
 *
 * The followings are the available columns in table 'Contacto_estado':
 * @property double $ID
 * @property integer $ID_Contacto
 * @property integer $ID_Asesor
 * @property integer $ID_Estadoproceso
 * @property integer $ID_Estadoweb
 * @property string $Fecha
 *
 * The followings are the available model relations:
 * @property Asesor $iDAsesor
 * @property EstadosWeb $iDEstadoweb
 * @property Contacto $iDContacto
 * @property EstadoVenta $iDEstadoproceso
 */
class ContactoEstado extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Contacto_estado';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Contacto, ID_Asesor, ID_Estadoproceso, ID_Estadoweb, Fecha', 'required'),
            array('ID_Contacto, ID_Asesor, ID_Estadoproceso, ID_Estadoweb', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Contacto, ID_Asesor, ID_Estadoproceso, ID_Estadoweb, Fecha', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
            'iDEstadoweb' => array(self::BELONGS_TO, 'EstadosWeb', 'ID_Estadoweb'),
            'iDContacto' => array(self::BELONGS_TO, 'Contacto', 'ID_Contacto'),
            'iDEstadoproceso' => array(self::BELONGS_TO, 'EstadoVenta', 'ID_Estadoproceso'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Contacto' => 'Id Contacto',
            'ID_Asesor' => 'Id Asesor',
            'ID_Estadoproceso' => 'Id Estadoproceso',
            'ID_Estadoweb' => 'Id Estadoweb',
            'Fecha' => 'Fecha',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('ID_Asesor', $this->ID_Asesor);
        $criteria->compare('ID_Estadoproceso', $this->ID_Estadoproceso);
        $criteria->compare('ID_Estadoweb', $this->ID_Estadoweb);
        $criteria->compare('Fecha', $this->Fecha, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContactoEstado the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @param type $idcontacto
     * @param type $idestado
     */
    public function registro($idcontacto, $idestadoproceso, $idestadoweb) {
        $contacto = new ContactoEstado();
        $contacto->ID_Asesor = Yii::app()->user->getState('id_usuario');
        $contacto->ID_Contacto = $idcontacto;
        $contacto->ID_Estadoproceso = $idestadoproceso;
        $contacto->ID_Estadoweb = $idestadoweb;
        $contacto->Fecha = date('Y-m-d H:i:s');
        $contacto->save();
    }

    /**
     * 
     * @param type $idcontacto
     */
    public function getEstadoWeb($idcontacto) {
        $criteria = new CDbCriteria();
        $criteria->select = 'ID_Estadoproceso, ID_Estadoweb';
        $criteria->addCondition('ID_Contacto = ' . $idcontacto);
        $criteria->group = 'ID_Estadoweb';
        $criteria->order = 'Fecha ASC';
        return $this->model()->findAll($criteria);
    }

    /**
     * 
     * @param type $idcontacto
     * @return type
     */
    public function getEstadoProceso($idcontacto) {
        $criteria = new CDbCriteria();
        $criteria->select = 'ID_Estadoproceso, ID_Estadoweb';
        $criteria->addCondition('ID_Contacto = ' . $idcontacto);
        $criteria->order = 'Fecha DESC';
        return $this->model()->find($criteria);
    }
}