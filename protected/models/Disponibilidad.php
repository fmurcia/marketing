<?php

/**
 * This is the model class for table "Disponibilidad".
 *
 * The followings are the available columns in table 'Disponibilidad':
 * @property integer $ID
 * @property integer $Asesor
 * @property string $Fecha_disponible
 * @property string $Hora_inicial
 * @property string $Hora_final
 * @property integer $ID_Tipo
 *
 * The followings are the available model relations:
 * @property TipoDisponibilidad $iDTipo
 * @property Asesor $asesor
 */
class Disponibilidad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Disponibilidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Asesor', 'required'),
			array('Asesor, ID_Tipo', 'numerical', 'integerOnly'=>true),
			array('Fecha_disponible, Hora_inicial, Hora_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Asesor, Fecha_disponible, Hora_inicial, Hora_final, ID_Tipo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDTipo' => array(self::BELONGS_TO, 'TipoDisponibilidad', 'ID_Tipo'),
			'asesor' => array(self::BELONGS_TO, 'Asesor', 'Asesor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Asesor' => 'Asesor',
			'Fecha_disponible' => 'Fecha Disponible',
			'Hora_inicial' => 'Hora Inicial',
			'Hora_final' => 'Hora Final',
			'ID_Tipo' => 'Id Tipo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Asesor',$this->Asesor);
		$criteria->compare('Fecha_disponible',$this->Fecha_disponible,true);
		$criteria->compare('Hora_inicial',$this->Hora_inicial,true);
		$criteria->compare('Hora_final',$this->Hora_final,true);
		$criteria->compare('ID_Tipo',$this->ID_Tipo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Disponibilidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
