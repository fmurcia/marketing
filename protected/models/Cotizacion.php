<?php

/**
 * This is the model class for table "Cotizacion".
 *
 * The followings are the available columns in table 'Cotizacion':
 * @property integer $ID
 * @property integer $ID_Grupo
 * @property string $Codigo
 * @property string $Tipo_cotizacion
 * @property string $Valor_cotizacion
 * @property integer $Contacto
 * @property integer $Servicio
 * @property integer $ID_Negociacion
 * @property integer $Plazo
 * @property integer $Forma_pago
 * @property string $Fecha_creacion
 * @property string $Hora_creacion
 * @property integer $Asesor
 * @property integer $Agencia
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 * @property integer $Ciudad
 * @property integer $Principal
 * @property string $Cotiza_relacion
 *
 * The followings are the available model relations:
 * @property Contacto $contacto
 * @property Negociacion $iDNegociacion
 * @property Servicios $servicio
 * @property GrupoCotizacion $iDGrupo
 * @property DetalleCotizacion[] $detalleCotizacions
 */
class Cotizacion extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public $attr = array(
        "Estado"
    );

    public function tableName() {
        return 'Cotizacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Grupo, Contacto, Servicio, ID_Negociacion, Plazo, Forma_pago, Asesor, Agencia, Estado, Ciudad, Principal', 'numerical', 'integerOnly' => true),
            array('Codigo', 'length', 'max' => 40),
            array('Tipo_cotizacion, Valor_cotizacion', 'length', 'max' => 45),
            array('Cotiza_relacion', 'length', 'max' => 100),
            array('Fecha_creacion, Hora_creacion, Fecha_inicial, Fecha_final', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Grupo, Codigo, Tipo_cotizacion, Valor_cotizacion, Contacto, Servicio, ID_Negociacion, Plazo, Forma_pago, Fecha_creacion, Hora_creacion, Asesor, Agencia, Estado, Fecha_inicial, Fecha_final, Ciudad, Principal, Cotiza_relacion', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contacto' => array(self::BELONGS_TO, 'Contacto', 'Contacto'),
            'negociacion' => array(self::BELONGS_TO, 'Negociacion', 'ID_Negociacion'),
            'servicio' => array(self::BELONGS_TO, 'Servicios', 'Servicio'),
            'grupo' => array(self::BELONGS_TO, 'GrupoCotizacion', 'ID_Grupo'),
            'detalles' => array(self::HAS_MANY, 'DetalleCotizacion', 'ID_Cotizacion'),
            "totales" => array(self::HAS_MANY, "TotalesCotizacion", "ID_Cotizacion"),
            'equipos' => array(self::HAS_MANY, 'DetalleCotizacion', 'ID_Cotizacion', "condition" => "servicios.Clasificacion='Elemento'"),
            'servicios' => array(self::HAS_MANY, 'DetalleCotizacion', 'ID_Cotizacion', "condition" => "servicios.Clasificacion='Servicio'"),
            'instalaciones' => array(self::HAS_MANY, 'DetalleCotizacion', 'ID_Cotizacion', "condition" => "servicios.Clasificacion='Instalacion'"),
            'adicionales' => array(self::HAS_MANY, 'DetalleCotizacion', 'ID_Cotizacion', "condition" => "servicios.Clasificacion='Adicional'"),
            "agencia" => array(self::BELONGS_TO, 'Agencia', 'Agencia'),
            "asesor" => array(self::BELONGS_TO, 'Asesor', 'Asesor'),
            "clienteventa" => array(self::HAS_ONE, 'ClienteVenta', 'ID_Cotizacion'),
            "plantillas" => array(self::HAS_MANY, 'PlantillaDocumentos', 'ID_Cotizacion'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Grupo' => 'Id Grupo',
            'Codigo' => 'Codigo',
            'Tipo_cotizacion' => 'Tipo Cotizacion',
            'Valor_cotizacion' => 'Valor Cotizacion',
            'Contacto' => 'Contacto',
            'Servicio' => 'Servicio',
            'ID_Negociacion' => 'Id Negociacion',
            'Plazo' => 'Plazo',
            'Forma_pago' => 'Forma Pago',
            'Fecha_creacion' => 'Fecha Creacion',
            'Hora_creacion' => 'Hora Creacion',
            'Asesor' => 'Asesor',
            'Agencia' => 'Agencia',
            'Estado' => 'Estado',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
            'Ciudad' => 'Ciudad',
            'Principal' => 'Principal',
            'Cotiza_relacion' => 'Cotiza Relacion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Grupo', $this->ID_Grupo);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('Tipo_cotizacion', $this->Tipo_cotizacion, true);
        $criteria->compare('Valor_cotizacion', $this->Valor_cotizacion, true);
        $criteria->compare('Contacto', $this->Contacto);
        $criteria->compare('Servicio', $this->Servicio);
        $criteria->compare('ID_Negociacion', $this->ID_Negociacion);
        $criteria->compare('Plazo', $this->Plazo);
        $criteria->compare('Forma_pago', $this->Forma_pago);
        $criteria->compare('Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Hora_creacion', $this->Hora_creacion, true);
        $criteria->compare('Asesor', $this->Asesor);
        $criteria->compare('Agencia', $this->Agencia);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);
        $criteria->compare('Ciudad', $this->Ciudad);
        $criteria->compare('Principal', $this->Principal);
        $criteria->compare('Cotiza_relacion', $this->Cotiza_relacion, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cotizacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    protected function afterValidate() {
        parent::afterValidate();
        $this->Codigo = $this->calcularCodigo();
    }

    public function getElementos() {
        $elementos = $this->detalles;
        if ($elementos != null) {
            return $elementos;
        }
        return null;
    }

    private function calcularCodigo() {
        if ($this->Codigo == "") {
            $cons = Parametros::model()->getParam("conscot");
            $code = "C" . $this->Servicio . str_pad($cons, 8, "0", STR_PAD_LEFT);
            $cons = $cons + 1;
            Parametros::model()->setParam("conscot", $cons);
        } else {
            $code = $this->Codigo;
        }
        return $code;
    }

    public function getServicios() {
        $servs = DetalleCotizacion::model()->findAllByAttributes(array(
            "ID_Cotizacion" => $this->ID,
            "Clasificacion" => "Servicio"
        ));
        return $servs;
    }

    public function getServVenta($vta = null) {
        $servlist = array();
        $servs = DetalleCotizacion::model()->findAllByAttributes(array(
            "ID_Cotizacion" => $this->ID,
            "Clasificacion" => "Servicio"
        ));

        if (!empty($servs)) {
            foreach ($servs as $serv) {
                $descripcion = "";
                if ($vta != null) {
                    $detserv = DetalleServicio::model()->findByAttributes(array(
                        "ID_servicio" => $vta,
                        "Codigo_servicio" => $serv->Codigo
                    ));
                    if ($detserv != null) {
                        $descripcion = $detserv->Descripcion;
                    }
                }
                $slist = array(
                    "ID" => $serv->ID,
                    "Codigo" => $serv->Codigo,
                    "Codigo4D" => $serv->getCodigo4d(),
                    "Nombre" => $serv->getNombreElemento(),
                    "Valor" => $serv->Valor_venta,
                    "Total" => round($serv->Valor_venta * $serv->getIVA() / 100, 0) + $serv->Valor_venta,
                    "Descripcion" => $descripcion,
                );
                $servlist[$serv->Codigo] = $slist;
            }
        }
        return $servlist;
    }

    public function getUNegocio() {
        $uneg = Servicios::model()->findByPk($this->Servicio);
        if ($uneg != null) {
            return $uneg->Nombre;
        }
        return "Sin Unidad";
    }

    public function getNegoPrinc() {
        $negopri = 99;
        $negoid = 0;
        $dets = $this->detalles;
        if (!empty($dets)) {
            foreach ($dets as $det) {
                if ($det->Clasificacion == "Elemento") {
                    $nego = Negociacion::model()->findByPk($det->ID_Negociacion);
                    if ($nego != null) {
                        if ($negopri > $nego->Prioridad) {
                            $negopri = $nego->Prioridad;
                            $negoid = $nego->ID;
                        }
                    }
                }
            }
        }
        return $negoid;
    }

    public function getPLDocs() {
        $arr = array();
        $doclist = PlantillaDocumentos::model()->findAllByAttributes(array(
            "ID_Cotizacion" => $this->ID
        ));
        if (count($doclist) > 0) {
            foreach ($doclist as $d) {
                $arr[] = $d->ID_Documento;
            }
        }
        return $arr;
    }

    public function getTipo() {
        $grupo = $this->grupo;
        if ($grupo != null) {
            return $grupo->Tipo;
        }
        return "";
    }

    ///////////////PARA CALCULOS DE TOTALES////////////////////



    public function getValEquipos() {
        $totales = $this->getTotalCotizacion();
        $sum = 0;
        if (count($totales) > 0) {
            $sum = isset($totales["equinst"]["Equipos"]["apagar"]) ? $totales["equinst"]["Equipos"]["apagar"] : 0;
        }
        return $sum;
    }

    public function getValServicios() {
        $totales = $this->getTotalCotizacion();
        $sum = 0;

        if (count($totales) > 0) {
            $servs = $totales["servicios"];
            if (count($servs) > 0) {
                foreach ($servs as $serv) {
                    $sum+= $serv["totiva"];
                }
            }
        }
        return $sum;
    }

    public function getValInstalacion() {
        $totales = $this->getTotalCotizacion();
        $sum = 0;
        if (count($totales) > 0) {
            $sum = isset($totales["equinst"]["Instalacion"]["apagar"]) ? $totales["equinst"]["Instalacion"]["apagar"] : 0;
        }
        return $sum;
    }

    public function getCuotaArriendos() {
        $sum = 0;
        $totales = TotalesCotizacion::model()->findAllByAttributes(array(
            "ID_Cotizacion" => $this->ID,
            "Clasificacion" => "Elemento",
                ), array(
            "condition" => "Valor_cuota>0",
        ));
        if (count($totales) > 0) {
            foreach ($totales as $total) {
                $sum+=$total->Valor_cuota;
            }
        }
        return $sum;
    }

    public function getComodatos() {
        $sum = 0;
        $totales = TotalesCotizacion::model()->findAllByAttributes(array(
            "ID_Cotizacion" => $this->ID,
            "Clasificacion" => "Elemento",
            "ID_Negociacion" => 3
        ));
        if (count($totales) > 0) {
            foreach ($totales as $total) {
                $sum+=$total->Precio_venta;
            }
        }
        return $sum;
    }

    public function getObsequios() {
        $sum = 0;
        $totales = TotalesCotizacion::model()->findAllByAttributes(array(
            "ID_Cotizacion" => $this->ID,
            "Clasificacion" => "Elemento",
                ), array(
            "condition" => "Dcto_desp_iva>0"
        ));
        if (count($totales) > 0) {
            foreach ($totales as $total) {
                $sum+=$total->Dcto_desp_iva;
            }
        }
        return $sum;
    }

    public function getCotizacion() {
        $grupo = $this->grupo;
        if ($grupo != null) {
            return $grupo->Codigo;
        }
        return "";
    }

    public function getGranTotal() {
        return $this->getValEquipos() + $this->getValInstalacion();
    }

    public function getTotalCotizacion() {

        $totres = array(
            "equinst" => array(),
            "arriendos" => array(),
            "servicios" => array(),
            "nocargo" => array(),
        );
        $eqbase = array(
            "nombre" => "",
            "stotal" => 0,
            "dcto" => 0,
            "dctoval" => 0,
            "totalniva" => 0,
            "iva" => 0,
            "totaliva" => 0,
            "ddi" => 0,
            "apagar" => 0,
            "cif" => 0,
        );
        $arrbase = array(
            "plazo" => 0,
            "valor" => 0,
            "cuotaniva" => 0,
            "iva" => 0,
            "cuotaiva" => 0,
            "cif" => 0
        );
        $ncbase = array(
            "nombre" => "",
            "plazo" => 0,
            "valor" => 0,
            "cif" => 0
        );
        $servbase = array(
            "nombre" => "",
            "totniva" => 0,
            "piva" => 0,
            "iva" => 0,
            "totiva" => 0,
            "cif" => 0
        );


        ///Traigo los totales///


        $totlist = $this->totales;
        if (!empty($totlist)) {
            foreach ($totlist as $tot) {
                $totales["Cotizacion"][] = $tot->ID_Cotizacion;
                $totales["UNegocio"][] = $tot->ID_Servicio;
                $totales["Negociacion"][] = $tot->ID_Negociacion;
                $totales["Clasificacion"][] = $tot->Clasificacion;
                $totales["Plazo"][] = $tot->Plazo;
                $totales["Valor_proveedor"][] = $tot->Precio_proveedor;
                $totales["Valor_CIF"][] = $tot->Precio_CIF;
                $totales["Valor_venta"][] = $tot->Precio_venta;
                $totales["Cuota"][] = $tot->Valor_cuota;
                $totales["Descuento"][] = $tot->Descuento;
                $totales["DDI"][] = $tot->Dcto_desp_iva;
                $totales["Sumar"][] = $tot->Sumar;
                $totales["Obsequio"][] = $tot->Obsequio;
                $totales["Borrar"][] = 0;
            }
        }

        if (!empty($totales)) {
//Primero equipos
            $pos1 = queryArray("Elemento", $totales["Clasificacion"]);
//Segundo ventas
            $pos2 = querySelectionArray(1, $totales["Negociacion"], $pos1);
            if (count($pos2) > 0) {
                foreach ($pos2 as $pos) {
                    if (!isset($totres["equinst"]["Equipos"])) {
                        $totres["equinst"]["Equipos"] = $eqbase;
                        $totres["equinst"]["Equipos"]["nombre"] = "Equipos";
                    }
                    $totres["equinst"]["Equipos"]["stotal"]+=$totales["Valor_venta"][$pos];
                    $totres["equinst"]["Equipos"]["dcto"] = ($totales["Descuento"][$pos] > $totres["equinst"]["Equipos"]["dcto"] ? $totales["Descuento"][$pos] : $totres["equinst"]["Equipos"]["dcto"]);
                    $totres["equinst"]["Equipos"]["ddi"]+=$totales["DDI"][$pos];
                    $totres["equinst"]["Equipos"]["cif"]+=$totales["Valor_CIF"][$pos];
                }
            }
//Tercero arriendos
            $pos3 = querySelectionArray(2, $totales["Negociacion"], $pos1);

            if (count($pos3) > 0) {
                foreach ($pos3 as $pos) {
                    if (!isset($totres["arriendos"][$totales["Plazo"][$pos]])) {
                        $totres["arriendos"][$totales["Plazo"][$pos]] = $arrbase;
                        $totres["arriendos"][$totales["Plazo"][$pos]]["plazo"] = $totales["Plazo"][$pos];
                    }
                    $totres["arriendos"][$totales["Plazo"][$pos]]["valor"]+=$totales["Valor_venta"][$pos];
                    $totres["arriendos"][$totales["Plazo"][$pos]]["cif"]+=$totales["Valor_CIF"][$pos];
                    $totres["arriendos"][$totales["Plazo"][$pos]]["cuotaniva"]+=$totales["Cuota"][$pos];
                }
            }
//Cuarto comodatos
            $pos4 = querySelectionArray(3, $totales["Negociacion"], $pos1);

            if (count($pos4) > 0) {
                foreach ($pos4 as $pos) {
                    if (!isset($totres["nocargo"]["Comodato"])) {
                        $totres["nocargo"]["Comodato"] = $ncbase;
                        $totres["nocargo"]["Comodato"]["nombre"] = "Comodato";
                    }
                    $totres["nocargo"]["Comodato"]["plazo"] = ($totres["nocargo"]["Comodato"]["plazo"] > $totales["Plazo"][$pos] ? $totres["nocargo"]["Comodato"]["plazo"] : $totales["Plazo"][$pos]);
                    $totres["nocargo"]["Comodato"]["valor"]+=$totales["Valor_venta"][$pos];
                    $totres["nocargo"]["Comodato"]["cif"]+=$totales["Valor_CIF"][$pos];
                }
            }
//Quinto obsequios

            $pos5 = querySelectionArray(4, $totales["Negociacion"], $pos1);

            if (count($pos5) > 0) {
                foreach ($pos5 as $pos) {
                    if (!isset($totres["nocargo"]["Obsequio"])) {
                        $totres["nocargo"]["Obsequio"] = $ncbase;
                        $totres["nocargo"]["Obsequio"]["nombre"] = "Obsequio";
                    }
                    $totres["nocargo"]["Obsequio"]["plazo"] = 0;
                    $totres["nocargo"]["Obsequio"]["valor"]+=$totales["Valor_venta"][$pos];
                    $totres["nocargo"]["Obsequio"]["cif"]+=$totales["Valor_CIF"][$pos];
                }
            }

//Sexto bonos

            $pos5 = querySelectionArray(5, $totales["Negociacion"], $pos1);

            if (count($pos5) > 0) {
                foreach ($pos5 as $pos) {
                    if (!isset($totres["nocargo"]["Bono"])) {
                        $totres["nocargo"]["Bono"] = $ncbase;
                        $totres["nocargo"]["Bono"]["nombre"] = "Bono";
                    }
                    $totres["nocargo"]["Bono"]["plazo"] = 0;
                    $totres["nocargo"]["Bono"]["valor"]+=$totales["Valor_venta"][$pos];
                    $totres["nocargo"]["Bono"]["cif"]+=$totales["Valor_CIF"][$pos];
                }
            }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Traer instalacion

            $pos1 = queryArray("Instalacion", $totales["Clasificacion"]);

            if (count($pos1) > 0) {
                foreach ($pos1 as $pos) {
                    if (!isset($totres["equinst"]["Instalacion"])) {
                        $totres["equinst"]["Instalacion"] = $eqbase;
                        $totres["equinst"]["Instalacion"]["nombre"] = "Instalacion";
                    }
                    if ($totales["Sumar"][$pos] == 1) {
                        $totres["equinst"]["Instalacion"]["stotal"]+=$totales["Valor_venta"][$pos];
                    }
                    $totres["equinst"]["Instalacion"]["dcto"] = ($totales["Descuento"][$pos] > $totres["equinst"]["Instalacion"]["dcto"] ? $totales["Descuento"][$pos] : $totres["equinst"]["Instalacion"]["dcto"]);
                    $totres["equinst"]["Instalacion"]["ddi"]+=$totales["DDI"][$pos];
                    $totres["equinst"]["Instalacion"]["cif"]+=$totales["Valor_CIF"][$pos];
                }
            }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Traer servicios


            $servs = DetalleCotizacion::model()->findAllByAttributes(array(
                "ID_Cotizacion" => $this->ID,
                "Clasificacion" => "Servicio"
            ));

            if (count($servs) > 0) {
                foreach ($servs as $serv) {
                    if (!isset($totres["servicios"][$serv->Codigo])) {
                        $totres["servicios"][$serv->Codigo] = $servbase;
                    }
                    $ele = DetalleLista::model()->findByPk($serv->ID_ElementoLP);
                    if ($ele != null) {
                        $totres["servicios"][$serv->Codigo]["nombre"] = $ele->getNombreElemento();
                        $totres["servicios"][$serv->Codigo]["totniva"] += $serv->Valor_venta;
                        $totres["servicios"][$serv->Codigo]["piva"] = $ele->getIVA();
                        $totres["servicios"][$serv->Codigo]["cif"] = $serv->Valor_CIF;
                    }
                }
            }
///////////FINALIZANDO CON LOS TOTALES AHORA A CALCULAR FALTANTES////////////



            $equinst = $totres["equinst"];
            $arriendos = $totres["arriendos"];
            $servicios = $totres["servicios"];

//Primero equipos + instalacion
            $n = count($equinst);
            if ($n > 0) {
                foreach ($equinst as $id => $info) {
                    $info["dctoval"] = round(($info["stotal"] * $info["dcto"]) / 100, 0);
                    $info["totalniva"] = $info["stotal"] - $info["dctoval"];
                    $info["iva"] = round(($info["totalniva"] * 16) / 100, 0);
                    $info["totaliva"] = $info["totalniva"] + $info["iva"];
                    $info["apagar"] = $info["totaliva"] - $info["ddi"];
                    $equinst[$id] = $info;
                }
            }

            $n = count($arriendos);

            if ($n > 0) {
                foreach ($arriendos as $id => $info) {
                    $info["iva"] = round(($info["cuotaniva"] * 16) / 100, 0);
                    $info["cuotaiva"] = $info["cuotaniva"] + $info["iva"];
                    $arriendos[$id] = $info;
                }
            }

            $n = count($servicios);

            if ($n > 0) {
                foreach ($servicios as $id => $info) {
                    $info["iva"] = round(($info["totniva"] * $info["piva"]) / 100, 0);
                    $info["totiva"] = $info["totniva"] + $info["iva"];
                    $servicios[$id] = $info;
                }
            }

            $totres["equinst"] = $equinst;
            $totres["arriendos"] = $arriendos;

            $totres["servicios"] = $servicios;
        }

        return $totres;
    }

    public function getDuracion() {
        return $this->Plazo;
    }

    public function enviarCotizacionA4D() {

        $cont = true;
        $result = array(
            "status" => true,
            "message" => ""
        );
        if ($this->Enviado4D == false) {
            $info = array();
            $grupo = $this->grupo;
            if ($grupo != null) {
                $contacto = $grupo->contacto;
                if ($contacto != null) {
//                    $rescto = $contacto->enviarContactoA4D();
//                    if ($rescto["status"] == true) {
                    $info["GrupoID"] = $grupo->Codigo;
                    $info["ContactoID"] = $contacto->ID;
                    $info["Contrato"] = $this->getContrato();
                    $info["CotizaID"] = $this->Codigo;
                    $info["Fecha"] = date("m/d/Y", strtotime($this->Fecha_creacion));
                    $info["AsesorID"] = $this->Asesor;
                    $info["AgenciaID"] = $this->Agencia;

                    $info["De_Compra"] = $grupo->Decision_compra;
                    //$info["Dur_Contrato"] = $grupo->Plazo;
                    $info["UNegocio"] = $this->servicio->Nombre;
                    $info["Regional"] = $this->getRegional("Codigo4D");

                    //Solo para propositos de pruebas
                    $asesor=app()->user->getState("id_usuario");
                    if($asesor==1) {
                        $info["Demo"] = "True";
                    }

                    $detalles = $this->detalles;
                    if (!empty($detalles)) {
                        $detalist = array();
                        foreach ($detalles as $detalle) {
                            $det = array();
                            $det["Elemento"] = $detalle->getRefElemento();
                            $det["Clasificacion"] = $detalle->Clasificacion;
                            $det["Cantidad"] = $detalle->Cantidad;
                            $det["VProveedor"] = $detalle->Valor_proveedor;
                            $det["CIF"] = $detalle->Valor_CIF;
                            $det["VBase"] = $detalle->Valor_base;
                            $det["VVenta"] = $detalle->Valor_venta;
                            $det["Negociacion"] = $detalle->ID_Negociacion;
                            $det["Cuotas"] = $detalle->Cuotas;
                            $det["Descuento"] = $this->getDescuento($detalle->ID_Negociacion, $detalle->Clasificacion, $detalle->Cuotas);
                            $det["DDI"] = $detalle->getDDI();
                            $detalist[] = $det;
                        }
                        $info["Detalles"] = $detalist;
                    } else {
                        $info["Detalles"] = array();
                    }
//                    } else {
//                        $result["status"] = false;
//                        $result["message"] = $rescto["message"];
//                        $cont = false;
//                    }
                } else {
                    $result["status"] = false;
                    $result["message"] = "Esta cotizacion no esta asociada a un contacto";
                    $cont = false;
                }
            } else {
                $result["status"] = false;
                $result["message"] = "Esta cotizacion no esta asociada a ningun grupo";
                $cont = false;
            }

            if ($cont == true) {
                $params = array(
                    "cotizacion" => json_encode($info),
                );

                $ruta = $this->getRegional("Conexion4D");
                if ($ruta != null) {
                    $ruta.="/4DAction/VW_RecibirCotizacion"; //Ruta del metodo en 4D
                    $response = $this->conexion($ruta, $params, "POST");
                    if ($response["status"] == true) {
                        $info = json_decode($response["message"], true);
                        if ($info["status"] == true) {
                            $this->Enviado4D = true;
                            if ($this->save()) {
                                $grupo = $this->grupo;
                                $grupo->Editable = 0;
                                $grupo->save();
                                $result = array(
                                    "status" => true,
                                    "message" => "OK",
                                );
                            } else {
                                $result = array(
                                    "status" => false,
                                    "message" => "No se pudo guardar el cambio de estado 4D",
                                );
                            }
                        } else {
                            $result = array(
                                "status" => false,
                                "message" => $info["error"],
                            );
                        }
                    } else {
                        $result = array(
                            "status" => false,
                            "message" => $response["message"],
                        );
                    }
                } else {
                    $result = array(
                        "status" => false,
                        "message" => "La regional '" . $this->getRegional() . "' no tiene ruta de conexion",
                    );
                }
            }
        } else {
            $result = array(
                "status" => true,
                "message" => "Ya fue enviado a 4D",
            );
        }
        return $result;
    }

    public function getRegional($field = "") {
        $agencia = $this->agencia;
        if ($agencia != null) {
            return $agencia->getRegional(null, $field);
        }
        return null;
    }

    public function getDescuento($nego, $clas, $plazo) {
        return TotalesCotizacion::model()->getDescuento($this->ID, $this->Servicio, $nego, $clas, $plazo);
    }

    public function getContrato() {
        $venta = $this->clienteventa;
        if (!empty($venta)) {
            return $venta->getContrato();
        }
        return null;
    }

}
