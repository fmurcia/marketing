<?php

/**
 * This is the model class for table "Rbac".
 *
 * The followings are the available columns in table 'Rbac':
 * @property integer $ID
 * @property integer $ID_Cargo
 * @property integer $ID_Task
 * @property integer $ID_Accion
 * @property string $Rbac
 *
 * The followings are the available model relations:
 * @property Acciones $iDAccion
 * @property Cargos $iDCargo
 * @property Task $iDTask
 */
class Rbac extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Rbac';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_Cargo, ID_Task, ID_Accion, Rbac', 'required'),
			array('ID_Cargo, ID_Task, ID_Accion', 'numerical', 'integerOnly'=>true),
			array('Rbac', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, ID_Cargo, ID_Task, ID_Accion, Rbac', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDAccion' => array(self::BELONGS_TO, 'Acciones', 'ID_Accion'),
			'iDCargo' => array(self::BELONGS_TO, 'Cargos', 'ID_Cargo'),
			'iDTask' => array(self::BELONGS_TO, 'Task', 'ID_Task'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_Cargo' => 'Id Cargo',
			'ID_Task' => 'Id Task',
			'ID_Accion' => 'Id Accion',
			'Rbac' => 'Rbac',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_Cargo',$this->ID_Cargo);
		$criteria->compare('ID_Task',$this->ID_Task);
		$criteria->compare('ID_Accion',$this->ID_Accion);
		$criteria->compare('Rbac',$this->Rbac,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rbac the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
