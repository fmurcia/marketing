<?php

/**
 * This is the model class for table "Elemento".
 *
 * The followings are the available columns in table 'Elemento':
 * @property integer $ID
 * @property string $Categoria
 * @property string $Conexion
 * @property string $Ambiente
 * @property string $Clasificacion
 * @property string $Nombre
 * @property string $Codigo
 * @property string $Referencia
 * @property integer $Servicio
 * @property string $Imagen
 * @property string $Contenido_web
 * @property integer $Orden
 * @property double $IVA
 * @property integer $Estado
 * @property string $Fecha_inicial
 * @property string $Fecha_final
 * @property string $seudonimo
 * @property string $Imagen2
 *
 * The followings are the available model relations:
 * @property DetalleElemento[] $detalleElementos
 * @property DetalleElemento[] $detalleElementos1
 * @property Servicios $servicio
 * @property DetalleLista[] $detalleListas
 * @property ElementoGrupo[] $elementoGrupos
 * @property PromocionElemento[] $promocionElementos
 * @property ServicioElemento[] $servicioElementos
 */
class Elemento extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Elemento';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Clasificacion, Nombre, Referencia, Servicio, Categoria, Ambiente, Conexion', 'required'),
            array('Servicio, Orden, Estado', 'numerical', 'integerOnly' => true),
            array('IVA', 'numerical'),
            array('Categoria, Conexion, Ambiente, Imagen, Imagen2', 'length', 'max' => 45),
            array('Clasificacion', 'length', 'max' => 100),
            array('Nombre', 'length', 'max' => 500),
            array('Codigo', 'length', 'max' => 20),
            array('Referencia', 'length', 'max' => 200),
            array('seudonimo', 'length', 'max' => 50),
            array('Contenido_web, Fecha_inicial, Fecha_final, Multiplicar', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Categoria, Conexion, Ambiente, Clasificacion, Nombre, Codigo, Referencia, Servicio, Imagen, Contenido_web, Orden, IVA, Estado, Fecha_inicial, Fecha_final, seudonimo, Imagen2', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'detallesH' => array(self::HAS_MANY, 'DetalleElemento', 'ID_ElementoH'),
            'detallesP' => array(self::HAS_MANY, 'DetalleElemento', 'ID_ElementoP'),
            'servicio' => array(self::BELONGS_TO, 'Servicios', 'Servicio'),
            'detalleListas' => array(self::HAS_MANY, 'DetalleLista', 'ID_Elemento'),
            'elementoGrupos' => array(self::HAS_MANY, 'ElementoGrupo', 'ID_Elemento'),
            'promocionElementos' => array(self::HAS_MANY, 'PromocionElemento', 'ID_Elemento'),
            'servicioElementos' => array(self::HAS_MANY, 'ServicioElemento', 'ID_Elemento'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Categoria' => 'Grupo',
            'Conexion' => 'Conexion',
            'Ambiente' => 'Ambiente',
            'Clasificacion' => 'Tipo',
            'Nombre' => 'Nombre',
            'Codigo' => 'Codigo',
            'Referencia' => 'Referencia 4D',
            'Servicio' => 'U. Negocio',
            'Imagen' => 'Imagen',
            'Contenido_web' => 'Descripcion',
            'Orden' => 'Orden',
            'IVA' => 'Iva',
            'Estado' => 'Activo',
            'Fecha_inicial' => 'Fecha Inicial',
            'Fecha_final' => 'Fecha Final',
            'seudonimo' => 'Seudonimo',
            'Imagen2' => 'Imagen2',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Categoria', $this->Categoria, true);
        $criteria->compare('Conexion', $this->Conexion, true);
        $criteria->compare('Ambiente', $this->Ambiente, true);
        $criteria->compare('Clasificacion', $this->Clasificacion, true);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Codigo', $this->Codigo, true);
        $criteria->compare('Referencia', $this->Referencia, true);
        $criteria->compare('Servicio', $this->Servicio);
        $criteria->compare('Imagen', $this->Imagen, true);
        $criteria->compare('Contenido_web', $this->Contenido_web, true);
        $criteria->compare('Orden', $this->Orden);
        $criteria->compare('IVA', $this->IVA);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);
        $criteria->compare('seudonimo', $this->seudonimo, true);
        $criteria->compare('Imagen2', $this->Imagen2, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Elemento the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getUnidadNegocio() {
        if ($this->servicio != null) {
            return $this->servicio->Nombre;
        }
        return "[Sin servicio asociado]";
    }

    public function getNombreElemento() {
        return $this->Nombre;
    }

    public function getNegociacion($nego) {
        $neg = Negociacion::model()->findByPk($nego);
        if ($neg != null) {
            return $neg->Nombre;
        }
        return "No aplica";
    }

    public function getGrupos() {
        $grupos = Yii::app()->db->createCommand()
                ->setFetchMode(PDO::FETCH_OBJ)
                ->select('distinct(e.Categoria)')
                ->from('Elemento e')
                ->queryAll();
        return $grupos;
    }

    public function getConexion() {
        $conexiones = Yii::app()->db->createCommand()
                ->setFetchMode(PDO::FETCH_OBJ)
                ->select('distinct(e.Conexion)')
                ->from('Elemento e')
                ->queryAll();
        return $conexiones;
    }

    ////Autocrear el codigo de elementos

    protected function afterValidate() {
        parent::afterValidate();
        $this->Codigo = $this->calcularCodigo();
    }

    private function calcularCodigo() {
        if ($this->Codigo == "") {
            $cons = Parametros::model()->getParam("consele");
            $code = "ELE" . str_pad($cons, 8, "0", STR_PAD_LEFT);
            $cons = $cons + 1;
            Parametros::model()->setParam("consele", $cons);
        } else {
            $code = $this->Codigo;
        }
        return $code;
    }

    public function checkDelRules() {
        $valid = true;
        //Primero, tiene detalles
        $detsH = $this->detallesH;
        $detsP = $this->detallesH;
        if ($detsH != null || $detsP != null) {
            return false;
        }
        $detslist = $this->detalleListas;
        if ($detslist != null) {
            return false;
        }
        $detspromo = $this->promocionElementos;
        if ($detspromo != null) {
            return false;
        }

        $detscot = DetalleCotizacion::model()->findByAttributes(array(
            "Codigo" => $this->Codigo
        ));

        if (count($detscot) > 0) {
            return false;
        }

        return true;
    }

    public function getElementos() {
        $elementos = Yii::app()->db->createCommand()
                ->setFetchMode(PDO::FETCH_OBJ)
                ->select('e.ID, e.Nombre as Nombre, s.Nombre as Servicio')
                ->from('Elemento e')
                ->leftJoin("Servicios s", "s.ID=e.Servicio")
                ->queryAll();
        return CHtml::listData($elementos, "ID", "Nombre", 'Servicio');
    }

    public function getServConf() {
        $config=array(
            "Formato"=>"",
            "Obligatorio"=>0,
            "Descripcion"=>""
        ); 
        if ($this->Clasificacion == "Servicio") {
            $serv = Servicios::model()->findByAttributes(array(
                "Codigo_4D" => $this->Codigo4D
            ));
            if ($serv != null) {
                $config["Formato"]=$serv->Formato;
                $config["Obligatorio"]=$serv->Obligatorio;
                $config["Descripcion"]=$serv->Descr_campo;
            }
        }
        return $serv;
    }

}
