<?php

/**
 * This is the model class for table "Georeferencia".
 *
 * The followings are the available columns in table 'Georeferencia':
 * @property integer $ID
 * @property integer $ID_Usuario
 * @property string $Imei
 * @property string $Latitud
 * @property string $Longitud
 * @property string $Precision
 * @property integer $Bateria
 * @property string $Fecha
 * @property string $Hora
 * @property integer $Estado
 * @property string $VersionApp
 * @property integer $GPS
 *
 * The followings are the available model relations:
 * @property Asesor $iDUsuario
 * @property Estados $estado
 */
class Georeferencia extends CActiveRecord {

    public $Asesor;
    public $status;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Georeferencia';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Usuario, Imei, Latitud, Longitud, Precision, Fecha, Hora, Estado, VersionApp', 'required'),
            array('ID_Usuario, Bateria, Estado, GPS', 'numerical', 'integerOnly' => true),
            array('Imei, Latitud, Longitud, Precision, Asesor', 'length', 'max' => 100),
            array('VersionApp', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Usuario, Imei, Latitud, Longitud, Precision, Bateria, Fecha, Hora, Estado, VersionApp, GPS, Asesor, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDUsuario' => array(self::BELONGS_TO, 'Asesor', 'ID_Usuario'),
            'estado' => array(self::BELONGS_TO, 'Estados', 'Estado'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Usuario' => 'Id Usuario',
            'Imei' => 'Imei',
            'Latitud' => 'Latitud',
            'Longitud' => 'Longitud',
            'Precision' => 'Precision',
            'Bateria' => 'Bateria',
            'Fecha' => 'Fecha',
            'Hora' => 'Hora',
            'Estado' => 'Estado',
            'VersionApp' => 'Version App',
            'GPS' => 'Gps',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Usuario', $this->ID_Usuario);
        $criteria->compare('Imei', $this->Imei, true);
        $criteria->compare('Latitud', $this->Latitud, true);
        $criteria->compare('Longitud', $this->Longitud, true);
        $criteria->compare('Precision', $this->Precision, true);
        $criteria->compare('Bateria', $this->Bateria);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Hora', $this->Hora, true);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('VersionApp', $this->VersionApp, true);
        $criteria->compare('GPS', $this->GPS);
        $criteria->with = array(
            'iDUsuario' => array(
                'condition' => 'Nombre LIKE "%' . $this->Asesor . '%"'
            ),
            'estado' => array(
                'condition' => 'Descripcion LIKE "%' . $this->status . '%"'
            )
        );
        
        $area = Area::model()->with(array('areaAsesor' => array('condition' => 'Asesor = ' . Yii::app()->user->getState('id_usuario'))))->findAll();
        $asesores = array();
        foreach ($area as $a) :
            $asesores = Asesor::model()->with(array('asesorAreas' => array('condition' => 'Area = ' . $a->ID)))->findAll();
        endforeach;

        $cadena = array();
        foreach ($asesores as $a) :
            $cadena[] = $a->ID;
        endforeach;

        $criteria->addInCondition('ID_Usuario', $cadena);
        
        $criteria->addCondition('Fecha = "' . date('Y-m-d') . '"');
        $criteria->order = 'Hora DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 5,
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Georeferencia the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Georeferencia las Posiciones de los Asesores en Google Maps
     * @param type $height
     * @return \EGMap
     */
    public function geolocalizacion($height, $controlador) {
        Yii::import('ext.EGMap.*');

        $html_1 = '<div style="color:#000;border: 1px solid black; font-size: 16px; line-height: 16px; text-align:center; background: #f8f8f8; margin-top: 5px; padding: 5px;">
                   <img src="' . $controlador->assets . '/wifi.png' . '"></img><br>'
                . '<b>Telesentinel Ltda,</b><br> '
                . 'Carrera 7 # 32 - 29, <br> '
                . 'Bogotá, Colombia </div>';

        $icon_telesentinel = $controlador->assets . '/wifi.png';
        $icon_bateria = $controlador->assets . '/chuffle.png';
        $icon_gps = $controlador->assets . '/radar.png';
        $icon_inactivo = $controlador->assets . '/skull.png';

        $gMap = new EGMap();
        $gMap->setJsName('test_map');
        $gMap->width = '100%';
        $gMap->height = $height;
        $gMap->zoom = 11;

        /* Ubicacion Central de Busqueda - Bogotá */
        $gMap->setCenter(4.6188876, -74.0683687);

        // Create marker
        $icon_telesentinel = new EGMapMarkerImage($icon_telesentinel);

        $info_marker = new EGMapInfoBox($html_1);
        $info_marker->pixelOffset = new EGMapSize('-120', '-120');
        $info_marker->maxWidth = 0;
        $info_marker->boxStyle = array('width' => '"232px"', 'height' => '"160px"');
        $info_marker->closeBoxMargin = '"10px 4px 4px 4px"';
        $info_marker->infoBoxClearance = new EGMapSize(1, 1);
        $info_marker->enableEventPropagation = '"floatPane"';

        $marker = new EGMapMarker(4.6191354, -74.0698282, array('title' => 'Edificio Fenix Telesentinel', 'icon' => $icon_telesentinel, 'draggable' => false), 'marker');
        $marker->addHtmlInfoBox($info_marker);
        $gMap->addMarker($marker);

        $area = Area::model()->with(array('areaAsesor' => array('condition' => 'Asesor = ' . Yii::app()->user->getState('id_usuario'))))->findAll();
        $asesores = array();
        foreach ($area as $a) :
            $asesores = Asesor::model()->with(array('asesorAreas' => array('condition' => 'Area = ' . $a->ID)))->findAll();
        endforeach;

        $cadena = array();
        foreach ($asesores as $a) :
            $cadena[] = $a->ID;
        endforeach;

        $criteria = new CDbCriteria();
        $criteria->select = 'ID_Usuario, Latitud, Longitud, Estado, Bateria, Fecha, Hora';
        $criteria->addCondition('Fecha = :Fecha');
        $criteria->params = array('Fecha' => date('Y-m-d'));
        $criteria->addInCondition('ID_Usuario', $cadena);
        $registros = Georeferencia::model()->findAll($criteria);

        foreach ($registros as $r) :

            if ($r->Estado == 21) :
                if ($r->Bateria >= 16) :
                    $icon_tecnico = new EGMapMarkerImage($icon_gps);
                else :
                    $icon_tecnico = new EGMapMarkerImage($icon_bateria);
                endif;
            else :
                $icon_tecnico = new EGMapMarkerImage($icon_inactivo);
            endif;

            $html_2 = '<div style="color:#000;border: 1px solid black; font-size: 16px; line-height: 16px; text-align:center; background: #f8f8f8; margin-top: 5px; padding: 5px;">'
                    . '<b>'.$r->iDUsuario->Nombre.', </b><br> '
                    . 'Bateria : '.$r->Bateria.'%, <br> '
                    . 'Ult. Reporte : '.$r->Fecha.' '.$r->Hora.', <br> '
                    . 'Bogotá, Colombia </div>';

            $marker = new EGMapMarker($r->Latitud, $r->Longitud, array('title' => $r->iDUsuario->Nombre, 'icon' => $icon_tecnico, 'draggable' => false, 'shadow' => true/* , 'animation'=>'google.maps.Animation.BOUNCE' */));

            $info_box = new EGMapInfoBox($html_2);
            $info_box->pixelOffset = new EGMapSize('-120', '-120');
            $info_box->maxWidth = 0;
            $info_box->boxStyle = array('width' => '"232px"', 'height' => '"160px"');
            $info_box->closeBoxMargin = '"10px 4px 4px 4px"';
            $info_box->infoBoxClearance = new EGMapSize(1, 1);
            $info_box->enableEventPropagation = '"floatPane"';

            $marker->addHtmlInfoBox($info_box);
            $gMap->addMarker($marker);
            
        endforeach;

        return $gMap;
    }

}
