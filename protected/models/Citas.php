<?php

/**
 * This is the model class for table "Citas".
 *
 * The followings are the available columns in table 'Citas':
 * @property double $ID
 * @property string $Fecha
 * @property integer $ID_Usuario
 * @property integer $ID_Asesor
 * @property integer $ID_Contacto
 * @property integer $ID_Oportunidad
 *
 * The followings are the available model relations:
 * @property Asesor $iDUsuario
 * @property Asesor $iDAsesor
 */
class Citas extends CActiveRecord {

    public $Total;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Citas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Fecha, ID_Usuario, ID_Asesor, ID_Contacto, ID_Oportunidad', 'required'),
            array('ID_Usuario, ID_Asesor, ID_Contacto, ID_Oportunidad', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Fecha, ID_Usuario, ID_Asesor, ID_Contacto, ID_Oportunidad', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDUsuario' => array(self::BELONGS_TO, 'Asesor', 'ID_Usuario'),
            'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Fecha' => 'Fecha',
            'ID_Usuario' => 'Id Usuario',
            'ID_Asesor' => 'Id Asesor',
            'ID_Contacto' => 'Id Contacto',
            'ID_Oportunidad' => 'Id Oportunidad',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('ID_Usuario', $this->ID_Usuario);
        $criteria->compare('ID_Asesor', $this->ID_Asesor);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('ID_Oportunidad', $this->ID_Oportunidad);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Citas the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * 
     * @param type $idcontacto
     * @param type $fecha
     * @param type $asesor
     * @param type $idoportunidad
     */
    public function registro($idcontacto, $fecha, $asesor, $idoportunidad = 0) {
        if($idoportunidad == 0) :
            $cita = $this->model()->findByAttributes(array('ID_Contacto' => $idcontacto, 'Fecha' => $fecha));
        else :
            $cita = $this->model()->findByAttributes(array('ID_Oportunidad' => $idoportunidad, 'Fecha' => $fecha));
        endif;
        
        if ($cita == NULL) :
            $cita = new Citas();
        endif;
        $cita->ID_Oportunidad = $idoportunidad;
        $cita->ID_Contacto = $idcontacto;
        $cita->ID_Asesor = $asesor;
        $cita->ID_Usuario = Yii::app()->user->getState('id_usuario');
        $cita->Fecha = $fecha;
        $cita->save();
    }
}
