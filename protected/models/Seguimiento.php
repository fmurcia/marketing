<?php

/**
 * This is the model class for table "Seguimiento".
 *
 * The followings are the available columns in table 'Seguimiento':
 * @property integer $ID
 * @property integer $ID_Usuario
 * @property integer $ID_Solicitud
 * @property integer $ID_Accion
 * @property string $Imei
 * @property string $Latitud
 * @property string $Longitud
 * @property string $Precision
 * @property string $Fecha
 * @property string $Hora
 * @property string $Descripcion
 * @property integer $ID_Documento
 * @property integer $En4D
 * @property string $Timebd
 *
 * The followings are the available model relations:
 * @property AccionSeguimiento $iDAccion
 * @property Asesor $iDUsuario
 */
class Seguimiento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Seguimiento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Timebd', 'required'),
			array('ID_Usuario, ID_Solicitud, ID_Accion, ID_Documento, En4D', 'numerical', 'integerOnly'=>true),
			array('Imei', 'length', 'max'=>50),
			array('Latitud, Longitud, Precision', 'length', 'max'=>45),
			array('Fecha, Hora, Descripcion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, ID_Usuario, ID_Solicitud, ID_Accion, Imei, Latitud, Longitud, Precision, Fecha, Hora, Descripcion, ID_Documento, En4D, Timebd', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDAccion' => array(self::BELONGS_TO, 'AccionSeguimiento', 'ID_Accion'),
			'iDUsuario' => array(self::BELONGS_TO, 'Asesor', 'ID_Usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_Usuario' => 'Id Usuario',
			'ID_Solicitud' => 'Id Solicitud',
			'ID_Accion' => 'Id Accion',
			'Imei' => 'Imei',
			'Latitud' => 'Latitud',
			'Longitud' => 'Longitud',
			'Precision' => 'Precision',
			'Fecha' => 'Fecha',
			'Hora' => 'Hora',
			'Descripcion' => 'Descripcion',
			'ID_Documento' => 'Id Documento',
			'En4D' => 'En4 D',
			'Timebd' => 'Timebd',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_Usuario',$this->ID_Usuario);
		$criteria->compare('ID_Solicitud',$this->ID_Solicitud);
		$criteria->compare('ID_Accion',$this->ID_Accion);
		$criteria->compare('Imei',$this->Imei,true);
		$criteria->compare('Latitud',$this->Latitud,true);
		$criteria->compare('Longitud',$this->Longitud,true);
		$criteria->compare('Precision',$this->Precision,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Hora',$this->Hora,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('ID_Documento',$this->ID_Documento);
		$criteria->compare('En4D',$this->En4D);
		$criteria->compare('Timebd',$this->Timebd,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Seguimiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
