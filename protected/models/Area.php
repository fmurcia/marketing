<?php

/**
 * This is the model class for table "Area".
 *
 * The followings are the available columns in table 'Area':
 * @property integer $ID
 * @property string $Codigo
 * @property string $Nombre
 * @property integer $Activo
 *
 * The followings are the available model relations:
 * @property Solicitud[] $solicituds
 * @property AreaMotivo[] $areaMotivos
 */
class Area extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Area';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Activo', 'numerical', 'integerOnly'=>true),
			array('Codigo', 'length', 'max'=>5),
			array('Nombre', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Codigo, Nombre, Activo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                    'solicituds' => array(self::HAS_MANY, 'Solicitud', 'Area_solicitud'),
                    'areaMotivos' => array(self::HAS_MANY, 'AreaMotivo', 'ID_Area'),
                    'areaAsesor' => array(self::HAS_MANY, 'AsesorArea', 'Area'),
            );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Codigo' => 'Codigo',
			'Nombre' => 'Nombre',
			'Activo' => 'Activo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Codigo',$this->Codigo,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Activo',$this->Activo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Area the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
