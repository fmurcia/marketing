<?php

/**
 * This is the model class for table "Mensajeria".
 *
 * The followings are the available columns in table 'Mensajeria':
 * @property integer $ID
 * @property integer $ID_Remitente
 * @property integer $ID_Destinatario
 * @property string $Texto
 * @property string $TipoMensaje
 * @property integer $ContadorEnvios
 * @property integer $ID_Contacto
 * @property string $Fecha
 * @property string $Hora
 *
 * The followings are the available model relations:
 * @property Asesor $iDRemitente
 * @property Asesor $iDDestinatario
 * @property Contacto $iDContacto
 */
class Mensajeria extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Mensajeria';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('ID_Remitente, ID_Destinatario', 'required'),
            array('ID_Remitente, ID_Destinatario, ContadorEnvios, ID_Contacto', 'numerical', 'integerOnly' => true),
            array('TipoMensaje', 'length', 'max' => 45),
            array('Texto, Fecha, Hora', 'safe'),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('ID, ID_Remitente, ID_Destinatario, Texto, TipoMensaje, ContadorEnvios, ID_Contacto, Fecha, Hora', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDRemitente' => array(self::BELONGS_TO, 'Asesor', 'ID_Remitente'),
            'iDDestinatario' => array(self::BELONGS_TO, 'Asesor', 'ID_Destinatario'),
            'iDContacto' => array(self::BELONGS_TO, 'Contacto', 'ID_Contacto'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Remitente' => 'Id Remitente',
            'ID_Destinatario' => 'Id Destinatario',
            'Texto' => 'Texto',
            'TipoMensaje' => 'Tipo Mensaje',
            'ContadorEnvios' => 'Contador Envios',
            'ID_Contacto' => 'Id Contacto',
            'Fecha' => 'Fecha',
            'Hora' => 'Hora',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Remitente', $this->ID_Remitente);
        $criteria->compare('ID_Destinatario', $this->ID_Destinatario);
        $criteria->compare('Texto', $this->Texto, true);
        $criteria->compare('TipoMensaje', $this->TipoMensaje, true);
        $criteria->compare('ContadorEnvios', $this->ContadorEnvios);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Hora', $this->Hora, true);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Mensajeria the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function logMensajeria($comercial, $opcion, $texto, $idcontacto) {
        $registro = new Mensajeria();
        $registro->ID_Destinatario = $comercial;
        $registro->ID_Remitente = Yii::app()->user->getState('id_usuario');
        $registro->Texto = $texto;
        $registro->TipoMensaje = $opcion;
        $registro->ContadorEnvios = 1;
        $registro->ID_Contacto = $idcontacto;
        $registro->Fecha = date('Y-m-d');
        $registro->Hora = date('H:i:s');
        $registro->save();
    }
}
