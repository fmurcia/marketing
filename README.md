# TELEMARK

Aplicacion hecha en Yii para la administracion de los registros de los clientes y campañas digitales

## Comenzando 🚀

Instrucciones para su descarga y uso


## Descarga 🔧

git clone https://fmurcia@bitbucket.org/fmurcia/marketing.git


## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [PHP7.0](https://php.net/manual/es/intro-whatis.php) - El lenguaje web usado
* [YiiFramework](https://www.yiiframework.com/) - El Framework web usado
* [APACHE](https://maven.apache.org/) - Manejador de dependencias
* [MYSQL](https://www.mysql.com/) - Motor de BD

## Versionado 📌

* [GIT](https://git-scm.com/) - Manejador de Versiones

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Team Telesetinel** - *Desarrolladores* - [Telesentinel](https://www.telesentinel.com)

## Licencia 📄

Este proyecto está bajo la Licencia (Telesentinel Ltda) 

## Expresiones de Gratitud 🎁