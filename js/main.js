/**
 * Funcion Ajax sin Div 
 * @param {type} url
 * @param {type} destino
 * @returns {undefined}
 */
function loadAjaxAlert(url, destino)
{
    jQuery.ajax({
        'type': 'POST',
        'url': url,
        'success':
                function (respuesta) {
                    $("#" + destino).hide();
                    $("#" + destino).html(respuesta);
                    $("#" + destino).fadeIn("fast");
                },
        'cache': false,
        'data': jQuery(this).parents("form").serialize()
    });
}