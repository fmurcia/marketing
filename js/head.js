function addEncargado() {
    $.ajax({
        type: "post",
        url: "/telemark/index.php/contacto/addEncargado",
        beforeSend: function () {
        },
        success: function (data) {
            $('#encargados').append(data);
        }
    });
}

function delEncargado() {
    if ($('.encargado').length > 1) {
        swal({
            title: "¿Confirma la eliminacion del encargado?",
            text: "Este proceso es Irreversible",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Claro!",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        swal("¡Hecho!", "Registro Desactivado", "success");
                        $.ajax({
                            type: "post",
                            url: "/telemark/index.php/contacto/delEncargado",
                            data: {
                                "id": $('.encargado').last().find('.pcontid').val()
                            },
                            beforeSend: function () {
                            },
                            success: function (data) {
                                if (data == "ok") {
                                    $('.encargado').last().remove();
                                }
                            }

                        });
                    }
                });
    }
}

function adicionarEncargado(posicion)
{
    var nombre = $("#PersonaContacto_" + posicion + "_Nombre_completo").val();
    var documento = $("#PersonaContacto_" + posicion + "_Documento").val();
    var direccion = $("#PersonaContacto_" + posicion + "_Direccion").val();
    var telefono = $("#PersonaContacto_" + posicion + "_Telefono").val();
    var email = $("#PersonaContacto_" + posicion + "_Email").val();
    var ciudad = $("#PersonaContacto_" + posicion + "_Ciudad").val();

    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/contacto/upEncargado',
        'data': 'nombre=' + nombre + '&documento=' + documento + '&direccion=' + direccion
                + '&telefono=' + telefono + '&email=' + email + '&ciudad=' + ciudad + '&idcontacto=' + $('#idcontactosend').val(),
        'success':
                function (respuesta) {
                    console.log(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}


function mostrarAgenda()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/site/agendados',
        'success': function (respuesta) {
            if(respuesta.length > 2) {
                $('#modalAlerta').modal('show');
                jQuery.ajax({
                    'type': 'POST',
                    'url': '/telemark/index.php/admage/viewage',
                    'data': 'items=' + respuesta,
                    'success':
                            function (data) {
                                $("#modalcuerpo").html(data);
                            },
                    'error': function (m, e, a) {
                        swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
                    },
                    'cache': false
                });
                return false;
            }
        },
        'cache': false
    });
    return false;
}

function mostrarAgendaOp()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/site/agendadosop',
        'success': function (respuesta) {
            if(respuesta.length > 2) {
                $('#modalAlerta').modal('show');
                jQuery.ajax({
                    'type': 'POST',
                    'url': '/telemark/index.php/admage/viewageop',
                    'data': 'items=' + respuesta,
                    'success':
                            function (data) {
                                $("#modalcuerpo").html(data);
                            },
                    'error': function (m, e, a) {
                        swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
                    },
                    'cache': false
                });
                return false;
            }
        },
        'cache': false
    });
    return false;
}


function addGestion() {
    $.ajax({
        type: "post",
        url: "/telemark/index.php/contacto/addGestion",
        beforeSend: function () {
        },
        success: function (data) {
            $('#divgestioncomercial').append(data);
        }
    });
}

function delGestion() {
    if ($('.divgestioncomercial').length > 1) {
        swal({
            title: "¿Confirma la eliminacion del Registro?",
            text: "Este proceso es Irreversible",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Claro!",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        swal("¡Hecho!", "Registro Desactivado", "success");
                        $.ajax({
                            type: "post",
                            url: "/telemark/index.php/contacto/delGestion",
                            data: {
                                "id": $('.divgestioncomercial').last().find('.pcontid').val()
                            },
                            beforeSend: function () {
                            },
                            success: function (data) {
                                if (data == "ok") {
                                    $('.divgestioncomercial').last().remove();
                                }
                            }

                        });
                    }
                });
    }
}

function adicionarGestion(posicion)
{
    var contrato = $("#GestionComercial_" + posicion + "_Contrato").val();
    var fecha = $("#GestionComercial_" + posicion + "_Fecha_visita").val();
    var valoreq = $("#GestionComercial_" + posicion + "_Valor_equipos").val();
    var valorin = $("#GestionComercial_" + posicion + "_Valor_instalacion").val();
    var valorse = $("#GestionComercial_" + posicion + "_Valor_servicio").val();
    var decision = $("#GestionComercial_" + posicion + "_Decision_compra").val();
    var comentarios = $("#GestionComercial_" + posicion + "_Comentarios").val();
    var servicio = $("#GestionComercial_" + posicion + "_Servicio").val();

    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/contacto/upGestion',
        'data': 'contrato=' + contrato + '&fecha=' + fecha + '&valoreq=' + valoreq + '&posicion=' + posicion
                + '&valorin=' + valorin + '&valorse=' + valorse + '&decision=' + decision + '&servicio=' + servicio
                + '&idcontacto=' + $('#idcontactosend').val() + '&comentarios=' + comentarios,
        'success':
                function (respuesta) {
                    console.log(respuesta);
                    location.reload();
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function descartarGestion(idgestioncomercial)
{
    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/contacto/descgestion',
        'data': 'id=' + idgestioncomercial + '&idcontacto=' + $('#idcontactosend').val(),
        'success':
                function (respuesta) {
                    $('#modalGestion').modal('show');
                    $("#divgestion").hide();
                    $("#divgestion").html(respuesta);
                    $("#divgestion").fadeIn("fast");
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function guardarDescartado()
{
    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/contacto/guardardescartado',
        'data': 'idcontacto=' + $('#idcontactosend').val() + '&memo=' + $('#memo').val() + '&motivodescartado=' + $('#motivodescartado').val() + '&id=' + $('#id').val(),
        'success':
                function (respuesta) {
                    if (respuesta == 'OK') {
                        swal("¡Hecho!", "Registro Descartado", "success");
                        location.reload();
                    } else {
                        swal("¡Alto!", "No se Actualizo el Registro", "error");
                    }
                    $('#modalGestion').modal('hide');
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}

function calendar(input) {
    $('#cal_' + input).datetimepicker({
        inline: false,
        timepicker: false,
        format: 'Y-m-d',
        onChangeDateTime: function (dp, $input) {
            $("#GestionComercial_" + input + "_Fecha_visita").val($input.val());
        }
    });
}


function reenviarsms()
{
    alert('en construccion');
}

function reenviaremail()
{
    alert('en construccion');
}



function actualizarContacto(id)
{
    var accion = function (res) {
        if (res) {
            transferirContacto(id);
        }
    };
    confirmar("Aviso", 'Este Registro de Oportunidad sera registrado como Contacto esta seguro de Enviarlo?', "warning", accion);
    return false;
}

function transferirContacto(id) {
    $.ajax({
        type: "POST",
        url: '/telemark/index.php/oportunidad/transferencia',
        data: {
            id: id
        },
        success: function (data) {
            if (data === "OK") {
                window.location.href = '/telemark/index.php/seguimientos/oportunidad';
            }
        },
        error: function (err) {
            error(err.responseText);
        }
    });
}

function error() {
    var args = arguments.length;
    switch (args) {
        case 1:
            swal("Error", arguments[0], "error");
            break;
        case 2:
            swal(arguments[0], arguments[1], "error");
            break;
        case 3:
            swal(arguments[0], arguments[1], arguments[2]);
            break;
    }
}

function exito() {
    var args = arguments.length;
    switch (args) {
        case 1:
            swal("Hecho", arguments[0], "success");
            break;
        case 2:
            swal(arguments[0], arguments[1], "success");
            break;
        case 3:
            swal(arguments[0], arguments[1], arguments[2]);
            break;
    }
}



function confirmar() {
    var accion = undefined;
    var args = arguments.length;
    switch (args) {
        case 1:
            var params = {
                title: "Confirmar",
                text: arguments[0]
            };
            break;
        case 2:
            var params = {
                title: arguments[0],
                text: arguments[1]
            };
            break;
        case 3:
            var params = {
                title: arguments[0],
                text: arguments[1],
                type: arguments[2]
            };
            break;
        case 4:
            var params = {
                title: arguments[0],
                text: arguments[1],
                type: arguments[2]
            };
            var accion = arguments[3];
            break;
    }

    var base = {
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        closeOnConfirm: true,
        closeOnCancel: true
    };

    if (accion !== undefined) {
        swal($.extend(params, base), function (pass) {
            accion(pass);
        });
    } else {
        swal($.extend(params, base));
    }
}

function editar(cod) {
    window.location.href = '/telemark/index.php/oportunidad/update?id=' + cod;
}


function notydesktop(ultid) {
    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/site/shownoty',
        'data': 'ultid=' + parseInt(ultid),
        'success': function (respuesta) {
            if (respuesta > 0) {
                showNotification('Registro Nuevo', 'Campaña Web');
            }
        },
        'cache': false
    });
    return false;
}


var n = 1;
function showNotification(message, z) {
    var icon = 'http://www.telesentinel.com/telemark/images/teleweb.png';
    var notif = showWebNotification('Mensaje TELEMARK ', message + '!\n Sin Ver : ' + z, icon, null, 7000);
    //handle different events
    notif.addEventListener("show", Notification_OnEvent);
    notif.addEventListener("click", Notification_OnEvent);
    notif.addEventListener("close", Notification_OnEvent);
    n++;
}

function Notification_OnEvent(event) {
    //A reference to the Notification object
    //var notif = event.currentTarget;
    //document.getElementById("msgs").innerHTML += "<br>Notification <strong>'" + notif.title + "'</strong> received event '" + event.type + "' at " + new Date().toLocaleString();
}



function loadcontenedor(div)
{
    if (div == 'ventas') {
        $('.contenedorventas').
                animate({
                    center: '+=20',
                    width: 'toggle'
                }, 800, function () {
                    $('.contenedorventas').fadeIn('fast');
                    $('.contenedorasesor').fadeOut('fast');
                    $('.contenedorcomercial').fadeOut('fast');
                    $('.contenedorpreventas').fadeOut('fast');
                }
                );
    } else {
        $('.contenedorventas').fadeOut('fast');
        $('.contenedor' + div).animate({
            center: '+=20',
            width: 'toggle'
        }, 1000, function () {
            $('.contenedorasesor').fadeOut('fast');
            $('.contenedorcomercial').fadeOut('fast');
            $('.contenedorpreventas').fadeOut('fast');
            $('.contenedor' + div).fadeIn('fast');
        }
        );
    }
}

function modalAlertas(tipo)
{
    $("#modalAlerta").modal('show');
    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/admage/notificacion',
        'data': 'tipo=' + tipo,
        'beforeSend': function () {
            $('#modalcuerpo').html($('#modalwait').html());
        },
        'success':
                function (respuesta) {
                    $("#modalcuerpo").html(respuesta);
                },
        'error': function (m, e, a) {
            swal("¡Error!", "Consulte al Administrador del Sistema...", 'error');
        },
        'cache': false
    });
    return false;
}